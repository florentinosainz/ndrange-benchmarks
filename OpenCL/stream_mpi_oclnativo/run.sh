#!/bin/bash

# @ partition = projects
# @ output = stream.log
# @ error = stream.log
# @ initialdir = .
# @ total_tasks = 4
# @ cpus_per_task = 12
# @ gpus_per_node = 2
# @ node_usage = not_shared
# @ wall_clock_limit = 00:20:00

mpirun -n 4 ./stream-strongscaling $1

