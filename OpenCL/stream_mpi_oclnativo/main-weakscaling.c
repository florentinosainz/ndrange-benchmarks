/*-----------------------------------------------------------------------*/
/* Program: Stream                                                       */
/* Adapted to StarSs by Rosa M. Badia (Barcelona Supercomputing Center)	 */
/* This version does not insert barriers after each set of operations,   */
/* to promote task chaining in StarSs					 */
/* Ported to OmpSs ndrange by Florentino Sainz (Barcelona Supercomputing Center)   */
/* Revision: $Id: stream.c,v 5.8 2007/02/19 23:57:39 mccalpin Exp mccalpin $ */
/* Original code developed by John D. McCalpin                           */
/* Programmers: John D. McCalpin                                         */
/*              Joe R. Zagar                                             */
/*                                                                       */
/* This program measures memory transfer rates in MB/s for simple        */
/* computational kernels coded in C.                                     */
/*-----------------------------------------------------------------------*/
/* Copyright 1991-2005: John D. McCalpin                                 */
/*-----------------------------------------------------------------------*/
/* License:                                                              */
/*  1. You are free to use this program and/or to redistribute           */
/*     this program.                                                     */
/*  2. You are free to modify this program for your own use,             */
/*     including commercial use, subject to the publication              */
/*     restrictions in item 3.                                           */
/*  3. You are free to publish results obtained from running this        */
/*     program, or from works that you derive from this program,         */
/*     with the following limitations:                                   */
/*     3a. In order to be referred to as "STREAM benchmark results",     */
/*         published results must be in conformance to the STREAM        */
/*         Run Rules, (briefly reviewed below) published at              */
/*         http://www.cs.virginia.edu/stream/ref.html                    */
/*         and incorporated herein by reference.                         */
/*         As the copyright holder, John McCalpin retains the            */
/*         right to determine conformity with the Run Rules.             */
/*     3b. Results based on modified source code or on runs not in       */
/*         accordance with the STREAM Run Rules must be clearly          */
/*         labelled whenever they are published.  Examples of            */
/*         proper labelling include:                                     */
/*         "tuned STREAM benchmark results"                              */
/*         "based on a variant of the STREAM benchmark code"             */
/*         Other comparable, clear and reasonable labelling is           */
/*         acceptable.                                                   */
/*     3c. Submission of results to the STREAM benchmark web site        */
/*         is encouraged, but not required.                              */
/*  4. Use of this program or creation of derived works based on this    */
/*     program constitutes acceptance of these licensing restrictions.   */
/*  5. Absolutely no warranty is expressed or implied.                   */
/*-----------------------------------------------------------------------*/
#include <stdio.h>
#include <math.h>
#include <float.h>
#include <stdlib.h>
#include "limits.h"
#include <sys/time.h>
#include <omp.h>
#include <mpi.h>

// Include kernels


#define HLINE "-------------------------------------------------------------\n"

#ifndef MIN
# define MIN(x,y) ((x)<(y)?(x):(y))
#endif
#ifndef MAX
# define MAX(x,y) ((x)>(y)?(x):(y))
#endif

#define N      8*1024*1024
#define NTIMES 10000
const int BSIZE=N/16;

static double *a_h, *b_h, *c_h;

#define BYTES  (10 * sizeof(double) * N)

extern void checkSTREAMresults();
extern double getElapsedTime(struct timeval start, struct timeval finish);

extern void displayAllGPUsProperties();


/* Host functions */
void init(double *a, double *b, double *c, int size);
void copy(double *a, double *c, int size);
void scale (double *b, double *c, double scalar, int size);
void add (double *a, double *b, double *c, int size);
void triad (double *a, double *b, double *c, double scalar, int size);

/* GPU functions */
//void init_gpu(double *a, double *b, double *c, int size);
//void copy_gpu(double *a, double *c, int size);
//void scale_gpu (double *b, double *c, double scalar, int size);
//void add_gpu (double *a, double *b, double *c, int size);
//void triad_gpu (double *a, double *b, double *c, double scalar, int size);
#include <CL/opencl.h>

#define N_DEVICES 2

cl_device_id device_id[N_DEVICES];             // compute device id 
cl_context context;                 // compute context
cl_command_queue commands[N_DEVICES];          // compute command queue
cl_program program;                 // compute program
cl_mem buff_A[N_DEVICES];
cl_mem buff_B[N_DEVICES];
cl_mem buff_C[N_DEVICES];


void init_opencl(){
	int gpu=1;
	cl_int err;
	cl_platform_id plats[4];
	clGetPlatformIDs(4,plats,NULL);
	int i;

	err = clGetDeviceIDs(plats[0], gpu ? CL_DEVICE_TYPE_GPU : CL_DEVICE_TYPE_CPU, N_DEVICES, device_id, NULL);
    if (err != CL_SUCCESS)
    {
        printf("Error: Failed to create a device group! %d\n",err);
    }
	context = clCreateContext(0, N_DEVICES, device_id, NULL, NULL, &err);
    if (!context)
    {
        printf("Error: Failed to create a compute context!\n");
    }
	for (i=0; i<N_DEVICES;i++){	
	  char device_name[200] = "";
	  clGetDeviceInfo
				  (device_id[i], CL_DEVICE_NAME, 200, device_name,
				   NULL);
	  printf("Device: \"%s\"\n", device_name);
		commands[i] = clCreateCommandQueue(context, device_id[i], 0, &err);
		if (!commands[i])
		{
			printf("Error: Failed to create a command commands!\n");
		}
	}
	char* ompss_code;    
    FILE *fp;
	size_t source_size;
	fp = fopen("stream_gpu.cl", "r");
	if (!fp) {
		printf("Failed to open file when loading kernel from file\n");
	}      
	fseek(fp, 0, SEEK_END); // seek to end of file;
	size_t size = ftell(fp); // get current file pointer
	fseek(fp, 0, SEEK_SET); // seek back to beginning of file
	ompss_code = (char*) malloc(sizeof(char)*size);
	source_size = fread( ompss_code, 1, size, fp);
	fclose(fp); 
	ompss_code[size]=0;
		
	program = clCreateProgramWithSource(context, 1, (const char **) &ompss_code, NULL, &err);
    if (!program)
    {
        printf("Error: Failed to create compute program!\n");
    }
	clBuildProgram(program, 0, NULL, "", NULL, NULL);
	free(ompss_code);
}

static unsigned int real_N;
static unsigned int real_BSIZE;
int main(int argc, char *argv[])
{

	init_opencl();
	int i;
    int			BytesPerWord;
    int	k;
	size_t j;
    double		scalar, total_time;
    struct timeval	start, finish;
    int			useCPU;
	cl_int err;
	/* MPI CODE */
	int myrank, rank;
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
	MPI_Comm_size(MPI_COMM_WORLD, &rank);
	/* MPI CODE */

   real_N = N;
   real_BSIZE = real_N/16;
	
	a_h = (double *) malloc( real_N * sizeof(double) );
	b_h = (double *) malloc( real_N * sizeof(double) );
	c_h = (double *) malloc( real_N * sizeof(double) );
#if 0
	/* print cuda devices of each task */
	for ( j = 0; j < rank; j++)
	{
		if ( j == myrank )
		{
			char myHostname[256];
			if ( gethostname( myHostname, 256 ) != 0 )
			{
				fprintf(stderr, "os: Error getting the hostname.\n");
			}
			printf("I am rank %d and my hostname is %s, GPU properties:\n", myrank, myHostname);
			//displayAllGPUsProperties();
		}
		MPI_Barrier(MPI_COMM_WORLD);
	} 
#endif

   if ( myrank == 0)
   {

      /* --- SETUP --- determine precision and check timing --- */

      printf(HLINE);

      printf("STREAM version $Revision: 5.8 $\n");
      printf(HLINE);
      BytesPerWord = sizeof(double);
      printf("This system uses %d bytes per DOUBLE PRECISION word.\n", BytesPerWord);

      printf(HLINE);
      printf("Array size = %d, divided between %d processes\n" , N, rank);
      printf("Total memory required = %.1f MB.\n", rank * (3.0 * BytesPerWord) * ( (double) N / 1048576.0));
      printf(HLINE);
   }

    useCPU = 0;


    if (useCPU) {
        gettimeofday(&start, NULL);
    
        for (j = 0; j < N; j+= BSIZE)
            init(&a_h[j], &b_h[j], &c_h[j], BSIZE);
    
        /*	--- MAIN LOOP --- repeat test cases NTIMES times --- */
    
        // assuming N % BSIZE == 0
        scalar = 3.0;
        for (k=0; k<NTIMES; k++) {
            for (j = 0; j < N; j+= BSIZE)
                copy(&a_h[j], &c_h[j], BSIZE);
            
            for (j = 0; j < N; j+= BSIZE)
                scale(&b_h[j], &c_h[j], scalar, BSIZE);
            
            for (j = 0; j < N; j+= BSIZE)
                add(&a_h[j], &b_h[j], &c_h[j], BSIZE);
            
            for (j = 0; j < N; j+= BSIZE)
                triad(&a_h[j], &b_h[j], &c_h[j], scalar, BSIZE);
        }

        gettimeofday(&finish, NULL);
    } else {
       
        gettimeofday(&start, NULL);
		
		/* MPI CODE */
		MPI_Barrier(MPI_COMM_WORLD);
		/* MPI CODE */
        /*	--- MAIN LOOP --- repeat test cases NTIMES times --- */
		
		real_N=real_N/N_DEVICES;
		real_BSIZE=real_BSIZE/N_DEVICES;
    
        for (i = 0; i< N_DEVICES; i++){
			buff_A[i] = clCreateBuffer(context,  CL_MEM_READ_WRITE,  real_N * sizeof(double), NULL, &err);
			if (err != CL_SUCCESS){
				printf("Error: Failed to create buffer, err %d!\n",err);
			}
			buff_B[i] = clCreateBuffer(context,  CL_MEM_READ_WRITE,  real_N * sizeof(double), NULL, &err);
			if (err != CL_SUCCESS){
				printf("Error: Failed to create buffer, err %d!\n",err);
			}
			buff_C[i] = clCreateBuffer(context,  CL_MEM_READ_WRITE,  real_N * sizeof(double), NULL, &err);
			if (err != CL_SUCCESS){
				printf("Error: Failed to create buffer, err %d!\n",err);
			}
		}
    
        // assuming N % BSIZE == 0
        scalar = 3.0;
		int n_device=0;
		cl_kernel copy_gpu[N_DEVICES];
		cl_kernel scale_gpu[N_DEVICES];
		cl_kernel add_gpu[N_DEVICES];
		cl_kernel triad_gpu[N_DEVICES];
		cl_kernel init_gpu[N_DEVICES];
		for (n_device=0; n_device<N_DEVICES;n_device++){
		
			init_gpu[n_device] = clCreateKernel(program, "init_gpu", &err);			
			clSetKernelArg(init_gpu[n_device], 0, sizeof(cl_mem),&buff_A[n_device]);
			clSetKernelArg(init_gpu[n_device], 1, sizeof(cl_mem),&buff_B[n_device]);
			clSetKernelArg(init_gpu[n_device], 2, sizeof(cl_mem),&buff_C[n_device]);
			
			copy_gpu[n_device] = clCreateKernel(program, "copy_gpu", &err);
			clSetKernelArg(copy_gpu[n_device], 0, sizeof(cl_mem),&buff_A[n_device]);
			clSetKernelArg(copy_gpu[n_device], 1, sizeof(cl_mem),&buff_C[n_device]);
			
			scale_gpu[n_device] = clCreateKernel(program, "scale_gpu", &err);
			clSetKernelArg(scale_gpu[n_device], 0, sizeof(cl_mem),&buff_B[n_device]);
			clSetKernelArg(scale_gpu[n_device], 1, sizeof(cl_mem),&buff_C[n_device]);
			clSetKernelArg(scale_gpu[n_device], 2, sizeof(double),&scalar);
			
			add_gpu[n_device] = clCreateKernel(program, "add_gpu", &err);
			clSetKernelArg(add_gpu[n_device], 0, sizeof(cl_mem),&buff_A[n_device]);
			clSetKernelArg(add_gpu[n_device], 1, sizeof(cl_mem),&buff_B[n_device]);
			clSetKernelArg(add_gpu[n_device], 2, sizeof(cl_mem),&buff_C[n_device]);
			
			triad_gpu[n_device] = clCreateKernel(program, "triad_gpu", &err);
			clSetKernelArg(triad_gpu[n_device], 0, sizeof(cl_mem),&buff_A[n_device]);
			clSetKernelArg(triad_gpu[n_device], 1, sizeof(cl_mem),&buff_B[n_device]);
			clSetKernelArg(triad_gpu[n_device], 2, sizeof(cl_mem),&buff_C[n_device]);
			clSetKernelArg(triad_gpu[n_device], 3, sizeof(double),&scalar);
		}
		size_t local_size_ptr[1];
		size_t global_size_ptr[1];
		local_size_ptr[0]=128;
		global_size_ptr[0]=real_BSIZE;
		local_size_ptr[0] = global_size_ptr[0] < local_size_ptr[0] ? global_size_ptr[0] : local_size_ptr[0];
		global_size_ptr[0] = global_size_ptr[0] < local_size_ptr[0] ? global_size_ptr[0] : global_size_ptr[0] + (global_size_ptr[0] % local_size_ptr[0] == 0 ? 0 : local_size_ptr[0] - global_size_ptr[0] % local_size_ptr[0]);
         	
		for (n_device=0; n_device<N_DEVICES; n_device++){
			for (j = 0; j < real_N; j+= real_BSIZE){
				int l=real_BSIZE+j;
				clSetKernelArg(init_gpu[n_device], 3, sizeof(int),&l);
				clEnqueueNDRangeKernel(commands[n_device], init_gpu[n_device], 1, &j, global_size_ptr, local_size_ptr, 0, NULL, NULL);	
			}
		}
		
		n_device=0;
		//Each device has to do the operation once (so we repeat NTIMES*N_DEVICES)
        for (k=0; k<NTIMES*N_DEVICES; k++) {
			clFinish(commands[n_device]);

			/* MPI CODE */
			MPI_Barrier(MPI_COMM_WORLD);
			/* MPI CODE */
				for (j = 0; j < real_N; j+= real_BSIZE){
					int l=real_BSIZE+j;
					clSetKernelArg(copy_gpu[n_device], 2, sizeof(int),&l);
					clEnqueueNDRangeKernel(commands[n_device], copy_gpu[n_device], 1, &j, global_size_ptr, local_size_ptr, 0, NULL, NULL);	
				}
			/* MPI CODE */
			MPI_Barrier(MPI_COMM_WORLD);
			/* MPI CODE */


			/* MPI CODE */
			MPI_Barrier(MPI_COMM_WORLD);
			/* MPI CODE */
				for (j = 0; j < real_N; j+= real_BSIZE){
					int l=real_BSIZE+j;
					clSetKernelArg(scale_gpu[n_device], 3, sizeof(int),&l);
					clEnqueueNDRangeKernel(commands[n_device], scale_gpu[n_device], 1, &j, global_size_ptr, local_size_ptr, 0, NULL, NULL);	
				}
			/* MPI CODE */
			MPI_Barrier(MPI_COMM_WORLD);
			/* MPI CODE */

			/* MPI CODE */
			MPI_Barrier(MPI_COMM_WORLD);
			/* MPI CODE */
				for (j = 0; j < real_N; j+= real_BSIZE){
					int l=real_BSIZE+j;
					clSetKernelArg(add_gpu[n_device], 3, sizeof(int),&l);
					clEnqueueNDRangeKernel(commands[n_device], add_gpu[n_device], 1, &j, global_size_ptr, local_size_ptr, 0, NULL, NULL);
				}					
			/* MPI CODE */
			MPI_Barrier(MPI_COMM_WORLD);
			/* MPI CODE */


			/* MPI CODE */
			MPI_Barrier(MPI_COMM_WORLD);
			/* MPI CODE */
				for (j = 0; j < real_N; j+= real_BSIZE){
					int l=real_BSIZE+j;
					clSetKernelArg(triad_gpu[n_device], 4, sizeof(int),&l);
					clEnqueueNDRangeKernel(commands[n_device], triad_gpu[n_device], 1, &j, global_size_ptr, local_size_ptr, 0, NULL, NULL);	
				}
					
				

			/* MPI CODE */
			MPI_Barrier(MPI_COMM_WORLD);	
		
			/* MPI CODE */
			n_device++;
			if (n_device==N_DEVICES) n_device=0;
        }
		for (n_device=0; n_device<N_DEVICES; n_device++){
			clEnqueueReadBuffer(commands[n_device], buff_A[n_device], CL_FALSE, 0, sizeof(double) * real_N, &a_h[n_device*real_N], 0, NULL, NULL ); 	
			clEnqueueReadBuffer(commands[n_device], buff_B[n_device], CL_FALSE, 0, sizeof(double) * real_N, &b_h[n_device*real_N], 0, NULL, NULL ); 	
			clEnqueueReadBuffer(commands[n_device], buff_C[n_device], CL_FALSE, 0, sizeof(double) * real_N, &c_h[n_device*real_N], 0, NULL, NULL ); 	
		}
		for (n_device=0; n_device<N_DEVICES; n_device++){
			clFinish(commands[n_device]);
		}
        gettimeofday(&finish, NULL);
		
		for (i=0; i<N_DEVICES;i++){	
			clReleaseKernel(init_gpu[i]);
			clReleaseKernel(copy_gpu[i]);
			clReleaseKernel(scale_gpu[i]);
			clReleaseKernel(add_gpu[i]);
			clReleaseKernel(triad_gpu[i]);
			clReleaseMemObject(buff_A[i]);
			clReleaseMemObject(buff_B[i]);
			clReleaseMemObject(buff_C[i]);
		}
	}


    
    total_time = getElapsedTime(start, finish);
    /*	--- SUMMARY --- */

	/* MPI CODE */
	MPI_Barrier(MPI_COMM_WORLD);
	/* MPI CODE */
	if ( myrank == 0 )
	{
    printf ("Average Rate (MB/s): %11.4f \n", 1.0E-06 * BYTES *NTIMES*rank/total_time);
    printf("TOTAL time (including initialization) =  %11.4f seconds\n", total_time);
    printf(HLINE);
	}

    /* --- Check Results --- */

    checkSTREAMresults();

	if ( myrank == 0 )
	{
    printf(HLINE);
	}
	
	free(a_h);
	free(b_h);
	free(c_h);
	
	
	for (i=0; i<N_DEVICES;i++){	
		clReleaseCommandQueue(commands[i]);
		clReleaseContext(context);
	}

	MPI_Finalize();
    return 0;
}

void checkSTREAMresults ()
{
	double aj,bj,cj,scalar;
	double asum,bsum,csum;
	double epsilon;
	int	j,k;

    /* reproduce initialization */
	aj = 1.0;
	bj = 2.0;
	cj = 0.0;
    /* a[] is modified during timing check */
	aj = 2.0E0 * aj;
    /* now execute timing loop */
	scalar = 3.0;
	for (k=0; k<NTIMES; k++)
        {
            cj = aj;
            bj = scalar*cj;
            cj = aj+bj;
            aj = bj+scalar*cj;
        }
	aj = aj * (double) (N);
	bj = bj * (double) (N);
	cj = cj * (double) (N);

	asum = 0.0;
	bsum = 0.0;
	csum = 0.0;
	for (j=0; j<N; j++) {
		asum += a_h[j];
		bsum += b_h[j];
		csum += c_h[j];
	}
#ifdef VERBOSE
	printf ("Results Comparison: \n");
	printf ("        Expected  : %f %f %f \n",aj,bj,cj);
	printf ("        Observed  : %f %f %f \n",asum,bsum,csum);
#endif

#ifndef abs
#define abs(a) ((a) >= 0 ? (a) : -(a))
#endif
	epsilon = 1.e-8;

	if (abs(aj-asum)/asum > epsilon) {
		printf ("Failed Validation on array a[]\n");
		printf ("        Expected  : %f \n",aj);
		printf ("        Observed  : %f \n",asum);
		for (j = 0; j < 10; j++) {
			printf ("[%d] Observed: %f\n", j, a_h[j]);
		}

	}
	else if (abs(bj-bsum)/bsum > epsilon) {
		printf ("Failed Validation on array b[]\n");
		printf ("        Expected  : %f \n",bj);
		printf ("        Observed  : %f \n",bsum);
	}
	else if (abs(cj-csum)/csum > epsilon) {
		printf ("Failed Validation on array c[]\n");
		printf ("        Expected  : %f \n",cj);
		printf ("        Observed  : %f \n",csum);
	}
	else {
		printf ("Solution Validates\n");
	}
}

double getElapsedTime(struct timeval start, struct timeval finish)
{
	    return (finish.tv_sec + finish.tv_usec * 1e-6) - (start.tv_sec + start.tv_usec * 1e-6); 
}
