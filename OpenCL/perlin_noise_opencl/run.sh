#!/bin/bash


# @ partition = projects
# @ output = perlin.log
# @ error = perlin.log
# @ initialdir = .
# @ total_tasks = 1
# @ cpus_per_task = 12
# @ gpus_per_node = 2
# @ node_usage = not_shared
# @ wall_clock_limit = 00:20:00
# Number of GPUs to use (2 by default)



NX_PES=4 NX_OPENCL_MAX_DEVICES=${1:-1} NX_OPENCL_DEVICE_TYPE=GPU ./perlin
