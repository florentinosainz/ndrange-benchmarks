/*-----------------------------------------------------------------------*/
/* Program: Perlin Noise                                                 */
/* Adapted to OmpSs by the Barcelona Supercomputing Center               */
/*************************************************************************/
/*                                                                       */
/* Licensed Materials - Property of IBM                                  */
/*                                                                       */
/* Created by Mark Nutter on 1/28/09.                                    */
/* (C) Copyright IBM Corp. 2009                                          */
/*                                                                       */
/* Derived from Improved Noise (http://mrl.nyu.edu/~perlin/noise/)       */
/* (C) Copyright Ken Perlin 2002                                         */
/*                                                                       */
/*************************************************************************/
#include <math.h>
#include <stdio.h>
#include <omp.h>
#include <stdlib.h>

#include "perlin_auxiliar_header.h"

/**
 * compute_perlin_noise
 *
 * Compute perlin noise algorithm given time offset, rowstride and image size.
 */
void compute_perlin_noise_device(pixel * output, float time, unsigned int rowstride, int img_height, int img_width)
{
    unsigned int i, j;
    float vy, vt;
    int BSy = 1;
    int BSx = 128;
    int BS = img_height/nanos_get_opencl_num_devices();

		for (j = 0; j < img_height; j+=BS) {
		pixel* addr=&output[j*rowstride];
		opencl_perlin(&output[j*rowstride], time, j, rowstride, img_width, BS);
	}
}

/* Host version */

/**
 * compute_perlin_noise
 *
 * Compute perlin noise algorithm given time offset, rowstride and image size.
 */
void compute_perlin_noise(pixel * output, const float time, const unsigned int rowstride, int img_height, int img_width)
{
    unsigned int i, j;
    float vy, vt;
    float vdx = 0.03125f;
    float vdy = 0.0125f;
    float vs = 2.0f;
    float bias = 0.35f;
    float vx = 0.0f;
    float red, green, blue;
    float xx, yy;

    for (j = 0; j < img_height; j++) {
        for (i = 0; i < img_width; i++) {
            vx = ((float) i) * vdx;
            vy = ((float) j) * vdy;
            vt = time * vs;

            xx = vx * vs;
            yy = vy * vs;

            red = noise3(xx, vt, yy);
            green = noise3(vt, yy, xx);
            blue = noise3(yy, xx, vt);

            red += bias;
            green += bias;
            blue += bias;

            // Clamp to within [0 .. 1]
            red = (red > 1.0f) ? 1.0f : red;
            green = (green > 1.0f) ? 1.0f : green;
            blue = (blue > 1.0f) ? 1.0f : blue;

            red = (red < 0.0f) ? 0.0f : red;
            green = (green < 0.0f) ? 0.0f : green;
            blue = (blue < 0.0f) ? 0.0f : blue;

            red *= 255.0f;
            green *= 255.0f;
            blue *= 255.0f;

            output[(j * rowstride) + i].r = (unsigned char) red;
            output[(j * rowstride) + i].g = (unsigned char) green;
            output[(j * rowstride) + i].b = (unsigned char) blue;
            output[(j * rowstride) + i].a = 255;
        }
    }
}

/**
 * compute_host_and_verify
 *
 * Perform the host-side perlin noise computation, report performance and optionally verify the device results.
 */
int compute_host_and_verify(int iterations, pixel * output_device, int rowstride,
                            int img_height, int img_width, int verify, int data_size, float device_mpix)
{
    int retval = 0;
    int i;
    float host_mpix;
    double delta;
    float time;
    pixel *output;

    if (verify) {
       
        printf("Compute Host Data\n");
       // Create the output array
       
        output = calloc (img_width*img_height,sizeof(pixel));
        if (output==NULL) {
          perror ("malloc (output)");
          exit (1);
        }
        
        time = 0.0f;
        double start = omp_get_wtime();

        for (i = 0; i < iterations; i++) {
            compute_perlin_noise((pixel *) output, time, rowstride, img_height, img_width);
            time += 0.05f;
        }

        delta = omp_get_wtime() - start;


        host_mpix = (double) (img_width * img_height) * (double) (iterations) / (1000000.0 * (double) (delta));
        printf("Host -- %d Computes\n", iterations);
        printf("computation time (in seconds): %f\n", delta);
        printf("Mpixels/sec: %f\n", host_mpix);
        printf("%f Speedup\n", device_mpix / host_mpix);


        // Verify results
        //
        printf("Verifying....\n");

        int failure_count = 0;
        for (i = 0; i < img_width * img_height ; i++) {
            pixel *device_pixel = (pixel *) & output_device[i];
            pixel *host_pixel = (pixel *) & output[i];

            //The rgb values may be off by 1 but still be correct because of rounding error.
            //For example, if the host computes 164.000015 and the device 163.999985,
            //the resulting values will be 164 and 163, respectively.

            if ((abs(device_pixel->r - host_pixel->r) > 1) ||
                    (abs(device_pixel->g - host_pixel->g) > 1) ||
                    (abs(device_pixel->b - host_pixel->b) > 1) ||
                    (abs(device_pixel->a - host_pixel->a) > 1)) {
                retval = 1;
                fprintf(stderr, "Error [%d]: H 0x%08X 0x%08X\n", i, output[i], output_device[i]);
                failure_count++;
            }
        }

        if (failure_count) {
            printf("Verification Complete: %d errors out of %d pixels\n", failure_count, data_size / sizeof(pixel));
        } else {
            printf("Verification: Ok\n");
        }
    }

    return retval;
}
