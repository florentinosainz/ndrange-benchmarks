/*-----------------------------------------------------------------------*/
/* Program: Stream                                                       */
/* Adapted to StarSs by Rosa M. Badia (Barcelona Supercomputing Center)	 */
/* This version does not insert barriers after each set of operations,   */
/* to promote task chaining in StarSs					 */
/* Ported to OmpSs ndrange by Florentino Sainz (Barcelona Supercomputing Center)   */
/* Revision: $Id: stream.c,v 5.8 2007/02/19 23:57:39 mccalpin Exp mccalpin $ */
/* Original code developed by John D. McCalpin                           */
/* Programmers: John D. McCalpin                                         */
/*              Joe R. Zagar                                             */
/*                                                                       */
/* This program measures memory transfer rates in MB/s for simple        */
/* computational kernels coded in C.                                     */
/*-----------------------------------------------------------------------*/
/* Copyright 1991-2005: John D. McCalpin                                 */
/*-----------------------------------------------------------------------*/
/* License:                                                              */
/*  1. You are free to use this program and/or to redistribute           */
/*     this program.                                                     */
/*  2. You are free to modify this program for your own use,             */
/*     including commercial use, subject to the publication              */
/*     restrictions in item 3.                                           */
/*  3. You are free to publish results obtained from running this        */
/*     program, or from works that you derive from this program,         */
/*     with the following limitations:                                   */
/*     3a. In order to be referred to as "STREAM benchmark results",     */
/*         published results must be in conformance to the STREAM        */
/*         Run Rules, (briefly reviewed below) published at              */
/*         http://www.cs.virginia.edu/stream/ref.html                    */
/*         and incorporated herein by reference.                         */
/*         As the copyright holder, John McCalpin retains the            */
/*         right to determine conformity with the Run Rules.             */
/*     3b. Results based on modified source code or on runs not in       */
/*         accordance with the STREAM Run Rules must be clearly          */
/*         labelled whenever they are published.  Examples of            */
/*         proper labelling include:                                     */
/*         "tuned STREAM benchmark results"                              */
/*         "based on a variant of the STREAM benchmark code"             */
/*         Other comparable, clear and reasonable labelling is           */
/*         acceptable.                                                   */
/*     3c. Submission of results to the STREAM benchmark web site        */
/*         is encouraged, but not required.                              */
/*  4. Use of this program or creation of derived works based on this    */
/*     program constitutes acceptance of these licensing restrictions.   */
/*  5. Absolutely no warranty is expressed or implied.                   */
/*-----------------------------------------------------------------------*/
#include <stdio.h>
#include <math.h>
#include <float.h>
#include <stdlib.h>
#include "limits.h"
#include <sys/time.h>
#include <omp.h>
#include <mpi.h>

// Include kernels
#include "stream_gpu.clh"

#define HLINE "-------------------------------------------------------------\n"

#ifndef MIN
# define MIN(x,y) ((x)<(y)?(x):(y))
#endif
#ifndef MAX
# define MAX(x,y) ((x)>(y)?(x):(y))
#endif

#define N      8*1024*1024
#define NTIMES 10000
#define BSIZE N/16

static unsigned int real_N;
static unsigned int real_BSIZE;

//static double  a_h[N], b_h[N], c_h[N];
static double  *a_h, *b_h, *c_h;

#define BYTES  (10 * sizeof(double) * N)

extern void checkSTREAMresults();
extern double getElapsedTime(struct timeval start, struct timeval finish);

extern void displayAllGPUsProperties();


/* Host functions */
void init(double *a, double *b, double *c, int size);
void copy(double *a, double *c, int size);
void scale (double *b, double *c, double scalar, int size);
void add (double *a, double *b, double *c, int size);
void triad (double *a, double *b, double *c, double scalar, int size);

/* GPU functions */
//void init_gpu(double *a, double *b, double *c, int size);
//void copy_gpu(double *a, double *c, int size);
//void scale_gpu (double *b, double *c, double scalar, int size);
//void add_gpu (double *a, double *b, double *c, int size);
//void triad_gpu (double *a, double *b, double *c, double scalar, int size);

int main(int argc, char *argv[])
{
    int			BytesPerWord;
    register int	j, k;
    double		scalar, total_time;
    struct timeval	start, finish;
    int			useCPU;
	
	/* MPI CODE */
	int myrank, rank;
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
	MPI_Comm_size(MPI_COMM_WORLD, &rank);
	/* MPI CODE */

   /* set the final size of the arrays */
   real_N = N/rank;
   real_BSIZE = real_N/16;

	a_h = (double *) malloc( real_N * sizeof(double) );
	b_h = (double *) malloc( real_N * sizeof(double) );
	c_h = (double *) malloc( real_N * sizeof(double) );

#if 0
	/* print cuda devices of each task */
	for ( j = 0; j < rank; j++)
	{
		if ( j == myrank )
		{
			char myHostname[256];
			if ( gethostname( myHostname, 256 ) != 0 )
			{
				fprintf(stderr, "os: Error getting the hostname.\n");
			}
			printf("I am rank %d and my hostname is %s, GPU properties:\n", myrank, myHostname);
			//displayAllGPUsProperties();
		}
		MPI_Barrier(MPI_COMM_WORLD);
	} 
#endif

   if ( myrank == 0)
   {

      /* --- SETUP --- determine precision and check timing --- */

      printf(HLINE);

      printf("STREAM version $Revision: 5.8 $\n");
      printf(HLINE);
      BytesPerWord = sizeof(double);
      printf("This system uses %d bytes per DOUBLE PRECISION word.\n", BytesPerWord);

      printf(HLINE);
      printf("Array size = %d, divided between %d tasks\n" , N, rank);
      printf("Total memory required = %.1f MB. %.1f MB per task\n", (3.0 * BytesPerWord) * ( (double) N / 1048576.0), (3.0 * BytesPerWord) * ( (double) real_N / 1048576.0) );
      printf(HLINE);
   }

	MPI_Barrier(MPI_COMM_WORLD);
    useCPU = 0;


    if (useCPU) {
        gettimeofday(&start, NULL);
    
        for (j = 0; j < real_N; j+= real_BSIZE)
            init(&a_h[j], &b_h[j], &c_h[j], real_BSIZE);
    
        /*	--- MAIN LOOP --- repeat test cases NTIMES times --- */
    
        // assuming N % real_BSIZE == 0
        scalar = 3.0;
        for (k=0; k<NTIMES; k++) {
            for (j = 0; j < real_N; j+= real_BSIZE)
                copy(&a_h[j], &c_h[j], real_BSIZE);
            
            for (j = 0; j < real_N; j+= real_BSIZE)
                scale(&b_h[j], &c_h[j], scalar, real_BSIZE);
            
            for (j = 0; j < real_N; j+= real_BSIZE)
                add(&a_h[j], &b_h[j], &c_h[j], real_BSIZE);
            
            for (j = 0; j < real_N; j+= real_BSIZE)
                triad(&a_h[j], &b_h[j], &c_h[j], scalar, real_BSIZE);
        }

        gettimeofday(&finish, NULL);
    } else {

        gettimeofday(&start, NULL);
		
        for (j = 0; j < real_N; j+= real_BSIZE)
            init_gpu(a_h+j, b_h+j, c_h+j, real_BSIZE);
		/* MPI CODE */
		MPI_Barrier(MPI_COMM_WORLD);
		/* MPI CODE */
        /*	--- MAIN LOOP --- repeat test cases NTIMES times --- */
    
        // assuming N % real_BSIZE == 0
        scalar = 3.0;
        for (k=0; k<NTIMES; k++) {


			/* MPI CODE */
			MPI_Barrier(MPI_COMM_WORLD);
			/* MPI CODE */
				for (j = 0; j < real_N; j+= real_BSIZE)
					copy_gpu(a_h+j, c_h+j, real_BSIZE);
			/* MPI CODE */
			MPI_Barrier(MPI_COMM_WORLD);
			/* MPI CODE */


			/* MPI CODE */
			MPI_Barrier(MPI_COMM_WORLD);
			/* MPI CODE */
				for (j = 0; j < real_N; j+= real_BSIZE)
					scale_gpu(b_h+j, c_h+j, scalar, real_BSIZE);
			/* MPI CODE */
			MPI_Barrier(MPI_COMM_WORLD);
			/* MPI CODE */

			/* MPI CODE */
			MPI_Barrier(MPI_COMM_WORLD);
			/* MPI CODE */
				for (j = 0; j < real_N; j+= real_BSIZE)
					add_gpu(a_h+j, b_h+j, c_h+j, real_BSIZE);
			/* MPI CODE */
			MPI_Barrier(MPI_COMM_WORLD);
			/* MPI CODE */


			/* MPI CODE */
			MPI_Barrier(MPI_COMM_WORLD);
			/* MPI CODE */
				for (j = 0; j < real_N; j+= real_BSIZE)
					triad_gpu(a_h+j, b_h+j, c_h+j, scalar, real_BSIZE);
			/* MPI CODE */
			MPI_Barrier(MPI_COMM_WORLD);
			/* MPI CODE */
        }
        #pragma omp taskwait
        gettimeofday(&finish, NULL);
	}


    
    total_time = getElapsedTime(start, finish);
    /*	--- SUMMARY --- */

	/* MPI CODE */
	MPI_Barrier(MPI_COMM_WORLD);
	/* MPI CODE */
	if ( myrank == 0 )
	{
    printf ("Average Rate (MB/s): %11.4f \n", 1.0E-06 * BYTES *NTIMES*rank/total_time);
    printf("TOTAL time (including initialization) =  %11.4f seconds\n", total_time);
    printf(HLINE);
	}

    /* --- Check Results --- */

    checkSTREAMresults();

	if ( myrank == 0 )
	{
    printf(HLINE);
	}
	
	free(a_h);
	free(b_h);
	free(c_h);

	MPI_Finalize();
    return 0;
}

void checkSTREAMresults ()
{
	double aj,bj,cj,scalar;
	double asum,bsum,csum;
	double epsilon;
	int	j,k;

    /* reproduce initialization */
	aj = 1.0;
	bj = 2.0;
	cj = 0.0;
    /* a[] is modified during timing check */
	aj = 2.0E0 * aj;
    /* now execute timing loop */
	scalar = 3.0;
	for (k=0; k<NTIMES; k++)
        {
            cj = aj;
            bj = scalar*cj;
            cj = aj+bj;
            aj = bj+scalar*cj;
        }
	aj = aj * (double) (real_N);
	bj = bj * (double) (real_N);
	cj = cj * (double) (real_N);

	asum = 0.0;
	bsum = 0.0;
	csum = 0.0;
	for (j=0; j<real_N; j++) {
		asum += a_h[j];
		bsum += b_h[j];
		csum += c_h[j];
	}
#ifdef VERBOSE
	printf ("Results Comparison: \n");
	printf ("        Expected  : %f %f %f \n",aj,bj,cj);
	printf ("        Observed  : %f %f %f \n",asum,bsum,csum);
#endif

#ifndef abs
#define abs(a) ((a) >= 0 ? (a) : -(a))
#endif
	epsilon = 1.e-8;

	if (abs(aj-asum)/asum > epsilon) {
		printf ("Failed Validation on array a[]\n");
		printf ("        Expected  : %f \n",aj);
		printf ("        Observed  : %f \n",asum);
		for (j = 0; j < 10; j++) {
			printf ("[%d] Observed: %f\n", j, a_h[j]);
		}

	}
	else if (abs(bj-bsum)/bsum > epsilon) {
		printf ("Failed Validation on array b[]\n");
		printf ("        Expected  : %f \n",bj);
		printf ("        Observed  : %f \n",bsum);
	}
	else if (abs(cj-csum)/csum > epsilon) {
		printf ("Failed Validation on array c[]\n");
		printf ("        Expected  : %f \n",cj);
		printf ("        Observed  : %f \n",csum);
	}
	else {
		printf ("Solution Validates\n");
	}
}

double getElapsedTime(struct timeval start, struct timeval finish)
{
	    return (finish.tv_sec + finish.tv_usec * 1e-6) - (start.tv_sec + start.tv_usec * 1e-6); 
}
