typedef __builtin_va_list __gnuc_va_list;
struct _IO_FILE;
typedef struct _IO_FILE FILE;
extern int vfprintf(FILE *__restrict __s, const char *__restrict __format, __gnuc_va_list __arg);
extern struct _IO_FILE *stdout;
extern __inline __attribute__((__gnu_inline__)) int vprintf(const char *__restrict __fmt, __gnuc_va_list __arg)
{
  return vfprintf(stdout, __fmt, __arg);
}
typedef struct _IO_FILE _IO_FILE;
extern int _IO_getc(_IO_FILE *__fp);
extern struct _IO_FILE *stdin;
extern __inline __attribute__((__gnu_inline__)) int getchar(void)
{
  return _IO_getc(stdin);
}
struct _IO_marker;
typedef long int __off_t;
typedef void _IO_lock_t;
typedef long int __off64_t;
typedef unsigned long int size_t;
struct  _IO_FILE
{
  int _flags;
  char *_IO_read_ptr;
  char *_IO_read_end;
  char *_IO_read_base;
  char *_IO_write_base;
  char *_IO_write_ptr;
  char *_IO_write_end;
  char *_IO_buf_base;
  char *_IO_buf_end;
  char *_IO_save_base;
  char *_IO_backup_base;
  char *_IO_save_end;
  struct _IO_marker *_markers;
  struct _IO_FILE *_chain;
  int _fileno;
  int _flags2;
  __off_t _old_offset;
  unsigned short int _cur_column;
  signed char _vtable_offset;
  char _shortbuf[1];
  _IO_lock_t *_lock;
  __off64_t _offset;
  void *__pad1;
  void *__pad2;
  void *__pad3;
  void *__pad4;
  size_t __pad5;
  int _mode;
  char _unused2[20U];
};
extern int __uflow(_IO_FILE *);
extern __inline __attribute__((__gnu_inline__)) int fgetc_unlocked(FILE *__fp)
{
  return __builtin_expect((*__fp)._IO_read_ptr >= (*__fp)._IO_read_end, 0) ? __uflow(__fp) : *((unsigned char *)(*__fp)._IO_read_ptr++);
}
extern __inline __attribute__((__gnu_inline__)) int getc_unlocked(FILE *__fp)
{
  return __builtin_expect((*__fp)._IO_read_ptr >= (*__fp)._IO_read_end, 0) ? __uflow(__fp) : *((unsigned char *)(*__fp)._IO_read_ptr++);
}
extern __inline __attribute__((__gnu_inline__)) int getchar_unlocked(void)
{
  return __builtin_expect((*stdin)._IO_read_ptr >= (*stdin)._IO_read_end, 0) ? __uflow(stdin) : *((unsigned char *)(*stdin)._IO_read_ptr++);
}
extern int _IO_putc(int __c, _IO_FILE *__fp);
extern __inline __attribute__((__gnu_inline__)) int putchar(int __c)
{
  return _IO_putc(__c, stdout);
}
extern int __overflow(_IO_FILE *, int);
extern __inline __attribute__((__gnu_inline__)) int fputc_unlocked(int __c, FILE *__stream)
{
  return __builtin_expect((*__stream)._IO_write_ptr >= (*__stream)._IO_write_end, 0) ? __overflow(__stream, (unsigned char)__c) : (unsigned char)(*(*__stream)._IO_write_ptr++ = __c);
}
extern __inline __attribute__((__gnu_inline__)) int putc_unlocked(int __c, FILE *__stream)
{
  return __builtin_expect((*__stream)._IO_write_ptr >= (*__stream)._IO_write_end, 0) ? __overflow(__stream, (unsigned char)__c) : (unsigned char)(*(*__stream)._IO_write_ptr++ = __c);
}
extern __inline __attribute__((__gnu_inline__)) int putchar_unlocked(int __c)
{
  return __builtin_expect((*stdout)._IO_write_ptr >= (*stdout)._IO_write_end, 0) ? __overflow(stdout, (unsigned char)__c) : (unsigned char)(*(*stdout)._IO_write_ptr++ = __c);
}
extern __inline __attribute__((__nothrow__)) __attribute__((__gnu_inline__)) int feof_unlocked(FILE *__stream)
{
  return ((*__stream)._flags & 16) != 0;
}
extern __inline __attribute__((__nothrow__)) __attribute__((__gnu_inline__)) int ferror_unlocked(FILE *__stream)
{
  return ((*__stream)._flags & 32) != 0;
}
struct  cudaPitchedPtr
{
  void *ptr;
  size_t pitch;
  size_t xsize;
  size_t ysize;
};
static __inline  struct cudaPitchedPtr make_cudaPitchedPtr(void *d, size_t p, size_t xsz, size_t ysz)
{
  struct cudaPitchedPtr s;
  s.ptr = d;
  s.pitch = p;
  s.xsize = xsz;
  s.ysize = ysz;
  return s;
}
struct  cudaPos
{
  size_t x;
  size_t y;
  size_t z;
};
static __inline  struct cudaPos make_cudaPos(size_t x, size_t y, size_t z)
{
  struct cudaPos p;
  p.x = x;
  p.y = y;
  p.z = z;
  return p;
}
struct  cudaExtent
{
  size_t width;
  size_t height;
  size_t depth;
};
static __inline  struct cudaExtent make_cudaExtent(size_t w, size_t h, size_t d)
{
  struct cudaExtent e;
  e.width = w;
  e.height = h;
  e.depth = d;
  return e;
}
struct  char1
{
  signed char x;
};
typedef struct char1 char1;
static __inline  char1 make_char1(signed char x)
{
  char1 t;
  t.x = x;
  return t;
}
struct  uchar1
{
  unsigned char x;
};
typedef struct uchar1 uchar1;
static __inline  uchar1 make_uchar1(unsigned char x)
{
  uchar1 t;
  t.x = x;
  return t;
}
struct __attribute__((aligned(2))) char2
{
  signed char x;
  signed char y;
};
typedef struct char2 char2;
static __inline  char2 make_char2(signed char x, signed char y)
{
  char2 t;
  t.x = x;
  t.y = y;
  return t;
}
struct __attribute__((aligned(2))) uchar2
{
  unsigned char x;
  unsigned char y;
};
typedef struct uchar2 uchar2;
static __inline  uchar2 make_uchar2(unsigned char x, unsigned char y)
{
  uchar2 t;
  t.x = x;
  t.y = y;
  return t;
}
struct  char3
{
  signed char x;
  signed char y;
  signed char z;
};
typedef struct char3 char3;
static __inline  char3 make_char3(signed char x, signed char y, signed char z)
{
  char3 t;
  t.x = x;
  t.y = y;
  t.z = z;
  return t;
}
struct  uchar3
{
  unsigned char x;
  unsigned char y;
  unsigned char z;
};
typedef struct uchar3 uchar3;
static __inline  uchar3 make_uchar3(unsigned char x, unsigned char y, unsigned char z)
{
  uchar3 t;
  t.x = x;
  t.y = y;
  t.z = z;
  return t;
}
struct __attribute__((aligned(4))) char4
{
  signed char x;
  signed char y;
  signed char z;
  signed char w;
};
typedef struct char4 char4;
static __inline  char4 make_char4(signed char x, signed char y, signed char z, signed char w)
{
  char4 t;
  t.x = x;
  t.y = y;
  t.z = z;
  t.w = w;
  return t;
}
struct __attribute__((aligned(4))) uchar4
{
  unsigned char x;
  unsigned char y;
  unsigned char z;
  unsigned char w;
};
typedef struct uchar4 uchar4;
static __inline  uchar4 make_uchar4(unsigned char x, unsigned char y, unsigned char z, unsigned char w)
{
  uchar4 t;
  t.x = x;
  t.y = y;
  t.z = z;
  t.w = w;
  return t;
}
struct  short1
{
  short int x;
};
typedef struct short1 short1;
static __inline  short1 make_short1(short int x)
{
  short1 t;
  t.x = x;
  return t;
}
struct  ushort1
{
  unsigned short int x;
};
typedef struct ushort1 ushort1;
static __inline  ushort1 make_ushort1(unsigned short int x)
{
  ushort1 t;
  t.x = x;
  return t;
}
struct __attribute__((aligned(4))) short2
{
  short int x;
  short int y;
};
typedef struct short2 short2;
static __inline  short2 make_short2(short int x, short int y)
{
  short2 t;
  t.x = x;
  t.y = y;
  return t;
}
struct __attribute__((aligned(4))) ushort2
{
  unsigned short int x;
  unsigned short int y;
};
typedef struct ushort2 ushort2;
static __inline  ushort2 make_ushort2(unsigned short int x, unsigned short int y)
{
  ushort2 t;
  t.x = x;
  t.y = y;
  return t;
}
struct  short3
{
  short int x;
  short int y;
  short int z;
};
typedef struct short3 short3;
static __inline  short3 make_short3(short int x, short int y, short int z)
{
  short3 t;
  t.x = x;
  t.y = y;
  t.z = z;
  return t;
}
struct  ushort3
{
  unsigned short int x;
  unsigned short int y;
  unsigned short int z;
};
typedef struct ushort3 ushort3;
static __inline  ushort3 make_ushort3(unsigned short int x, unsigned short int y, unsigned short int z)
{
  ushort3 t;
  t.x = x;
  t.y = y;
  t.z = z;
  return t;
}
struct __attribute__((aligned(8))) short4
{
  short int x;
  short int y;
  short int z;
  short int w;
};
typedef struct short4 short4;
static __inline  short4 make_short4(short int x, short int y, short int z, short int w)
{
  short4 t;
  t.x = x;
  t.y = y;
  t.z = z;
  t.w = w;
  return t;
}
struct __attribute__((aligned(8))) ushort4
{
  unsigned short int x;
  unsigned short int y;
  unsigned short int z;
  unsigned short int w;
};
typedef struct ushort4 ushort4;
static __inline  ushort4 make_ushort4(unsigned short int x, unsigned short int y, unsigned short int z, unsigned short int w)
{
  ushort4 t;
  t.x = x;
  t.y = y;
  t.z = z;
  t.w = w;
  return t;
}
struct  int1
{
  int x;
};
typedef struct int1 int1;
static __inline  int1 make_int1(int x)
{
  int1 t;
  t.x = x;
  return t;
}
struct  uint1
{
  unsigned int x;
};
typedef struct uint1 uint1;
static __inline  uint1 make_uint1(unsigned int x)
{
  uint1 t;
  t.x = x;
  return t;
}
struct __attribute__((aligned(8))) int2
{
  int x;
  int y;
};
typedef struct int2 int2;
static __inline  int2 make_int2(int x, int y)
{
  int2 t;
  t.x = x;
  t.y = y;
  return t;
}
struct __attribute__((aligned(8))) uint2
{
  unsigned int x;
  unsigned int y;
};
typedef struct uint2 uint2;
static __inline  uint2 make_uint2(unsigned int x, unsigned int y)
{
  uint2 t;
  t.x = x;
  t.y = y;
  return t;
}
struct  int3
{
  int x;
  int y;
  int z;
};
typedef struct int3 int3;
static __inline  int3 make_int3(int x, int y, int z)
{
  int3 t;
  t.x = x;
  t.y = y;
  t.z = z;
  return t;
}
struct  uint3
{
  unsigned int x;
  unsigned int y;
  unsigned int z;
};
typedef struct uint3 uint3;
static __inline  uint3 make_uint3(unsigned int x, unsigned int y, unsigned int z)
{
  uint3 t;
  t.x = x;
  t.y = y;
  t.z = z;
  return t;
}
struct __attribute__((aligned(16))) int4
{
  int x;
  int y;
  int z;
  int w;
};
typedef struct int4 int4;
static __inline  int4 make_int4(int x, int y, int z, int w)
{
  int4 t;
  t.x = x;
  t.y = y;
  t.z = z;
  t.w = w;
  return t;
}
struct __attribute__((aligned(16))) uint4
{
  unsigned int x;
  unsigned int y;
  unsigned int z;
  unsigned int w;
};
typedef struct uint4 uint4;
static __inline  uint4 make_uint4(unsigned int x, unsigned int y, unsigned int z, unsigned int w)
{
  uint4 t;
  t.x = x;
  t.y = y;
  t.z = z;
  t.w = w;
  return t;
}
struct  long1
{
  long int x;
};
typedef struct long1 long1;
static __inline  long1 make_long1(long int x)
{
  long1 t;
  t.x = x;
  return t;
}
struct  ulong1
{
  unsigned long int x;
};
typedef struct ulong1 ulong1;
static __inline  ulong1 make_ulong1(unsigned long int x)
{
  ulong1 t;
  t.x = x;
  return t;
}
struct __attribute__((aligned(2 * sizeof(long int)))) long2
{
  long int x;
  long int y;
};
typedef struct long2 long2;
static __inline  long2 make_long2(long int x, long int y)
{
  long2 t;
  t.x = x;
  t.y = y;
  return t;
}
struct __attribute__((aligned(2 * sizeof(unsigned long int)))) ulong2
{
  unsigned long int x;
  unsigned long int y;
};
typedef struct ulong2 ulong2;
static __inline  ulong2 make_ulong2(unsigned long int x, unsigned long int y)
{
  ulong2 t;
  t.x = x;
  t.y = y;
  return t;
}
struct  long3
{
  long int x;
  long int y;
  long int z;
};
typedef struct long3 long3;
static __inline  long3 make_long3(long int x, long int y, long int z)
{
  long3 t;
  t.x = x;
  t.y = y;
  t.z = z;
  return t;
}
struct  ulong3
{
  unsigned long int x;
  unsigned long int y;
  unsigned long int z;
};
typedef struct ulong3 ulong3;
static __inline  ulong3 make_ulong3(unsigned long int x, unsigned long int y, unsigned long int z)
{
  ulong3 t;
  t.x = x;
  t.y = y;
  t.z = z;
  return t;
}
struct __attribute__((aligned(16))) long4
{
  long int x;
  long int y;
  long int z;
  long int w;
};
typedef struct long4 long4;
static __inline  long4 make_long4(long int x, long int y, long int z, long int w)
{
  long4 t;
  t.x = x;
  t.y = y;
  t.z = z;
  t.w = w;
  return t;
}
struct __attribute__((aligned(16))) ulong4
{
  unsigned long int x;
  unsigned long int y;
  unsigned long int z;
  unsigned long int w;
};
typedef struct ulong4 ulong4;
static __inline  ulong4 make_ulong4(unsigned long int x, unsigned long int y, unsigned long int z, unsigned long int w)
{
  ulong4 t;
  t.x = x;
  t.y = y;
  t.z = z;
  t.w = w;
  return t;
}
struct  float1
{
  float x;
};
typedef struct float1 float1;
static __inline  float1 make_float1(float x)
{
  float1 t;
  t.x = x;
  return t;
}
struct __attribute__((aligned(8))) float2
{
  float x;
  float y;
};
typedef struct float2 float2;
static __inline  float2 make_float2(float x, float y)
{
  float2 t;
  t.x = x;
  t.y = y;
  return t;
}
struct  float3
{
  float x;
  float y;
  float z;
};
typedef struct float3 float3;
static __inline  float3 make_float3(float x, float y, float z)
{
  float3 t;
  t.x = x;
  t.y = y;
  t.z = z;
  return t;
}
struct __attribute__((aligned(16))) float4
{
  float x;
  float y;
  float z;
  float w;
};
typedef struct float4 float4;
static __inline  float4 make_float4(float x, float y, float z, float w)
{
  float4 t;
  t.x = x;
  t.y = y;
  t.z = z;
  t.w = w;
  return t;
}
struct  longlong1
{
  long long int x;
};
typedef struct longlong1 longlong1;
static __inline  longlong1 make_longlong1(long long int x)
{
  longlong1 t;
  t.x = x;
  return t;
}
struct  ulonglong1
{
  unsigned long long int x;
};
typedef struct ulonglong1 ulonglong1;
static __inline  ulonglong1 make_ulonglong1(unsigned long long int x)
{
  ulonglong1 t;
  t.x = x;
  return t;
}
struct __attribute__((aligned(16))) longlong2
{
  long long int x;
  long long int y;
};
typedef struct longlong2 longlong2;
static __inline  longlong2 make_longlong2(long long int x, long long int y)
{
  longlong2 t;
  t.x = x;
  t.y = y;
  return t;
}
struct __attribute__((aligned(16))) ulonglong2
{
  unsigned long long int x;
  unsigned long long int y;
};
typedef struct ulonglong2 ulonglong2;
static __inline  ulonglong2 make_ulonglong2(unsigned long long int x, unsigned long long int y)
{
  ulonglong2 t;
  t.x = x;
  t.y = y;
  return t;
}
struct  longlong3
{
  long long int x;
  long long int y;
  long long int z;
};
typedef struct longlong3 longlong3;
static __inline  longlong3 make_longlong3(long long int x, long long int y, long long int z)
{
  longlong3 t;
  t.x = x;
  t.y = y;
  t.z = z;
  return t;
}
struct  ulonglong3
{
  unsigned long long int x;
  unsigned long long int y;
  unsigned long long int z;
};
typedef struct ulonglong3 ulonglong3;
static __inline  ulonglong3 make_ulonglong3(unsigned long long int x, unsigned long long int y, unsigned long long int z)
{
  ulonglong3 t;
  t.x = x;
  t.y = y;
  t.z = z;
  return t;
}
struct __attribute__((aligned(16))) longlong4
{
  long long int x;
  long long int y;
  long long int z;
  long long int w;
};
typedef struct longlong4 longlong4;
static __inline  longlong4 make_longlong4(long long int x, long long int y, long long int z, long long int w)
{
  longlong4 t;
  t.x = x;
  t.y = y;
  t.z = z;
  t.w = w;
  return t;
}
struct __attribute__((aligned(16))) ulonglong4
{
  unsigned long long int x;
  unsigned long long int y;
  unsigned long long int z;
  unsigned long long int w;
};
typedef struct ulonglong4 ulonglong4;
static __inline  ulonglong4 make_ulonglong4(unsigned long long int x, unsigned long long int y, unsigned long long int z, unsigned long long int w)
{
  ulonglong4 t;
  t.x = x;
  t.y = y;
  t.z = z;
  t.w = w;
  return t;
}
struct  double1
{
  double x;
};
typedef struct double1 double1;
static __inline  double1 make_double1(double x)
{
  double1 t;
  t.x = x;
  return t;
}
struct __attribute__((aligned(16))) double2
{
  double x;
  double y;
};
typedef struct double2 double2;
static __inline  double2 make_double2(double x, double y)
{
  double2 t;
  t.x = x;
  t.y = y;
  return t;
}
struct  double3
{
  double x;
  double y;
  double z;
};
typedef struct double3 double3;
static __inline  double3 make_double3(double x, double y, double z)
{
  double3 t;
  t.x = x;
  t.y = y;
  t.z = z;
  return t;
}
struct __attribute__((aligned(16))) double4
{
  double x;
  double y;
  double z;
  double w;
};
typedef struct double4 double4;
static __inline  double4 make_double4(double x, double y, double z, double w)
{
  double4 t;
  t.x = x;
  t.y = y;
  t.z = z;
  t.w = w;
  return t;
}
extern __inline __attribute__((__nothrow__)) __attribute__((__const__)) __attribute__((__gnu_inline__)) int __signbitf(float __x)
{
  int __m;
  __asm__ ("pmovmskb %1, %0" : "=r"(__m) : "x"(__x));
  return __m & 8;
}
extern __inline __attribute__((__nothrow__)) __attribute__((__const__)) __attribute__((__gnu_inline__)) int __signbit(double __x)
{
  int __m;
  __asm__ ("pmovmskb %1, %0" : "=r"(__m) : "x"(__x));
  return __m & 128;
}
extern __inline __attribute__((__nothrow__)) __attribute__((__const__)) __attribute__((__gnu_inline__)) int __signbitl(long double __x)
{
  union  mcc_union_anon_67
  {
    long double __l;
    int __i[3];
  };
  union mcc_union_anon_67 __u = { .__l = __x };
  return (__u.__i[2] & 32768) != 0;
}
typedef float2 cuFloatComplex;
static __inline  float cuCrealf(cuFloatComplex x)
{
  return x.x;
}
static __inline  float cuCimagf(cuFloatComplex x)
{
  return x.y;
}
static __inline  cuFloatComplex make_cuFloatComplex(float r, float i)
{
  cuFloatComplex res;
  res.x = r;
  res.y = i;
  return res;
}
static __inline  cuFloatComplex cuConjf(cuFloatComplex x)
{
  return make_cuFloatComplex(cuCrealf(x),  -cuCimagf(x));
}
static __inline  cuFloatComplex cuCaddf(cuFloatComplex x, cuFloatComplex y)
{
  return make_cuFloatComplex(cuCrealf(x) + cuCrealf(y), cuCimagf(x) + cuCimagf(y));
}
static __inline  cuFloatComplex cuCsubf(cuFloatComplex x, cuFloatComplex y)
{
  return make_cuFloatComplex(cuCrealf(x) - cuCrealf(y), cuCimagf(x) - cuCimagf(y));
}
static __inline  cuFloatComplex cuCmulf(cuFloatComplex x, cuFloatComplex y)
{
  cuFloatComplex prod;
  prod = make_cuFloatComplex(cuCrealf(x) * cuCrealf(y) - cuCimagf(x) * cuCimagf(y), cuCrealf(x) * cuCimagf(y) + cuCimagf(x) * cuCrealf(y));
  return prod;
}
extern float fabsf(float __x)__attribute__((__nothrow__)) __attribute__((__const__));
static __inline  cuFloatComplex cuCdivf(cuFloatComplex x, cuFloatComplex y)
{
  cuFloatComplex quot;
  float s = fabsf(cuCrealf(y)) + fabsf(cuCimagf(y));
  float oos = 1.000000000000000000000000e+00f / s;
  float ars = cuCrealf(x) * oos;
  float ais = cuCimagf(x) * oos;
  float brs = cuCrealf(y) * oos;
  float bis = cuCimagf(y) * oos;
  s = brs * brs + bis * bis;
  oos = 1.000000000000000000000000e+00f / s;
  quot = make_cuFloatComplex((ars * brs + ais * bis) * oos, (ais * brs - ars * bis) * oos);
  return quot;
}
extern float sqrtf(float __x)__attribute__((__nothrow__));
static __inline  float cuCabsf(cuFloatComplex x)
{
  float v;
  float w;
  float t;
  float a = cuCrealf(x);
  float b = cuCimagf(x);
  a = fabsf(a);
  b = fabsf(b);
  if (a > b)
    {
      v = a;
      w = b;
    }
  else
    {
      v = b;
      w = a;
    }
  t = w / v;
  t = 1.000000000000000000000000e+00f + t * t;
  t = v * sqrtf(t);
  if ((v == 0.000000000000000000000000e+00f || v > 3.402823466385288598117042e+38f) || w > 3.402823466385288598117042e+38f)
    {
      t = v + w;
    }
  return t;
}
typedef double2 cuDoubleComplex;
static __inline  double cuCreal(cuDoubleComplex x)
{
  return x.x;
}
static __inline  double cuCimag(cuDoubleComplex x)
{
  return x.y;
}
static __inline  cuDoubleComplex make_cuDoubleComplex(double r, double i)
{
  cuDoubleComplex res;
  res.x = r;
  res.y = i;
  return res;
}
static __inline  cuDoubleComplex cuConj(cuDoubleComplex x)
{
  return make_cuDoubleComplex(cuCreal(x),  -cuCimag(x));
}
static __inline  cuDoubleComplex cuCadd(cuDoubleComplex x, cuDoubleComplex y)
{
  return make_cuDoubleComplex(cuCreal(x) + cuCreal(y), cuCimag(x) + cuCimag(y));
}
static __inline  cuDoubleComplex cuCsub(cuDoubleComplex x, cuDoubleComplex y)
{
  return make_cuDoubleComplex(cuCreal(x) - cuCreal(y), cuCimag(x) - cuCimag(y));
}
static __inline  cuDoubleComplex cuCmul(cuDoubleComplex x, cuDoubleComplex y)
{
  cuDoubleComplex prod;
  prod = make_cuDoubleComplex(cuCreal(x) * cuCreal(y) - cuCimag(x) * cuCimag(y), cuCreal(x) * cuCimag(y) + cuCimag(x) * cuCreal(y));
  return prod;
}
extern double fabs(double __x)__attribute__((__nothrow__)) __attribute__((__const__));
static __inline  cuDoubleComplex cuCdiv(cuDoubleComplex x, cuDoubleComplex y)
{
  cuDoubleComplex quot;
  double s = fabs(cuCreal(y)) + fabs(cuCimag(y));
  double oos = 1.00000000000000000000000000000000000000000000000000000e+00 / s;
  double ars = cuCreal(x) * oos;
  double ais = cuCimag(x) * oos;
  double brs = cuCreal(y) * oos;
  double bis = cuCimag(y) * oos;
  s = brs * brs + bis * bis;
  oos = 1.00000000000000000000000000000000000000000000000000000e+00 / s;
  quot = make_cuDoubleComplex((ars * brs + ais * bis) * oos, (ais * brs - ars * bis) * oos);
  return quot;
}
extern double sqrt(double __x)__attribute__((__nothrow__));
static __inline  double cuCabs(cuDoubleComplex x)
{
  double v;
  double w;
  double t;
  double a = cuCreal(x);
  double b = cuCimag(x);
  a = fabs(a);
  b = fabs(b);
  if (a > b)
    {
      v = a;
      w = b;
    }
  else
    {
      v = b;
      w = a;
    }
  t = w / v;
  t = 1.00000000000000000000000000000000000000000000000000000e+00 + t * t;
  t = v * sqrt(t);
  if ((v == 0.00000000000000000000000000000000000000000000000000000e+00 || v > 1.79769313486231570814527423731704356798070567525844997e+308) || w > 1.79769313486231570814527423731704356798070567525844997e+308)
    {
      t = v + w;
    }
  return t;
}
typedef cuFloatComplex cuComplex;
static __inline  cuComplex make_cuComplex(float x, float y)
{
  return make_cuFloatComplex(x, y);
}
static __inline  cuDoubleComplex cuComplexFloatToDouble(cuFloatComplex c)
{
  return make_cuDoubleComplex((double)cuCrealf(c), (double)cuCimagf(c));
}
static __inline  cuFloatComplex cuComplexDoubleToFloat(cuDoubleComplex c)
{
  return make_cuFloatComplex((float)cuCreal(c), (float)cuCimag(c));
}
static __inline  cuComplex cuCfmaf(cuComplex x, cuComplex y, cuComplex d)
{
  float real_res;
  float imag_res;
  real_res = cuCrealf(x) * cuCrealf(y) + cuCrealf(d);
  imag_res = cuCrealf(x) * cuCimagf(y) + cuCimagf(d);
  real_res =  -(cuCimagf(x) * cuCimagf(y)) + real_res;
  imag_res = cuCimagf(x) * cuCrealf(y) + imag_res;
  return make_cuComplex(real_res, imag_res);
}
static __inline  cuDoubleComplex cuCfma(cuDoubleComplex x, cuDoubleComplex y, cuDoubleComplex d)
{
  double real_res;
  double imag_res;
  real_res = cuCreal(x) * cuCreal(y) + cuCreal(d);
  imag_res = cuCreal(x) * cuCimag(y) + cuCimag(d);
  real_res =  -(cuCimag(x) * cuCimag(y)) + real_res;
  imag_res = cuCimag(x) * cuCreal(y) + imag_res;
  return make_cuDoubleComplex(real_res, imag_res);
}
void init(double *a, double *b, double *c, int size)
{
  int j;
  for (j = 0; j < size; j++)
    {
      a[j] = 1.00000000000000000000000000000000000000000000000000000e+00;
      b[j] = 2.00000000000000000000000000000000000000000000000000000e+00;
      c[j] = 0.00000000000000000000000000000000000000000000000000000e+00;
      a[j] = 2.00000000000000000000000000000000000000000000000000000e+00 * a[j];
    }
}
void copy(double *a, double *c, int size)
{
  int j;
  for (j = 0; j < size; j++)
    c[j] = a[j];
}
void scale(double *b, double *c, double scalar, int size)
{
  int j;
  for (j = 0; j < size; j++)
    b[j] = scalar * c[j];
}
void add(double *a, double *b, double *c, int size)
{
  int j;
  for (j = 0; j < size; j++)
    c[j] = a[j] + b[j];
}
void triad(double *a, double *b, double *c, double scalar, int size)
{
  int j;
  for (j = 0; j < size; j++)
    a[j] = b[j] + scalar * c[j];
}
typedef void nanos_init_func_t(void *);
struct  mcc_struct_anon_65
{
  void (*func)(void *);
  void *data;
};
typedef struct mcc_struct_anon_65 nanos_init_desc_t;
void nanos_omp_set_interface(void *);
__attribute__((weak)) __attribute__((section("nanos_init"))) nanos_init_desc_t __section__nanos_init = { nanos_omp_set_interface, (void *)0 };
