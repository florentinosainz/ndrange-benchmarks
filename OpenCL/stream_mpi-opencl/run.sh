#!/bin/bash

# @ partition = projects
# @ output = stream.log
# @ error = stream.log
# @ initialdir = .
# @ total_tasks = 4
# @ cpus_per_task = 12
# @ gpus_per_node = 2
# @ node_usage = not_shared
# @ wall_clock_limit = 00:20:00

NX_SCHEDULER=affinity NX_OPENCL_DEVICE_TYPE=GPU NX_OPENCL_MAX_DEVICES=${1:-1} NX_ARGS="--instrumentation=extrae --opencl-cache-policy=wb --disable-cuda" mpirun -n 4 ./stream-strongscaling


