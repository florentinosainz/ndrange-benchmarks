typedef __builtin_va_list __gnuc_va_list;
struct _IO_FILE;
typedef struct _IO_FILE FILE;
extern int vfprintf(FILE *__restrict __s, const char *__restrict __format, __gnuc_va_list __arg);
extern struct _IO_FILE *stdout;
extern __inline __attribute__((__gnu_inline__)) int vprintf(const char *__restrict __fmt, __gnuc_va_list __arg)
{
  return vfprintf(stdout, __fmt, __arg);
}
typedef struct _IO_FILE _IO_FILE;
extern int _IO_getc(_IO_FILE *__fp);
extern struct _IO_FILE *stdin;
extern __inline __attribute__((__gnu_inline__)) int getchar(void)
{
  return _IO_getc(stdin);
}
struct _IO_marker;
typedef long int __off_t;
typedef void _IO_lock_t;
typedef long int __off64_t;
typedef unsigned long int size_t;
struct  _IO_FILE
{
  int _flags;
  char *_IO_read_ptr;
  char *_IO_read_end;
  char *_IO_read_base;
  char *_IO_write_base;
  char *_IO_write_ptr;
  char *_IO_write_end;
  char *_IO_buf_base;
  char *_IO_buf_end;
  char *_IO_save_base;
  char *_IO_backup_base;
  char *_IO_save_end;
  struct _IO_marker *_markers;
  struct _IO_FILE *_chain;
  int _fileno;
  int _flags2;
  __off_t _old_offset;
  unsigned short int _cur_column;
  signed char _vtable_offset;
  char _shortbuf[1];
  _IO_lock_t *_lock;
  __off64_t _offset;
  void *__pad1;
  void *__pad2;
  void *__pad3;
  void *__pad4;
  size_t __pad5;
  int _mode;
  char _unused2[20U];
};
extern int __uflow(_IO_FILE *);
extern __inline __attribute__((__gnu_inline__)) int fgetc_unlocked(FILE *__fp)
{
  return __builtin_expect((*__fp)._IO_read_ptr >= (*__fp)._IO_read_end, 0) ? __uflow(__fp) : *((unsigned char *)(*__fp)._IO_read_ptr++);
}
extern __inline __attribute__((__gnu_inline__)) int getc_unlocked(FILE *__fp)
{
  return __builtin_expect((*__fp)._IO_read_ptr >= (*__fp)._IO_read_end, 0) ? __uflow(__fp) : *((unsigned char *)(*__fp)._IO_read_ptr++);
}
extern __inline __attribute__((__gnu_inline__)) int getchar_unlocked(void)
{
  return __builtin_expect((*stdin)._IO_read_ptr >= (*stdin)._IO_read_end, 0) ? __uflow(stdin) : *((unsigned char *)(*stdin)._IO_read_ptr++);
}
extern int _IO_putc(int __c, _IO_FILE *__fp);
extern __inline __attribute__((__gnu_inline__)) int putchar(int __c)
{
  return _IO_putc(__c, stdout);
}
extern int __overflow(_IO_FILE *, int);
extern __inline __attribute__((__gnu_inline__)) int fputc_unlocked(int __c, FILE *__stream)
{
  return __builtin_expect((*__stream)._IO_write_ptr >= (*__stream)._IO_write_end, 0) ? __overflow(__stream, (unsigned char)__c) : (unsigned char)(*(*__stream)._IO_write_ptr++ = __c);
}
extern __inline __attribute__((__gnu_inline__)) int putc_unlocked(int __c, FILE *__stream)
{
  return __builtin_expect((*__stream)._IO_write_ptr >= (*__stream)._IO_write_end, 0) ? __overflow(__stream, (unsigned char)__c) : (unsigned char)(*(*__stream)._IO_write_ptr++ = __c);
}
extern __inline __attribute__((__gnu_inline__)) int putchar_unlocked(int __c)
{
  return __builtin_expect((*stdout)._IO_write_ptr >= (*stdout)._IO_write_end, 0) ? __overflow(stdout, (unsigned char)__c) : (unsigned char)(*(*stdout)._IO_write_ptr++ = __c);
}
extern __inline __attribute__((__nothrow__)) __attribute__((__gnu_inline__)) int feof_unlocked(FILE *__stream)
{
  return ((*__stream)._flags & 16) != 0;
}
extern __inline __attribute__((__nothrow__)) __attribute__((__gnu_inline__)) int ferror_unlocked(FILE *__stream)
{
  return ((*__stream)._flags & 32) != 0;
}
struct  cudaPitchedPtr
{
  void *ptr;
  size_t pitch;
  size_t xsize;
  size_t ysize;
};
static __inline  struct cudaPitchedPtr make_cudaPitchedPtr(void *d, size_t p, size_t xsz, size_t ysz)
{
  struct cudaPitchedPtr s;
  s.ptr = d;
  s.pitch = p;
  s.xsize = xsz;
  s.ysize = ysz;
  return s;
}
struct  cudaPos
{
  size_t x;
  size_t y;
  size_t z;
};
static __inline  struct cudaPos make_cudaPos(size_t x, size_t y, size_t z)
{
  struct cudaPos p;
  p.x = x;
  p.y = y;
  p.z = z;
  return p;
}
struct  cudaExtent
{
  size_t width;
  size_t height;
  size_t depth;
};
static __inline  struct cudaExtent make_cudaExtent(size_t w, size_t h, size_t d)
{
  struct cudaExtent e;
  e.width = w;
  e.height = h;
  e.depth = d;
  return e;
}
struct  char1
{
  signed char x;
};
typedef struct char1 char1;
static __inline  char1 make_char1(signed char x)
{
  char1 t;
  t.x = x;
  return t;
}
struct  uchar1
{
  unsigned char x;
};
typedef struct uchar1 uchar1;
static __inline  uchar1 make_uchar1(unsigned char x)
{
  uchar1 t;
  t.x = x;
  return t;
}
struct __attribute__((aligned(2))) char2
{
  signed char x;
  signed char y;
};
typedef struct char2 char2;
static __inline  char2 make_char2(signed char x, signed char y)
{
  char2 t;
  t.x = x;
  t.y = y;
  return t;
}
struct __attribute__((aligned(2))) uchar2
{
  unsigned char x;
  unsigned char y;
};
typedef struct uchar2 uchar2;
static __inline  uchar2 make_uchar2(unsigned char x, unsigned char y)
{
  uchar2 t;
  t.x = x;
  t.y = y;
  return t;
}
struct  char3
{
  signed char x;
  signed char y;
  signed char z;
};
typedef struct char3 char3;
static __inline  char3 make_char3(signed char x, signed char y, signed char z)
{
  char3 t;
  t.x = x;
  t.y = y;
  t.z = z;
  return t;
}
struct  uchar3
{
  unsigned char x;
  unsigned char y;
  unsigned char z;
};
typedef struct uchar3 uchar3;
static __inline  uchar3 make_uchar3(unsigned char x, unsigned char y, unsigned char z)
{
  uchar3 t;
  t.x = x;
  t.y = y;
  t.z = z;
  return t;
}
struct __attribute__((aligned(4))) char4
{
  signed char x;
  signed char y;
  signed char z;
  signed char w;
};
typedef struct char4 char4;
static __inline  char4 make_char4(signed char x, signed char y, signed char z, signed char w)
{
  char4 t;
  t.x = x;
  t.y = y;
  t.z = z;
  t.w = w;
  return t;
}
struct __attribute__((aligned(4))) uchar4
{
  unsigned char x;
  unsigned char y;
  unsigned char z;
  unsigned char w;
};
typedef struct uchar4 uchar4;
static __inline  uchar4 make_uchar4(unsigned char x, unsigned char y, unsigned char z, unsigned char w)
{
  uchar4 t;
  t.x = x;
  t.y = y;
  t.z = z;
  t.w = w;
  return t;
}
struct  short1
{
  short int x;
};
typedef struct short1 short1;
static __inline  short1 make_short1(short int x)
{
  short1 t;
  t.x = x;
  return t;
}
struct  ushort1
{
  unsigned short int x;
};
typedef struct ushort1 ushort1;
static __inline  ushort1 make_ushort1(unsigned short int x)
{
  ushort1 t;
  t.x = x;
  return t;
}
struct __attribute__((aligned(4))) short2
{
  short int x;
  short int y;
};
typedef struct short2 short2;
static __inline  short2 make_short2(short int x, short int y)
{
  short2 t;
  t.x = x;
  t.y = y;
  return t;
}
struct __attribute__((aligned(4))) ushort2
{
  unsigned short int x;
  unsigned short int y;
};
typedef struct ushort2 ushort2;
static __inline  ushort2 make_ushort2(unsigned short int x, unsigned short int y)
{
  ushort2 t;
  t.x = x;
  t.y = y;
  return t;
}
struct  short3
{
  short int x;
  short int y;
  short int z;
};
typedef struct short3 short3;
static __inline  short3 make_short3(short int x, short int y, short int z)
{
  short3 t;
  t.x = x;
  t.y = y;
  t.z = z;
  return t;
}
struct  ushort3
{
  unsigned short int x;
  unsigned short int y;
  unsigned short int z;
};
typedef struct ushort3 ushort3;
static __inline  ushort3 make_ushort3(unsigned short int x, unsigned short int y, unsigned short int z)
{
  ushort3 t;
  t.x = x;
  t.y = y;
  t.z = z;
  return t;
}
struct __attribute__((aligned(8))) short4
{
  short int x;
  short int y;
  short int z;
  short int w;
};
typedef struct short4 short4;
static __inline  short4 make_short4(short int x, short int y, short int z, short int w)
{
  short4 t;
  t.x = x;
  t.y = y;
  t.z = z;
  t.w = w;
  return t;
}
struct __attribute__((aligned(8))) ushort4
{
  unsigned short int x;
  unsigned short int y;
  unsigned short int z;
  unsigned short int w;
};
typedef struct ushort4 ushort4;
static __inline  ushort4 make_ushort4(unsigned short int x, unsigned short int y, unsigned short int z, unsigned short int w)
{
  ushort4 t;
  t.x = x;
  t.y = y;
  t.z = z;
  t.w = w;
  return t;
}
struct  int1
{
  int x;
};
typedef struct int1 int1;
static __inline  int1 make_int1(int x)
{
  int1 t;
  t.x = x;
  return t;
}
struct  uint1
{
  unsigned int x;
};
typedef struct uint1 uint1;
static __inline  uint1 make_uint1(unsigned int x)
{
  uint1 t;
  t.x = x;
  return t;
}
struct __attribute__((aligned(8))) int2
{
  int x;
  int y;
};
typedef struct int2 int2;
static __inline  int2 make_int2(int x, int y)
{
  int2 t;
  t.x = x;
  t.y = y;
  return t;
}
struct __attribute__((aligned(8))) uint2
{
  unsigned int x;
  unsigned int y;
};
typedef struct uint2 uint2;
static __inline  uint2 make_uint2(unsigned int x, unsigned int y)
{
  uint2 t;
  t.x = x;
  t.y = y;
  return t;
}
struct  int3
{
  int x;
  int y;
  int z;
};
typedef struct int3 int3;
static __inline  int3 make_int3(int x, int y, int z)
{
  int3 t;
  t.x = x;
  t.y = y;
  t.z = z;
  return t;
}
struct  uint3
{
  unsigned int x;
  unsigned int y;
  unsigned int z;
};
typedef struct uint3 uint3;
static __inline  uint3 make_uint3(unsigned int x, unsigned int y, unsigned int z)
{
  uint3 t;
  t.x = x;
  t.y = y;
  t.z = z;
  return t;
}
struct __attribute__((aligned(16))) int4
{
  int x;
  int y;
  int z;
  int w;
};
typedef struct int4 int4;
static __inline  int4 make_int4(int x, int y, int z, int w)
{
  int4 t;
  t.x = x;
  t.y = y;
  t.z = z;
  t.w = w;
  return t;
}
struct __attribute__((aligned(16))) uint4
{
  unsigned int x;
  unsigned int y;
  unsigned int z;
  unsigned int w;
};
typedef struct uint4 uint4;
static __inline  uint4 make_uint4(unsigned int x, unsigned int y, unsigned int z, unsigned int w)
{
  uint4 t;
  t.x = x;
  t.y = y;
  t.z = z;
  t.w = w;
  return t;
}
struct  long1
{
  long int x;
};
typedef struct long1 long1;
static __inline  long1 make_long1(long int x)
{
  long1 t;
  t.x = x;
  return t;
}
struct  ulong1
{
  unsigned long int x;
};
typedef struct ulong1 ulong1;
static __inline  ulong1 make_ulong1(unsigned long int x)
{
  ulong1 t;
  t.x = x;
  return t;
}
struct __attribute__((aligned(2 * sizeof(long int)))) long2
{
  long int x;
  long int y;
};
typedef struct long2 long2;
static __inline  long2 make_long2(long int x, long int y)
{
  long2 t;
  t.x = x;
  t.y = y;
  return t;
}
struct __attribute__((aligned(2 * sizeof(unsigned long int)))) ulong2
{
  unsigned long int x;
  unsigned long int y;
};
typedef struct ulong2 ulong2;
static __inline  ulong2 make_ulong2(unsigned long int x, unsigned long int y)
{
  ulong2 t;
  t.x = x;
  t.y = y;
  return t;
}
struct  long3
{
  long int x;
  long int y;
  long int z;
};
typedef struct long3 long3;
static __inline  long3 make_long3(long int x, long int y, long int z)
{
  long3 t;
  t.x = x;
  t.y = y;
  t.z = z;
  return t;
}
struct  ulong3
{
  unsigned long int x;
  unsigned long int y;
  unsigned long int z;
};
typedef struct ulong3 ulong3;
static __inline  ulong3 make_ulong3(unsigned long int x, unsigned long int y, unsigned long int z)
{
  ulong3 t;
  t.x = x;
  t.y = y;
  t.z = z;
  return t;
}
struct __attribute__((aligned(16))) long4
{
  long int x;
  long int y;
  long int z;
  long int w;
};
typedef struct long4 long4;
static __inline  long4 make_long4(long int x, long int y, long int z, long int w)
{
  long4 t;
  t.x = x;
  t.y = y;
  t.z = z;
  t.w = w;
  return t;
}
struct __attribute__((aligned(16))) ulong4
{
  unsigned long int x;
  unsigned long int y;
  unsigned long int z;
  unsigned long int w;
};
typedef struct ulong4 ulong4;
static __inline  ulong4 make_ulong4(unsigned long int x, unsigned long int y, unsigned long int z, unsigned long int w)
{
  ulong4 t;
  t.x = x;
  t.y = y;
  t.z = z;
  t.w = w;
  return t;
}
struct  float1
{
  float x;
};
typedef struct float1 float1;
static __inline  float1 make_float1(float x)
{
  float1 t;
  t.x = x;
  return t;
}
struct __attribute__((aligned(8))) float2
{
  float x;
  float y;
};
typedef struct float2 float2;
static __inline  float2 make_float2(float x, float y)
{
  float2 t;
  t.x = x;
  t.y = y;
  return t;
}
struct  float3
{
  float x;
  float y;
  float z;
};
typedef struct float3 float3;
static __inline  float3 make_float3(float x, float y, float z)
{
  float3 t;
  t.x = x;
  t.y = y;
  t.z = z;
  return t;
}
struct __attribute__((aligned(16))) float4
{
  float x;
  float y;
  float z;
  float w;
};
typedef struct float4 float4;
static __inline  float4 make_float4(float x, float y, float z, float w)
{
  float4 t;
  t.x = x;
  t.y = y;
  t.z = z;
  t.w = w;
  return t;
}
struct  longlong1
{
  long long int x;
};
typedef struct longlong1 longlong1;
static __inline  longlong1 make_longlong1(long long int x)
{
  longlong1 t;
  t.x = x;
  return t;
}
struct  ulonglong1
{
  unsigned long long int x;
};
typedef struct ulonglong1 ulonglong1;
static __inline  ulonglong1 make_ulonglong1(unsigned long long int x)
{
  ulonglong1 t;
  t.x = x;
  return t;
}
struct __attribute__((aligned(16))) longlong2
{
  long long int x;
  long long int y;
};
typedef struct longlong2 longlong2;
static __inline  longlong2 make_longlong2(long long int x, long long int y)
{
  longlong2 t;
  t.x = x;
  t.y = y;
  return t;
}
struct __attribute__((aligned(16))) ulonglong2
{
  unsigned long long int x;
  unsigned long long int y;
};
typedef struct ulonglong2 ulonglong2;
static __inline  ulonglong2 make_ulonglong2(unsigned long long int x, unsigned long long int y)
{
  ulonglong2 t;
  t.x = x;
  t.y = y;
  return t;
}
struct  longlong3
{
  long long int x;
  long long int y;
  long long int z;
};
typedef struct longlong3 longlong3;
static __inline  longlong3 make_longlong3(long long int x, long long int y, long long int z)
{
  longlong3 t;
  t.x = x;
  t.y = y;
  t.z = z;
  return t;
}
struct  ulonglong3
{
  unsigned long long int x;
  unsigned long long int y;
  unsigned long long int z;
};
typedef struct ulonglong3 ulonglong3;
static __inline  ulonglong3 make_ulonglong3(unsigned long long int x, unsigned long long int y, unsigned long long int z)
{
  ulonglong3 t;
  t.x = x;
  t.y = y;
  t.z = z;
  return t;
}
struct __attribute__((aligned(16))) longlong4
{
  long long int x;
  long long int y;
  long long int z;
  long long int w;
};
typedef struct longlong4 longlong4;
static __inline  longlong4 make_longlong4(long long int x, long long int y, long long int z, long long int w)
{
  longlong4 t;
  t.x = x;
  t.y = y;
  t.z = z;
  t.w = w;
  return t;
}
struct __attribute__((aligned(16))) ulonglong4
{
  unsigned long long int x;
  unsigned long long int y;
  unsigned long long int z;
  unsigned long long int w;
};
typedef struct ulonglong4 ulonglong4;
static __inline  ulonglong4 make_ulonglong4(unsigned long long int x, unsigned long long int y, unsigned long long int z, unsigned long long int w)
{
  ulonglong4 t;
  t.x = x;
  t.y = y;
  t.z = z;
  t.w = w;
  return t;
}
struct  double1
{
  double x;
};
typedef struct double1 double1;
static __inline  double1 make_double1(double x)
{
  double1 t;
  t.x = x;
  return t;
}
struct __attribute__((aligned(16))) double2
{
  double x;
  double y;
};
typedef struct double2 double2;
static __inline  double2 make_double2(double x, double y)
{
  double2 t;
  t.x = x;
  t.y = y;
  return t;
}
struct  double3
{
  double x;
  double y;
  double z;
};
typedef struct double3 double3;
static __inline  double3 make_double3(double x, double y, double z)
{
  double3 t;
  t.x = x;
  t.y = y;
  t.z = z;
  return t;
}
struct __attribute__((aligned(16))) double4
{
  double x;
  double y;
  double z;
  double w;
};
typedef struct double4 double4;
static __inline  double4 make_double4(double x, double y, double z, double w)
{
  double4 t;
  t.x = x;
  t.y = y;
  t.z = z;
  t.w = w;
  return t;
}
extern __inline __attribute__((__nothrow__)) __attribute__((__const__)) __attribute__((__gnu_inline__)) int __signbitf(float __x)
{
  int __m;
  __asm__ ("pmovmskb %1, %0" : "=r"(__m) : "x"(__x));
  return __m & 8;
}
extern __inline __attribute__((__nothrow__)) __attribute__((__const__)) __attribute__((__gnu_inline__)) int __signbit(double __x)
{
  int __m;
  __asm__ ("pmovmskb %1, %0" : "=r"(__m) : "x"(__x));
  return __m & 128;
}
extern __inline __attribute__((__nothrow__)) __attribute__((__const__)) __attribute__((__gnu_inline__)) int __signbitl(long double __x)
{
  union  mcc_union_anon_23
  {
    long double __l;
    int __i[3];
  };
  union mcc_union_anon_23 __u = { .__l = __x };
  return (__u.__i[2] & 32768) != 0;
}
typedef float2 cuFloatComplex;
static __inline  float cuCrealf(cuFloatComplex x)
{
  return x.x;
}
static __inline  float cuCimagf(cuFloatComplex x)
{
  return x.y;
}
static __inline  cuFloatComplex make_cuFloatComplex(float r, float i)
{
  cuFloatComplex res;
  res.x = r;
  res.y = i;
  return res;
}
static __inline  cuFloatComplex cuConjf(cuFloatComplex x)
{
  return make_cuFloatComplex(cuCrealf(x),  -cuCimagf(x));
}
static __inline  cuFloatComplex cuCaddf(cuFloatComplex x, cuFloatComplex y)
{
  return make_cuFloatComplex(cuCrealf(x) + cuCrealf(y), cuCimagf(x) + cuCimagf(y));
}
static __inline  cuFloatComplex cuCsubf(cuFloatComplex x, cuFloatComplex y)
{
  return make_cuFloatComplex(cuCrealf(x) - cuCrealf(y), cuCimagf(x) - cuCimagf(y));
}
static __inline  cuFloatComplex cuCmulf(cuFloatComplex x, cuFloatComplex y)
{
  cuFloatComplex prod;
  prod = make_cuFloatComplex(cuCrealf(x) * cuCrealf(y) - cuCimagf(x) * cuCimagf(y), cuCrealf(x) * cuCimagf(y) + cuCimagf(x) * cuCrealf(y));
  return prod;
}
extern float fabsf(float __x)__attribute__((__nothrow__)) __attribute__((__const__));
static __inline  cuFloatComplex cuCdivf(cuFloatComplex x, cuFloatComplex y)
{
  cuFloatComplex quot;
  float s = fabsf(cuCrealf(y)) + fabsf(cuCimagf(y));
  float oos = 1.000000000000000000000000e+00f / s;
  float ars = cuCrealf(x) * oos;
  float ais = cuCimagf(x) * oos;
  float brs = cuCrealf(y) * oos;
  float bis = cuCimagf(y) * oos;
  s = brs * brs + bis * bis;
  oos = 1.000000000000000000000000e+00f / s;
  quot = make_cuFloatComplex((ars * brs + ais * bis) * oos, (ais * brs - ars * bis) * oos);
  return quot;
}
extern float sqrtf(float __x)__attribute__((__nothrow__));
static __inline  float cuCabsf(cuFloatComplex x)
{
  float v;
  float w;
  float t;
  float a = cuCrealf(x);
  float b = cuCimagf(x);
  a = fabsf(a);
  b = fabsf(b);
  if (a > b)
    {
      v = a;
      w = b;
    }
  else
    {
      v = b;
      w = a;
    }
  t = w / v;
  t = 1.000000000000000000000000e+00f + t * t;
  t = v * sqrtf(t);
  if ((v == 0.000000000000000000000000e+00f || v > 3.402823466385288598117042e+38f) || w > 3.402823466385288598117042e+38f)
    {
      t = v + w;
    }
  return t;
}
typedef double2 cuDoubleComplex;
static __inline  double cuCreal(cuDoubleComplex x)
{
  return x.x;
}
static __inline  double cuCimag(cuDoubleComplex x)
{
  return x.y;
}
static __inline  cuDoubleComplex make_cuDoubleComplex(double r, double i)
{
  cuDoubleComplex res;
  res.x = r;
  res.y = i;
  return res;
}
static __inline  cuDoubleComplex cuConj(cuDoubleComplex x)
{
  return make_cuDoubleComplex(cuCreal(x),  -cuCimag(x));
}
static __inline  cuDoubleComplex cuCadd(cuDoubleComplex x, cuDoubleComplex y)
{
  return make_cuDoubleComplex(cuCreal(x) + cuCreal(y), cuCimag(x) + cuCimag(y));
}
static __inline  cuDoubleComplex cuCsub(cuDoubleComplex x, cuDoubleComplex y)
{
  return make_cuDoubleComplex(cuCreal(x) - cuCreal(y), cuCimag(x) - cuCimag(y));
}
static __inline  cuDoubleComplex cuCmul(cuDoubleComplex x, cuDoubleComplex y)
{
  cuDoubleComplex prod;
  prod = make_cuDoubleComplex(cuCreal(x) * cuCreal(y) - cuCimag(x) * cuCimag(y), cuCreal(x) * cuCimag(y) + cuCimag(x) * cuCreal(y));
  return prod;
}
extern double fabs(double __x)__attribute__((__nothrow__)) __attribute__((__const__));
static __inline  cuDoubleComplex cuCdiv(cuDoubleComplex x, cuDoubleComplex y)
{
  cuDoubleComplex quot;
  double s = fabs(cuCreal(y)) + fabs(cuCimag(y));
  double oos = 1.00000000000000000000000000000000000000000000000000000e+00 / s;
  double ars = cuCreal(x) * oos;
  double ais = cuCimag(x) * oos;
  double brs = cuCreal(y) * oos;
  double bis = cuCimag(y) * oos;
  s = brs * brs + bis * bis;
  oos = 1.00000000000000000000000000000000000000000000000000000e+00 / s;
  quot = make_cuDoubleComplex((ars * brs + ais * bis) * oos, (ais * brs - ars * bis) * oos);
  return quot;
}
extern double sqrt(double __x)__attribute__((__nothrow__));
static __inline  double cuCabs(cuDoubleComplex x)
{
  double v;
  double w;
  double t;
  double a = cuCreal(x);
  double b = cuCimag(x);
  a = fabs(a);
  b = fabs(b);
  if (a > b)
    {
      v = a;
      w = b;
    }
  else
    {
      v = b;
      w = a;
    }
  t = w / v;
  t = 1.00000000000000000000000000000000000000000000000000000e+00 + t * t;
  t = v * sqrt(t);
  if ((v == 0.00000000000000000000000000000000000000000000000000000e+00 || v > 1.79769313486231570814527423731704356798070567525844997e+308) || w > 1.79769313486231570814527423731704356798070567525844997e+308)
    {
      t = v + w;
    }
  return t;
}
typedef cuFloatComplex cuComplex;
static __inline  cuComplex make_cuComplex(float x, float y)
{
  return make_cuFloatComplex(x, y);
}
static __inline  cuDoubleComplex cuComplexFloatToDouble(cuFloatComplex c)
{
  return make_cuDoubleComplex((double)cuCrealf(c), (double)cuCimagf(c));
}
static __inline  cuFloatComplex cuComplexDoubleToFloat(cuDoubleComplex c)
{
  return make_cuFloatComplex((float)cuCreal(c), (float)cuCimag(c));
}
static __inline  cuComplex cuCfmaf(cuComplex x, cuComplex y, cuComplex d)
{
  float real_res;
  float imag_res;
  real_res = cuCrealf(x) * cuCrealf(y) + cuCrealf(d);
  imag_res = cuCrealf(x) * cuCimagf(y) + cuCimagf(d);
  real_res =  -(cuCimagf(x) * cuCimagf(y)) + real_res;
  imag_res = cuCimagf(x) * cuCrealf(y) + imag_res;
  return make_cuComplex(real_res, imag_res);
}
static __inline  cuDoubleComplex cuCfma(cuDoubleComplex x, cuDoubleComplex y, cuDoubleComplex d)
{
  double real_res;
  double imag_res;
  real_res = cuCreal(x) * cuCreal(y) + cuCreal(d);
  imag_res = cuCreal(x) * cuCimag(y) + cuCimag(d);
  real_res =  -(cuCimag(x) * cuCimag(y)) + real_res;
  imag_res = cuCimag(x) * cuCreal(y) + imag_res;
  return make_cuDoubleComplex(real_res, imag_res);
}
extern double strtod(const char *__restrict __nptr, char **__restrict __endptr)__attribute__((__nothrow__)) __attribute__((__nonnull__(1)));
extern __inline __attribute__((__nothrow__)) __attribute__((__pure__)) __attribute__((__nonnull__(1))) __attribute__((__gnu_inline__)) double atof(const char *__nptr)
{
  return strtod(__nptr, (char **)(void *)0);
}
extern long int strtol(const char *__restrict __nptr, char **__restrict __endptr, int __base)__attribute__((__nothrow__)) __attribute__((__nonnull__(1)));
extern __inline __attribute__((__nothrow__)) __attribute__((__pure__)) __attribute__((__nonnull__(1))) __attribute__((__gnu_inline__)) int atoi(const char *__nptr)
{
  return (int)strtol(__nptr, (char **)(void *)0, 10);
}
extern __inline __attribute__((__nothrow__)) __attribute__((__pure__)) __attribute__((__nonnull__(1))) __attribute__((__gnu_inline__)) long int atol(const char *__nptr)
{
  return strtol(__nptr, (char **)(void *)0, 10);
}
extern long long int strtoll(const char *__restrict __nptr, char **__restrict __endptr, int __base)__attribute__((__nothrow__)) __attribute__((__nonnull__(1)));
extern __inline __attribute__((__nothrow__)) __attribute__((__pure__)) __attribute__((__nonnull__(1))) __attribute__((__gnu_inline__)) long long int atoll(const char *__nptr)
{
  return strtoll(__nptr, (char **)(void *)0, 10);
}
extern __inline __attribute__((__nothrow__)) __attribute__((__gnu_inline__)) unsigned int gnu_dev_major(unsigned long long int __dev)
{
  return (__dev >> 8 & 4095) | ((unsigned int)(__dev >> 32) & ~4095);
}
extern __inline __attribute__((__nothrow__)) __attribute__((__gnu_inline__)) unsigned int gnu_dev_minor(unsigned long long int __dev)
{
  return (__dev & 255) | ((unsigned int)(__dev >> 12) & ~255);
}
extern __inline __attribute__((__nothrow__)) __attribute__((__gnu_inline__)) unsigned long long int gnu_dev_makedev(unsigned int __major, unsigned int __minor)
{
  return (((__minor & 255) | (__major & 4095) << 8) | (unsigned long long int)(__minor & ~255) << 12) | (unsigned long long int)(__major & ~4095) << 32;
}
static unsigned int real_N;
static unsigned int real_BSIZE;
static double *a_h;
static double *b_h;
static double *c_h;
extern void *nanos_create_current_kernel(const char *kernel_name, const char *opencl_code, const char *compiler_opts);
enum mcc_enum_anon_8
{
  NANOS_OK = 0,
  NANOS_UNKNOWN_ERR = 1,
  NANOS_UNIMPLEMENTED = 2,
  NANOS_ENOMEM = 3,
  NANOS_INVALID_PARAM = 4
};
typedef enum mcc_enum_anon_8 nanos_err_t;
extern nanos_err_t nanos_opencl_set_bufferarg(void *opencl_kernel, int arg_num, void *pointer);
extern nanos_err_t nanos_opencl_set_arg(void *opencl_kernel, int arg_num, size_t size, void *pointer);
extern nanos_err_t nanos_exec_kernel(void *opencl_kernel, int work_dim, size_t *ndr_offset, size_t *ndr_local_size, size_t *ndr_global_size);
void ocl_ol_init_gpu_1_unpacked(double **const mcc_arg_0, double **const mcc_arg_1, double **const mcc_arg_2, int *const mcc_arg_3)
{
  {
    size_t offset_tmp[1];
    size_t *offset_ptr;
    size_t *local_size_ptr;
    size_t *global_size_ptr;
    size_t offset_arr[1];
    size_t local_size_arr[1];
    size_t global_size_arr[1];
    size_t *final_local_size_ptr;
    void *ompss_kernel_ocl = nanos_create_current_kernel("init_gpu", "stream_gpu.cl", "");
    nanos_opencl_set_bufferarg(ompss_kernel_ocl, 0, (*mcc_arg_0));
    nanos_opencl_set_bufferarg(ompss_kernel_ocl, 1, (*mcc_arg_1));
    nanos_opencl_set_bufferarg(ompss_kernel_ocl, 2, (*mcc_arg_2));
    nanos_opencl_set_arg(ompss_kernel_ocl, 3, sizeof(int), (int *) mcc_arg_3);
    int num_dim = 1;
    short int local_size_zero = 0;
    int i = 0;
    const int mcc_vla_0 = num_dim;
    size_t local_size_tmp[mcc_vla_0];
    const int mcc_vla_1 = num_dim;
    size_t global_size_tmp[mcc_vla_1];
    offset_tmp[0] = 0;
    local_size_tmp[0] = 128;
    global_size_tmp[0] = (*mcc_arg_3);
    offset_ptr = offset_tmp;
    local_size_ptr = local_size_tmp;
    global_size_ptr = global_size_tmp;
    for (i = 0; i < num_dim; ++i)
      {
        if (local_size_ptr[i] == 0)
          local_size_zero = 1;
      }
    ;
    if (local_size_zero)
      {
        for (i = 0; i < num_dim; ++i)
          {
            local_size_ptr[i] = 1;
          }
      }
    for (i = 0; i < num_dim; ++i)
      {
        offset_arr[i] = offset_ptr[i];
        local_size_arr[i] = global_size_ptr[i] < local_size_ptr[i] ? global_size_ptr[i] : local_size_ptr[i];
        global_size_arr[i] = global_size_ptr[i] < local_size_ptr[i] ? global_size_ptr[i] : global_size_ptr[i] + (global_size_ptr[i] % local_size_ptr[i] == 0 ? 0 : local_size_ptr[i] - global_size_ptr[i] % local_size_ptr[i]);
      }
    if (local_size_zero)
      {
        final_local_size_ptr = 0;
      }
    else
      {
        final_local_size_ptr = local_size_arr;
      }
    nanos_exec_kernel(ompss_kernel_ocl, 1, offset_arr, final_local_size_ptr, global_size_arr);
  }
}
struct  nanos_args_0_t
{
  double *a;
  double *b;
  double *c;
  int n;
};
typedef unsigned int nanos_event_key_t;
extern nanos_err_t nanos_instrument_get_key(const char *key, nanos_event_key_t *event_key);
extern void nanos_handle_error(nanos_err_t err);
typedef unsigned long long int nanos_event_value_t;
extern nanos_err_t nanos_instrument_register_value_with_val(nanos_event_value_t val, const char *key, const char *value, const char *description, _Bool abort_when_registered);
enum mcc_enum_anon_4
{
  NANOS_STATE_START = 0,
  NANOS_STATE_END = 1,
  NANOS_SUBSTATE_START = 2,
  NANOS_SUBSTATE_END = 3,
  NANOS_BURST_START = 4,
  NANOS_BURST_END = 5,
  NANOS_PTP_START = 6,
  NANOS_PTP_END = 7,
  NANOS_POINT = 8,
  EVENT_TYPES = 9
};
typedef enum mcc_enum_anon_4 nanos_event_type_t;
enum mcc_enum_anon_6
{
  NANOS_WD_DOMAIN = 0,
  NANOS_WD_DEPENDENCY = 1,
  NANOS_WAIT = 2,
  NANOS_WD_REMOTE = 3,
  NANOS_XFER_PUT = 4,
  NANOS_XFER_GET = 5
};
typedef enum mcc_enum_anon_6 nanos_event_domain_t;
typedef long long int nanos_event_id_t;
struct  mcc_struct_anon_20
{
  nanos_event_type_t type;
  nanos_event_key_t key;
  nanos_event_value_t value;
  nanos_event_domain_t domain;
  nanos_event_id_t id;
};
typedef struct mcc_struct_anon_20 nanos_event_t;
extern nanos_err_t nanos_instrument_events(unsigned int num_events, nanos_event_t events[]);
extern nanos_err_t nanos_instrument_close_user_fun_event();
static void ocl_ol_init_gpu_1(struct nanos_args_0_t *const args)
{
  nanos_err_t err;
  nanos_event_t event;
  {
    static int nanos_funct_id_init = 0;
    static nanos_event_key_t nanos_instr_uf_location_key = 0;
    if (nanos_funct_id_init == 0)
      {
        err = nanos_instrument_get_key("user-funct-location", &nanos_instr_uf_location_key);
        if (err != NANOS_OK)
          nanos_handle_error(err);
        err = nanos_instrument_register_value_with_val((nanos_event_value_t)(void (*)(void *))(void (*)(struct nanos_args_0_t *const ))ocl_ol_init_gpu_1, "user-funct-location", "ocl_ol_init_gpu_1", "void init_gpu(double *, double *, double *, int)@main-strongscaling.c@193", 0);
        if (err != NANOS_OK)
          nanos_handle_error(err);
        nanos_funct_id_init = 1;
      }
    event.type = NANOS_BURST_START;
    event.key = nanos_instr_uf_location_key;
    event.value = (nanos_event_value_t)(void (*)(void *))(void (*)(struct nanos_args_0_t *const ))ocl_ol_init_gpu_1;
    err = nanos_instrument_events(1, &event);
    ocl_ol_init_gpu_1_unpacked(&((*args).a), &((*args).b), &((*args).c), &((*args).n));
    err = nanos_instrument_close_user_fun_event();
  }
}
typedef void *nanos_wd_t;
typedef unsigned int nanos_copy_id_t;
extern nanos_err_t nanos_get_addr(nanos_copy_id_t copy_id, void **addr, void *cwd);
static void nanos_xlate_fun_0(struct nanos_args_0_t *const arg, void *wd)
{
  {
    void *device_base_address;
    nanos_err_t err;
    device_base_address = 0;
    err = nanos_get_addr(0, &device_base_address, wd);
    if (err != NANOS_OK)
      nanos_handle_error(err);
    (*arg).a = (double *)device_base_address;
  }
  {
    void *device_base_address;
    nanos_err_t err;
    device_base_address = 0;
    err = nanos_get_addr(1, &device_base_address, wd);
    if (err != NANOS_OK)
      nanos_handle_error(err);
    (*arg).b = (double *)device_base_address;
  }
  {
    void *device_base_address;
    nanos_err_t err;
    device_base_address = 0;
    err = nanos_get_addr(2, &device_base_address, wd);
    if (err != NANOS_OK)
      nanos_handle_error(err);
    (*arg).c = (double *)device_base_address;
  }
}
void ocl_ol_copy_gpu_3_unpacked(double **const mcc_arg_4, double **const mcc_arg_5, int *const mcc_arg_6)
{
  {
    size_t offset_tmp[1];
    size_t *offset_ptr;
    size_t *local_size_ptr;
    size_t *global_size_ptr;
    size_t offset_arr[1];
    size_t local_size_arr[1];
    size_t global_size_arr[1];
    size_t *final_local_size_ptr;
    void *ompss_kernel_ocl = nanos_create_current_kernel("copy_gpu", "stream_gpu.cl", "");
    nanos_opencl_set_bufferarg(ompss_kernel_ocl, 0, (*mcc_arg_4));
    nanos_opencl_set_bufferarg(ompss_kernel_ocl, 1, (*mcc_arg_5));
    nanos_opencl_set_arg(ompss_kernel_ocl, 2, sizeof(int), (int *) mcc_arg_6);
    int num_dim = 1;
    short int local_size_zero = 0;
    int i = 0;
    const int mcc_vla_2 = num_dim;
    size_t local_size_tmp[mcc_vla_2];
    const int mcc_vla_3 = num_dim;
    size_t global_size_tmp[mcc_vla_3];
    offset_tmp[0] = 0;
    local_size_tmp[0] = 128;
    global_size_tmp[0] = (*mcc_arg_6);
    offset_ptr = offset_tmp;
    local_size_ptr = local_size_tmp;
    global_size_ptr = global_size_tmp;
    for (i = 0; i < num_dim; ++i)
      {
        if (local_size_ptr[i] == 0)
          local_size_zero = 1;
      }
    ;
    if (local_size_zero)
      {
        for (i = 0; i < num_dim; ++i)
          {
            local_size_ptr[i] = 1;
          }
      }
    for (i = 0; i < num_dim; ++i)
      {
        offset_arr[i] = offset_ptr[i];
        local_size_arr[i] = global_size_ptr[i] < local_size_ptr[i] ? global_size_ptr[i] : local_size_ptr[i];
        global_size_arr[i] = global_size_ptr[i] < local_size_ptr[i] ? global_size_ptr[i] : global_size_ptr[i] + (global_size_ptr[i] % local_size_ptr[i] == 0 ? 0 : local_size_ptr[i] - global_size_ptr[i] % local_size_ptr[i]);
      }
    if (local_size_zero)
      {
        final_local_size_ptr = 0;
      }
    else
      {
        final_local_size_ptr = local_size_arr;
      }
    nanos_exec_kernel(ompss_kernel_ocl, 1, offset_arr, final_local_size_ptr, global_size_arr);
  }
}
struct  nanos_args_1_t
{
  double *a;
  double *c;
  int n;
};
static void ocl_ol_copy_gpu_3(struct nanos_args_1_t *const args)
{
  nanos_err_t err;
  nanos_event_t event;
  {
    static int nanos_funct_id_init = 0;
    static nanos_event_key_t nanos_instr_uf_location_key = 0;
    if (nanos_funct_id_init == 0)
      {
        err = nanos_instrument_get_key("user-funct-location", &nanos_instr_uf_location_key);
        if (err != NANOS_OK)
          nanos_handle_error(err);
        err = nanos_instrument_register_value_with_val((nanos_event_value_t)(void (*)(void *))(void (*)(struct nanos_args_1_t *const ))ocl_ol_copy_gpu_3, "user-funct-location", "ocl_ol_copy_gpu_3", "void copy_gpu(double *, double *, int)@main-strongscaling.c@208", 0);
        if (err != NANOS_OK)
          nanos_handle_error(err);
        nanos_funct_id_init = 1;
      }
    event.type = NANOS_BURST_START;
    event.key = nanos_instr_uf_location_key;
    event.value = (nanos_event_value_t)(void (*)(void *))(void (*)(struct nanos_args_1_t *const ))ocl_ol_copy_gpu_3;
    err = nanos_instrument_events(1, &event);
    ocl_ol_copy_gpu_3_unpacked(&((*args).a), &((*args).c), &((*args).n));
    err = nanos_instrument_close_user_fun_event();
  }
}
static void nanos_xlate_fun_1(struct nanos_args_1_t *const arg, void *wd)
{
  {
    void *device_base_address;
    nanos_err_t err;
    device_base_address = 0;
    err = nanos_get_addr(0, &device_base_address, wd);
    if (err != NANOS_OK)
      nanos_handle_error(err);
    (*arg).a = (double *)device_base_address;
  }
  {
    void *device_base_address;
    nanos_err_t err;
    device_base_address = 0;
    err = nanos_get_addr(1, &device_base_address, wd);
    if (err != NANOS_OK)
      nanos_handle_error(err);
    (*arg).c = (double *)device_base_address;
  }
}
void ocl_ol_scale_gpu_5_unpacked(double **const mcc_arg_7, double **const mcc_arg_8, double *const mcc_arg_9, int *const mcc_arg_10)
{
  {
    size_t offset_tmp[1];
    size_t *offset_ptr;
    size_t *local_size_ptr;
    size_t *global_size_ptr;
    size_t offset_arr[1];
    size_t local_size_arr[1];
    size_t global_size_arr[1];
    size_t *final_local_size_ptr;
    void *ompss_kernel_ocl = nanos_create_current_kernel("scale_gpu", "stream_gpu.cl", "");
    nanos_opencl_set_bufferarg(ompss_kernel_ocl, 0, (*mcc_arg_7));
    nanos_opencl_set_bufferarg(ompss_kernel_ocl, 1, (*mcc_arg_8));
    nanos_opencl_set_arg(ompss_kernel_ocl, 2, sizeof(double), (double *) mcc_arg_9);
    nanos_opencl_set_arg(ompss_kernel_ocl, 3, sizeof(int), (int *) mcc_arg_10);
    int num_dim = 1;
    short int local_size_zero = 0;
    int i = 0;
    const int mcc_vla_4 = num_dim;
    size_t local_size_tmp[mcc_vla_4];
    const int mcc_vla_5 = num_dim;
    size_t global_size_tmp[mcc_vla_5];
    offset_tmp[0] = 0;
    local_size_tmp[0] = 128;
    global_size_tmp[0] = (*mcc_arg_10);
    offset_ptr = offset_tmp;
    local_size_ptr = local_size_tmp;
    global_size_ptr = global_size_tmp;
    for (i = 0; i < num_dim; ++i)
      {
        if (local_size_ptr[i] == 0)
          local_size_zero = 1;
      }
    ;
    if (local_size_zero)
      {
        for (i = 0; i < num_dim; ++i)
          {
            local_size_ptr[i] = 1;
          }
      }
    for (i = 0; i < num_dim; ++i)
      {
        offset_arr[i] = offset_ptr[i];
        local_size_arr[i] = global_size_ptr[i] < local_size_ptr[i] ? global_size_ptr[i] : local_size_ptr[i];
        global_size_arr[i] = global_size_ptr[i] < local_size_ptr[i] ? global_size_ptr[i] : global_size_ptr[i] + (global_size_ptr[i] % local_size_ptr[i] == 0 ? 0 : local_size_ptr[i] - global_size_ptr[i] % local_size_ptr[i]);
      }
    if (local_size_zero)
      {
        final_local_size_ptr = 0;
      }
    else
      {
        final_local_size_ptr = local_size_arr;
      }
    nanos_exec_kernel(ompss_kernel_ocl, 1, offset_arr, final_local_size_ptr, global_size_arr);
  }
}
struct  nanos_args_2_t
{
  double *b;
  double *c;
  double scalar;
  int n;
};
static void ocl_ol_scale_gpu_5(struct nanos_args_2_t *const args)
{
  nanos_err_t err;
  nanos_event_t event;
  {
    static int nanos_funct_id_init = 0;
    static nanos_event_key_t nanos_instr_uf_location_key = 0;
    if (nanos_funct_id_init == 0)
      {
        err = nanos_instrument_get_key("user-funct-location", &nanos_instr_uf_location_key);
        if (err != NANOS_OK)
          nanos_handle_error(err);
        err = nanos_instrument_register_value_with_val((nanos_event_value_t)(void (*)(void *))(void (*)(struct nanos_args_2_t *const ))ocl_ol_scale_gpu_5, "user-funct-location", "ocl_ol_scale_gpu_5", "void scale_gpu(double *, double *, double, int)@main-strongscaling.c@218", 0);
        if (err != NANOS_OK)
          nanos_handle_error(err);
        nanos_funct_id_init = 1;
      }
    event.type = NANOS_BURST_START;
    event.key = nanos_instr_uf_location_key;
    event.value = (nanos_event_value_t)(void (*)(void *))(void (*)(struct nanos_args_2_t *const ))ocl_ol_scale_gpu_5;
    err = nanos_instrument_events(1, &event);
    ocl_ol_scale_gpu_5_unpacked(&((*args).b), &((*args).c), &((*args).scalar), &((*args).n));
    err = nanos_instrument_close_user_fun_event();
  }
}
static void nanos_xlate_fun_2(struct nanos_args_2_t *const arg, void *wd)
{
  {
    void *device_base_address;
    nanos_err_t err;
    device_base_address = 0;
    err = nanos_get_addr(0, &device_base_address, wd);
    if (err != NANOS_OK)
      nanos_handle_error(err);
    (*arg).b = (double *)device_base_address;
  }
  {
    void *device_base_address;
    nanos_err_t err;
    device_base_address = 0;
    err = nanos_get_addr(1, &device_base_address, wd);
    if (err != NANOS_OK)
      nanos_handle_error(err);
    (*arg).c = (double *)device_base_address;
  }
}
void ocl_ol_add_gpu_7_unpacked(double **const mcc_arg_11, double **const mcc_arg_12, double **const mcc_arg_13, int *const mcc_arg_14)
{
  {
    size_t offset_tmp[1];
    size_t *offset_ptr;
    size_t *local_size_ptr;
    size_t *global_size_ptr;
    size_t offset_arr[1];
    size_t local_size_arr[1];
    size_t global_size_arr[1];
    size_t *final_local_size_ptr;
    void *ompss_kernel_ocl = nanos_create_current_kernel("add_gpu", "stream_gpu.cl", "");
    nanos_opencl_set_bufferarg(ompss_kernel_ocl, 0, (*mcc_arg_11));
    nanos_opencl_set_bufferarg(ompss_kernel_ocl, 1, (*mcc_arg_12));
    nanos_opencl_set_bufferarg(ompss_kernel_ocl, 2, (*mcc_arg_13));
    nanos_opencl_set_arg(ompss_kernel_ocl, 3, sizeof(int), (int *) mcc_arg_14);
    int num_dim = 1;
    short int local_size_zero = 0;
    int i = 0;
    const int mcc_vla_6 = num_dim;
    size_t local_size_tmp[mcc_vla_6];
    const int mcc_vla_7 = num_dim;
    size_t global_size_tmp[mcc_vla_7];
    offset_tmp[0] = 0;
    local_size_tmp[0] = 128;
    global_size_tmp[0] = (*mcc_arg_14);
    offset_ptr = offset_tmp;
    local_size_ptr = local_size_tmp;
    global_size_ptr = global_size_tmp;
    for (i = 0; i < num_dim; ++i)
      {
        if (local_size_ptr[i] == 0)
          local_size_zero = 1;
      }
    ;
    if (local_size_zero)
      {
        for (i = 0; i < num_dim; ++i)
          {
            local_size_ptr[i] = 1;
          }
      }
    for (i = 0; i < num_dim; ++i)
      {
        offset_arr[i] = offset_ptr[i];
        local_size_arr[i] = global_size_ptr[i] < local_size_ptr[i] ? global_size_ptr[i] : local_size_ptr[i];
        global_size_arr[i] = global_size_ptr[i] < local_size_ptr[i] ? global_size_ptr[i] : global_size_ptr[i] + (global_size_ptr[i] % local_size_ptr[i] == 0 ? 0 : local_size_ptr[i] - global_size_ptr[i] % local_size_ptr[i]);
      }
    if (local_size_zero)
      {
        final_local_size_ptr = 0;
      }
    else
      {
        final_local_size_ptr = local_size_arr;
      }
    nanos_exec_kernel(ompss_kernel_ocl, 1, offset_arr, final_local_size_ptr, global_size_arr);
  }
}
struct  nanos_args_3_t
{
  double *a;
  double *b;
  double *c;
  int n;
};
static void ocl_ol_add_gpu_7(struct nanos_args_3_t *const args)
{
  nanos_err_t err;
  nanos_event_t event;
  {
    static int nanos_funct_id_init = 0;
    static nanos_event_key_t nanos_instr_uf_location_key = 0;
    if (nanos_funct_id_init == 0)
      {
        err = nanos_instrument_get_key("user-funct-location", &nanos_instr_uf_location_key);
        if (err != NANOS_OK)
          nanos_handle_error(err);
        err = nanos_instrument_register_value_with_val((nanos_event_value_t)(void (*)(void *))(void (*)(struct nanos_args_3_t *const ))ocl_ol_add_gpu_7, "user-funct-location", "ocl_ol_add_gpu_7", "void add_gpu(double *, double *, double *, int)@main-strongscaling.c@227", 0);
        if (err != NANOS_OK)
          nanos_handle_error(err);
        nanos_funct_id_init = 1;
      }
    event.type = NANOS_BURST_START;
    event.key = nanos_instr_uf_location_key;
    event.value = (nanos_event_value_t)(void (*)(void *))(void (*)(struct nanos_args_3_t *const ))ocl_ol_add_gpu_7;
    err = nanos_instrument_events(1, &event);
    ocl_ol_add_gpu_7_unpacked(&((*args).a), &((*args).b), &((*args).c), &((*args).n));
    err = nanos_instrument_close_user_fun_event();
  }
}
static void nanos_xlate_fun_3(struct nanos_args_3_t *const arg, void *wd)
{
  {
    void *device_base_address;
    nanos_err_t err;
    device_base_address = 0;
    err = nanos_get_addr(0, &device_base_address, wd);
    if (err != NANOS_OK)
      nanos_handle_error(err);
    (*arg).a = (double *)device_base_address;
  }
  {
    void *device_base_address;
    nanos_err_t err;
    device_base_address = 0;
    err = nanos_get_addr(1, &device_base_address, wd);
    if (err != NANOS_OK)
      nanos_handle_error(err);
    (*arg).b = (double *)device_base_address;
  }
  {
    void *device_base_address;
    nanos_err_t err;
    device_base_address = 0;
    err = nanos_get_addr(2, &device_base_address, wd);
    if (err != NANOS_OK)
      nanos_handle_error(err);
    (*arg).c = (double *)device_base_address;
  }
}
void ocl_ol_triad_gpu_9_unpacked(double **const mcc_arg_15, double **const mcc_arg_16, double **const mcc_arg_17, double *const mcc_arg_18, int *const mcc_arg_19)
{
  {
    size_t offset_tmp[1];
    size_t *offset_ptr;
    size_t *local_size_ptr;
    size_t *global_size_ptr;
    size_t offset_arr[1];
    size_t local_size_arr[1];
    size_t global_size_arr[1];
    size_t *final_local_size_ptr;
    void *ompss_kernel_ocl = nanos_create_current_kernel("triad_gpu", "stream_gpu.cl", "");
    nanos_opencl_set_bufferarg(ompss_kernel_ocl, 0, (*mcc_arg_15));
    nanos_opencl_set_bufferarg(ompss_kernel_ocl, 1, (*mcc_arg_16));
    nanos_opencl_set_bufferarg(ompss_kernel_ocl, 2, (*mcc_arg_17));
    nanos_opencl_set_arg(ompss_kernel_ocl, 3, sizeof(double), (double *) mcc_arg_18);
    nanos_opencl_set_arg(ompss_kernel_ocl, 4, sizeof(int), (int *) mcc_arg_19);
    int num_dim = 1;
    short int local_size_zero = 0;
    int i = 0;
    const int mcc_vla_8 = num_dim;
    size_t local_size_tmp[mcc_vla_8];
    const int mcc_vla_9 = num_dim;
    size_t global_size_tmp[mcc_vla_9];
    offset_tmp[0] = 0;
    local_size_tmp[0] = 128;
    global_size_tmp[0] = (*mcc_arg_19);
    offset_ptr = offset_tmp;
    local_size_ptr = local_size_tmp;
    global_size_ptr = global_size_tmp;
    for (i = 0; i < num_dim; ++i)
      {
        if (local_size_ptr[i] == 0)
          local_size_zero = 1;
      }
    ;
    if (local_size_zero)
      {
        for (i = 0; i < num_dim; ++i)
          {
            local_size_ptr[i] = 1;
          }
      }
    for (i = 0; i < num_dim; ++i)
      {
        offset_arr[i] = offset_ptr[i];
        local_size_arr[i] = global_size_ptr[i] < local_size_ptr[i] ? global_size_ptr[i] : local_size_ptr[i];
        global_size_arr[i] = global_size_ptr[i] < local_size_ptr[i] ? global_size_ptr[i] : global_size_ptr[i] + (global_size_ptr[i] % local_size_ptr[i] == 0 ? 0 : local_size_ptr[i] - global_size_ptr[i] % local_size_ptr[i]);
      }
    if (local_size_zero)
      {
        final_local_size_ptr = 0;
      }
    else
      {
        final_local_size_ptr = local_size_arr;
      }
    nanos_exec_kernel(ompss_kernel_ocl, 1, offset_arr, final_local_size_ptr, global_size_arr);
  }
}
struct  nanos_args_4_t
{
  double *a;
  double *b;
  double *c;
  double scalar;
  int n;
};
static void ocl_ol_triad_gpu_9(struct nanos_args_4_t *const args)
{
  nanos_err_t err;
  nanos_event_t event;
  {
    static int nanos_funct_id_init = 0;
    static nanos_event_key_t nanos_instr_uf_location_key = 0;
    if (nanos_funct_id_init == 0)
      {
        err = nanos_instrument_get_key("user-funct-location", &nanos_instr_uf_location_key);
        if (err != NANOS_OK)
          nanos_handle_error(err);
        err = nanos_instrument_register_value_with_val((nanos_event_value_t)(void (*)(void *))(void (*)(struct nanos_args_4_t *const ))ocl_ol_triad_gpu_9, "user-funct-location", "ocl_ol_triad_gpu_9", "void triad_gpu(double *, double *, double *, double, int)@main-strongscaling.c@237", 0);
        if (err != NANOS_OK)
          nanos_handle_error(err);
        nanos_funct_id_init = 1;
      }
    event.type = NANOS_BURST_START;
    event.key = nanos_instr_uf_location_key;
    event.value = (nanos_event_value_t)(void (*)(void *))(void (*)(struct nanos_args_4_t *const ))ocl_ol_triad_gpu_9;
    err = nanos_instrument_events(1, &event);
    ocl_ol_triad_gpu_9_unpacked(&((*args).a), &((*args).b), &((*args).c), &((*args).scalar), &((*args).n));
    err = nanos_instrument_close_user_fun_event();
  }
}
static void nanos_xlate_fun_4(struct nanos_args_4_t *const arg, void *wd)
{
  {
    void *device_base_address;
    nanos_err_t err;
    device_base_address = 0;
    err = nanos_get_addr(0, &device_base_address, wd);
    if (err != NANOS_OK)
      nanos_handle_error(err);
    (*arg).a = (double *)device_base_address;
  }
  {
    void *device_base_address;
    nanos_err_t err;
    device_base_address = 0;
    err = nanos_get_addr(1, &device_base_address, wd);
    if (err != NANOS_OK)
      nanos_handle_error(err);
    (*arg).b = (double *)device_base_address;
  }
  {
    void *device_base_address;
    nanos_err_t err;
    device_base_address = 0;
    err = nanos_get_addr(2, &device_base_address, wd);
    if (err != NANOS_OK)
      nanos_handle_error(err);
    (*arg).c = (double *)device_base_address;
  }
}
int MPI_Init(int *argc, char ***argv)__attribute__((visibility("default")));
struct ompi_communicator_t;
typedef struct ompi_communicator_t *MPI_Comm;
int MPI_Comm_rank(struct ompi_communicator_t *comm, int *rank)__attribute__((visibility("default")));
struct ompi_predefined_communicator_t;
extern __attribute__((visibility("default"))) struct ompi_predefined_communicator_t ompi_mpi_comm_world;
int MPI_Comm_size(struct ompi_communicator_t *comm, int *size)__attribute__((visibility("default")));
extern void *malloc(size_t __size)__attribute__((__nothrow__)) __attribute__((__malloc__));
extern int printf(const char *__restrict __format, ...);
int MPI_Barrier(struct ompi_communicator_t *comm)__attribute__((visibility("default")));
struct timeval;
struct timezone;
typedef struct timezone *__restrict __timezone_ptr_t;
extern int gettimeofday(struct timeval *__restrict __tv, struct timezone *__tz)__attribute__((__nothrow__)) __attribute__((__nonnull__(1)));
typedef long int __time_t;
typedef long int __suseconds_t;
struct  timeval
{
  __time_t tv_sec;
  __suseconds_t tv_usec;
};
void init(double *a, double *b, double *c, int size);
void copy(double *a, double *c, int size);
void scale(double *b, double *c, double scalar, int size);
void add(double *a, double *b, double *c, int size);
void triad(double *a, double *b, double *c, double scalar, int size);
struct  mcc_struct_anon_24
{
  void (*outline)(void *);
};
typedef struct mcc_struct_anon_24 nanos_opencl_args_t;
struct  mcc_struct_anon_16
{
  _Bool mandatory_creation:1;
  _Bool tied:1;
  _Bool clear_chunk:1;
  _Bool reserved0:1;
  _Bool reserved1:1;
  _Bool reserved2:1;
  _Bool reserved3:1;
  _Bool reserved4:1;
};
typedef struct mcc_struct_anon_16 nanos_wd_props_t;
struct  nanos_const_wd_definition_tag
{
  nanos_wd_props_t props;
  size_t data_alignment;
  size_t num_copies;
  size_t num_devices;
  size_t num_dimensions;
  const char *description;
};
typedef struct nanos_const_wd_definition_tag nanos_const_wd_definition_t;
struct  mcc_struct_anon_18
{
  void *(*factory)(void *);
  void *arg;
};
typedef struct mcc_struct_anon_18 nanos_device_t;
struct  nanos_const_wd_definition_1
{
  nanos_const_wd_definition_t base;
  nanos_device_t devices[1];
};
extern void *nanos_opencl_factory(void *args);
typedef void *nanos_thread_t;
struct  mcc_struct_anon_17
{
  void *tie_to;
  unsigned int priority;
};
typedef struct mcc_struct_anon_17 nanos_wd_dyn_props_t;
struct mcc_struct_anon_9;
typedef struct mcc_struct_anon_9 nanos_copy_data_internal_t;
typedef nanos_copy_data_internal_t nanos_copy_data_t;
struct mcc_struct_anon_5;
typedef struct mcc_struct_anon_5 nanos_region_dimension_internal_t;
typedef void *nanos_wg_t;
extern nanos_err_t nanos_create_wd_compact(void **wd, nanos_const_wd_definition_t *const_data, nanos_wd_dyn_props_t *dyn_props, size_t data_size, void **data, void *wg, nanos_copy_data_t **copies, nanos_region_dimension_internal_t **dimensions);
extern void *nanos_current_wd(void);
struct  mcc_struct_anon_5
{
  size_t size;
  size_t lower_bound;
  size_t accessed_length;
};
typedef nanos_region_dimension_internal_t nanos_region_dimension_t;
struct  mcc_struct_anon_6
{
  _Bool input:1;
  _Bool output:1;
  _Bool can_rename:1;
  _Bool concurrent:1;
  _Bool commutative:1;
};
typedef struct mcc_struct_anon_6 nanos_access_type_internal_t;
typedef long int ptrdiff_t;
struct  mcc_struct_anon_7
{
  void *address;
  nanos_access_type_internal_t flags;
  short int dimension_count;
  const nanos_region_dimension_internal_t *dimensions;
  ptrdiff_t offset;
};
typedef struct mcc_struct_anon_7 nanos_data_access_internal_t;
typedef nanos_data_access_internal_t nanos_data_access_t;
enum mcc_enum_anon_3
{
  NANOS_PRIVATE = 0,
  NANOS_SHARED = 1
};
typedef enum mcc_enum_anon_3 nanos_sharing_t;
struct  mcc_struct_anon_10
{
  _Bool input:1;
  _Bool output:1;
};
struct  mcc_struct_anon_9
{
  void *address;
  nanos_sharing_t sharing;
  struct mcc_struct_anon_10 flags;
  short int dimension_count;
  const nanos_region_dimension_internal_t *dimensions;
  ptrdiff_t offset;
};
typedef void (*nanos_translate_args_t)(void *, void *);
extern nanos_err_t nanos_set_translate_function(void *wd, void (*translate_args)(void *, void *));
typedef void *nanos_team_t;
extern nanos_err_t nanos_submit(void *wd, size_t num_data_accesses, nanos_data_access_t *data_accesses, void *team);
extern nanos_err_t nanos_create_wd_and_run_compact(nanos_const_wd_definition_t *const_data, nanos_wd_dyn_props_t *dyn_props, size_t data_size, void *data, size_t num_data_accesses, nanos_data_access_t *data_accesses, nanos_copy_data_t *copies, nanos_region_dimension_internal_t *dimensions, void (*translate_args)(void *, void *));
extern nanos_err_t nanos_wg_wait_completion(void *wg, _Bool avoid_flush);
extern double getElapsedTime(struct timeval start, struct timeval finish);
extern void checkSTREAMresults();
extern void free(void *__ptr)__attribute__((__nothrow__));
int MPI_Finalize(void)__attribute__((visibility("default")));
int main(int argc, char *argv[])
{
  int myrank;
  int rank;
  int BytesPerWord;
  int useCPU;
  struct timeval start;
  register int j;
  double scalar;
  register int k;
  struct timeval finish;
  double total_time;
  MPI_Init(&argc, &argv);
  MPI_Comm_rank((struct ompi_communicator_t *)(void *)&ompi_mpi_comm_world, &myrank);
  MPI_Comm_size((struct ompi_communicator_t *)(void *)&ompi_mpi_comm_world, &rank);
  real_N = 8 * 1024 * 1024 / rank;
  real_BSIZE = real_N / 16;
  a_h = (double *)malloc(real_N * sizeof(double));
  b_h = (double *)malloc(real_N * sizeof(double));
  c_h = (double *)malloc(real_N * sizeof(double));
  if (myrank == 0)
    {
      printf("-------------------------------------------------------------\n");
      printf("STREAM version $Revision: 5.8 $\n");
      printf("-------------------------------------------------------------\n");
      BytesPerWord = sizeof(double);
      printf("This system uses %d bytes per DOUBLE PRECISION word.\n", BytesPerWord);
      printf("-------------------------------------------------------------\n");
      printf("Array size = %d, divided between %d tasks\n", 8 * 1024 * 1024, rank);
      printf("Total memory required = %.1f MB. %.1f MB per task\n", 3.00000000000000000000000000000000000000000000000000000e+00 * BytesPerWord * ((double)8 * 1024 * 1024 / 1.04857600000000000000000000000000000000000000000000000e+06), 3.00000000000000000000000000000000000000000000000000000e+00 * BytesPerWord * ((double)real_N / 1.04857600000000000000000000000000000000000000000000000e+06));
      printf("-------------------------------------------------------------\n");
    }
  MPI_Barrier((struct ompi_communicator_t *)(void *)&ompi_mpi_comm_world);
  useCPU = 0;
  if (useCPU)
    {
      gettimeofday(&start, (void *)0);
      for (j = 0; j < real_N; j += real_BSIZE)
        init(&a_h[j], &b_h[j], &c_h[j], real_BSIZE);
      scalar = 3.00000000000000000000000000000000000000000000000000000e+00;
      for (k = 0; k < 10000; k++)
        {
          for (j = 0; j < real_N; j += real_BSIZE)
            copy(&a_h[j], &c_h[j], real_BSIZE);
          for (j = 0; j < real_N; j += real_BSIZE)
            scale(&b_h[j], &c_h[j], scalar, real_BSIZE);
          for (j = 0; j < real_N; j += real_BSIZE)
            add(&a_h[j], &b_h[j], &c_h[j], real_BSIZE);
          for (j = 0; j < real_N; j += real_BSIZE)
            triad(&a_h[j], &b_h[j], &c_h[j], scalar, real_BSIZE);
        }
      gettimeofday(&finish, (void *)0);
    }
  else
    {
      gettimeofday(&start, (void *)0);
      for (j = 0; j < real_N; j += real_BSIZE)
        {
          double *mcc_arg_0 = a_h + j;
          double *mcc_arg_1 = b_h + j;
          double *mcc_arg_2 = c_h + j;
          int mcc_arg_3 = real_BSIZE;
          {
            static nanos_opencl_args_t ocl_ol_init_gpu_1_args;
            nanos_wd_dyn_props_t nanos_wd_dyn_props;
            struct nanos_args_0_t *ol_args;
            nanos_err_t err;
            struct nanos_args_0_t imm_args;
            /* OpenCL device descriptor */
            ocl_ol_init_gpu_1_args.outline = (void (*)(void *))ocl_ol_init_gpu_1;
            static struct nanos_const_wd_definition_1 nanos_wd_const_data = { { { 1, 1, 0, 0, 0, 0, 0, 0 }, __alignof__(struct nanos_args_0_t), 3, 1, 3 }, { { &nanos_opencl_factory, &ocl_ol_init_gpu_1_args } } };
            nanos_wd_dyn_props.tie_to = 0;
            nanos_wd_dyn_props.priority = 0;
            ol_args = (struct nanos_args_0_t *)0;
            void *nanos_wd_ = (void *)0;
            nanos_copy_data_t *ol_copy_data = (nanos_copy_data_t *)0;
            nanos_region_dimension_internal_t *ol_copy_dimensions = (nanos_region_dimension_internal_t *)0;
            err = nanos_create_wd_compact(&nanos_wd_, &nanos_wd_const_data.base, &nanos_wd_dyn_props, sizeof(struct nanos_args_0_t), (void **)&ol_args, nanos_current_wd(), &ol_copy_data, &ol_copy_dimensions);
            if (err != NANOS_OK)
              nanos_handle_error(err);
            if (nanos_wd_ != (void *)0)
              {
                (*ol_args).a = mcc_arg_0;
                (*ol_args).b = mcc_arg_1;
                (*ol_args).c = mcc_arg_2;
                (*ol_args).n = mcc_arg_3;
                nanos_region_dimension_t dimensions_0[1] = { { sizeof(double) * ((mcc_arg_3 + 0 - 1 - 0) + 1), sizeof(double) * (0 - 0), sizeof(double) * ((mcc_arg_3 + 0 - 1 - 0) + 1) } };
                nanos_region_dimension_t dimensions_1[1] = { { sizeof(double) * ((mcc_arg_3 + 0 - 1 - 0) + 1), sizeof(double) * (0 - 0), sizeof(double) * ((mcc_arg_3 + 0 - 1 - 0) + 1) } };
                nanos_region_dimension_t dimensions_2[1] = { { sizeof(double) * ((mcc_arg_3 + 0 - 1 - 0) + 1), sizeof(double) * (0 - 0), sizeof(double) * ((mcc_arg_3 + 0 - 1 - 0) + 1) } };
                nanos_data_access_t dependences[3] = { { mcc_arg_0, { 0, 1, 0, 0, 0 }, 1, dimensions_0, 8 * 0 }, { mcc_arg_1, { 0, 1, 0, 0, 0 }, 1, dimensions_1, 8 * 0 }, { mcc_arg_2, { 0, 1, 0, 0, 0 }, 1, dimensions_2, 8 * 0 } };
                ;
                ol_copy_dimensions[0].size = ((mcc_arg_3 + 0 - 1 - 0) + 1) * sizeof(double);
                ol_copy_dimensions[0].lower_bound = 0 * sizeof(double);
                ol_copy_dimensions[0].accessed_length = (mcc_arg_3 + 0 - 1 - 0 + 1) * sizeof(double);
                ol_copy_data[0].sharing = NANOS_SHARED;
                ol_copy_data[0].address = (void *)mcc_arg_0;
                ol_copy_data[0].flags.input = 0;
                ol_copy_data[0].flags.output = 1;
                ol_copy_data[0].dimension_count = 1;
                ol_copy_data[0].dimensions = &ol_copy_dimensions[0];
                ol_copy_data[0].offset = 8 * 0;
                ol_copy_dimensions[1].size = ((mcc_arg_3 + 0 - 1 - 0) + 1) * sizeof(double);
                ol_copy_dimensions[1].lower_bound = 0 * sizeof(double);
                ol_copy_dimensions[1].accessed_length = (mcc_arg_3 + 0 - 1 - 0 + 1) * sizeof(double);
                ol_copy_data[1].sharing = NANOS_SHARED;
                ol_copy_data[1].address = (void *)mcc_arg_1;
                ol_copy_data[1].flags.input = 0;
                ol_copy_data[1].flags.output = 1;
                ol_copy_data[1].dimension_count = 1;
                ol_copy_data[1].dimensions = &ol_copy_dimensions[1];
                ol_copy_data[1].offset = 8 * 0;
                ol_copy_dimensions[2].size = ((mcc_arg_3 + 0 - 1 - 0) + 1) * sizeof(double);
                ol_copy_dimensions[2].lower_bound = 0 * sizeof(double);
                ol_copy_dimensions[2].accessed_length = (mcc_arg_3 + 0 - 1 - 0 + 1) * sizeof(double);
                ol_copy_data[2].sharing = NANOS_SHARED;
                ol_copy_data[2].address = (void *)mcc_arg_2;
                ol_copy_data[2].flags.input = 0;
                ol_copy_data[2].flags.output = 1;
                ol_copy_data[2].dimension_count = 1;
                ol_copy_data[2].dimensions = &ol_copy_dimensions[2];
                ol_copy_data[2].offset = 8 * 0;
                err = nanos_set_translate_function(nanos_wd_, (void (*)(void *, void *))nanos_xlate_fun_0);
                if (err != NANOS_OK)
                  nanos_handle_error(err);
                err = nanos_submit(nanos_wd_, 3, dependences, (void *)0);
                if (err != NANOS_OK)
                  nanos_handle_error(err);
              }
            else
              {
                nanos_region_dimension_internal_t imm_copy_dimensions[3];
                nanos_copy_data_t imm_copy_data[3];
                imm_args.a = mcc_arg_0;
                imm_args.b = mcc_arg_1;
                imm_args.c = mcc_arg_2;
                imm_args.n = mcc_arg_3;
                nanos_region_dimension_t dimensions_0[1] = { { sizeof(double) * ((mcc_arg_3 + 0 - 1 - 0) + 1), sizeof(double) * (0 - 0), sizeof(double) * ((mcc_arg_3 + 0 - 1 - 0) + 1) } };
                nanos_region_dimension_t dimensions_1[1] = { { sizeof(double) * ((mcc_arg_3 + 0 - 1 - 0) + 1), sizeof(double) * (0 - 0), sizeof(double) * ((mcc_arg_3 + 0 - 1 - 0) + 1) } };
                nanos_region_dimension_t dimensions_2[1] = { { sizeof(double) * ((mcc_arg_3 + 0 - 1 - 0) + 1), sizeof(double) * (0 - 0), sizeof(double) * ((mcc_arg_3 + 0 - 1 - 0) + 1) } };
                nanos_data_access_t dependences[3] = { { mcc_arg_0, { 0, 1, 0, 0, 0 }, 1, dimensions_0, 8 * 0 }, { mcc_arg_1, { 0, 1, 0, 0, 0 }, 1, dimensions_1, 8 * 0 }, { mcc_arg_2, { 0, 1, 0, 0, 0 }, 1, dimensions_2, 8 * 0 } };
                ;
                imm_copy_dimensions[0].size = ((mcc_arg_3 + 0 - 1 - 0) + 1) * sizeof(double);
                imm_copy_dimensions[0].lower_bound = 0 * sizeof(double);
                imm_copy_dimensions[0].accessed_length = (mcc_arg_3 + 0 - 1 - 0 + 1) * sizeof(double);
                imm_copy_data[0].sharing = NANOS_SHARED;
                imm_copy_data[0].address = (void *)mcc_arg_0;
                imm_copy_data[0].flags.input = 0;
                imm_copy_data[0].flags.output = 1;
                imm_copy_data[0].dimension_count = 1;
                imm_copy_data[0].dimensions = &imm_copy_dimensions[0];
                imm_copy_data[0].offset = 8 * 0;
                imm_copy_dimensions[1].size = ((mcc_arg_3 + 0 - 1 - 0) + 1) * sizeof(double);
                imm_copy_dimensions[1].lower_bound = 0 * sizeof(double);
                imm_copy_dimensions[1].accessed_length = (mcc_arg_3 + 0 - 1 - 0 + 1) * sizeof(double);
                imm_copy_data[1].sharing = NANOS_SHARED;
                imm_copy_data[1].address = (void *)mcc_arg_1;
                imm_copy_data[1].flags.input = 0;
                imm_copy_data[1].flags.output = 1;
                imm_copy_data[1].dimension_count = 1;
                imm_copy_data[1].dimensions = &imm_copy_dimensions[1];
                imm_copy_data[1].offset = 8 * 0;
                imm_copy_dimensions[2].size = ((mcc_arg_3 + 0 - 1 - 0) + 1) * sizeof(double);
                imm_copy_dimensions[2].lower_bound = 0 * sizeof(double);
                imm_copy_dimensions[2].accessed_length = (mcc_arg_3 + 0 - 1 - 0 + 1) * sizeof(double);
                imm_copy_data[2].sharing = NANOS_SHARED;
                imm_copy_data[2].address = (void *)mcc_arg_2;
                imm_copy_data[2].flags.input = 0;
                imm_copy_data[2].flags.output = 1;
                imm_copy_data[2].dimension_count = 1;
                imm_copy_data[2].dimensions = &imm_copy_dimensions[2];
                imm_copy_data[2].offset = 8 * 0;
                err = nanos_create_wd_and_run_compact(&nanos_wd_const_data.base, &nanos_wd_dyn_props, sizeof(struct nanos_args_0_t), &imm_args, 3, dependences, imm_copy_data, imm_copy_dimensions, (void (*)(void *, void *))nanos_xlate_fun_0);
                if (err != NANOS_OK)
                  nanos_handle_error(err);
              }
          }
        }
      MPI_Barrier((struct ompi_communicator_t *)(void *)&ompi_mpi_comm_world);
      scalar = 3.00000000000000000000000000000000000000000000000000000e+00;
      for (k = 0; k < 10000; k++)
        {
          MPI_Barrier((struct ompi_communicator_t *)(void *)&ompi_mpi_comm_world);
          for (j = 0; j < real_N; j += real_BSIZE)
            {
              double *mcc_arg_4 = a_h + j;
              double *mcc_arg_5 = c_h + j;
              int mcc_arg_6 = real_BSIZE;
              {
                static nanos_opencl_args_t ocl_ol_copy_gpu_3_args;
                nanos_wd_dyn_props_t nanos_wd_dyn_props;
                struct nanos_args_1_t *ol_args;
                nanos_err_t err;
                struct nanos_args_1_t imm_args;
                /* OpenCL device descriptor */
                ocl_ol_copy_gpu_3_args.outline = (void (*)(void *))ocl_ol_copy_gpu_3;
                static struct nanos_const_wd_definition_1 nanos_wd_const_data = { { { 1, 1, 0, 0, 0, 0, 0, 0 }, __alignof__(struct nanos_args_1_t), 2, 1, 2 }, { { &nanos_opencl_factory, &ocl_ol_copy_gpu_3_args } } };
                nanos_wd_dyn_props.tie_to = 0;
                nanos_wd_dyn_props.priority = 0;
                ol_args = (struct nanos_args_1_t *)0;
                void *nanos_wd_ = (void *)0;
                nanos_copy_data_t *ol_copy_data = (nanos_copy_data_t *)0;
                nanos_region_dimension_internal_t *ol_copy_dimensions = (nanos_region_dimension_internal_t *)0;
                err = nanos_create_wd_compact(&nanos_wd_, &nanos_wd_const_data.base, &nanos_wd_dyn_props, sizeof(struct nanos_args_1_t), (void **)&ol_args, nanos_current_wd(), &ol_copy_data, &ol_copy_dimensions);
                if (err != NANOS_OK)
                  nanos_handle_error(err);
                if (nanos_wd_ != (void *)0)
                  {
                    (*ol_args).a = mcc_arg_4;
                    (*ol_args).c = mcc_arg_5;
                    (*ol_args).n = mcc_arg_6;
                    nanos_region_dimension_t dimensions_0[1] = { { sizeof(double) * ((mcc_arg_6 + 0 - 1 - 0) + 1), sizeof(double) * (0 - 0), sizeof(double) * ((mcc_arg_6 + 0 - 1 - 0) + 1) } };
                    nanos_region_dimension_t dimensions_1[1] = { { sizeof(double) * ((mcc_arg_6 + 0 - 1 - 0) + 1), sizeof(double) * (0 - 0), sizeof(double) * ((mcc_arg_6 + 0 - 1 - 0) + 1) } };
                    nanos_data_access_t dependences[2] = { { mcc_arg_4, { 1, 0, 0, 0, 0 }, 1, dimensions_0, 8 * 0 }, { mcc_arg_5, { 0, 1, 0, 0, 0 }, 1, dimensions_1, 8 * 0 } };
                    ;
                    ol_copy_dimensions[0].size = ((mcc_arg_6 + 0 - 1 - 0) + 1) * sizeof(double);
                    ol_copy_dimensions[0].lower_bound = 0 * sizeof(double);
                    ol_copy_dimensions[0].accessed_length = (mcc_arg_6 + 0 - 1 - 0 + 1) * sizeof(double);
                    ol_copy_data[0].sharing = NANOS_SHARED;
                    ol_copy_data[0].address = (void *)mcc_arg_4;
                    ol_copy_data[0].flags.input = 1;
                    ol_copy_data[0].flags.output = 0;
                    ol_copy_data[0].dimension_count = 1;
                    ol_copy_data[0].dimensions = &ol_copy_dimensions[0];
                    ol_copy_data[0].offset = 8 * 0;
                    ol_copy_dimensions[1].size = ((mcc_arg_6 + 0 - 1 - 0) + 1) * sizeof(double);
                    ol_copy_dimensions[1].lower_bound = 0 * sizeof(double);
                    ol_copy_dimensions[1].accessed_length = (mcc_arg_6 + 0 - 1 - 0 + 1) * sizeof(double);
                    ol_copy_data[1].sharing = NANOS_SHARED;
                    ol_copy_data[1].address = (void *)mcc_arg_5;
                    ol_copy_data[1].flags.input = 0;
                    ol_copy_data[1].flags.output = 1;
                    ol_copy_data[1].dimension_count = 1;
                    ol_copy_data[1].dimensions = &ol_copy_dimensions[1];
                    ol_copy_data[1].offset = 8 * 0;
                    err = nanos_set_translate_function(nanos_wd_, (void (*)(void *, void *))nanos_xlate_fun_1);
                    if (err != NANOS_OK)
                      nanos_handle_error(err);
                    err = nanos_submit(nanos_wd_, 2, dependences, (void *)0);
                    if (err != NANOS_OK)
                      nanos_handle_error(err);
                  }
                else
                  {
                    nanos_region_dimension_internal_t imm_copy_dimensions[2];
                    nanos_copy_data_t imm_copy_data[2];
                    imm_args.a = mcc_arg_4;
                    imm_args.c = mcc_arg_5;
                    imm_args.n = mcc_arg_6;
                    nanos_region_dimension_t dimensions_0[1] = { { sizeof(double) * ((mcc_arg_6 + 0 - 1 - 0) + 1), sizeof(double) * (0 - 0), sizeof(double) * ((mcc_arg_6 + 0 - 1 - 0) + 1) } };
                    nanos_region_dimension_t dimensions_1[1] = { { sizeof(double) * ((mcc_arg_6 + 0 - 1 - 0) + 1), sizeof(double) * (0 - 0), sizeof(double) * ((mcc_arg_6 + 0 - 1 - 0) + 1) } };
                    nanos_data_access_t dependences[2] = { { mcc_arg_4, { 1, 0, 0, 0, 0 }, 1, dimensions_0, 8 * 0 }, { mcc_arg_5, { 0, 1, 0, 0, 0 }, 1, dimensions_1, 8 * 0 } };
                    ;
                    imm_copy_dimensions[0].size = ((mcc_arg_6 + 0 - 1 - 0) + 1) * sizeof(double);
                    imm_copy_dimensions[0].lower_bound = 0 * sizeof(double);
                    imm_copy_dimensions[0].accessed_length = (mcc_arg_6 + 0 - 1 - 0 + 1) * sizeof(double);
                    imm_copy_data[0].sharing = NANOS_SHARED;
                    imm_copy_data[0].address = (void *)mcc_arg_4;
                    imm_copy_data[0].flags.input = 1;
                    imm_copy_data[0].flags.output = 0;
                    imm_copy_data[0].dimension_count = 1;
                    imm_copy_data[0].dimensions = &imm_copy_dimensions[0];
                    imm_copy_data[0].offset = 8 * 0;
                    imm_copy_dimensions[1].size = ((mcc_arg_6 + 0 - 1 - 0) + 1) * sizeof(double);
                    imm_copy_dimensions[1].lower_bound = 0 * sizeof(double);
                    imm_copy_dimensions[1].accessed_length = (mcc_arg_6 + 0 - 1 - 0 + 1) * sizeof(double);
                    imm_copy_data[1].sharing = NANOS_SHARED;
                    imm_copy_data[1].address = (void *)mcc_arg_5;
                    imm_copy_data[1].flags.input = 0;
                    imm_copy_data[1].flags.output = 1;
                    imm_copy_data[1].dimension_count = 1;
                    imm_copy_data[1].dimensions = &imm_copy_dimensions[1];
                    imm_copy_data[1].offset = 8 * 0;
                    err = nanos_create_wd_and_run_compact(&nanos_wd_const_data.base, &nanos_wd_dyn_props, sizeof(struct nanos_args_1_t), &imm_args, 2, dependences, imm_copy_data, imm_copy_dimensions, (void (*)(void *, void *))nanos_xlate_fun_1);
                    if (err != NANOS_OK)
                      nanos_handle_error(err);
                  }
              }
            }
          MPI_Barrier((struct ompi_communicator_t *)(void *)&ompi_mpi_comm_world);
          MPI_Barrier((struct ompi_communicator_t *)(void *)&ompi_mpi_comm_world);
          for (j = 0; j < real_N; j += real_BSIZE)
            {
              double *mcc_arg_7 = b_h + j;
              double *mcc_arg_8 = c_h + j;
              double mcc_arg_9 = scalar;
              int mcc_arg_10 = real_BSIZE;
              {
                static nanos_opencl_args_t ocl_ol_scale_gpu_5_args;
                nanos_wd_dyn_props_t nanos_wd_dyn_props;
                struct nanos_args_2_t *ol_args;
                nanos_err_t err;
                struct nanos_args_2_t imm_args;
                /* OpenCL device descriptor */
                ocl_ol_scale_gpu_5_args.outline = (void (*)(void *))ocl_ol_scale_gpu_5;
                static struct nanos_const_wd_definition_1 nanos_wd_const_data = { { { 1, 1, 0, 0, 0, 0, 0, 0 }, __alignof__(struct nanos_args_2_t), 2, 1, 2 }, { { &nanos_opencl_factory, &ocl_ol_scale_gpu_5_args } } };
                nanos_wd_dyn_props.tie_to = 0;
                nanos_wd_dyn_props.priority = 0;
                ol_args = (struct nanos_args_2_t *)0;
                void *nanos_wd_ = (void *)0;
                nanos_copy_data_t *ol_copy_data = (nanos_copy_data_t *)0;
                nanos_region_dimension_internal_t *ol_copy_dimensions = (nanos_region_dimension_internal_t *)0;
                err = nanos_create_wd_compact(&nanos_wd_, &nanos_wd_const_data.base, &nanos_wd_dyn_props, sizeof(struct nanos_args_2_t), (void **)&ol_args, nanos_current_wd(), &ol_copy_data, &ol_copy_dimensions);
                if (err != NANOS_OK)
                  nanos_handle_error(err);
                if (nanos_wd_ != (void *)0)
                  {
                    (*ol_args).b = mcc_arg_7;
                    (*ol_args).c = mcc_arg_8;
                    (*ol_args).scalar = mcc_arg_9;
                    (*ol_args).n = mcc_arg_10;
                    nanos_region_dimension_t dimensions_0[1] = { { sizeof(double) * ((mcc_arg_10 + 0 - 1 - 0) + 1), sizeof(double) * (0 - 0), sizeof(double) * ((mcc_arg_10 + 0 - 1 - 0) + 1) } };
                    nanos_region_dimension_t dimensions_1[1] = { { sizeof(double) * ((mcc_arg_10 + 0 - 1 - 0) + 1), sizeof(double) * (0 - 0), sizeof(double) * ((mcc_arg_10 + 0 - 1 - 0) + 1) } };
                    nanos_data_access_t dependences[2] = { { mcc_arg_7, { 0, 1, 0, 0, 0 }, 1, dimensions_0, 8 * 0 }, { mcc_arg_8, { 1, 0, 0, 0, 0 }, 1, dimensions_1, 8 * 0 } };
                    ;
                    ol_copy_dimensions[0].size = ((mcc_arg_10 + 0 - 1 - 0) + 1) * sizeof(double);
                    ol_copy_dimensions[0].lower_bound = 0 * sizeof(double);
                    ol_copy_dimensions[0].accessed_length = (mcc_arg_10 + 0 - 1 - 0 + 1) * sizeof(double);
                    ol_copy_data[0].sharing = NANOS_SHARED;
                    ol_copy_data[0].address = (void *)mcc_arg_7;
                    ol_copy_data[0].flags.input = 0;
                    ol_copy_data[0].flags.output = 1;
                    ol_copy_data[0].dimension_count = 1;
                    ol_copy_data[0].dimensions = &ol_copy_dimensions[0];
                    ol_copy_data[0].offset = 8 * 0;
                    ol_copy_dimensions[1].size = ((mcc_arg_10 + 0 - 1 - 0) + 1) * sizeof(double);
                    ol_copy_dimensions[1].lower_bound = 0 * sizeof(double);
                    ol_copy_dimensions[1].accessed_length = (mcc_arg_10 + 0 - 1 - 0 + 1) * sizeof(double);
                    ol_copy_data[1].sharing = NANOS_SHARED;
                    ol_copy_data[1].address = (void *)mcc_arg_8;
                    ol_copy_data[1].flags.input = 1;
                    ol_copy_data[1].flags.output = 0;
                    ol_copy_data[1].dimension_count = 1;
                    ol_copy_data[1].dimensions = &ol_copy_dimensions[1];
                    ol_copy_data[1].offset = 8 * 0;
                    err = nanos_set_translate_function(nanos_wd_, (void (*)(void *, void *))nanos_xlate_fun_2);
                    if (err != NANOS_OK)
                      nanos_handle_error(err);
                    err = nanos_submit(nanos_wd_, 2, dependences, (void *)0);
                    if (err != NANOS_OK)
                      nanos_handle_error(err);
                  }
                else
                  {
                    nanos_region_dimension_internal_t imm_copy_dimensions[2];
                    nanos_copy_data_t imm_copy_data[2];
                    imm_args.b = mcc_arg_7;
                    imm_args.c = mcc_arg_8;
                    imm_args.scalar = mcc_arg_9;
                    imm_args.n = mcc_arg_10;
                    nanos_region_dimension_t dimensions_0[1] = { { sizeof(double) * ((mcc_arg_10 + 0 - 1 - 0) + 1), sizeof(double) * (0 - 0), sizeof(double) * ((mcc_arg_10 + 0 - 1 - 0) + 1) } };
                    nanos_region_dimension_t dimensions_1[1] = { { sizeof(double) * ((mcc_arg_10 + 0 - 1 - 0) + 1), sizeof(double) * (0 - 0), sizeof(double) * ((mcc_arg_10 + 0 - 1 - 0) + 1) } };
                    nanos_data_access_t dependences[2] = { { mcc_arg_7, { 0, 1, 0, 0, 0 }, 1, dimensions_0, 8 * 0 }, { mcc_arg_8, { 1, 0, 0, 0, 0 }, 1, dimensions_1, 8 * 0 } };
                    ;
                    imm_copy_dimensions[0].size = ((mcc_arg_10 + 0 - 1 - 0) + 1) * sizeof(double);
                    imm_copy_dimensions[0].lower_bound = 0 * sizeof(double);
                    imm_copy_dimensions[0].accessed_length = (mcc_arg_10 + 0 - 1 - 0 + 1) * sizeof(double);
                    imm_copy_data[0].sharing = NANOS_SHARED;
                    imm_copy_data[0].address = (void *)mcc_arg_7;
                    imm_copy_data[0].flags.input = 0;
                    imm_copy_data[0].flags.output = 1;
                    imm_copy_data[0].dimension_count = 1;
                    imm_copy_data[0].dimensions = &imm_copy_dimensions[0];
                    imm_copy_data[0].offset = 8 * 0;
                    imm_copy_dimensions[1].size = ((mcc_arg_10 + 0 - 1 - 0) + 1) * sizeof(double);
                    imm_copy_dimensions[1].lower_bound = 0 * sizeof(double);
                    imm_copy_dimensions[1].accessed_length = (mcc_arg_10 + 0 - 1 - 0 + 1) * sizeof(double);
                    imm_copy_data[1].sharing = NANOS_SHARED;
                    imm_copy_data[1].address = (void *)mcc_arg_8;
                    imm_copy_data[1].flags.input = 1;
                    imm_copy_data[1].flags.output = 0;
                    imm_copy_data[1].dimension_count = 1;
                    imm_copy_data[1].dimensions = &imm_copy_dimensions[1];
                    imm_copy_data[1].offset = 8 * 0;
                    err = nanos_create_wd_and_run_compact(&nanos_wd_const_data.base, &nanos_wd_dyn_props, sizeof(struct nanos_args_2_t), &imm_args, 2, dependences, imm_copy_data, imm_copy_dimensions, (void (*)(void *, void *))nanos_xlate_fun_2);
                    if (err != NANOS_OK)
                      nanos_handle_error(err);
                  }
              }
            }
          MPI_Barrier((struct ompi_communicator_t *)(void *)&ompi_mpi_comm_world);
          MPI_Barrier((struct ompi_communicator_t *)(void *)&ompi_mpi_comm_world);
          for (j = 0; j < real_N; j += real_BSIZE)
            {
              double *mcc_arg_11 = a_h + j;
              double *mcc_arg_12 = b_h + j;
              double *mcc_arg_13 = c_h + j;
              int mcc_arg_14 = real_BSIZE;
              {
                static nanos_opencl_args_t ocl_ol_add_gpu_7_args;
                nanos_wd_dyn_props_t nanos_wd_dyn_props;
                struct nanos_args_3_t *ol_args;
                nanos_err_t err;
                struct nanos_args_3_t imm_args;
                /* OpenCL device descriptor */
                ocl_ol_add_gpu_7_args.outline = (void (*)(void *))ocl_ol_add_gpu_7;
                static struct nanos_const_wd_definition_1 nanos_wd_const_data = { { { 1, 1, 0, 0, 0, 0, 0, 0 }, __alignof__(struct nanos_args_3_t), 3, 1, 3 }, { { &nanos_opencl_factory, &ocl_ol_add_gpu_7_args } } };
                nanos_wd_dyn_props.tie_to = 0;
                nanos_wd_dyn_props.priority = 0;
                ol_args = (struct nanos_args_3_t *)0;
                void *nanos_wd_ = (void *)0;
                nanos_copy_data_t *ol_copy_data = (nanos_copy_data_t *)0;
                nanos_region_dimension_internal_t *ol_copy_dimensions = (nanos_region_dimension_internal_t *)0;
                err = nanos_create_wd_compact(&nanos_wd_, &nanos_wd_const_data.base, &nanos_wd_dyn_props, sizeof(struct nanos_args_3_t), (void **)&ol_args, nanos_current_wd(), &ol_copy_data, &ol_copy_dimensions);
                if (err != NANOS_OK)
                  nanos_handle_error(err);
                if (nanos_wd_ != (void *)0)
                  {
                    (*ol_args).a = mcc_arg_11;
                    (*ol_args).b = mcc_arg_12;
                    (*ol_args).c = mcc_arg_13;
                    (*ol_args).n = mcc_arg_14;
                    nanos_region_dimension_t dimensions_0[1] = { { sizeof(double) * ((mcc_arg_14 + 0 - 1 - 0) + 1), sizeof(double) * (0 - 0), sizeof(double) * ((mcc_arg_14 + 0 - 1 - 0) + 1) } };
                    nanos_region_dimension_t dimensions_1[1] = { { sizeof(double) * ((mcc_arg_14 + 0 - 1 - 0) + 1), sizeof(double) * (0 - 0), sizeof(double) * ((mcc_arg_14 + 0 - 1 - 0) + 1) } };
                    nanos_region_dimension_t dimensions_2[1] = { { sizeof(double) * ((mcc_arg_14 + 0 - 1 - 0) + 1), sizeof(double) * (0 - 0), sizeof(double) * ((mcc_arg_14 + 0 - 1 - 0) + 1) } };
                    nanos_data_access_t dependences[3] = { { mcc_arg_11, { 1, 0, 0, 0, 0 }, 1, dimensions_0, 8 * 0 }, { mcc_arg_12, { 1, 0, 0, 0, 0 }, 1, dimensions_1, 8 * 0 }, { mcc_arg_13, { 0, 1, 0, 0, 0 }, 1, dimensions_2, 8 * 0 } };
                    ;
                    ol_copy_dimensions[0].size = ((mcc_arg_14 + 0 - 1 - 0) + 1) * sizeof(double);
                    ol_copy_dimensions[0].lower_bound = 0 * sizeof(double);
                    ol_copy_dimensions[0].accessed_length = (mcc_arg_14 + 0 - 1 - 0 + 1) * sizeof(double);
                    ol_copy_data[0].sharing = NANOS_SHARED;
                    ol_copy_data[0].address = (void *)mcc_arg_11;
                    ol_copy_data[0].flags.input = 1;
                    ol_copy_data[0].flags.output = 0;
                    ol_copy_data[0].dimension_count = 1;
                    ol_copy_data[0].dimensions = &ol_copy_dimensions[0];
                    ol_copy_data[0].offset = 8 * 0;
                    ol_copy_dimensions[1].size = ((mcc_arg_14 + 0 - 1 - 0) + 1) * sizeof(double);
                    ol_copy_dimensions[1].lower_bound = 0 * sizeof(double);
                    ol_copy_dimensions[1].accessed_length = (mcc_arg_14 + 0 - 1 - 0 + 1) * sizeof(double);
                    ol_copy_data[1].sharing = NANOS_SHARED;
                    ol_copy_data[1].address = (void *)mcc_arg_12;
                    ol_copy_data[1].flags.input = 1;
                    ol_copy_data[1].flags.output = 0;
                    ol_copy_data[1].dimension_count = 1;
                    ol_copy_data[1].dimensions = &ol_copy_dimensions[1];
                    ol_copy_data[1].offset = 8 * 0;
                    ol_copy_dimensions[2].size = ((mcc_arg_14 + 0 - 1 - 0) + 1) * sizeof(double);
                    ol_copy_dimensions[2].lower_bound = 0 * sizeof(double);
                    ol_copy_dimensions[2].accessed_length = (mcc_arg_14 + 0 - 1 - 0 + 1) * sizeof(double);
                    ol_copy_data[2].sharing = NANOS_SHARED;
                    ol_copy_data[2].address = (void *)mcc_arg_13;
                    ol_copy_data[2].flags.input = 0;
                    ol_copy_data[2].flags.output = 1;
                    ol_copy_data[2].dimension_count = 1;
                    ol_copy_data[2].dimensions = &ol_copy_dimensions[2];
                    ol_copy_data[2].offset = 8 * 0;
                    err = nanos_set_translate_function(nanos_wd_, (void (*)(void *, void *))nanos_xlate_fun_3);
                    if (err != NANOS_OK)
                      nanos_handle_error(err);
                    err = nanos_submit(nanos_wd_, 3, dependences, (void *)0);
                    if (err != NANOS_OK)
                      nanos_handle_error(err);
                  }
                else
                  {
                    nanos_region_dimension_internal_t imm_copy_dimensions[3];
                    nanos_copy_data_t imm_copy_data[3];
                    imm_args.a = mcc_arg_11;
                    imm_args.b = mcc_arg_12;
                    imm_args.c = mcc_arg_13;
                    imm_args.n = mcc_arg_14;
                    nanos_region_dimension_t dimensions_0[1] = { { sizeof(double) * ((mcc_arg_14 + 0 - 1 - 0) + 1), sizeof(double) * (0 - 0), sizeof(double) * ((mcc_arg_14 + 0 - 1 - 0) + 1) } };
                    nanos_region_dimension_t dimensions_1[1] = { { sizeof(double) * ((mcc_arg_14 + 0 - 1 - 0) + 1), sizeof(double) * (0 - 0), sizeof(double) * ((mcc_arg_14 + 0 - 1 - 0) + 1) } };
                    nanos_region_dimension_t dimensions_2[1] = { { sizeof(double) * ((mcc_arg_14 + 0 - 1 - 0) + 1), sizeof(double) * (0 - 0), sizeof(double) * ((mcc_arg_14 + 0 - 1 - 0) + 1) } };
                    nanos_data_access_t dependences[3] = { { mcc_arg_11, { 1, 0, 0, 0, 0 }, 1, dimensions_0, 8 * 0 }, { mcc_arg_12, { 1, 0, 0, 0, 0 }, 1, dimensions_1, 8 * 0 }, { mcc_arg_13, { 0, 1, 0, 0, 0 }, 1, dimensions_2, 8 * 0 } };
                    ;
                    imm_copy_dimensions[0].size = ((mcc_arg_14 + 0 - 1 - 0) + 1) * sizeof(double);
                    imm_copy_dimensions[0].lower_bound = 0 * sizeof(double);
                    imm_copy_dimensions[0].accessed_length = (mcc_arg_14 + 0 - 1 - 0 + 1) * sizeof(double);
                    imm_copy_data[0].sharing = NANOS_SHARED;
                    imm_copy_data[0].address = (void *)mcc_arg_11;
                    imm_copy_data[0].flags.input = 1;
                    imm_copy_data[0].flags.output = 0;
                    imm_copy_data[0].dimension_count = 1;
                    imm_copy_data[0].dimensions = &imm_copy_dimensions[0];
                    imm_copy_data[0].offset = 8 * 0;
                    imm_copy_dimensions[1].size = ((mcc_arg_14 + 0 - 1 - 0) + 1) * sizeof(double);
                    imm_copy_dimensions[1].lower_bound = 0 * sizeof(double);
                    imm_copy_dimensions[1].accessed_length = (mcc_arg_14 + 0 - 1 - 0 + 1) * sizeof(double);
                    imm_copy_data[1].sharing = NANOS_SHARED;
                    imm_copy_data[1].address = (void *)mcc_arg_12;
                    imm_copy_data[1].flags.input = 1;
                    imm_copy_data[1].flags.output = 0;
                    imm_copy_data[1].dimension_count = 1;
                    imm_copy_data[1].dimensions = &imm_copy_dimensions[1];
                    imm_copy_data[1].offset = 8 * 0;
                    imm_copy_dimensions[2].size = ((mcc_arg_14 + 0 - 1 - 0) + 1) * sizeof(double);
                    imm_copy_dimensions[2].lower_bound = 0 * sizeof(double);
                    imm_copy_dimensions[2].accessed_length = (mcc_arg_14 + 0 - 1 - 0 + 1) * sizeof(double);
                    imm_copy_data[2].sharing = NANOS_SHARED;
                    imm_copy_data[2].address = (void *)mcc_arg_13;
                    imm_copy_data[2].flags.input = 0;
                    imm_copy_data[2].flags.output = 1;
                    imm_copy_data[2].dimension_count = 1;
                    imm_copy_data[2].dimensions = &imm_copy_dimensions[2];
                    imm_copy_data[2].offset = 8 * 0;
                    err = nanos_create_wd_and_run_compact(&nanos_wd_const_data.base, &nanos_wd_dyn_props, sizeof(struct nanos_args_3_t), &imm_args, 3, dependences, imm_copy_data, imm_copy_dimensions, (void (*)(void *, void *))nanos_xlate_fun_3);
                    if (err != NANOS_OK)
                      nanos_handle_error(err);
                  }
              }
            }
          MPI_Barrier((struct ompi_communicator_t *)(void *)&ompi_mpi_comm_world);
          MPI_Barrier((struct ompi_communicator_t *)(void *)&ompi_mpi_comm_world);
          for (j = 0; j < real_N; j += real_BSIZE)
            {
              double *mcc_arg_15 = a_h + j;
              double *mcc_arg_16 = b_h + j;
              double *mcc_arg_17 = c_h + j;
              double mcc_arg_18 = scalar;
              int mcc_arg_19 = real_BSIZE;
              {
                static nanos_opencl_args_t ocl_ol_triad_gpu_9_args;
                nanos_wd_dyn_props_t nanos_wd_dyn_props;
                struct nanos_args_4_t *ol_args;
                nanos_err_t err;
                struct nanos_args_4_t imm_args;
                /* OpenCL device descriptor */
                ocl_ol_triad_gpu_9_args.outline = (void (*)(void *))ocl_ol_triad_gpu_9;
                static struct nanos_const_wd_definition_1 nanos_wd_const_data = { { { 1, 1, 0, 0, 0, 0, 0, 0 }, __alignof__(struct nanos_args_4_t), 3, 1, 3 }, { { &nanos_opencl_factory, &ocl_ol_triad_gpu_9_args } } };
                nanos_wd_dyn_props.tie_to = 0;
                nanos_wd_dyn_props.priority = 0;
                ol_args = (struct nanos_args_4_t *)0;
                void *nanos_wd_ = (void *)0;
                nanos_copy_data_t *ol_copy_data = (nanos_copy_data_t *)0;
                nanos_region_dimension_internal_t *ol_copy_dimensions = (nanos_region_dimension_internal_t *)0;
                err = nanos_create_wd_compact(&nanos_wd_, &nanos_wd_const_data.base, &nanos_wd_dyn_props, sizeof(struct nanos_args_4_t), (void **)&ol_args, nanos_current_wd(), &ol_copy_data, &ol_copy_dimensions);
                if (err != NANOS_OK)
                  nanos_handle_error(err);
                if (nanos_wd_ != (void *)0)
                  {
                    (*ol_args).a = mcc_arg_15;
                    (*ol_args).b = mcc_arg_16;
                    (*ol_args).c = mcc_arg_17;
                    (*ol_args).scalar = mcc_arg_18;
                    (*ol_args).n = mcc_arg_19;
                    nanos_region_dimension_t dimensions_0[1] = { { sizeof(double) * ((mcc_arg_19 + 0 - 1 - 0) + 1), sizeof(double) * (0 - 0), sizeof(double) * ((mcc_arg_19 + 0 - 1 - 0) + 1) } };
                    nanos_region_dimension_t dimensions_1[1] = { { sizeof(double) * ((mcc_arg_19 + 0 - 1 - 0) + 1), sizeof(double) * (0 - 0), sizeof(double) * ((mcc_arg_19 + 0 - 1 - 0) + 1) } };
                    nanos_region_dimension_t dimensions_2[1] = { { sizeof(double) * ((mcc_arg_19 + 0 - 1 - 0) + 1), sizeof(double) * (0 - 0), sizeof(double) * ((mcc_arg_19 + 0 - 1 - 0) + 1) } };
                    nanos_data_access_t dependences[3] = { { mcc_arg_15, { 0, 1, 0, 0, 0 }, 1, dimensions_0, 8 * 0 }, { mcc_arg_16, { 1, 0, 0, 0, 0 }, 1, dimensions_1, 8 * 0 }, { mcc_arg_17, { 1, 0, 0, 0, 0 }, 1, dimensions_2, 8 * 0 } };
                    ;
                    ol_copy_dimensions[0].size = ((mcc_arg_19 + 0 - 1 - 0) + 1) * sizeof(double);
                    ol_copy_dimensions[0].lower_bound = 0 * sizeof(double);
                    ol_copy_dimensions[0].accessed_length = (mcc_arg_19 + 0 - 1 - 0 + 1) * sizeof(double);
                    ol_copy_data[0].sharing = NANOS_SHARED;
                    ol_copy_data[0].address = (void *)mcc_arg_15;
                    ol_copy_data[0].flags.input = 0;
                    ol_copy_data[0].flags.output = 1;
                    ol_copy_data[0].dimension_count = 1;
                    ol_copy_data[0].dimensions = &ol_copy_dimensions[0];
                    ol_copy_data[0].offset = 8 * 0;
                    ol_copy_dimensions[1].size = ((mcc_arg_19 + 0 - 1 - 0) + 1) * sizeof(double);
                    ol_copy_dimensions[1].lower_bound = 0 * sizeof(double);
                    ol_copy_dimensions[1].accessed_length = (mcc_arg_19 + 0 - 1 - 0 + 1) * sizeof(double);
                    ol_copy_data[1].sharing = NANOS_SHARED;
                    ol_copy_data[1].address = (void *)mcc_arg_16;
                    ol_copy_data[1].flags.input = 1;
                    ol_copy_data[1].flags.output = 0;
                    ol_copy_data[1].dimension_count = 1;
                    ol_copy_data[1].dimensions = &ol_copy_dimensions[1];
                    ol_copy_data[1].offset = 8 * 0;
                    ol_copy_dimensions[2].size = ((mcc_arg_19 + 0 - 1 - 0) + 1) * sizeof(double);
                    ol_copy_dimensions[2].lower_bound = 0 * sizeof(double);
                    ol_copy_dimensions[2].accessed_length = (mcc_arg_19 + 0 - 1 - 0 + 1) * sizeof(double);
                    ol_copy_data[2].sharing = NANOS_SHARED;
                    ol_copy_data[2].address = (void *)mcc_arg_17;
                    ol_copy_data[2].flags.input = 1;
                    ol_copy_data[2].flags.output = 0;
                    ol_copy_data[2].dimension_count = 1;
                    ol_copy_data[2].dimensions = &ol_copy_dimensions[2];
                    ol_copy_data[2].offset = 8 * 0;
                    err = nanos_set_translate_function(nanos_wd_, (void (*)(void *, void *))nanos_xlate_fun_4);
                    if (err != NANOS_OK)
                      nanos_handle_error(err);
                    err = nanos_submit(nanos_wd_, 3, dependences, (void *)0);
                    if (err != NANOS_OK)
                      nanos_handle_error(err);
                  }
                else
                  {
                    nanos_region_dimension_internal_t imm_copy_dimensions[3];
                    nanos_copy_data_t imm_copy_data[3];
                    imm_args.a = mcc_arg_15;
                    imm_args.b = mcc_arg_16;
                    imm_args.c = mcc_arg_17;
                    imm_args.scalar = mcc_arg_18;
                    imm_args.n = mcc_arg_19;
                    nanos_region_dimension_t dimensions_0[1] = { { sizeof(double) * ((mcc_arg_19 + 0 - 1 - 0) + 1), sizeof(double) * (0 - 0), sizeof(double) * ((mcc_arg_19 + 0 - 1 - 0) + 1) } };
                    nanos_region_dimension_t dimensions_1[1] = { { sizeof(double) * ((mcc_arg_19 + 0 - 1 - 0) + 1), sizeof(double) * (0 - 0), sizeof(double) * ((mcc_arg_19 + 0 - 1 - 0) + 1) } };
                    nanos_region_dimension_t dimensions_2[1] = { { sizeof(double) * ((mcc_arg_19 + 0 - 1 - 0) + 1), sizeof(double) * (0 - 0), sizeof(double) * ((mcc_arg_19 + 0 - 1 - 0) + 1) } };
                    nanos_data_access_t dependences[3] = { { mcc_arg_15, { 0, 1, 0, 0, 0 }, 1, dimensions_0, 8 * 0 }, { mcc_arg_16, { 1, 0, 0, 0, 0 }, 1, dimensions_1, 8 * 0 }, { mcc_arg_17, { 1, 0, 0, 0, 0 }, 1, dimensions_2, 8 * 0 } };
                    ;
                    imm_copy_dimensions[0].size = ((mcc_arg_19 + 0 - 1 - 0) + 1) * sizeof(double);
                    imm_copy_dimensions[0].lower_bound = 0 * sizeof(double);
                    imm_copy_dimensions[0].accessed_length = (mcc_arg_19 + 0 - 1 - 0 + 1) * sizeof(double);
                    imm_copy_data[0].sharing = NANOS_SHARED;
                    imm_copy_data[0].address = (void *)mcc_arg_15;
                    imm_copy_data[0].flags.input = 0;
                    imm_copy_data[0].flags.output = 1;
                    imm_copy_data[0].dimension_count = 1;
                    imm_copy_data[0].dimensions = &imm_copy_dimensions[0];
                    imm_copy_data[0].offset = 8 * 0;
                    imm_copy_dimensions[1].size = ((mcc_arg_19 + 0 - 1 - 0) + 1) * sizeof(double);
                    imm_copy_dimensions[1].lower_bound = 0 * sizeof(double);
                    imm_copy_dimensions[1].accessed_length = (mcc_arg_19 + 0 - 1 - 0 + 1) * sizeof(double);
                    imm_copy_data[1].sharing = NANOS_SHARED;
                    imm_copy_data[1].address = (void *)mcc_arg_16;
                    imm_copy_data[1].flags.input = 1;
                    imm_copy_data[1].flags.output = 0;
                    imm_copy_data[1].dimension_count = 1;
                    imm_copy_data[1].dimensions = &imm_copy_dimensions[1];
                    imm_copy_data[1].offset = 8 * 0;
                    imm_copy_dimensions[2].size = ((mcc_arg_19 + 0 - 1 - 0) + 1) * sizeof(double);
                    imm_copy_dimensions[2].lower_bound = 0 * sizeof(double);
                    imm_copy_dimensions[2].accessed_length = (mcc_arg_19 + 0 - 1 - 0 + 1) * sizeof(double);
                    imm_copy_data[2].sharing = NANOS_SHARED;
                    imm_copy_data[2].address = (void *)mcc_arg_17;
                    imm_copy_data[2].flags.input = 1;
                    imm_copy_data[2].flags.output = 0;
                    imm_copy_data[2].dimension_count = 1;
                    imm_copy_data[2].dimensions = &imm_copy_dimensions[2];
                    imm_copy_data[2].offset = 8 * 0;
                    err = nanos_create_wd_and_run_compact(&nanos_wd_const_data.base, &nanos_wd_dyn_props, sizeof(struct nanos_args_4_t), &imm_args, 3, dependences, imm_copy_data, imm_copy_dimensions, (void (*)(void *, void *))nanos_xlate_fun_4);
                    if (err != NANOS_OK)
                      nanos_handle_error(err);
                  }
              }
            }
          MPI_Barrier((struct ompi_communicator_t *)(void *)&ompi_mpi_comm_world);
        }
      {
        nanos_err_t err;
        void *nanos_wd_ = nanos_current_wd();
        err = nanos_wg_wait_completion(nanos_wd_, 0);
        if (err != NANOS_OK)
          nanos_handle_error(err);
      }
      gettimeofday(&finish, (void *)0);
    }
  total_time = getElapsedTime(start, finish);
  MPI_Barrier((struct ompi_communicator_t *)(void *)&ompi_mpi_comm_world);
  if (myrank == 0)
    {
      printf("Average Rate (MB/s): %11.4f \n", 9.99999999999999954748111825886258685613938723690807819e-07 * (10 * sizeof(double) * 8 * 1024 * 1024) * 10000 * rank / total_time);
      printf("TOTAL time (including initialization) =  %11.4f seconds\n", total_time);
      printf("-------------------------------------------------------------\n");
    }
  checkSTREAMresults();
  if (myrank == 0)
    {
      printf("-------------------------------------------------------------\n");
    }
  free(a_h);
  free(b_h);
  free(c_h);
  MPI_Finalize();
  return 0;
}
extern void checkSTREAMresults()
{
  double aj;
  double bj;
  double cj;
  double scalar;
  int k;
  double asum;
  double bsum;
  double csum;
  int j;
  double epsilon;
  aj = 1.00000000000000000000000000000000000000000000000000000e+00;
  bj = 2.00000000000000000000000000000000000000000000000000000e+00;
  cj = 0.00000000000000000000000000000000000000000000000000000e+00;
  aj = 2.00000000000000000000000000000000000000000000000000000e+00 * aj;
  scalar = 3.00000000000000000000000000000000000000000000000000000e+00;
  for (k = 0; k < 10000; k++)
    {
      cj = aj;
      bj = scalar * cj;
      cj = aj + bj;
      aj = bj + scalar * cj;
    }
  aj = aj * (double)real_N;
  bj = bj * (double)real_N;
  cj = cj * (double)real_N;
  asum = 0.00000000000000000000000000000000000000000000000000000e+00;
  bsum = 0.00000000000000000000000000000000000000000000000000000e+00;
  csum = 0.00000000000000000000000000000000000000000000000000000e+00;
  for (j = 0; j < real_N; j++)
    {
      asum += a_h[j];
      bsum += b_h[j];
      csum += c_h[j];
    }
  epsilon = 1.00000000000000002092256083012847267532663408928783610e-08;
  if ((aj - asum >= 0 ? aj - asum :  -(aj - asum)) / asum > epsilon)
    {
      printf("Failed Validation on array a[]\n");
      printf("        Expected  : %f \n", aj);
      printf("        Observed  : %f \n", asum);
      for (j = 0; j < 10; j++)
        {
          printf("[%d] Observed: %f\n", j, a_h[j]);
        }
    }
  else
    if ((bj - bsum >= 0 ? bj - bsum :  -(bj - bsum)) / bsum > epsilon)
      {
        printf("Failed Validation on array b[]\n");
        printf("        Expected  : %f \n", bj);
        printf("        Observed  : %f \n", bsum);
      }
    else
      if ((cj - csum >= 0 ? cj - csum :  -(cj - csum)) / csum > epsilon)
        {
          printf("Failed Validation on array c[]\n");
          printf("        Expected  : %f \n", cj);
          printf("        Observed  : %f \n", csum);
        }
      else
        {
          printf("Solution Validates\n");
        }
}
extern double getElapsedTime(struct timeval start, struct timeval finish)
{
  return finish.tv_sec + finish.tv_usec * 9.99999999999999954748111825886258685613938723690807819e-07 - (start.tv_sec + start.tv_usec * 9.99999999999999954748111825886258685613938723690807819e-07);
}
typedef void nanos_init_func_t(void *);
struct  mcc_struct_anon_21
{
  void (*func)(void *);
  void *data;
};
typedef struct mcc_struct_anon_21 nanos_init_desc_t;
void nanos_omp_set_interface(void *);
__attribute__((weak)) __attribute__((section("nanos_init"))) nanos_init_desc_t __section__nanos_init = { nanos_omp_set_interface, (void *)0 };
