/*-----------------------------------------------------------------------*/
/* Program: Perlin Noise                                                 */
/* Adapted to OmpSs by the Barcelona Supercomputing Center               */
/*************************************************************************/
/*                                                                       */
/* Licensed Materials - Property of IBM                                  */
/*                                                                       */
/* Created by Mark Nutter on 1/28/09.                                    */
/* (C) Copyright IBM Corp. 2009                                          */
/*                                                                       */
/* Derived from Improved Noise (http://mrl.nyu.edu/~perlin/noise/)       */
/* (C) Copyright Ken Perlin 2002                                         */
/*                                                                       */
/*************************************************************************/
#include <math.h>
#include <stdio.h>
#include <omp.h>
#include <stdlib.h>
#include <CL/opencl.h>


#include "perlin_auxiliar_header.h"



int N_DEVICES = 1;

cl_device_id* device_id;             // compute device id 
cl_context context;                 // compute context
cl_command_queue* commands;          // compute command queue
cl_program program;                 // compute program
cl_mem*  outputs;

void init_opencl(int buffsize){
	//Dirty way to initialise :)	
	device_id=(cl_device_id*) malloc(sizeof(cl_device_id)*N_DEVICES);             // compute device id 
	commands=(cl_command_queue*) malloc(sizeof(cl_command_queue)*N_DEVICES);             // compute device id 
	outputs=(cl_mem*) malloc(sizeof(cl_mem)*N_DEVICES);             // compute device id 
	int gpu=1;
	cl_int err;
	cl_platform_id plats[4];
	clGetPlatformIDs(4,plats,NULL);
	int i;

	err = clGetDeviceIDs(plats[0], gpu ? CL_DEVICE_TYPE_GPU : CL_DEVICE_TYPE_CPU, N_DEVICES, device_id, NULL);
    if (err != CL_SUCCESS)
    {
        printf("Error: Failed to create a device group! %d\n",err);
    }
	context = clCreateContext(0, N_DEVICES, device_id, NULL, NULL, &err);
    if (!context)
    {
        printf("Error: Failed to create a compute context!\n");
    }
	for (i=0; i<N_DEVICES;i++){	
	  char device_name[200] = "";
	  clGetDeviceInfo
				  (device_id[i], CL_DEVICE_NAME, 200, device_name,
				   NULL);
	  printf("Device: \"%s\"\n", device_name);
		commands[i] = clCreateCommandQueue(context, device_id[i], 0, &err);
		if (!commands[i])
		{
			printf("Error: Failed to create a command commands!\n");
		}
	}
	char* ompss_code;    
    FILE *fp;
	size_t source_size;
	fp = fopen("perlin_kernel.cl", "r");
	if (!fp) {
		printf("Failed to open file when loading kernel from file\n");
	}      
	fseek(fp, 0, SEEK_END); // seek to end of file;
	size_t size = ftell(fp); // get current file pointer
	fseek(fp, 0, SEEK_SET); // seek back to beginning of file
	ompss_code = (char*) malloc(sizeof(char)*size);
	source_size = fread( ompss_code, 1, size, fp);
	fclose(fp); 
	ompss_code[size]=0;
	

	for (i = 0; i< N_DEVICES; i++){
		outputs[i] = clCreateBuffer(context,  CL_MEM_READ_WRITE,  buffsize, NULL, &err);
		if (err != CL_SUCCESS){
			printf("Error: Failed to create buffer, err %d!\n",err);
		}
	}
	
	program = clCreateProgramWithSource(context, 1, (const char **) &ompss_code, NULL, &err);
    if (!program)
    {
        printf("Error: Failed to create compute program!\n");
    }
	clBuildProgram(program, 0, NULL, "", NULL, NULL);
	free(ompss_code);
}

/**
 * compute_perlin_noise
 *
 * Compute perlin noise algorithm given time offset, rowstride and image size.
 */
void compute_perlin_noise_device(int ndevs, int iter, pixel * output, float time, unsigned int rowstride, int img_height, int img_width)
{
	N_DEVICES=ndevs;
    unsigned int i, j;
    float vy, vt;
    int BSy = 1;
    int BSx = 128;
	cl_int err;
    int BS = img_height/N_DEVICES;
	if (iter==0){
		init_opencl(sizeof(pixel)*BS*rowstride);
	};

	cl_kernel kernels[N_DEVICES];
	for (i=0; i<N_DEVICES;++i){
		kernels[i] = clCreateKernel(program, "cuda_perlin", &err);
		if (err != CL_SUCCESS)
		{
			printf("Error: Failed to create compute kernel %d!\n",err);
			exit(1);
		}
		clSetKernelArg(kernels[i], 0, sizeof(cl_mem),&outputs[i]);
		clSetKernelArg(kernels[i], 1, sizeof(float), &time);
		clSetKernelArg(kernels[i], 3, sizeof(unsigned int), &rowstride);
		clSetKernelArg(kernels[i], 4, sizeof(int), &img_height);
		clSetKernelArg(kernels[i], 5, sizeof(int),&img_width);
		
	}	

	size_t local_size_ptr[2];
	size_t global_size_ptr[2];
	local_size_ptr[0]=BSx;
	local_size_ptr[1]=BSy;
	global_size_ptr[0]=img_width;
	global_size_ptr[1]=BS;
	int p=0;
	for (p = 0; p < 2; ++p)
	{
		local_size_ptr[p] = global_size_ptr[p] < local_size_ptr[p] ? global_size_ptr[p] : local_size_ptr[p];
		global_size_ptr[p] = global_size_ptr[p] < local_size_ptr[p] ? global_size_ptr[p] : global_size_ptr[p] + (global_size_ptr[p] % local_size_ptr[p] == 0 ? 0 : local_size_ptr[p] - global_size_ptr[p] % local_size_ptr[p]);
	}
	int n_device;
    for (j = 0; j < img_height; j+=BS) {
			n_device=(j)/BS;
			clSetKernelArg(kernels[n_device],2 , sizeof(int), &j);
			clFinish(commands[n_device]);		
			clEnqueueNDRangeKernel(commands[n_device], kernels[n_device], 2, NULL, global_size_ptr, local_size_ptr, 0, NULL, NULL);

			clEnqueueReadBuffer(commands[n_device], outputs[n_device], CL_FALSE, 0, sizeof(pixel)*BS*rowstride, &output[j*rowstride], 0, NULL, NULL ); 		
	}
	//Wait for end and release
	for (i=0; i<N_DEVICES;++i){
	//	clFinish(commands[i]);
		clReleaseKernel(kernels[i]);
	}
}

void release(){
	int i;
	for (i=0; i<N_DEVICES;i++){	
		clReleaseMemObject(outputs[i]);
		clReleaseCommandQueue(commands[i]);
		clReleaseContext(context);
	}
	free(outputs);
	free(commands);
	free(device_id);
}

/* Host version */

/**
 * compute_perlin_noise
 *
 * Compute perlin noise algorithm given time offset, rowstride and image size.
 */
void compute_perlin_noise(pixel * output, const float time, const unsigned int rowstride, int img_height, int img_width)
{
    unsigned int i, j;
    float vy, vt;
    float vdx = 0.03125f;
    float vdy = 0.0125f;
    float vs = 2.0f;
    float bias = 0.35f;
    float vx = 0.0f;
    float red, green, blue;
    float xx, yy;

    for (j = 0; j < img_height; j++) {
        for (i = 0; i < img_width; i++) {
            vx = ((float) i) * vdx;
            vy = ((float) j) * vdy;
            vt = time * vs;

            xx = vx * vs;
            yy = vy * vs;

            red = noise3(xx, vt, yy);
            green = noise3(vt, yy, xx);
            blue = noise3(yy, xx, vt);

            red += bias;
            green += bias;
            blue += bias;

            // Clamp to within [0 .. 1]
            red = (red > 1.0f) ? 1.0f : red;
            green = (green > 1.0f) ? 1.0f : green;
            blue = (blue > 1.0f) ? 1.0f : blue;

            red = (red < 0.0f) ? 0.0f : red;
            green = (green < 0.0f) ? 0.0f : green;
            blue = (blue < 0.0f) ? 0.0f : blue;

            red *= 255.0f;
            green *= 255.0f;
            blue *= 255.0f;

            output[(j * rowstride) + i].r = (unsigned char) red;
            output[(j * rowstride) + i].g = (unsigned char) green;
            output[(j * rowstride) + i].b = (unsigned char) blue;
            output[(j * rowstride) + i].a = 255;
        }
    }
}

/**
 * compute_host_and_verify
 *
 * Perform the host-side perlin noise computation, report performance and optionally verify the device results.
 */
int compute_host_and_verify(int iterations, pixel * output_device, int rowstride,
                            int img_height, int img_width, int verify, int data_size, float device_mpix)
{
    int retval = 0;
    int i;
    float host_mpix;
    double delta;
    float time;
    pixel *output;

    if (verify) {
       
        printf("Compute Host Data\n");
       // Create the output array
       
        output = calloc (img_width*img_height,sizeof(pixel));
        if (output==NULL) {
          perror ("malloc (output)");
          exit (1);
        }
        
        time = 0.0f;
        double start = omp_get_wtime();

        for (i = 0; i < iterations; i++) {
            compute_perlin_noise((pixel *) output, time, rowstride, img_height, img_width);
            time += 0.05f;
        }

        delta = omp_get_wtime() - start;


        host_mpix = (double) (img_width * img_height) * (double) (iterations) / (1000000.0 * (double) (delta));
        printf("Host -- %d Computes\n", iterations);
        printf("computation time (in seconds): %f\n", delta);
        printf("Mpixels/sec: %f\n", host_mpix);
        printf("%f Speedup\n", device_mpix / host_mpix);


        // Verify results
        //
        printf("Verifying....\n");

        int failure_count = 0;
        for (i = 0; i < img_width * img_height ; i++) {
            pixel *device_pixel = (pixel *) & output_device[i];
            pixel *host_pixel = (pixel *) & output[i];

            //The rgb values may be off by 1 but still be correct because of rounding error.
            //For example, if the host computes 164.000015 and the device 163.999985,
            //the resulting values will be 164 and 163, respectively.

            if ((abs(device_pixel->r - host_pixel->r) > 1) ||
                    (abs(device_pixel->g - host_pixel->g) > 1) ||
                    (abs(device_pixel->b - host_pixel->b) > 1) ||
                    (abs(device_pixel->a - host_pixel->a) > 1)) {
                retval = 1;
                fprintf(stderr, "Error [%d]: H 0x%08X 0x%08X\n", i, output[i], output_device[i]);
                failure_count++;
            }
        }

        if (failure_count) {
            printf("Verification Complete: %d errors out of %d pixels\n", failure_count, data_size / sizeof(pixel));
        } else {
            printf("Verification: Ok\n");
        }
    }

    return retval;
}
