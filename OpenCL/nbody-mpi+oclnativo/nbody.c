/* nbody.c */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <mpi.h>
#include "nbody.h"
#include <CL/opencl.h>
#include <time.h>

#define default_domain_size_x               1.0e+18 /* m  */
#define default_domain_size_y               1.0e+18 /* m  */
#define default_domain_size_z               1.0e+18 /* m  */
#define default_mass_maximum                1.0e+18 /* kg */
#define default_time_interval               1.0e+18 /* s  */
#define default_number_of_particles         1000
#define default_number_of_timesteps         100
#define default_timesteps_between_outputs   default_number_of_timesteps
#define default_random_seed                 12345
#define default_base_filename               "nbody"

#define program_failure_code               -1
#define program_success_code                0

static float mass_maximum;

#define DEFAULT_STRING_LENGTH  1023

static const int    default_string_length = DEFAULT_STRING_LENGTH;
static const int    server = 0;
static char         base_filename[DEFAULT_STRING_LENGTH+1];
static float        domain_size_x;
static float        domain_size_y;
static float        domain_size_z;
static float        time_interval;
static int          number_of_particles;
static int          number_of_timesteps;
static int          timesteps_between_outputs;
static unsigned int random_seed;

int N_DEVICES=1;

cl_device_id* device_id;             // compute device id 
cl_context context;                 // compute context
cl_command_queue* commands;          // compute command queue
cl_program program;                 // compute program
cl_mem* inputs;
cl_mem* outputs;


static void Particle_input_arguments();
static Particle* Particle_array_construct(int number_of_particles);
static void Particle_array_initialize(Particle* this_particle_array, int number_of_particles);
static void Particle_array_output(char* base_filename, Particle* this_particle_array, int number_of_particles, int timestep);
static Particle* Particle_array_destruct(Particle* this_particle_array, int number_of_particles);
static void Particle_array_output_xyz(FILE * fileptr, Particle* this_particle_array, int number_of_particles);
void Particle_broadcast_arguments();  
	
void Particle_array_communicate_positions ( Particle *this_particle_array, int num_particles, int, int first_local_particle, int last_local_particle );

void Particle_array_calculate_forces(Particle* this_particle_array, Particle *output_array, int number_of_particles, int particles_per_process, float time_interval, int first_local, int last_local ) {
        const int bs = particles_per_process/N_DEVICES;
		cl_int err;
        int i;
		int n_device;
		cl_kernel kernels[N_DEVICES];
		//Prepare and launch kernels
        for ( i = first_local; i < first_local+particles_per_process; i += bs )
        {   
			n_device=(i-first_local)/bs;
			err = clEnqueueWriteBuffer(commands[n_device], inputs[n_device], CL_FALSE, 0, sizeof(Particle) * number_of_particles, this_particle_array, 0, NULL, NULL);
			if (err != CL_SUCCESS)
			{
				printf("Error: Failed to write to source array %d into device %d!\n",err,n_device);
				exit(1);
			}
		    kernels[n_device] = clCreateKernel(program, "calculate_force_kernel", &err);
		    if (!kernels[n_device] || err != CL_SUCCESS)
			{
				printf("Error: Failed to create compute kernel %d!\n",err);
				exit(1);
			}
			int last_particle=i+bs-1;
			clSetKernelArg(kernels[n_device], 0, sizeof(int), &bs);
			clSetKernelArg(kernels[n_device], 1, sizeof(float), &time_interval);
			clSetKernelArg(kernels[n_device], 2, sizeof(int), &number_of_particles);
			clSetKernelArg(kernels[n_device], 3, sizeof(cl_mem),&inputs[n_device]);
			clSetKernelArg(kernels[n_device], 4, sizeof(cl_mem),&outputs[n_device]);
			clSetKernelArg(kernels[n_device], 5, sizeof(int), &i );
			clSetKernelArg(kernels[n_device], 6, sizeof(int), &last_particle);	

			size_t local_size_ptr[1];
			size_t global_size_ptr[1];
			local_size_ptr[0]=128;
			global_size_ptr[0]=bs;
			local_size_ptr[0] = global_size_ptr[0] < local_size_ptr[0] ? global_size_ptr[0] : local_size_ptr[0];
			global_size_ptr[0] = global_size_ptr[0] < local_size_ptr[0] ? global_size_ptr[0] : global_size_ptr[0] + (global_size_ptr[0] % local_size_ptr[0] == 0 ? 0 : local_size_ptr[0] - global_size_ptr[0] % local_size_ptr[0]);
         	
			clEnqueueNDRangeKernel(commands[n_device], kernels[n_device], 1, NULL, global_size_ptr, local_size_ptr, 0, NULL, NULL);
		    //calculate_force_func(bs,time_interval,number_of_particles,this_particle_array, &output_array[i], i, i+bs-1);	
			clEnqueueReadBuffer(commands[n_device], outputs[n_device], CL_FALSE, 0, sizeof(Particle) * bs, &output_array[i], 0, NULL, NULL ); 		
        }
		//Wait for end and release
		for (i=0; i<N_DEVICES;++i){
			clFinish(commands[i]);
			clReleaseKernel(kernels[i]);
		}
}


double wall_time ( )
{
   struct timespec ts;
   double res;

   clock_gettime(CLOCK_MONOTONIC,&ts);

   res = (double) (ts.tv_sec)  + (double) ts.tv_nsec * 1.0e-9;

   return res;
}

int main (int argc, char** argv)
{ /* main */
    if (argc > 2) {
		N_DEVICES=atoi(argv[2]);
    }	
	//Dirty way to initialise :)
	cl_mem inputs_aux[N_DEVICES];
	cl_mem outputs_aux[N_DEVICES];
	cl_device_id device_id_aux[N_DEVICES];
	cl_command_queue commands_aux[N_DEVICES];
	inputs=inputs_aux;
	outputs=outputs_aux;
	device_id=device_id_aux;             // compute device id 
    commands=commands_aux;          // compute command queue
	
    Particle* particle_array = (Particle*)NULL;
    Particle* particle_array2 = (Particle*)NULL;
    int       timestep;
    int       mpi_error_code, my_rank, number_of_processes;

    mpi_error_code = MPI_Init(&argc, &argv); 
    mpi_error_code = MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
    mpi_error_code = MPI_Comm_size(MPI_COMM_WORLD, &number_of_processes);

    fprintf(stderr,"MPI %d of %d starting...\n",my_rank, number_of_processes);

       FILE *input_data = fopen(argv[1], "r");
    Particle_input_arguments(input_data);

    particle_array = Particle_array_construct(number_of_particles);
    particle_array2 = Particle_array_construct(number_of_particles);

    Particle_array_initialize(particle_array, number_of_particles);

	FILE * fileptr = fopen("nbody_out.xyz", "w");
	if (my_rank == server){
		Particle_array_output_xyz(fileptr, particle_array, number_of_particles);
	}
	char mpi_node_name[256]; int len;
	MPI_Get_processor_name(mpi_node_name, &len);

    int   particles_per_process;
    int   first_local_particle, last_local_particle;

    if (number_of_particles <= 1) return 0;

	int i;
    for ( i = 0; i < number_of_particles; i++) {
      particle_array2[i].mass = particle_array[i].mass;
    }
    double start = wall_time();

    particles_per_process = number_of_particles/number_of_processes;
    first_local_particle = my_rank * particles_per_process;
    last_local_particle = first_local_particle + particles_per_process - 1;
	
	int gpu=1;
	cl_int err;
	cl_platform_id plats[4];
	clGetPlatformIDs(4,plats,NULL);

	err = clGetDeviceIDs(plats[0], gpu ? CL_DEVICE_TYPE_GPU : CL_DEVICE_TYPE_CPU, N_DEVICES, device_id, NULL);
    if (err != CL_SUCCESS)
    {
        printf("Error: Failed to create a device group! %d\n",err);
        return EXIT_FAILURE;
    }
	context = clCreateContext(0, N_DEVICES, device_id, NULL, NULL, &err);
    if (!context)
    {
        printf("Error: Failed to create a compute context!\n");
        return EXIT_FAILURE;
    }
	for (i=0; i<N_DEVICES;i++){	
	  char device_name[200] = "";
	  clGetDeviceInfo
				  (device_id[i], CL_DEVICE_NAME, 200, device_name,
				   NULL);
	  printf("Device: \"%s\"\n", device_name);
		commands[i] = clCreateCommandQueue(context, device_id[i], 0, &err);
		if (!commands[i])
		{
			printf("Error: Failed to create a command commands!\n");
			return EXIT_FAILURE;
		}
	}
	char* ompss_code;    
    FILE *fp;
	size_t source_size;
	fp = fopen("opencl_kernels.cl", "r");
	if (!fp) {
		printf("Failed to open file when loading kernel from file\n");
		return EXIT_FAILURE;
	}      
	fseek(fp, 0, SEEK_END); // seek to end of file;
	size_t size = ftell(fp); // get current file pointer
	fseek(fp, 0, SEEK_SET); // seek back to beginning of file
	ompss_code = (char*) malloc(sizeof(char)*size);
	source_size = fread( ompss_code, 1, size, fp);
	fclose(fp); 
	ompss_code[size]=0;

	for (i = 0; i< N_DEVICES; i++){
		inputs[i] = clCreateBuffer(context,  CL_MEM_READ_WRITE, sizeof(Particle) * number_of_particles, NULL, &err);
		if (err != CL_SUCCESS){
			printf("Error: Failed to create buffer, err %d!\n",err);
			return EXIT_FAILURE;
		}
		outputs[i] = clCreateBuffer(context,  CL_MEM_READ_WRITE,  sizeof(Particle) * particles_per_process/N_DEVICES, NULL, &err);
		if (err != CL_SUCCESS){
			printf("Error: Failed to create buffer, err %d!\n",err);
			return EXIT_FAILURE;
		}
	}
	
	program = clCreateProgramWithSource(context, 1, (const char **) &ompss_code, NULL, &err);
    if (!program)
    {
        printf("Error: Failed to create compute program!\n");
        return EXIT_FAILURE;
    }

	clBuildProgram(program, 0, NULL, "", NULL, NULL);
	
    for (timestep = 1; timestep <= number_of_timesteps; timestep++) {

        if ((timestep % timesteps_between_outputs) == 0 && my_rank == server) fprintf(stderr, "Starting timestep #%d.\n", timestep);
		

        Particle_array_calculate_forces(particle_array, particle_array2 , number_of_particles , particles_per_process,time_interval, first_local_particle, last_local_particle);
		Particle_array_communicate_positions(particle_array2, number_of_particles, particles_per_process, first_local_particle, last_local_particle);
		
       /* swap arrays */
       Particle * tmp = particle_array;
       particle_array = particle_array2;
       particle_array2 = tmp;
    } /* for timestep */

    double end = wall_time ();

    if ( my_rank == server )
    {
      printf("Timesteps: %d s.\n", number_of_timesteps);
      printf("Time in seconds: %g s.\n", end - start);
      printf("Particles per second: %g \n", (number_of_particles*number_of_timesteps)/(end-start));
	  if ((number_of_timesteps % timesteps_between_outputs) != 0) {
		Particle_array_output_xyz(fileptr, particle_array, number_of_particles);
	  }
    }


    particle_array = Particle_array_destruct(particle_array, number_of_particles);
    
    if (fclose(fileptr) != 0) {
		fprintf(stderr, "ERROR: can't close the output file.\n");
        exit(program_failure_code);
	}

	for (i=0; i<N_DEVICES;i++){	
		clReleaseMemObject(inputs[i]);
		clReleaseMemObject(outputs[i]);
		clReleaseCommandQueue(commands[i]);
		clReleaseContext(context);
	}
	
    mpi_error_code = MPI_Finalize();
    return program_success_code;

} /* main */

/*
 * Get command line arguments.
 */
void Particle_input_arguments (FILE *input)
{ /* Particle_input_arguments */
    int my_rank, mpi_error_code;

    number_of_particles       = default_number_of_particles;
    domain_size_x             = default_domain_size_x;
    domain_size_y             = default_domain_size_y;
    domain_size_z             = default_domain_size_z;
    time_interval             = default_time_interval;
    number_of_timesteps       = default_number_of_timesteps;
    timesteps_between_outputs = default_timesteps_between_outputs;
    random_seed               = default_random_seed;
    mass_maximum              = default_mass_maximum;

    mpi_error_code = MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);

    if (my_rank==server) {

	    if (fscanf(input, "%d", &number_of_particles) != 1) {

	        fprintf(stderr,
	            "ERROR: cannot read number of particles from standard input!\n");

	        exit(program_failure_code);

	    } /* if (scanf("%d", &number_of_particles) != 1) */

	    if (number_of_particles < 1) {

	        fprintf(stderr, "ERROR: cannot have %d particles!\n",
	            number_of_particles);

	        exit(program_failure_code);

	    } /* if (number_of_particles < 1) */

	    if (number_of_particles == 1) {

	        fprintf(stderr,
	            "There is only one particle, therefore no forces.\n");

	        exit(program_failure_code);

	    } /* if (number_of_particles == 1) */

	    if (fscanf(input, "%f", &domain_size_x) != 1) {

	        fprintf(stderr,
	            "ERROR: cannot read domain size X from standard input!\n");

	        exit(program_failure_code);

	    } /* if (scanf("%f", &domain_size_x) != 1) */

	    if (domain_size_x <= 0.0) {

	        fprintf(stderr,
	            "ERROR: cannot have a domain whose X dimension has length %f!\n",
	            domain_size_x);

	        exit(program_failure_code);

	    } /* if (domain_size_x <= 0.0) */

	    if (fscanf(input, "%f", &domain_size_y) != 1) {
               
	        fprintf(stderr,
	            "ERROR: cannot read domain size Y from standard input!\n");

	        exit(program_failure_code);

	    } /* if (scanf("%f", &domain_size_y) != 1) */

	    if (domain_size_y <= 0.0) {

	        fprintf(stderr,
	            "ERROR: cannot have a domain whose Y dimension has length %f!\n",
	            domain_size_y);

	        exit(program_failure_code);

	    } /* if (domain_size_y <= 0.0) */

	    if (fscanf(input,"%f", &domain_size_z) != 1) {

	        fprintf(stderr,
	            "ERROR: cannot read domain size Z from standard input!\n");

	        exit(program_failure_code);

	    } /* if (scanf("%f", &domain_size_z) != 1) */

	    if (domain_size_z <= 0.0) {

	        fprintf(stderr,
	            "ERROR: cannot have a domain whose Z dimension has length %f!\n",
	            domain_size_z);

	        exit(program_failure_code);

	    } /* if (domain_size_z <= 0.0) */

	    if (fscanf(input, "%f", &time_interval) != 1) {

	        fprintf(stderr,
	            "ERROR: cannot read time interval from standard input!\n");

	        exit(program_failure_code);

	    } /* if (scanf("%f", &time_interval) != 1) */

	    if (time_interval <= 0.0) {

	        fprintf(stderr,
	            "ERROR: cannot have a time interval of %f!\n",
	            time_interval);

	        exit(program_failure_code);

	    } /* if (time_interval <= 0.0) */

	    if (fscanf(input, "%d", &number_of_timesteps) != 1) {

	        fprintf(stderr,
	            "ERROR: cannot read number of timesteps from standard input!\n");

	        exit(program_failure_code);

	    } /* if (scanf("%d", &number_of_timesteps) != 1) */

	    if (number_of_timesteps <= 0) {

	        fprintf(stderr, "ERROR: cannot have %d timesteps!\n",
	            number_of_timesteps);

	        exit(program_failure_code);

	    } /* if (number_of_timesteps <= 0) */

	    if (fscanf(input, "%d", &timesteps_between_outputs) != 1) {

	        fprintf(stderr,
	        "ERROR: cannot read timesteps between outputs from standard input!\n");

	        exit(program_failure_code);

	    } /* if (scanf("%d", &timesteps_between_outputs) != 1) */

	    if (timesteps_between_outputs <= 0) {

	        fprintf(stderr,
	            "ERROR: cannot have %d timesteps between outputs!\n",
	            timesteps_between_outputs);

	        exit(program_failure_code);

	    } /* if (timesteps_between_outputs <= 0) */

	    if (fscanf(input, "%d", &random_seed) != 1) {

	        fprintf(stderr,
	            "ERROR: cannot read random seed from standard input!\n");

	        exit(program_failure_code);

	    } /* if (scanf("%d", &random_seed) != 1) */

	    if (fscanf(input, "%f", &mass_maximum) != 1) {

	        fprintf(stderr,
	            "ERROR: cannot read mass maximum from standard input!\n");

	        exit(program_failure_code);

	    } /* if (scanf("%f", &mass_maximum) != 1) */

	    if (mass_maximum <= 0.0) {

	        fprintf(stderr,
	            "ERROR: cannot have a maximum mass of %f!\n",
	            mass_maximum);

	        exit(program_failure_code);

	    } /* if (mass_maximum <= 0.0) */

	    fgetc(input);
	    fgets(base_filename, default_string_length, input);
	    if (base_filename[strlen(base_filename)-1] == '\n') {
	        base_filename[strlen(base_filename)-1] = '\0';
	    } /* if (base_filename[strlen(base_filename)-1]...) */

	    //fprintf(stderr, "%d: base_filename=|%s|\n",
	    //    my_rank, base_filename);
	} /* if (my_rank == server) */ 

    Particle_broadcast_arguments();
} /* Particle_input_arguments */

void Particle_broadcast_arguments ()
{

    int mpi_error_code, base_filename_length;

    mpi_error_code =
        MPI_Bcast(&number_of_particles, 1, MPI_INT, server,
            MPI_COMM_WORLD);
 
    mpi_error_code =
        MPI_Bcast(&domain_size_x, 1, MPI_FLOAT, server,
            MPI_COMM_WORLD);

    mpi_error_code =
        MPI_Bcast(&domain_size_y, 1, MPI_FLOAT, server,
            MPI_COMM_WORLD);

    mpi_error_code =
        MPI_Bcast(&domain_size_z, 1, MPI_FLOAT, server,
            MPI_COMM_WORLD);

    mpi_error_code =
        MPI_Bcast(&time_interval, 1, MPI_FLOAT, server,
            MPI_COMM_WORLD);

    mpi_error_code =
        MPI_Bcast(&number_of_timesteps, 1, MPI_INT, server,
            MPI_COMM_WORLD);

    mpi_error_code =
        MPI_Bcast(&timesteps_between_outputs, 1, MPI_INT, server,
            MPI_COMM_WORLD);

    mpi_error_code =
        MPI_Bcast(&random_seed, 1, MPI_UNSIGNED, server,
            MPI_COMM_WORLD);

    mpi_error_code =
        MPI_Bcast(&mass_maximum, 1, MPI_FLOAT, server,
            MPI_COMM_WORLD);
    
    base_filename_length = strlen(base_filename);

    mpi_error_code =
        MPI_Bcast(&base_filename_length, 1, MPI_INT, server,
            MPI_COMM_WORLD);

    mpi_error_code =
        MPI_Bcast(base_filename, base_filename_length+1,
            MPI_CHAR, server, MPI_COMM_WORLD);

}


#ifdef CHECK
/*
 * Check that the particle exists.
 */
void Particle_check (Particle* this_particle, char* action, char* routine)
{ /* Particle_check */

    if (this_particle != (Particle*)NULL) return;

    fprintf(stderr, "ERROR: can't %s a nonexistent particle\n",
        ((action == (char*)NULL) || (strlen(action) == 0)) ?
            "perform an unknown action on" : action);
    fprintf(stderr, "  in %s\n",
        ((routine == (char*)NULL) || (strlen(routine) == 0)) ?
            "an unknown routine" : routine);

    exit(program_failure_code);

} /* Particle_check */

/*
 * Check that the pair of particles both exist.
 */
void Particle_pair_check (Particle* this_particle1, Particle* this_particle2,
         char* action, char* routine)
{ /* Particle_pair_check */

    if ((this_particle1 != (Particle*)NULL) &&
        (this_particle2 != (Particle*)NULL))
        return;

    fprintf(stderr, "ERROR: can't %s\n",
        ((action == (char*)NULL) || (strlen(action) == 0)) ?
            "perform an unknown action on" : action);

    if (this_particle1 == (Particle*)NULL) {

        if (this_particle2 == (Particle*)NULL) {

            fprintf(stderr, "  a pair of nonexistent particles\n");

        } /* if (this_particle2 == (Particle*)NULL) */
        else {

            fprintf(stderr,
                "  a nonexistent particle and an existing particle\n");

        } /* if (this_particle2 == (Particle*)NULL)...else */

    } /* if (this_particle1 == (Particle*)NULL) */
    else {

        fprintf(stderr,
            "  an existing particle and a nonexistent particle\n");

    } /* if (this_particle1 == (Particle*)NULL)...else */

    fprintf(stderr, "  in %s\n",
        ((routine == (char*)NULL) || (strlen(routine) == 0)) ?
            "an unknown routine" : routine);

    exit(program_failure_code);

} /* Particle_pair_check */
#endif /* #ifdef CHECK */

/*
 * Clear the particle's data.
 */
void Particle_clear (Particle* this_particle)
{ /* Particle_clear */

#ifdef CHECK
    Particle_check(this_particle, "clear", "Particle_clear");
#endif /* #ifdef CHECK */

    this_particle->position_x = 0.0;
    this_particle->position_y = 0.0;
    this_particle->position_z = 0.0;
    this_particle->velocity_x = 0.0;
    this_particle->velocity_y = 0.0;
    this_particle->velocity_z = 0.0;
    this_particle->mass       = 0.0;

} /* Particle_clear */

/*
 * Construct the particle.
 */
void Particle_construct (Particle* this_particle)
{ /* Particle_construct */

#ifdef CHECK
    Particle_check(this_particle, "construct", "Particle_construct");
#endif /* #ifdef CHECK */

    Particle_clear(this_particle);

} /* Particle_construct */

/*
 * Destroy the particle.
 */
void Particle_destruct (Particle* this_particle)
{ /* Particle_destruct */

#ifdef CHECK
    Particle_check(this_particle, "destruct", "Particle_destruct");
#endif /* #ifdef CHECK */

    Particle_clear(this_particle);

} /* Particle_destruct */

/*
 * Initialize the particle by setting its data randomly.
 */
void Particle_set_position_randomly (Particle* this_particle)
{ /* Particle_set_position_randomly */

#ifdef CHECK
    Particle_check(this_particle, "randomly set the position",
        "Particle_set_randomly");
#endif /* #ifdef CHECK */

    this_particle->position_x =
        domain_size_x * ((float) random() / ((float)RAND_MAX + 1.0));
    this_particle->position_y =
        domain_size_y * ((float) random() / ((float)RAND_MAX + 1.0));
    this_particle->position_z =
        domain_size_z * ((float) random() / ((float)RAND_MAX + 1.0));

} /* Particle_set_position_randomly */

/*
 * Initialize the particle by setting its data randomly.
 */
void Particle_initialize_randomly (Particle* this_particle)
{ /* Particle_initialize_randomly */

#ifdef CHECK
    Particle_check(this_particle, "randomly initialize",
        "Particle_initialize_randomly");
#endif /* #ifdef CHECK */

    Particle_clear(this_particle);

    Particle_set_position_randomly(this_particle);

    this_particle->mass       =
        mass_maximum  * ((float) random() / ((float)RAND_MAX + 1.0));

} /* Particle_initialize_randomly */

/*
 * Initialize the particle.
 */
void Particle_initialize (Particle* this_particle)
{ /* Particle_initialize */

#ifdef CHECK
    Particle_check(this_particle, "initialize", "Particle_initialize");
#endif /* #ifdef CHECK */

    Particle_initialize_randomly(this_particle);

} /* Particle_initialize */



void Particle_output (FILE* fileptr, Particle* this_particle)
{ /* Particle_output */

    fprintf(fileptr, "%g %g %g %g %g %g %g\n",
        this_particle->position_x,
        this_particle->position_y,
        this_particle->position_z,
        this_particle->velocity_x,
        this_particle->velocity_y,
        this_particle->velocity_z,
        this_particle->mass);

} /* Particle_output */

void Particle_output_xyz (FILE* fileptr, Particle* this_particle) {
	fprintf(fileptr, "C %g %g %g\n",
		this_particle->position_x,
        this_particle->position_y,
		this_particle->position_z);
}

#ifdef CHECK
void Particle_array_check (
         Particle* this_particle_array, int number_of_particles,
         char* action, char* routine)
{ /* Particle_array_check */

    if (number_of_particles < 0) {

        fprintf(stderr, "ERROR: illegal number of particles %d\n",
            number_of_particles);
        fprintf(stderr, "  to %s\n", 
            ((action == (char*)NULL) || (strlen(action) == 0)) ?
                "perform an unknown action on" : action);
        fprintf(stderr, "  in %s\n",
            ((routine == (char*)NULL) || (strlen(routine) == 0)) ?
                "an unknown routine" : routine);

        exit(program_failure_code);

    } /* if (number_of_particles < 0) */

    if (number_of_particles == 0) {

        if (this_particle_array == (Particle*)NULL) return (Particle*)NULL;

        fprintf(stderr, "ERROR: can't %s\n",
            ((action == (char*)NULL) || (strlen(action) == 0)) ?
                "perform an unknown action on" : action);
        fprintf(stderr, "  an existing particle array of length 0\n");
        fprintf(stderr, "  in %s\n",
            ((routine == (char*)NULL) || (strlen(routine) == 0)) ?
                "an unknown routine" : routine);

        exit(program_failure_code);

    } /* if (number_of_particles == 0) */
    if (this_particle_array == (Particle*)NULL) {

        fprintf(stderr, "ERROR: can't %s\n",
            ((action == (char*)NULL) || (strlen(action) == 0)) ?
                "perform an unknown action on" : action);
        fprintf(stderr, "  a nonexistent array of %d particles\n",
            number_of_particles);
        fprintf(stderr, "  in %s\n",
            ((routine == (char*)NULL) || (strlen(routine) == 0)) ?
                "an unknown routine" : routine);

        exit(program_failure_code);

    } /* if (this_particle_array == (Particle*)NULL) */

} /* Particle_array_check */
#endif /* #ifdef CHECK */

/*
 * Allocate and return an array of particles.
 */
Particle* Particle_array_allocate (int number_of_particles)
{ /* Particle_array_allocate */
    Particle* this_particle_array = (Particle*)NULL;

#ifdef CHECK
    if (number_of_particles < 0) {

        fprintf(stderr,
            "ERROR: illegal number of particles %d to allocate\n", 
            number_of_particles);
        fprintf(stderr, "  in Particle_array_construct\n");

        exit(program_failure_code);

    } /* if (number_of_particles < 0) */
#endif /* #ifdef CHECK */

    if (number_of_particles == 0) return (Particle*)NULL;

    this_particle_array =
        (Particle*)malloc(sizeof(Particle) * number_of_particles);

    if (this_particle_array == (Particle*)NULL) {

        fprintf(stderr,
            "ERROR: can't allocate a particle array of %d particles\n", 
            number_of_particles);
        fprintf(stderr, "  in Particle_array_construct\n");

        exit(program_failure_code);

    } /* if (this_particle_array == (Particle*)NULL) */

    return this_particle_array;

} /* Particle_array_allocate */

/*
 * Construct and return an array of particles, cleared.
 */
Particle* Particle_array_construct (int number_of_particles)
{ /* Particle_array_construct */
    Particle* this_particle_array = (Particle*)NULL;
    int index;

    this_particle_array =
        Particle_array_allocate(number_of_particles);

    for (index = 0; index < number_of_particles; index++) {

        Particle_construct(&(this_particle_array[index]));

    } /* for index */

    return this_particle_array;

} /* Particle_array_construct */

/*
 * Deallocate the array of particles, and return NULL.
 */
Particle* Particle_array_deallocate (Particle* this_particle_array,
              int number_of_particles)
{ /* Particle_array_deallocate */

#ifdef CHECK
    Particle_array_check(this_particle_array, number_of_particles,
        "deallocate", "Particle_array_deallocate");
#endif /* #ifdef CHECK */

    free(this_particle_array);

    this_particle_array = (Particle*)NULL;

    return (Particle*)NULL;

} /* Particle_array_deallocate */

/*
 * Destroy the array of particles, and return NULL.
 */
Particle* Particle_array_destruct (Particle* this_particle_array,
              int number_of_particles)
{ /* Particle_array_destruct */
    int index;

#ifdef CHECK
    Particle_array_check(this_particle_array, number_of_particles,
        "destroy", "Particle_array_destruct");
#endif /* #ifdef CHECK */

    for (index = number_of_particles - 1; index >= 0; index--) {

        Particle_destruct(&(this_particle_array[index]));

    } /* for index */

    return Particle_array_deallocate(this_particle_array, number_of_particles);

} /* Particle_array_destruct */

/*
 * Initialize the array of particles by setting its data randomly.
 */
void Particle_array_initialize_randomly (
         Particle* this_particle_array, int number_of_particles)
{ /* Particle_array_initialize_randomly */
    int index;

#ifdef CHECK
    Particle_array_check(this_particle_array, number_of_particles,
        "initialize randomly", "Particle_array_initialize_randomly");
#endif /* #ifdef CHECK */

    for (index = 0; index < number_of_particles; index++) {

        Particle_initialize_randomly(&(this_particle_array[index]));

    } /* for index */

} /* Particle_array_initialize_randomly */

/*
 * Initialize the array of particles.
 */
void Particle_array_initialize (
         Particle* this_particle_array, int number_of_particles)
{ /* Particle_array_initialize */

    Particle_array_initialize_randomly(
        this_particle_array, number_of_particles);

} /* Particle_array_initialize */


void Particle_array_communicate_positions ( Particle *this_particle_array, int num_particles, int particles_per_process,
                                         int first_local_particle, int last_local_particle )
{
    int index1;
    int mpi_error_code;
    float * local_array =
        (float*)malloc(sizeof(float) * particles_per_process * 6);

    float * global_array =
        (float*)malloc(sizeof(float) * number_of_particles * 6);

    for (index1 = first_local_particle;
         index1 <= last_local_particle; index1++) {

        local_array[(index1 - first_local_particle) * 6] =
            this_particle_array[index1].position_x;
			//printf("preparar %f\n",local_array[(index1 - first_local_particle) * 7]);

        local_array[(index1 - first_local_particle) * 6 + 1] =
            this_particle_array[index1].position_y;

        local_array[(index1 - first_local_particle) * 6 + 2] =
            this_particle_array[index1].position_z;
			
        local_array[(index1 - first_local_particle) * 6 + 3] =
            this_particle_array[index1].velocity_x;
			
        local_array[(index1 - first_local_particle) * 6 + 4] =
            this_particle_array[index1].velocity_y;
			
        local_array[(index1 - first_local_particle) * 6 + 5] =
            this_particle_array[index1].velocity_z;
			

    } /*for (index1) */

//      if (my_rank == server) {
//          fprintf(stderr, "%d: particles_per_process=%d\n",
//              my_rank, particles_per_process);
//
//          fprintf(stderr, "%d: number_of_particles=%d\n",
//              my_rank, number_of_particles);
//      }

    mpi_error_code =
        MPI_Allgather(
            local_array,  particles_per_process*6 , MPI_FLOAT,
            global_array, particles_per_process*6, MPI_FLOAT,
            MPI_COMM_WORLD);

    for (index1 = 0; index1 < number_of_particles; index1++) {

        this_particle_array[index1].position_x =
            global_array[index1 * 6];
			
        this_particle_array[index1].position_y =
            global_array[index1 * 6 + 1];

        this_particle_array[index1].position_z =
            global_array[index1 * 6 + 2];
			
        this_particle_array[index1].velocity_x =
            global_array[index1 * 6 + 3];
			
        this_particle_array[index1].velocity_y =
            global_array[index1 * 6 + 4];
			
        this_particle_array[index1].velocity_z =
            global_array[index1 * 6 + 5];
			

    } /*for (index1) */

    free(local_array);
    free(global_array);

}


void Particle_array_output (char* base_filename,
         Particle* this_particle_array, int number_of_particles, int timestep)
{ /* Particle_array_output */
    FILE* fileptr  = (FILE*)NULL;
    char* filename = (char*)NULL;
    int   filename_length;
    int   index;
    int   my_rank, mpi_error_code;

#ifdef CHECK
    Particle_array_check(this_particle_array, number_of_particles,
        "output", "Particle_array_output");
#endif /* #ifdef CHECK */

    mpi_error_code = MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);

    if (my_rank != server) return;


    filename_length = strlen(base_filename) + 1 + 8 + 1 + 3;

    filename = (char*)malloc(sizeof(char) * (filename_length + 1));

    if (filename == (char*)NULL) {

        fprintf(stderr, "ERROR: can't allocate the filename string\n");
        fprintf(stderr, "  %s_%8.8d.txt\n", base_filename, timestep);
        fprintf(stderr, "  in Particle_array_output\n");

        exit(program_failure_code);

    } /* if (filename == (char*)NULL) */

    sprintf(filename, "%s_%8.8d.txt", base_filename, timestep);

    fileptr = fopen(filename, "w");

    if (fileptr == (FILE*)NULL) {

        fprintf(stderr, "ERROR: can't open the output file named\n");
        fprintf(stderr, "  %s\n", filename);
        fprintf(stderr, "  in Particle_array_output\n");

        exit(program_failure_code);

    } /* if (fileptr == (FILE*)NULL) */

    fprintf(fileptr, "%d %d %d %g %g %g %g %g %d\n",
        number_of_particles, number_of_timesteps, timesteps_between_outputs,
        domain_size_x, domain_size_y, domain_size_z,
        mass_maximum, time_interval,
        random_seed);

    fprintf(fileptr, "%d\n", timestep);

    for (index = 0; index < number_of_particles; index++) {

        Particle_output(fileptr, &(this_particle_array[index]));

    } /* for index */

    if (fclose(fileptr) != 0) {

        fprintf(stderr, "ERROR: can't close the output file named\n");
        fprintf(stderr, "  %s\n", filename);
        fprintf(stderr, "  in Particle_array_output\n");

        exit(program_failure_code);

    } /* if (fclose(fileptr) != 0) */

    fileptr = (FILE*)NULL;

    free(filename); filename = (char*)NULL;

} /* Particle_array_output */

/* Outputs particle positions in a format that VMD can easily visualize. */
static void Particle_array_output_xyz(FILE * fileptr, 
				Particle* this_particle_array, int number_of_particles) {
	
	fprintf(fileptr, "%d\nNBody\n", number_of_particles);

	int index;
    for (index = 0; index < number_of_particles; index++) {

        Particle_output_xyz(fileptr, &(this_particle_array[index]));

    } /* for index */
} /* Particle_array_output_xyz */
