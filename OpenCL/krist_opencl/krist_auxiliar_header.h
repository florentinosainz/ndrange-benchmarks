
#define SHAREDSIZE 16384-2048

#define DIM2_H 4
#define DIM2_A 4
#define DIM2_E 2

#if DIM2_H == 4
#define TYPE_H float4
#endif
#if DIM2_H == 3
#define TYPE_H float3
#endif
#if DIM2_A == 4
#define TYPE_A float4
#endif
#if DIM2_A == 3
#define TYPE_A float3
#endif
#if DIM2_E == 4
#define TYPE_E float4
#endif
#if DIM2_E == 3
#define TYPE_E float3
#endif
#if DIM2_E == 2
#define TYPE_E float2
#endif
__constant int shsize=SHAREDSIZE;

#ifdef __cplusplus
extern "C"
{
#endif

#ifndef __OPENCL_VERSION__
#pragma omp target device(opencl) copy_deps ndrange(1,NE,128) shmem(shsize)
#pragma omp task in([NA] a, [NH] h) out([NE] E_out) 
__kernel void cstructfac(int nc, float f2,
                          int NA, __global float* a, int NH , __global float* h, int NE, __global float* E_out, __local TYPE_A* ashared);
#endif

#ifdef __cplusplus
}
#endif
