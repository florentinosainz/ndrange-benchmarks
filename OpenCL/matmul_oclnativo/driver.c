#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include "CL/opencl.h"

#include "driver.h"

#ifdef DP
#define REAL double
#else
#define REAL float
#endif

int N_DEVICES= 1;



cl_device_id* device_id;             // compute device id 
cl_context context;                 // compute context
cl_command_queue* commands;          // compute command queue
cl_program program;                 // compute program
cl_mem* inputs_A;
cl_mem*  inputs_H;
cl_mem*  outputs;

const int NB = BSIZE;


void matmul( int m, int l, int n, int mDIM, int lDIM, int nDIM, REAL **tileA, REAL **tileB,
             REAL **tileC )
{	
	int i, j, k;
	int n_device=0;
	cl_kernel kernels[N_DEVICES];
	cl_int err;

	cl_event events_dep[mDIM+nDIM];
	for (i=0; i<mDIM+nDIM;++i){
		clEnqueueMarker(commands[0], &events_dep[i]);
	}
	int inA[N_DEVICES];
	int inH[N_DEVICES];
	for (i=0; i<N_DEVICES;++i){
		inA[i]=-1;
		inH[i]=-1;
		kernels[i] = clCreateKernel(program, "Muld", &err);
		n_device=i;
		clSetKernelArg(kernels[n_device], 0, sizeof(cl_mem),&inputs_A[n_device]);
		clSetKernelArg(kernels[n_device], 1, sizeof(cl_mem),&inputs_H[n_device]);
		clSetKernelArg(kernels[n_device], 2, sizeof(int), &NB);
		clSetKernelArg(kernels[n_device], 3, sizeof(int), &NB);
		clSetKernelArg(kernels[n_device], 4, sizeof(cl_mem),&outputs[n_device]);
		clSetKernelArg(kernels[n_device], 5, sizeof(int), &NB);
	}
	n_device=0;
	for(i = 0;i < mDIM; i++){
		for (j = 0; j < nDIM; j++){
			err = clEnqueueWriteBuffer(commands[n_device], outputs[n_device], CL_FALSE, 0, sizeof(REAL) * NB * NB, tileC[i*nDIM+j],1, &events_dep[i+j], NULL);
			if (err != CL_SUCCESS)
			{
				printf("Error: Failed to write to source array %d into device %d!\n",err,n_device);
				exit(1);
			}
			for (k = 0; k < lDIM; k++){
				//Avoid extra copies if the data is already in
				if (inA[n_device]!=i*lDIM+k){
					err = clEnqueueWriteBuffer(commands[n_device], inputs_A[n_device], CL_FALSE, 0, sizeof(REAL) * NB * NB, tileA[i*lDIM+k], 0, NULL, NULL);
					if (err != CL_SUCCESS)
					{
						printf("Error: Failed to write to source array %d into device %d!\n",err,n_device);
						exit(1);
					}
					inA[n_device]=i*lDIM+k;
				}
				
				//Avoid extra copies if the data is already in
				if (inH[n_device]!=k*nDIM+j){
					err = clEnqueueWriteBuffer(commands[n_device], inputs_H[n_device], CL_FALSE, 0, sizeof(REAL) * NB * NB, tileB[k*nDIM+j],0, NULL, NULL);
					if (err != CL_SUCCESS)
					{
						printf("Error: Failed to write to source array %d into device %d!\n",err,n_device);
						exit(1);
					}
					inH[n_device]=k*nDIM+j;
				}

				
				size_t local_size_ptr[2];
				size_t global_size_ptr[2];
				local_size_ptr[0]=16;
				local_size_ptr[1]=16;
				global_size_ptr[0]=NB;
				global_size_ptr[1]=NB;
				int p=0;
				for (p = 0; p < 2; ++p)
				{
					local_size_ptr[p] = global_size_ptr[p] < local_size_ptr[p] ? global_size_ptr[p] : local_size_ptr[p];
					global_size_ptr[p] = global_size_ptr[p] < local_size_ptr[p] ? global_size_ptr[p] : global_size_ptr[p] + (global_size_ptr[p] % local_size_ptr[p] == 0 ? 0 : local_size_ptr[p] - global_size_ptr[p] % local_size_ptr[p]);
				}		
				err=clEnqueueNDRangeKernel(commands[n_device], kernels[n_device], 2, NULL, global_size_ptr, local_size_ptr, 0, NULL, NULL);
				if (err != CL_SUCCESS)
				{
					printf("Error: Failed to launch kernel , err %d into device %d!\n",err,n_device);
					exit(1);
				}	
			


			}					
			clEnqueueReadBuffer(commands[n_device], outputs[n_device], CL_FALSE, 0, sizeof(REAL) * NB * NB, tileC[i*nDIM+j], 0, NULL, &events_dep[i+j] );
			n_device=(n_device+1)%N_DEVICES;	
		}
	}
	//Wait for end and release
	for (i=0; i<N_DEVICES;++i){
		clFinish(commands[i]);
		clReleaseKernel(kernels[i]);	
	}
	for (i=0; i<mDIM+nDIM;++i){
		clReleaseEvent(events_dep[i]);
	}
}



//#define BSIZE 1024

double   cclock( void );
int      check( int nrep, int m, int l, int n, int mDIM, int nDIM, REAL **c );
void     gendat(int, int, int, int, int, int, REAL **, REAL **, REAL **);
void     matmul( int, int, int, int, int, int, REAL **a, REAL **b, REAL **c );
void     prthead( void );
void     prtspeed( int, int, int, int, double, int, unsigned long );

int calcdim(int x)
{
        int dimval;
        if(x%BSIZE != 0)
                dimval = x/BSIZE + 1;
        else
                dimval = x/BSIZE;

        return dimval;
}
	
int main(int argc, char*argv[])
{ 

    if (argc > 1) {
		N_DEVICES=atoi(argv[1]);
    }
	//Dirty way to initialise :)
	cl_mem inputs_A_aux[N_DEVICES];
	cl_mem inputs_H_aux[N_DEVICES];
	cl_mem outputs_aux[N_DEVICES];
	cl_device_id device_id_aux[N_DEVICES];
	cl_command_queue commands_aux[N_DEVICES];
	inputs_A=inputs_A_aux;
	inputs_H=inputs_H_aux;
	outputs=outputs_aux;
	device_id=device_id_aux;             // compute device id 
    commands=commands_aux;          // compute command queue
	int      lda, m, l, n;
	int      mDIM, lDIM, nDIM;
	int      ok, nrep;
	unsigned long nops;
	int      i,k,j,o;
	REAL   **a, **b, **c;
	double   time;
	FILE     *inl;
// ------------------------------------------------------------------------	
	int gpu=1;
	cl_int err;
	cl_platform_id plats[4];
	clGetPlatformIDs(4,plats,NULL);

	err = clGetDeviceIDs(plats[0], gpu ? CL_DEVICE_TYPE_GPU : CL_DEVICE_TYPE_CPU, N_DEVICES, device_id, NULL);
    if (err != CL_SUCCESS)
    {
        printf("Error: Failed to create a device group! %d\n",err);
        return EXIT_FAILURE;
    }
	context = clCreateContext(0, N_DEVICES, device_id, NULL, NULL, &err);
    if (!context)
    {
        printf("Error: Failed to create a compute context!\n");
        return EXIT_FAILURE;
    }
	for (i=0; i<N_DEVICES;i++){	
	  char device_name[200] = "";
	  clGetDeviceInfo
				  (device_id[i], CL_DEVICE_NAME, 200, device_name,
				   NULL);
	  printf("Device: \"%s\"\n", device_name);
		commands[i] = clCreateCommandQueue(context, device_id[i], 0, &err);
		if (!commands[i])
		{
			printf("Error: Failed to create a command commands!\n");
			return EXIT_FAILURE;
		}
	}
	char* ompss_code;    
    FILE *fp;
	size_t source_size;
	fp = fopen("matmul_kernel.cl", "r");
	if (!fp) {
		printf("Failed to open file when loading kernel from file\n");
		return EXIT_FAILURE;
	}      
	fseek(fp, 0, SEEK_END); // seek to end of file;
	size_t size = ftell(fp); // get current file pointer
	fseek(fp, 0, SEEK_SET); // seek back to beginning of file
	ompss_code = (char*) malloc(sizeof(char)*size);
	source_size = fread( ompss_code, 1, size, fp);
	fclose(fp); 
	ompss_code[size]=0;	
	program = clCreateProgramWithSource(context, 1, (const char **) &ompss_code, NULL, &err);
    if (!program)
    {
        printf("Error: Failed to create compute program!\n");
        return EXIT_FAILURE;
    }
	clBuildProgram(program, 0, NULL, "", NULL, NULL);
	free(ompss_code);
	

	inl = fopen( "test.in", "r" );
	if (inl == 0) {
		printf("No input file 'test.in' found.\n");
		exit(1);
	}

	for (i = 0; i< N_DEVICES; i++){
		inputs_A[i] = clCreateBuffer(context,  CL_MEM_READ_WRITE, sizeof(REAL) * NB * NB, NULL, &err);
		if (err != CL_SUCCESS){
			printf("Error: Failed to create buffer, err %d!\n",err);
			return EXIT_FAILURE;
		}
		inputs_H[i] = clCreateBuffer(context,  CL_MEM_READ_WRITE, sizeof(REAL)  * NB * NB, NULL, &err);
		if (err != CL_SUCCESS){
			printf("Error: Failed to create buffer, err %d!\n",err);
			return EXIT_FAILURE;
		}
		outputs[i] = clCreateBuffer(context,  CL_MEM_READ_WRITE,  sizeof(REAL)  * NB * NB, NULL, &err);
		if (err != CL_SUCCESS){
			printf("Error: Failed to create buffer, err %d!\n",err);
			return EXIT_FAILURE;
		}		
	}

	while( ( fscanf( inl, "%d%d%d%d\n", &m, &l, &n, &nrep ) != EOF ) ){
		lda = l + 1;

		mDIM = calcdim(m);
		lDIM = calcdim(l);
		nDIM = calcdim(n);

		a = (REAL **)malloc( mDIM*lDIM*sizeof( REAL *) );
		b = (REAL **)malloc( lDIM*nDIM*sizeof( REAL *) );
		c = (REAL **)malloc( mDIM*nDIM*sizeof( REAL *) );
      
		for(i=0;i<mDIM*lDIM;i++)
			a[i] = (REAL *)malloc(BSIZE*BSIZE*sizeof(REAL));

		for(i=0;i<lDIM*nDIM;i++)
			b[i] = (REAL *)malloc(BSIZE*BSIZE*sizeof(REAL));

		for(i=0;i<mDIM*nDIM;i++)
			c[i] = (REAL *)malloc(BSIZE*BSIZE*sizeof(REAL));


		gendat( mDIM, lDIM, nDIM, m, l, n, a, b, c );

		time = cclock();
		

		for( i = 0; i < nrep; i++ ){
			matmul( m, l, n, mDIM, lDIM, nDIM, a, b, c ); 
		}
		time = cclock() - time;
		ok   = check( nrep, m, l, n, mDIM, nDIM, c);

		time = time/nrep;
		nops  = (unsigned long) 2*m*l*n;
		prtspeed( m, l, n, BSIZE, time, ok, nops );

		for(i=0;i<mDIM*lDIM;i++)
			free( a[i] );

		for(i=0;i<lDIM*nDIM;i++)
			free( b[i] );
  
		for(i=0;i<mDIM*nDIM;i++)
			free( c[i] );

		free( a ); free( b ); free( c );
	}

	for (i=0; i<N_DEVICES;i++){	
		clReleaseMemObject(inputs_A[i]);
		clReleaseMemObject(inputs_H[i]);
		clReleaseMemObject(outputs[i]);
		clReleaseCommandQueue(commands[i]);
		clReleaseContext(context);
	}
	//printf( "----------------------------------------------------------\n" );
}
