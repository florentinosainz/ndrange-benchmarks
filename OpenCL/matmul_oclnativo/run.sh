#!/bin/bash

# @ partition = projects
# @ output = matmul.log
# @ error = matmul.log
# @ initialdir = .
# @ total_tasks = 1
# @ cpus_per_task = 8
# @ gpus_per_node = 4
# @ node_usage = not_shared
# @ wall_clock_limit = 00:20:00

./matmul ${1:-1}