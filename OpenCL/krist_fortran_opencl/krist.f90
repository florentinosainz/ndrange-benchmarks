MODULE CONSTANTS
    IMPLICIT NONE
    INTEGER, PARAMETER :: DIM2_H = 4, DIM2_A = 4, DIM2_E = 2
END MODULE CONSTANTS

PROGRAM MAIN
    USE CONSTANTS, ONLY: DIM2_H, DIM2_A, DIM2_E
    IMPLICIT NONE
    INTEGER :: NA ! NUMBER OF ATOMS
    INTEGER :: NR ! NUMBER OF REFLECTIONS
    INTEGER :: DIM_NH, DIM_NA, DIM_NE, TIMES, I, ARGC, CALC

    REAL, ALLOCATABLE :: H(:), E(:), E1(:), A(:)
    REAL :: EXEC_TIME

    DOUBLE PRECISION :: SUMDF

    LOGICAL :: COMPUTE_SERIAL
    CHARACTER(LEN=32) :: ARG

    INTERFACE

        SUBROUTINE INIT_RANDOM_SEED()
        END SUBROUTINE INIT_RANDOM_SEED

        SUBROUTINE TICK(T)
            INTEGER, INTENT(OUT) :: T
        END SUBROUTINE TICK

        REAL FUNCTION TOCK(T)
            INTEGER, INTENT(IN) :: T
        END FUNCTION TOCK

        SUBROUTINE DETA(NA, A)
            INTEGER :: NA
            REAL :: A(:)
        END SUBROUTINE DETA

        SUBROUTINE DETH(NR, H)
            INTEGER :: NR
            REAL :: H(:)
        END SUBROUTINE DETH

        SUBROUTINE STRUCTFAC(NA, NR, A, H, E)
            INTEGER :: NA, NR
            REAL :: A(:), H(:), E(:)
        END SUBROUTINE STRUCTFAC

        SUBROUTINE STRUCTFAC_GPUSS(NA, NR, DIM_NA, A, DIM_NH, H, DIM_NE, E)
            INTEGER :: NA, NR, DIM_NA, DIM_NH, DIM_NE
            REAL :: A(:), H(:), E(:)
        END SUBROUTINE STRUCTFAC_GPUSS

        FUNCTION SUMDIF(A, B) RESULT(X)
            REAL :: A(:), B(:)
            DOUBLE PRECISION :: X
        END FUNCTION SUMDIF

    END INTERFACE

    ! DEFAULT VALUES
    NA = 1000
    NR = 10000
    COMPUTE_SERIAL = .FALSE.

    ! READ THE COMMAND LINE
    ARGC = COMMAND_ARGUMENT_COUNT()
    IF (ARGC >= 1) THEN
        CALL GET_COMMAND_ARGUMENT(1, ARG)
        ARG = TRIM(ARG)
        READ(ARG, '(I10)') NA
        IF (ARGC >= 2) THEN
            CALL GET_COMMAND_ARGUMENT(2, ARG)
            ARG = TRIM(ARG)
            READ(ARG, '(I10)') NR
            IF (ARGC == 3) THEN
                CALL GET_COMMAND_ARGUMENT(3, ARG)
                ARG = TRIM(ARG)
                IF(ARG == "--serial") THEN
                    COMPUTE_SERIAL = .TRUE.
                END IF
            END IF
        END IF
    END IF

    DIM_NH = DIM2_H * NR;
    DIM_NA = DIM2_A * NA;
    DIM_NE = DIM2_E * NR;

    PRINT *, "Computation of crystallographic normalized structure factors"
    PRINT *, "                on the CPU and the GPU"
    PRINT *, "Number of atoms:       ", na
    PRINT *, "Number of reflections: ", nr

    ALLOCATE (H(DIM2_H*NR))
    ALLOCATE (E(DIM2_E*NR))
    ALLOCATE (E1(DIM2_E*NR))
    ALLOCATE (A(DIM2_A*NA))

    ! INITIALIZE THE ARRAYS TO ZERO
    E = 0.0
    E1 = 0.0

    CALL INIT_RANDOM_SEED()
    CALL DETA(NA,A)
    CALL DETH(NR,H)

    IF (COMPUTE_SERIAL) THEN
        PRINT *, "Running the CPU code"
        CALL TICK(CALC)
        CALL STRUCTFAC(NA, NR, A, H, E)
        EXEC_TIME = TOCK(CALC)
        PRINT *, "SMP:      computation time (in seconds): ", EXEC_TIME
    END IF

    TIMES = 1000
    PRINT *, "Running the GPU code ", times, "times"

    CALL TICK(CALC)
    DO I = 1, TIMES
        CALL STRUCTFAC_GPUSS(NA, NR, DIM_NA, A, DIM_NH, H, DIM_NE, E1)
    END DO

    !$OMP TASKWAIT
    EXEC_TIME = TOCK(CALC)
    PRINT *, "OpenCL:       wallclock time (in seconds):", (EXEC_TIME / TIMES)

    SUMDF = SUMDIF(E, E1)
    PRINT *, "OpenCL:      Sumdif: ", SUMDF, " mean: ", SUMDF/NR

    DEALLOCATE (H)
    DEALLOCATE (E)
    DEALLOCATE (E1)
    DEALLOCATE (A)
END PROGRAM MAIN

! http://gcc.gnu.org/onlinedocs/gfortran/RANDOM_005fSEED.html#RANDOM_005fSEED
SUBROUTINE INIT_RANDOM_SEED()
    IMPLICIT NONE
    INTEGER :: I, N, CLOCK
    INTEGER, DIMENSION(:), ALLOCATABLE :: SEED

    CALL RANDOM_SEED(SIZE = N)
    ALLOCATE(SEED(N))

    CALL SYSTEM_CLOCK(COUNT=CLOCK)

    SEED = CLOCK + 37 * (/ (I - 1, I = 1, N) /)
    CALL RANDOM_SEED(PUT = SEED)
    DEALLOCATE(SEED)
END SUBROUTINE INIT_RANDOM_SEED

! http://stackoverflow.com/questions/5083051/timing-a-fortran-multithreaded-program
SUBROUTINE TICK(T)
    INTEGER, INTENT(OUT) :: T
    CALL SYSTEM_CLOCK(T)
END SUBROUTINE TICK

! RETURNS TIME IN SECONDS FROM NOW TO TIME DESCRIBED BY T
REAL FUNCTION TOCK(T)
    INTEGER, INTENT(IN) :: T
    INTEGER :: NOW, CLOCK_RATE
    CALL SYSTEM_CLOCK(NOW,CLOCK_RATE)
    TOCK = REAL(NOW - T)/REAL(CLOCK_RATE)
END FUNCTION TOCK

SUBROUTINE DETA(NA, A)
    USE CONSTANTS, ONLY: DIM2_A
    IMPLICIT NONE
    INTEGER :: NA, I, J, IND
    REAL :: A(:)
    DO I = 1, NA
        IND = (DIM2_A * (I-1)) + 1

        IF (IAND(I+1,1) == 1) THEN
             A(IND) = 6.0
        ELSE
             A(IND) = 7.0
        END IF

         DO J=1, DIM2_A-1
             CALL RANDOM_NUMBER(A(IND + j))
         END DO
    END DO
END SUBROUTINE DETA

SUBROUTINE DETH(NR, H)
    USE CONSTANTS, ONLY: DIM2_H
    IMPLICIT NONE
    INTEGER :: NR, I, IND
    INTEGER, PARAMETER :: HMAX = 20
    INTEGER, PARAMETER :: KMAX = 30
    INTEGER, PARAMETER :: LMAX = 15
    REAL :: H(:), RAND
    DO I=1, NR
        IND = (DIM2_H * (I-1)) + 1
        CALL RANDOM_NUMBER(RAND)
        H(IND + 0) = NINT(2 * HMAX * RAND - HMAX)

        CALL RANDOM_NUMBER(RAND)
        H(IND + 1) = NINT(2 * KMAX * RAND - KMAX)

        CALL RANDOM_NUMBER(RAND)
        H(IND + 2) = NINT(2 * LMAX * RAND - LMAX)

        H(IND + 3) = 0.0
    END DO
END SUBROUTINE DETH

SUBROUTINE STRUCTFAC(NA, NR, A, H, E)
    USE CONSTANTS
    IMPLICIT NONE
    INTEGER :: NA, NR, I, J, IND_A, IND_H, IND_E
    REAL :: A(:), H(:), E(:), TWOPI, F2, AUX_A, AUX_B, ARG

    TWOPI = 6.28318584
    F2 = 0.0

    DO I=1, NA
        IND_A = (DIM2_A * (I-1)) + 1
        F2 = F2 + (A(IND_A)*A(IND_A))
    END DO
    F2 = 1.0 / SQRT(F2);

!     PRINT *, "scaling factor is", f2

    DO I = 1, NR
        AUX_A = 0.0
        AUX_B = 0.0
        IND_H = (DIM2_H * (I-1)) + 1
        IND_E = (DIM2_E * (I-1)) + 1

        DO J = 1, NA
            IND_A = (DIM2_A * (J-1)) + 1
            ARG = TWOPI * &
                ((H(IND_H + 0) * A(IND_A + 1)) + &
                (H(IND_H + 1) * A(IND_A + 2)) + &
                (H(IND_H + 2) * A(IND_A + 3)))

            AUX_A = AUX_A + (A(IND_A) * COS(ARG))
            AUX_B = AUX_B + (A(IND_A) * SIN(ARG))

        END DO
        E(IND_E + 0) = AUX_A * F2
        E(IND_E + 1) = AUX_B * F2
    END DO
END SUBROUTINE STRUCTFAC


SUBROUTINE STRUCTFAC_GPUSS(NA, NR, DIM_NA, A, DIM_NH, H, DIM_NE, E)
    USE CONSTANTS
    IMPLICIT NONE

    INTEGER :: NA, NR, DIM_NA, DIM_NH, DIM_NE, II, TASKS, NR_2, SHAREDSIZE, MAXATOMS, IND_H, IND_E, IND_A, I
    REAL :: A(:), H(:), E(:), F2, DUMMY_REAL

    INTERFACE
        !$OMP TARGET DEVICE(OPENCL) COPY_DEPS NDRANGE(1, DIM_NE, 128) SHMEM(LOCALSIZE) FILE(kernel.cl)
        !$OMP TASK IN (A, H) OUT(E)
        SUBROUTINE CSTRUCTFAC(MAXATOMS, F2, DIM_NA, A, DIM_NH, H, DIM_NE, E)
            INTEGER, VALUE :: MAXATOMS, DIM_NA, DIM_NH, DIM_NE
            REAL :: A(DIM_NA), H(DIM_NH), E(DIM_NE)
            REAL, VALUE :: F2
        END SUBROUTINE CSTRUCTFAC
    END INTERFACE

    F2 = 0.0
    DO I=1, NA
        IND_A = (DIM2_A * (I-1)) + 1
        F2 = F2 + (A(IND_A)*A(IND_A))
    END DO
    F2 = 1.0 / SQRT(F2);

    TASKS = NANOS_GET_OPENCL_NUM_DEVICES()
    
    NR_2 = NR / TASKS

    SHAREDSIZE = 16384 - 2048
    MAXATOMS = SHAREDSIZE/(SIZEOF(DUMMY_REAL)*DIM2_A)

    DO II = 1, NR, NR_2
        IND_H = (DIM2_H * (II - 1)) + 1
        IND_E = (DIM2_E * (II - 1)) + 1
         CALL CSTRUCTFAC(MAXATOMS, F2, DIM_NA, A, &
            DIM_NH / TASKS, H(IND_H : IND_H + (DIM_NH / TASKS)), &
            DIM_NE / TASKS, E(IND_E : IND_E + (DIM_NE / TASKS)))
    END DO
END SUBROUTINE STRUCTFAC_GPUSS

FUNCTION SUMDIF(A, B) RESULT(X)
    REAL :: A(:), B(:)
    DOUBLE PRECISION :: X
     X = SUM( ABS(A - B))

END FUNCTION SUMDIF
