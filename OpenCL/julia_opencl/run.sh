#!/bin/bash

# @ partition = projects
# @ output = julia.log
# @ error = julia.log
# @ initialdir = .
# @ total_tasks = 1
# @ cpus_per_task = 8
# @ gpus_per_node = 2
# @ node_usage = not_shared
# @ wall_clock_limit = 00:20:00

export NX_PES=4
export NX_OPENCL_MAX_DEVICES=${1:-1}
NX_OPENCL_DEVICE_TYPE=GPU NX_ARGS="--disable-cuda" ./julia_ocl --dumpfinal output 
