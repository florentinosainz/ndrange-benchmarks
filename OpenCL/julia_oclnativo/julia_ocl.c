/*************************************************************************/
/*                                                                       */
/* Licensed Materials - Property of IBM                                  */
/*                                                                       */
/*                                                                       */
/*                                                                       */
/* (C) Copyright IBM Corp. 2006, 2009                                    */
/* All Rights Reserved                                                   */
/*                                                                       */
/* US Government Users Restricted Rights - Use, duplication or           */
/* disclosure restricted by GSA ADP Schedule Contract with IBM Corp.     */
/*                                                                       */
/*************************************************************************/
/* --------------------------------------------------------------  */
/* Copyright (c) 1984-2005, Keenan Crane                           */
/* All rights reserved.                                            */
/*                                                                 */
/* Redistribution and use in source and binary forms, with or      */
/* without modification, are permitted provided that the following */
/* conditions are met:                                             */
/*                                                                 */
/* Redistributions of source code must retain the above copyright  */
/* notice, this list of conditions and the following disclaimer.   */
/* Redistributions in binary form must reproduce the above         */
/* copyright notice, this list of conditions and the following     */
/* disclaimer in the documentation and/or other materials provided */
/* with the distribution.                                          */
/*                                                                 */
/* The name of Keenan Crane may not be used to endorse or promote  */
/* products derived from this software without specific prior      */
/* written permission.                                             */
/*                                                                 */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND          */
/* CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,     */
/* INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF        */
/* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE        */
/* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR            */
/* CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,    */
/* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT    */
/* NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    */
/* LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)        */
/* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN       */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR    */
/* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,  */
/* EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              */
/* --------------------------------------------------------------  */
/* PROLOG END TAG zYx                                              */

#include <math.h>
#include <getopt.h>
#include <sys/stat.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <CL/cl.h>

#include "clock.h"
#include "check.h"


#ifdef PPM
#include "ppm_util.h"
#endif


#include "julia.h"

#define IMG_WIDTH	512
#define IMG_HEIGHT	512
#define DATA_SIZE (IMG_WIDTH * IMG_HEIGHT * sizeof(uint32_t))

/* Framebuffer utilities */
#define OTHER_FRAME(frame) (1 - frame)

void *alloc_frame_buffer() {
  void *retPtr;
  int rc;

  rc = posix_memalign(&retPtr, 16, DATA_SIZE);

  if (rc == 0) {
    return retPtr;
  }
  else {
    return NULL;
  }
}

#ifdef PS3
void ps3_init(int *img_width, int *img_height, int *stride);
void ps3_switch_fb();
void *get_image_buffer(int ps3_frame);
void put_image_buffer(int *ps3_frame);
void ps3_finish();

/*
 * Get a pointer to the PS3's framebuffer or allocate storage for a
 * framebuffer if PS3 mode isn't enabled.
 */
void *get_frame_buffer(int frame, int ps3) {
  if (ps3)
    return get_image_buffer(frame);
  else
    return alloc_frame_buffer();
}
#else /* PS3 */
void *get_frame_buffer(int frame, int ps3) {
  return alloc_frame_buffer();
}
#endif /* PS3 */


/*
 * This structure is passed once between the host and kernel for
 * data initialized before the run.
 */
struct julia_context {
  cl_float dir_top_start[4];
  cl_float dir_bottom_start[4];
  cl_float dir_bottom_stop[4];
  cl_float eyeP[4];
  cl_float lightP[4];
  cl_int   window_size[2];
  cl_float epsilon;
  cl_int maxIterations;
  cl_int stride;
  cl_int pad[3];
} jc;

/* Binary and source data */
#define BIN_FILE_PATH_LEN 1024
#define MAX_DEVICE_NAME_LEN 128
//#define BIN_FILE_PATH_LEN 65536
//#define MAX_DEVICE_NAME_LEN 8192
const char *filename = "julia_kernel.cl";
const char *binary_file_base = "julia_kernel_";

/* workgroup size variables */
size_t global_size[2] = { IMG_WIDTH / 4, IMG_HEIGHT };
size_t local_size[3] = {1, 1, 1};
size_t queried_local_size[3] = {1, 1, 1};
size_t wg_size;
#define MULTIPLE(x, y) ((x/y) * y == x)

/* morphing matrix */
float mu[4][4];

/* Camera parameters */
float N[3]; /* Normal to the image plane */
float T[3]; /* Tangent of the image plane */
float B[3]; /* Binormal of the image plane */
program_context_t rc; /* Misc screen parameters */

static unsigned char *
load_program_binary (const char *filename, size_t *size)
{
  FILE *fh = fopen (filename, "r");
  if (fh == NULL) {
    fprintf (stderr, "Failed to open %s: %s\n", filename,strerror(errno));
    return NULL;
  }
  
  struct stat statbuf;
  if(stat (filename, &statbuf)) {
    fprintf (stderr, "Failed to stat %s: %s\n", filename, strerror(errno));
    fclose (fh);
    return NULL;
  }
      
  unsigned char *binary = (unsigned char *) malloc (statbuf.st_size);
  //printf ("binary %p\n", binary);
  if((fread (binary, statbuf.st_size, 1, fh)) != 1) {
    fprintf (stderr, "Failed to read %s\n", filename);
    free(binary);
    fclose (fh);
    return NULL;
  }
  fclose (fh);
  
  *size = (size_t) statbuf.st_size;
  return binary;
}

static char *
load_program_source (const char *filename)
{
  struct stat statbuf;
  
  FILE *fh = fopen (filename, "r");
  if (fh == NULL) {
    fprintf (stderr, "Failed to open %s: %s\n", filename,strerror(errno));
    return NULL;
  }
  
  stat (filename, &statbuf);
  char *source = (char *) malloc (statbuf.st_size + 1);
  //printf ("source %p\n", source);
  if((fread (source, statbuf.st_size, 1, fh)) != 1) {
    fprintf (stderr, "Failed to read %s\n", filename);
    free(source);
    fclose (fh);
    return NULL;
  }

  source[statbuf.st_size] = '\0';
  fclose (fh);

  return source;
}


static int
save_program_binary(cl_program program, const char *binary_filename)
{
  size_t binary_size, size_ret;
  unsigned char *binary;
  int rc;
  
  /* Get and save the binary */
  if((rc = clGetProgramInfo(program, CL_PROGRAM_BINARY_SIZES, sizeof(binary_size), &binary_size, &size_ret)) != CL_SUCCESS) {
    fprintf(stderr,"Failed to get program binary sizes: %s\n",GetErrorString(rc));
    return -1;
  }

  binary = (unsigned char*)malloc(binary_size);
  //printf ("binary2 %p\n", binary);
  if((rc = clGetProgramInfo(program, CL_PROGRAM_BINARIES, binary_size, &binary, &size_ret)) != CL_SUCCESS) {
    fprintf(stderr,"Failed to get program binary: %s\n",GetErrorString(rc));
    free(binary);
    return -1;
  } 
  
  FILE *fh = fopen (binary_filename, "w");
  if (fh == NULL) {
    fprintf(stderr,"Failed to open output file %s: %s\n",binary_filename,strerror(errno));
    free(binary);
    return -1;
  }
  
  /* write the binary into the file */
  if(fwrite(binary, binary_size, 1, fh) != 1) {
    fprintf(stderr, "Failed to fully write output file %s\n",binary_filename);
    free(binary);
    fclose(fh);
    return -1;
  }
  
  fclose(fh);
  free(binary);

  return 0;
}

int
generate_binary_filename(char *binary_filename, int binary_filename_maxlen, 
			 const char *binary_file_base, char *device_name, 
			 int local_work_group_size) 
{
  int i = 0;
  /* Convert spaces in device name to underscores */
  char converted_device_name[MAX_DEVICE_NAME_LEN] = { 0 };
  strncpy(converted_device_name, device_name,MAX_DEVICE_NAME_LEN-1);
  for (i = 0; i < strlen(converted_device_name); i++) {
    if (converted_device_name[i] == ' ') {
      converted_device_name[i] = '_';
    }
  }
  /*
   * Concatenate the base name, device name and the local work group
   * size that binary is compiled for
   */
  if(snprintf(binary_filename, binary_filename_maxlen, "%s%s_lwgsize%d", 
	      binary_file_base, converted_device_name, local_work_group_size) >= binary_filename_maxlen) {
    fprintf(stderr, "Error: binary output filename truncated.\n");
    return -1;
  }

  return 0;
}

void calculateView(program_context_t * rc)
{
  /* 
   * First apply the view transformations to the initial eye, look at,
   * and up.  These will be used later to determine the basis.
   */

  int i, j;
  float mag;
  /* eye starts on the unit sphere */
  float eyeStart[4] = { 0.0f, 0.0f, 1.0f, 1.0f };

  /* initially look at the origin */
  float lookatStart[4] = { 0.0f, 0.0f, 0.0f, 1.0f };

  /* up is initially along the y-axis */
  float upStart[4] = { 0.0f, 1.0f, 0.0f, 0.0f };

  /* point light location */
  static float lookAt[4], up[4];


  /* translate the eye and look at points */
  eyeStart[0] += rc->translate[0];
  eyeStart[1] += rc->translate[1];
  eyeStart[2] += rc->zoom;
  lookatStart[0] += rc->translate[0];
  lookatStart[1] += rc->translate[1];
  lookatStart[2] += rc->zoom;

  /* rotate eye, lookat, and up by multiplying them with the current rotation matrix */
  for (i = 0; i < 4; i++) {
    jc.eyeP[i] = 0.0f;
    lookAt[i] = 0.0f;
    up[i] = 0.0f;

    for (j = 0; j < 4; j++) {
      jc.eyeP[i] += rc->curRotation[i * 4 + j] * eyeStart[j];
      lookAt[i] += rc->curRotation[i * 4 + j] * lookatStart[j];
      up[i] += rc->curRotation[i * 4 + j] * upStart[j];
    }
  }

  /* Now we construct the basis: */
  /* N = (look at) - (eye) */
  /* T = up */
  /* B = N x T */

  /* find and normalize N = (lookat - eye) */
  for (i = 0; i < 3; i++)
    N[i] = lookAt[i] - jc.eyeP[i];
  mag = 1.0f / sqrt(N[0] * N[0] + N[1] * N[1] + N[2] * N[2]);
  for (i = 0; i < 3; i++)
    N[i] *= mag;

  /* find and normalize T = up */
  for (i = 0; i < 3; i++)
    T[i] = up[i];
  mag = 1.0f / sqrt(T[0] * T[0] + T[1] * T[1] + T[2] * T[2]);
  for (i = 0; i < 3; i++)
    T[i] *= mag;

  /* find B = N x T (already unit length) */
  B[0] = N[1] * T[2] - N[2] * T[1];
  B[1] = N[2] * T[0] - N[0] * T[2];
  B[2] = N[0] * T[1] - N[1] * T[0];

  /* move the light a little bit up and to the right of the eye.*/
  for (i = 0; i < 3; i++) {
    jc.lightP[i] = jc.eyeP[i] - B[i] * 0.5f;
    jc.lightP[i] += T[i] * 0.5f;
  }
}


/* getCurMu()
 *
 * Get the interpolated constant for the current time (used for
 * mophing between two Julia sets). 
 */
void getCurMu(float *cur, float t)
{
  int i;
  float t0, t1, t2, t3;
  float mt = 1.0f - t;
  float tsq = t * t;
  float tcb = t * t * t;
  float inv6 = 1.0f / 6.0f;

  t0 = mt * mt * mt * inv6;
  t1 = (4.0f + 3.0f * tcb - 6.0f * tsq) * inv6;
  t2 = (1.0f + 3.0f * t + 3.0f * tsq - 3.0f * tcb) * inv6;
  t3 = tcb * inv6;
  for (i = 0; i < 4; i++)
    cur[i] = t0 * mu[0][i] + t1 * mu[1][i] + t2 * mu[2][i] + t3 * mu[3][i];
}

char *usage =
  "Usage: julia_ocl [DEVICE] [KERNELTYPE] [OPTIONS...]\n"
  "\n"
  "Examples:\n"
  "  julia_ocl --accel -i 100     # Run 100 iterations on accelerator\n"
  "  julia_ocl --cpu --source     # Compile from source, run on CPU\n"
  "\n"
  " Device Types:\n"
  "\n"
  "  -a, --accel              use CBEA Accelerator for compute\n"
  "  -c, --cpu                use CPU for compute\n"              
  "  -g, --gpu                use GPU for compute\n"
  "\n"
  " Kernel types:\n"
  "\n"
  "  -b, --binary             attempt to use a precompiled binary kernel\n"
  "  -s, --source             recompile kernel from source (default)\n"
  "\n"
  " Run parameters:\n"
  "\n"
  "  -i, --iterations N       number of iterations each pass (default: %d)\n"
  "  -k, --ndevs N       number of devices to use (default: 1)\n"
  "  -l, --lwgsize N          local work group size {1,2,4,8,16,32,64,128} (default: %ld)\n"
#ifdef PS3
#ifndef PPM
// Ugly duplication needed to make unidef output pretty
  "\n"
  " Output options:\n"
  "\n"
#endif
#endif
#ifdef PPM
  "\n"
  " Output options:\n"
  "\n"
  "  -j, --ppm                write ppms for each frame (default: off)\n"
  "  -d, --dumpfinal NAME     write final frame as NAME (default: off)\n"
  "\n"
#endif
#ifdef PS3
  "  -p, --ps3                display to ps3 framebuffer\n"
#endif
  "\n"
  ;

  
int N_DEVICES=1;

cl_device_id* device_id;             // compute device id 
cl_context context;                 // compute context
cl_command_queue* commands;          // compute command queue
cl_program program;                 // compute program

int main(int argc, char *argv[])
{
  int i, err;
  int iterations = 50;
  float delta = 0.0f;
  float currMu[4];
  float morphTimer = 0.0f;
  char *kernelSource;

  const float fRandMax = 1.0f / (float) RAND_MAX; /* used to normalize random values */

  int ps3 = 0;
#ifdef PPM
  int ppm = 0;
  int dumpfinal = 0;
  char finalframe_filename[BIN_FILE_PATH_LEN] = {0};
#endif
  int source = 1;

  int compute_frame = 0;
  int display_frame = 1;

  cl_device_type dev_type = CL_DEVICE_TYPE_DEFAULT;

  char binary_filename[BIN_FILE_PATH_LEN];

  static struct option long_options[] = {
    {"help", 0, NULL, 'h'},
    /* Devtype */
    {"accel", 0, NULL, 'a'},
    {"cpu", 0, NULL, 'c'},
    {"gpu", 0, NULL, 'g'},
    /* Control */
    {"iterations", 1, NULL, 'i'},
    /* Image output */
#ifdef PPM
    {"ppm", 0, NULL, 'j'},
    {"dumpfinal", required_argument, NULL, 'd'},
#endif
#ifdef PS3
    {"ps3", 0, NULL, 'p'},
#endif
    /* Binary */
    {"binary", 0, NULL, 'b'},
    {"source", 0, NULL, 's'},
    {"lwgsize", 1, NULL, 'l'},
    {"ndevs", 1, NULL, 'k'},
  };
  
  char optstring[128] = "hacgi:bsl:";

#ifdef PPM
  strncat(optstring,"d:j",127);
#endif
#ifdef PS3
  strncat(optstring,"p",127);
#endif
    

  while (1) {
    int opt;
    
    int option_index = 0;

    opt = getopt_long(argc, argv, optstring, long_options, &option_index);
        
    if (opt == -1) {
      break;
    }

    switch (opt) {
    case 'a':
      dev_type = CL_DEVICE_TYPE_ACCELERATOR;
      break;
    case 'c':
      dev_type = CL_DEVICE_TYPE_CPU;
      break;
    case 'g':
      dev_type = CL_DEVICE_TYPE_GPU;
      break;
    case 'i':
      iterations = atoi(optarg);
      break;
#ifdef PPM
    case 'j':
      ppm = 1;
      break;
#endif
#ifdef PS3
    case 'p':
      ps3 = 1;
      break;
#endif
    case 'b':
      source = 0;
      break;
    case 's':
      source = 1;
      break;
    case 'k':
      N_DEVICES = atoi(optarg);
      break;
    case 'l':
       local_size[0] = atoi(optarg);
       switch(local_size[0]) {
       case 1: case 2: case 4: case 8: case 16: case 32: case 64: case 128:
         break;
       default:
         fprintf(stderr, "Error, %zd is not a valid power of 2 between 1 and 128\n", local_size[0]);
         return 1;
       }
       break;
#ifdef PPM
    case 'd':
      dumpfinal = 1;
      strncpy(finalframe_filename,optarg,BIN_FILE_PATH_LEN-1);
      break;
#endif
    default:
    case 'h':
      fprintf (stderr, usage, iterations, local_size[0]);
      exit(0);
      break;

      return 1;
    }
  } /* while(1) */
  

  cl_platform_id platform_id;   /* compute platform id */
  cl_kernel kernel[N_DEVICES];             /* compute kernel */
  cl_mem outBuffer[2][N_DEVICES];          /* device memory used for the framebuffer */
  cl_event bufferEvent[2][N_DEVICES];      /* event to wait on for a buffer */
  char * framebuffer[2][N_DEVICES];        /* pointer to the framebuffer */
  char * mapped_addr[2][N_DEVICES];        /* mapped address returned from mapbuffer */
  unsigned int rowstride;       /* rowstride for output buffer */
  float alpha;                  /* height for aspect ratio */
  float beta;                   /* width for aspect ratio */
  
  cl_device_id device_id_aux[N_DEVICES];
  cl_command_queue commands_aux[N_DEVICES];
  device_id=device_id_aux;             // compute device id 
  commands=commands_aux;          // compute command queue

  /* Setup the initial morphing matrix */
  mu[0][0] = 0.0f;
  mu[0][1] = 0.0f;
  mu[0][2] = 0.0f;
  mu[0][3] = 0.0f;
  mu[1][0] = 0.0f;
  mu[1][1] = 0.0f;
  mu[1][2] = 0.0f;
  mu[1][3] = 0.0f;
  mu[2][0] = 0.0f;
  mu[2][1] = 0.0f;
  mu[2][2] = 0.0f;
  mu[2][3] = 0.0f;
  mu[3][0] = 2.0f * (rand() * fRandMax) - 1.0f;
  mu[3][1] = 2.0f * (rand() * fRandMax) - 1.0f;
  mu[3][2] = 2.0f * (rand() * fRandMax) - 1.0f;
  mu[3][3] = 2.0f * (rand() * fRandMax) - 1.0f;


  /* Set initial view parametrs */
  rc.translate[0] = 0.0F;
  rc.translate[1] = 0.0F;
  rc.zoom = 2.0F;
  rc.curRotation[0] = 0.0F;
  rc.curRotation[1] = 1.0F;
  rc.curRotation[2] = 0.0F;
  rc.curRotation[3] = 0.0F;
  rc.curRotation[4] = -1.0F;
  rc.curRotation[5] = 0.0F;
  rc.curRotation[6] = 0.0F;
  rc.curRotation[7] = 0.0F;
  rc.curRotation[8] = 0.0F;
  rc.curRotation[9] = 0.0F;
  rc.curRotation[10] = 1.0F;
  rc.curRotation[11] = 0.0F;
  rc.curRotation[12] = 0.0F;
  rc.curRotation[13] = 0.0F;
  rc.curRotation[14] = 0.0F;
  rc.curRotation[15] = 1.0F;
  rc.shadows = 0;

  rc.fov = 60.0f;
  rc.aspect = ((float) IMG_WIDTH) / ((float) IMG_HEIGHT);

  jc.window_size[0] = (float) IMG_WIDTH;
  jc.window_size[1] = (float) IMG_HEIGHT;
  jc.maxIterations = 4;
  jc.epsilon = 0.003f;
  jc.stride = IMG_WIDTH;

  calculateView(&rc);

  beta = tan((rc.fov * M_PI / 180.0f) / 2.0f);   /*find height */
  alpha = beta * rc.aspect;      /*find width */

  rowstride = IMG_WIDTH;

  /* Connect to a compute device */
  CHECK(clGetPlatformIDs(1, &platform_id, NULL));

  unsigned int num_devices = 0;

  /* This query will pick the first available device of dev_type */
  CHECK(clGetDeviceIDs(platform_id, dev_type, N_DEVICES, device_id, &num_devices));

  size_t size_ret;
  char *device_name;

  /* Create a compute context */
  context = clCreateContext(NULL, N_DEVICES, device_id, NULL, NULL, &err);
  CHECK2("clCreateContext", err);

  /* Create a command commands */
  for (i=0; i<N_DEVICES;i++){	
		commands[i] = clCreateCommandQueue(context, device_id[i],
                                  CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE, &err);
		CHECK2("clCreateCommandQueue", err);
  }


   device_name = malloc(MAX_DEVICE_NAME_LEN);
   CHECK(clGetDeviceInfo(device_id[0], CL_DEVICE_NAME, MAX_DEVICE_NAME_LEN, device_name, &size_ret));

  if(generate_binary_filename(binary_filename, BIN_FILE_PATH_LEN, binary_file_base, device_name, local_size[0]) != 0) {
    return EXIT_FAILURE;
  }
  
    

  if (source) {
    /* Build the program from source */
    fprintf (stderr, "Loading program source '%s'....\n", filename);
	kernelSource = load_program_source(filename);
	if (!kernelSource) {
          fprintf (stderr, "Error: Failed to load compute program from file!\n");
          return EXIT_FAILURE;
	}
	
	/* Create the compute program from the source buffer */
	program = clCreateProgramWithSource(context, 1, (const char **) & kernelSource, NULL, &err);
        CHECK2("clCreateProgramWithSource", err);
  }
  else {
    /* Load the program from a binary */
    size_t binary_size;
    int status;

	fprintf (stderr, "Loading program binary '%s'....\n", binary_filename);
	unsigned char * KernelBinary = load_program_binary(binary_filename, &binary_size);
	if (KernelBinary == NULL) {
          fprintf (stderr, "Error: Failed to load compute program from file!\n");
          return EXIT_FAILURE;
	}
	
	/* Create the compute program from the source buffer */
	program = clCreateProgramWithBinary(context, N_DEVICES, 
                                            device_id,
                                            (size_t *)&binary_size,
                                            (const unsigned char **) &KernelBinary,
                                            &status,
                                            &err);
        CHECK2("clCreateProgramWithBinary", err);
  }

  char build_options[64];
  sprintf(build_options, "-DLWGSIZE=16", local_size[0]);

  /* Build the program executable */
  err = clBuildProgram(program, 0, NULL, build_options, NULL, NULL);

  /* Print out the error log if we fail to compile */
  if (err != CL_SUCCESS) {
    size_t len;
    char *buffer = NULL;
    
    CHECK(clGetProgramBuildInfo(program, device_id[0], CL_PROGRAM_BUILD_LOG,
                                0, NULL, &len));
    
    buffer = malloc (len);
    //printf ("buffer %p\n", buffer);
    
    if (buffer) {
      CHECK(clGetProgramBuildInfo(program, device_id[0], CL_PROGRAM_BUILD_LOG,
                                  len, buffer, NULL));
      
      fprintf (stderr, "Error Log -- %s\n", buffer);
    }
    free (buffer);

    return EXIT_FAILURE;
  }
  

  


  #ifdef PS3
  if (ps3) {
    ps3_init(&jc.window_size[0], &jc.window_size[1], &jc.stride);
  }
  #endif

  /* Create both framebuffers */
  for (i=0; i<N_DEVICES;i++){	
	 /* Create the compute kernel in the program we wish to run */
	  kernel[i] = clCreateKernel(program, "compute_julia", &err);
	  CHECK2("clCreateKernel", err);
	  framebuffer[0][i] = get_frame_buffer(0, ps3);
	  outBuffer[0][i] = clCreateBuffer(context,
									CL_MEM_WRITE_ONLY|CL_MEM_USE_HOST_PTR,
									jc.window_size[0] * jc.window_size[1] * sizeof(uint32_t),
									framebuffer[0][i],
									&err);
	  CHECK2("clCreateBuffer", err);
	  
	  framebuffer[1][i] = get_frame_buffer(1, ps3);
	  outBuffer[1][i] = clCreateBuffer(context,
									CL_MEM_WRITE_ONLY|CL_MEM_USE_HOST_PTR,
									jc.window_size[0] * jc.window_size[1] * sizeof(uint32_t),
									framebuffer[1][i],
									&err);
	  CHECK2("clCreateBuffer", err);
  }

  /* Get the maximum work group size for executing the kernel on the device */
  CHECK(clGetKernelWorkGroupInfo(kernel[0], device_id[0], CL_KERNEL_WORK_GROUP_SIZE,
                                 sizeof(long), &wg_size, NULL));

  CHECK(clGetKernelWorkGroupInfo(kernel[0], device_id[0], CL_KERNEL_COMPILE_WORK_GROUP_SIZE,
                                 3*sizeof(queried_local_size[0]), queried_local_size, NULL));

  if ( !MULTIPLE(global_size[0], queried_local_size[0]) ||
       !MULTIPLE(global_size[1], queried_local_size[1])    ) {
    fprintf (stderr,
             "Global size [%zd][%zd] not divisible by local size [%zd][%zd]",
             global_size[0], global_size[1], 
             queried_local_size[0] , queried_local_size[1]);
    return EXIT_FAILURE;
  }

  /* rendering region: upper left corner */
  jc.dir_top_start[0] = -alpha * T[0] - beta * B[0] + N[0];
  jc.dir_top_start[1] = -alpha * T[1] - beta * B[1] + N[1];
  jc.dir_top_start[2] = -alpha * T[2] - beta * B[2] + N[2];

  /* rendering region: lower left corner */
  jc.dir_bottom_start[0] = -alpha * T[0] + beta * B[0] + N[0];
  jc.dir_bottom_start[1] = -alpha * T[1] + beta * B[1] + N[1];
  jc.dir_bottom_start[2] = -alpha * T[2] + beta * B[2] + N[2];

  /* rendering region: lower right corner */
  jc.dir_bottom_stop[0] = alpha * T[0] + beta * B[0] + N[0];
  jc.dir_bottom_stop[1] = alpha * T[1] + beta * B[1] + N[1];
  jc.dir_bottom_stop[2] = alpha * T[2] + beta * B[2] + N[2];

  for (i=0; i<N_DEVICES;i++){	
	CHECK(clSetKernelArg(kernel[i], 2, sizeof(struct julia_context), &jc));
  }
  

#ifdef PS3
  if (ps3) {
    ps3_switch_fb();
  }
#endif
  queried_local_size[0]=16;
  printf("using %d,%d local size\n",queried_local_size[0],queried_local_size[1]);

  startclock();

  for (i=0; i<N_DEVICES;i++){	
	  /* Create 2 dummy events so we can wait on them for the first round of the loop */
	  CHECK(clEnqueueMarker(commands[i], &bufferEvent[0][i]));
	  CHECK(clEnqueueMarker(commands[i], &bufferEvent[1][i]));
  }

  int n_device=-1;
  //We assume that iterations % N_DEVICES == 0
  for (i = 0; i < iterations; i=i+N_DEVICES) {
	for (n_device=0; n_device < N_DEVICES; ++n_device){
		getCurMu(currMu, morphTimer);
		morphTimer += 0.05f;

		CHECK(clSetKernelArg(kernel[n_device], 0, 4 * sizeof(float), currMu));
		CHECK(clSetKernelArg(kernel[n_device], 1, sizeof(cl_mem), &outBuffer[compute_frame][n_device]));

		/*
		 * Wait for the previous display_frame (now compute_frame) to
		 * finish unmapping before we begin computing
		 */
		cl_event compute_event = bufferEvent[compute_frame][n_device];
		
		CHECK(clEnqueueNDRangeKernel(commands[n_device],
									 kernel[n_device],
									 2, 
									 NULL,
									 global_size, queried_local_size,
									 1, &compute_event, &bufferEvent[compute_frame][n_device]));
		clReleaseEvent(compute_event);
		
		/* Alter the morphing matrix */
		if (morphTimer >= 1.0f) {
		  morphTimer -= 1.0f;

		  mu[0][0] = mu[1][0];
		  mu[0][1] = mu[1][1];
		  mu[0][2] = mu[1][2];
		  mu[0][3] = mu[1][3];

		  mu[1][0] = mu[2][0];
		  mu[1][1] = mu[2][1];
		  mu[1][2] = mu[2][2];
		  mu[1][3] = mu[2][3];

		  mu[2][0] = mu[3][0];
		  mu[2][1] = mu[3][1];
		  mu[2][2] = mu[3][2];
		  mu[2][3] = mu[3][3];

		  mu[3][0] = 2.0f * rand() / (float) (RAND_MAX) - 1.0f;
		  mu[3][1] = 2.0f * rand() / (float) (RAND_MAX) - 1.0f;
		  mu[3][2] = 2.0f * rand() / (float) (RAND_MAX) - 1.0f;
		  mu[3][3] = 2.0f * rand() / (float) (RAND_MAX) - 1.0f;
		}    
	
	}
	for (n_device=0; n_device < N_DEVICES; ++n_device){
		/* Read back the results from the device to display the framebuffer */
		mapped_addr[display_frame][n_device] = clEnqueueMapBuffer(commands[n_device],
														outBuffer[display_frame][n_device],
														CL_TRUE,
														CL_MAP_READ,
														0, jc.window_size[0] * jc.window_size[1] * sizeof(uint32_t),
														1, &bufferEvent[display_frame][n_device], NULL,
														&err);
		CHECK2("clEnqueueMapBuffer", err);
		//clReleaseEvent(bufferEvent[display_frame][n_device]);
		/* Write out ppm to a file */
		#ifdef PPM
		if (ppm && i > 0) {
		  char ppm_name[BIN_FILE_PATH_LEN];
		  snprintf(ppm_name, 1023, "julia_%04d.ppm", i - 1);
		  put_ppm(ppm_name, (unsigned int *) mapped_addr[display_frame][n_device],
				  jc.window_size[0], jc.window_size[1], jc.stride);
		}
		#endif

		CHECK(clEnqueueUnmapMemObject(commands[n_device],
									  outBuffer[display_frame][n_device],
									  mapped_addr[display_frame][n_device],
									  0, NULL, &bufferEvent[display_frame][n_device]));
	}
    /* Swap the frames for the next iteration */
	display_frame = compute_frame;
	compute_frame = OTHER_FRAME(compute_frame);
  } /* for */

  n_device--;
  #ifdef PPM
  if (ppm || dumpfinal) {
    char ppm_name[BIN_FILE_PATH_LEN];
    mapped_addr[display_frame][n_device] = clEnqueueMapBuffer(commands[n_device],
                                                    outBuffer[display_frame][n_device],
                                                    CL_TRUE,
                                                    CL_MAP_READ,
                                                    0, jc.window_size[0] * jc.window_size[1] * sizeof(uint32_t),
                                                    1, &bufferEvent[display_frame][n_device], NULL,
                                                    &err);
    if (ppm) {
      snprintf(ppm_name, 1023, "julia_%04d.ppm", i - 1);
      put_ppm(ppm_name, (unsigned int *) mapped_addr[display_frame][n_device],
              jc.window_size[0], jc.window_size[1], jc.stride);
    }

    if (dumpfinal) {
      put_ppm(finalframe_filename, 
              (unsigned int *) mapped_addr[display_frame][n_device],
              jc.window_size[0], jc.window_size[1], jc.stride);
    }

    CHECK(clEnqueueUnmapMemObject(commands[n_device],
                                  outBuffer[display_frame][n_device],
                                  mapped_addr[display_frame][n_device],
                                  0, NULL, NULL));
  } /* (ppm || dumpfinal) */
  #endif


  delta = stopclock();

#ifdef PS3
  if (ps3)
    ps3_finish();
#endif

  printf("%d Frames took %f seconds. Rate = %f Mpixels/sec\n",
         iterations, delta,
         (double) (IMG_WIDTH * IMG_HEIGHT) * (double) (iterations) /
         (1000000.0 * (double) (delta)));



  /* Shutdown and cleanup */
  CHECK(clReleaseContext(context));
  
  return 0;
}


