/* frag.c - SPU-C fractal raytracer, ported from 
 *
 * 	QJuliaFragment.cg 
 *	Keenan Crane (kcrane@uiuc.edu)
 *
 * See:
 *   http://www.devmaster.net/forums/showthread.php?t=4448
 */

#define BOUNDING_RADIUS_2	3.0f
#define ESCAPE_THRESHOLD       10.0f
#define DEL                    1e-4f

// Workaround
#define const

struct julia_context {
  float4 dir_top_start;
  float4 dir_bottom_start;
  float4 dir_bottom_stop;
  float4 eyeP;
  float4 lightP;
  int2 window_size;
  float epsilon;
  int maxIterations;
  int stride;
  int pad[3];
};

/**
 * normalize3_v
 *     Normalize 3 SOA vectors
 */
void
normalize3_v(float4 * rnx, float4 * rny, float4 * rnz,
             float4    nx, float4    ny, float4    nz)
{
  float4 dot, invlen;

  dot = nx * nx;
  dot = ny * ny + dot;
  dot = nz * nz + dot;
  invlen = rsqrt(dot);

  *rnx = nx * invlen;
  *rny = ny * invlen;
  *rnz = nz * invlen;
}

/**
 * length4_v
 *     Return the length of 4 SOA vectors
 */
float4 length4_v(float4 x, float4 y, float4 z, float4 w)
{
  float4 dot;

  dot = x * x;
  dot = y * y + dot;
  dot = z * z + dot;
  dot = w * w + dot;
  return (sqrt(dot));
}

/**
 * length4_v
 *     Return the length of 4 SOA vectors
 */
inline float4 length16_v(float16 v)
{
  return length4_v(v.lo.lo, v.lo.hi, v.hi.lo, v.hi.hi);
}


/**
 * dot_product3_v
 *     compute the dot product of 3 SOA vectors
 */

inline float4
dot_product3_v(float4 ax, float4 ay, float4 az, 
               float4 bx, float4 by, float4 bz)
{
  float4 dot;

  dot = ax * bx;
  dot = ay * by + dot;
  dot = az * bz + dot;
  return dot;
}


/**
 * dot_product4_v
 *     compute the dot product of 4 SOA vectors
 */
inline float4
dot_product4_v(float4 ax, float4 ay, float4 az, float4 aw,
               float4 bx, float4 by, float4 bz, float4 bw)
{
  float4 dot;

  dot = ax * bx;
  dot = ay * by + dot;
  dot = az * bz + dot;
  dot = aw * bw + dot;
  return dot;
}

inline float4 dot_product16_v(float16 a, float16 b)
{
  return dot_product4_v(a.lo.lo, a.lo.hi, a.hi.lo, a.hi.hi,
                        b.lo.lo, b.lo.hi, b.hi.lo, b.hi.hi);
}

/**
 * cross_product3_v
 *     compute cross product of 3 SOA vectors
 */
inline void
cross_product3_v(float4 * xOut, float4 * yOut, float4 * zOut,
                 float4     x1, float4     y1, float4     z1,
                 float4     x2, float4     y2, float4     z2)
{
  *xOut = (y1 * z2) - (z1 * y2);
  *yOut = (z1 * x2) - (x1 * z2);
  *zOut = (x1 * y2) - (y1 * x2);
}


/**
 * transpose
 *     transpose a 4x4 float array
 */
inline void transpose(float4 m[4])
{
  // Read Matrix into a float16 vector
  float16 x = {m[0], m[1], m[2], m[3]};
  float16 t;

  // Transpose
  t.even = x.lo;
  t.odd = x.hi;
  x.even = t.lo;
  x.odd = t.hi;

  // write back
  m[0] = x.lo.lo;
  m[1] = x.lo.hi;
  m[2] = x.hi.lo;
  m[3] = x.hi.hi;
}


/**
 * quatMult4
 *    Multiply 4 SOA quaternians
 */
void
quatMult4(float4 q1x, float4 q1y, float4 q1z,
          float4 q1w, float4 q2x, float4 q2y,
          float4 q2z, float4 q2w, float4 * rx,
          float4 * ry, float4 * rz, float4 * rw)
{
  float4 cy, cz, cw;

  *rx = dot_product3_v(q1y, q1z, q1w, q2y, q2z, q2w);
  *rx = q1x * q2x - *rx;
  cross_product3_v(&cy, &cz, &cw, q1y, q1z, q1w, q2y, q2z, q2w);
  *ry = q2x * q1y + cy;
  *rz = q2x * q1z + cz;
  *rw = q2x * q1w + cw;
  *ry = q1x * q2y + *ry;
  *rz = q1x * q2z + *rz;
  *rw = q1x * q2w + *rw;
}


/**
 * quatMult4
 *    Multiply 4 SOA quaternians
 */

void
quatSq4(float4   qx, float4   qy, float4   qz, float4   qw,
        float4 * rx, float4 * ry, float4 * rz, float4 * rw)
{
  float4 qx2;
  float4 dotq;

  qx2 = qx * 2.0f;
  dotq = dot_product3_v(qy, qz, qw, qy, qz, qw);
  *rx = qx * qx - dotq;
  *ry = qx2 * qy;
  *rz = qx2 * qz;
  *rw = qx2 * qw;
}

void quatSq16(float16 q, float16 * r)
{
  float4 rlolo, rlohi, rhilo, rhihi;

  quatSq4(q.lo.lo, q.lo.hi, q.hi.lo, q.hi.hi, &rlolo, &rlohi, &rhilo, &rhihi);

  *r = (float16) {rlolo, rlohi, rhilo, rhihi};
}

void iterateIntersect(float4 * q, float4 * qp, float4 * c, int maxIterations)
{
  int i = 0;
  float4 dqq;
  float4 qx = q[0];
  float4 qy = q[1];
  float4 qz = q[2];
  float4 qw = q[3];
  float4 qpx = qp[0];
  float4 qpy = qp[1];
  float4 qpz = qp[2];
  float4 qpw = qp[3];
  int4 write_mask =  0xFFFFFFFF;

  while ((i < maxIterations) && (any(write_mask))) {

    quatMult4(qx, qy, qz, qw, qpx, qpy, qpz, qpw, &qpx, &qpy, &qpz, &qpw);

    qpx = 2.0f * qpx;
    qpy = 2.0f * qpy;
    qpz = 2.0f * qpz;
    qpw = 2.0f * qpw;
    quatSq4(qx, qy, qz, qw, &qx, &qy, &qz, &qw);

    qx = qx + c[0];
    qy = qy + c[1];
    qz = qz + c[2];
    qw = qw + c[3];
    dqq = dot_product4_v(qx, qy, qz, qw, qx, qy, qz, qw);

    q[0] = bitselect(q[0], qx, as_float4(write_mask));
    q[1] = bitselect(q[1], qy, as_float4(write_mask));
    q[2] = bitselect(q[2], qz, as_float4(write_mask));
    q[3] = bitselect(q[3], qw, as_float4(write_mask));

    qp[0] = bitselect(qp[0], qpx, as_float4(write_mask));
    qp[1] = bitselect(qp[1], qpy, as_float4(write_mask));
    qp[2] = bitselect(qp[2], qpz, as_float4(write_mask));
    qp[3] = bitselect(qp[3], qpw, as_float4(write_mask));

    write_mask &= ~((dqq > ((float4) ESCAPE_THRESHOLD)));
    i++;
  }
}

void normEstimate4(float4 * norm, float4 * p, float4 * c, int maxIterations)
{
  int i;
  float4 zero = 0.0f;
  float16 qP = (float16) { p[0], p[1], p[2], zero };

  float4 Nx = 0.0f;
  float4 Ny = 0.0f;
  float4 Nz = 0.0f;

  float16 I = (float16) { DEL, DEL, DEL, DEL,
    0.0f, 0.0f, 0.0f, 0.0f,
    0.0f, 0.0f, 0.0f, 0.0f,
    0.0f, 0.0f, 0.0f, 0.0f
  };
  float16 J = (float16) { 0.0f, 0.0f, 0.0f, 0.0f,
    DEL, DEL, DEL, DEL,
    0.0f, 0.0f, 0.0f, 0.0f,
    0.0f, 0.0f, 0.0f, 0.0f
  };
  float16 K = (float16) { 0.0f, 0.0f, 0.0f, 0.0f,
    0.0f, 0.0f, 0.0f, 0.0f,
    DEL, DEL, DEL, DEL,
    0.0f, 0.0f, 0.0f, 0.0f
  };

  float16 gx1, gx2, gy1, gy2, gz1, gz2;

  float16 C = { c[0], c[1], c[2], c[3] };

  gx1 = qP - I;
  gx2 = qP + I;

  gy1 = qP - J;
  gy2 = qP + J;

  gz1 = qP - K;
  gz2 = qP + K;

  for (i = 0; i < maxIterations; i++) {
    quatSq16(gx1, &gx1);
    quatSq16(gx2, &gx2);
    quatSq16(gy1, &gy1);
    quatSq16(gy2, &gy2);
    quatSq16(gz1, &gz1);
    quatSq16(gz2, &gz2);

    gx1 = gx1 + C;
    gx2 = gx2 + C;

    gy1 = gy1 + C;
    gy2 = gy2 + C;

    gz1 = gz1 + C;
    gz2 = gz2 + C;
  }

  Nx = length16_v(gx2) - length16_v(gx1);
  Ny = length16_v(gy2) - length16_v(gy1);
  Nz = length16_v(gz2) - length16_v(gz1);

  normalize3_v(&norm[0], &norm[1], &norm[2], Nx, Ny, Nz);
}

float4
intersectQJulia(float4 * rO, float4 * rD, float4 * c,
                int maxIterations, float4 epsilon)
{
  float4 rOx = rO[0];
  float4 rOy = rO[1];
  float4 rOz = rO[2];
  float4 dist;
  float4 rdist = 0.0f;
  int4 write_mask = 0xFFFFFFFF;
  const float4 maxstep = 0.3f;

  while (any(write_mask)) {
    float4 z[4];
    float4 zp[4];
    float4 normZ;
    float4 nzlog, zplen;
    float4 vdot;
    const float4 vBR = BOUNDING_RADIUS_2;

    z[0] = rOx;
    z[1] = rOy;
    z[2] = rOz;
    z[3] = 0.0f;
    zp[0] = 1.0f;
    zp[1] = 0.0f;
    zp[2] = 0.0f;
    zp[3] = 0.0f;


    iterateIntersect(z, zp, c, maxIterations);
    normZ = length4_v(z[0], z[1], z[2], z[3]);
    nzlog = log(normZ);
    zplen = length4_v(zp[0], zp[1], zp[2], zp[3]);
    zplen = 1.0f / (zplen);
    dist = normZ * 0.5f;
    dist = dist * nzlog;
    dist = dist * zplen;
    dist = min(dist, maxstep);

    rOx = rD[0] * dist + rOx;
    rOy = rD[1] * dist + rOy;
    rOz = rD[2] * dist + rOz;

    vdot = dot_product3_v(rOx, rOy, rOz, rOx, rOy, rOz);

    rO[0] = bitselect(rO[0], rOx, as_float4(write_mask));
    rO[1] = bitselect(rO[1], rOy, as_float4(write_mask));
    rO[2] = bitselect(rO[2], rOz, as_float4(write_mask));
    rdist = bitselect(rdist, dist, as_float4(write_mask));
    write_mask &= ~((epsilon > dist) | (vdot > vBR));
  }
  return rdist;
}


void
phong4(float4 * color, float4 * light, float4 * eye, float4 * pt, float4 * norm)
{
  float4 NdotL;
  float4 NdotL2;
  float4 Rx, Ry, Rz;
  float4 diffR, diffG, diffB;
  float4 spec;
  float4 specExp = 64.0f;
  float4 EdotR;
  float4 Lx = light[0] - pt[0];
  float4 Ly = light[1] - pt[1];
  float4 Lz = light[2] - pt[2];
  float4 Ex = eye[0] - pt[0];
  float4 Ey = eye[1] - pt[1];
  float4 Ez = eye[2] - pt[2];
  float4 diffuseR = 1.0f;
  float4 diffuseG = 0.45f;
  float4 diffuseB = 0.25f;
  const float4 vnorm_mod = 0.3f;
  const float4 specularity = 0.45f;

  normalize3_v(&Lx, &Ly, &Lz, Lx, Ly, Lz);
  normalize3_v(&Ex, &Ey, &Ez, Ex, Ey, Ez);
  NdotL = dot_product3_v(norm[0], norm[1], norm[2], Lx, Ly, Lz);
  NdotL2 = 2.0f * NdotL;
  Rx = NdotL2 * norm[0] - Lx;
  Ry = NdotL2 * norm[1] - Ly;
  Rz = NdotL2 * norm[2] - Lz;
  EdotR = dot_product3_v(Ex, Ey, Ez, Rx, Ry, Rz);
  diffR = fabs(norm[0]) * vnorm_mod + diffuseR;
  diffG = fabs(norm[1]) * vnorm_mod + diffuseG;
  diffB = fabs(norm[2]) * vnorm_mod + diffuseB;
  spec = specularity * pow(max(EdotR, 0.0f), specExp);
  color[0] = diffR * max(NdotL, 0.0f) + spec;
  color[1] = diffG * max(NdotL, 0.0f) + spec;
  color[2] = diffB * max(NdotL, 0.0f) + spec;
}

void
intersectSphere(float4 * rOx, float4 * rOy, float4 * rOz,
                float4   rDx, float4   rDy, float4   rDz)
{
  float4 B, negB, C, d, t0, t1, t;
  const float4 vBR = BOUNDING_RADIUS_2;

  B = dot_product3_v(*rOx, *rOy, *rOz, rDx, rDy, rDz);
  C = dot_product3_v(*rOx, *rOy, *rOz, *rOx, *rOy, *rOz);
  B = B * 2.0f;
  C = C - vBR;
  d = (B * B) - (C * 4.0f);
  d = max(d, 0.0f);
  d = sqrt(d);

  negB = -B;
  t0 = d - B;
  t0 = t0 * 0.5f;
  t1 = negB - d;
  t1 = t1 * 0.5f;
  t = min(t0, t1);
  *rOx = t * rDx + *rOx;
  *rOy = t * rDy + *rOy;
  *rOz = t * rDz + *rOz;
}


void
fragmentShader4(float4 * rOT, float4 * rDT, float4 * muT,
                float4 epsilon, float4 * lightT,
                int maxIterations, int renderShadows, uchar16 * cbuffer)
{
  float4 colorT[4]; /* ARGB */
  float4 distT, distTs;
  float4 rOmT[3];
  float4 eyeT[3];
  float4 bg_colorR = 0.5f;
  float4 bg_colorG = 0.5f;
  float4 bg_colorB = 0.5f;
  const float4 shade = 0.4f;
  const uint4 bg_color = (0xff7f7f7f);
  eyeT[0] = rOT[0];
  eyeT[1] = rOT[1];
  eyeT[2] = rOT[2];

  rOmT[0] = rOT[0];
  rOmT[1] = rOT[1];
  rOmT[2] = rOT[2];

  normalize3_v(&rDT[0], &rDT[1], &rDT[2], rDT[0], rDT[1], rDT[2]);
  intersectSphere(&rOmT[0], &rOmT[1], &rOmT[2], rDT[0], rDT[1], rDT[2]);

  distT = intersectQJulia(rOmT, rDT, muT, maxIterations, epsilon);

  if (any(epsilon > distT)) {
    /* At least 1 ray hit the julia set */
    float4 norm[3];
    float4 rgba[4];

    normEstimate4(norm, rOmT, muT, maxIterations);

    phong4(colorT, lightT, eyeT, rOmT, norm);

    if (renderShadows) {
      float4 LT[3];
      float4 epsilon2 = epsilon * 2.0f;

      LT[0] = lightT[0] - rOmT[0];
      LT[1] = lightT[1] - rOmT[1];
      LT[2] = lightT[2] - rOmT[2];

      normalize3_v(&LT[0], &LT[1], &LT[2], LT[0], LT[1], LT[2]);

      rOmT[0] = norm[0] * epsilon2 + rOmT[0];
      rOmT[1] = norm[1] * epsilon2 + rOmT[1];
      rOmT[2] = norm[2] * epsilon2 + rOmT[2];

      distTs = intersectQJulia(rOmT, LT, muT, maxIterations, epsilon);

      if (any(epsilon > distTs)) {
        float4 tcolorR, tcolorG, tcolorB;
        tcolorR = colorT[0] * shade;
        tcolorG = colorT[1] * shade;
        tcolorB = colorT[2] * shade;
        colorT[0] = bitselect(colorT[0], tcolorR, as_float4(epsilon > distTs));
        colorT[1] = bitselect(colorT[1], tcolorG, as_float4(epsilon > distTs));
        colorT[2] = bitselect(colorT[2], tcolorB, as_float4(epsilon > distTs));
      }
    }

    /* Any rays that missed are background */
    rgba[0] = bitselect(bg_colorR, colorT[0], as_float4(epsilon > distT));
    rgba[1] = bitselect(bg_colorG, colorT[1], as_float4(epsilon > distT));
    rgba[2] = bitselect(bg_colorB, colorT[2], as_float4(epsilon > distT));
    rgba[3] = 255.0f;

    rgba[0] *= 255.0f;
    rgba[1] *= 255.0f;
    rgba[2] *= 255.0f;

    /* Swizzle from rgba to argb */
#if 0
    colorT[0] = rgba[3];
    colorT[1] = rgba[0];
    colorT[2] = rgba[1];
    colorT[3] = rgba[2];
#else
    colorT[0] = rgba[2];
    colorT[1] = rgba[1];
    colorT[2] = rgba[0];
    colorT[3] = rgba[3];
#endif

    transpose(colorT);

    cbuffer[0].lo.lo = convert_uchar4_sat(colorT[0]);
    cbuffer[0].lo.hi = convert_uchar4_sat(colorT[1]);
    cbuffer[0].hi.lo = convert_uchar4_sat(colorT[2]);
    cbuffer[0].hi.hi = convert_uchar4_sat(colorT[3]);
  } else {
    /* All rays missed, the bundle is all background */
    cbuffer[0] = as_uchar16(bg_color);
  }
}

__kernel __attribute__((reqd_work_group_size(LWGSIZE, 1, 1))) 
void compute_julia(const float4 muP,
                       __global uchar16 * framebuffer,
                       const struct julia_context jc)
{
  size_t i, j;
  int4 i_iv, j_iv;
  float4 i_fv, j_fv;
  __global uchar16 *output16;
  float4 rO[3], light[3], mu[4], curr_dir[3];
  float4 ddX, ddY;
  float4 ddXx, ddXy, ddXz;
  float4 ddYx, ddYy, ddYz;
  float4 rbsx, rbsy, rbsz;
  float4 px, py;
  float4 inv_width, inv_height;
  const float4 v0123 = {0.0f, 1.0f, 2.0f, 3.0f};
  const int renderShadows = 0;
  uchar16 pcolors;

  /* Setup eye and lighting positions */
  rO[0] = jc.eyeP.x;
  rO[1] = jc.eyeP.y;
  rO[2] = jc.eyeP.z;
  light[0] = jc.lightP.x;
  light[1] = jc.lightP.y;
  light[2] = jc.lightP.z;

  /* Copy the mutation matrix */
  mu[0] = muP.x;
  mu[1] = muP.y;
  mu[2] = muP.z;
  mu[3] = muP.w;

  inv_width = (1.0f / jc.window_size.x);
  inv_height = (1.0f / jc.window_size.y);

  /* The size of the screen */
  ddX = jc.dir_bottom_stop - jc.dir_bottom_start;
  ddY = jc.dir_top_start - jc.dir_bottom_start;

  ddX = ddX * inv_width;
  ddY = ddY * inv_height;

  /* The amount to iterate up and down */
  ddXx = ddX.x;
  ddXy = ddX.y;
  ddXz = ddX.z;

  ddYx = ddY.x;
  ddYy = ddY.y;
  ddYz = ddY.z;

  /* Offset from the bottom of the screen */
  rbsx = jc.dir_bottom_start.x;
  rbsy = jc.dir_bottom_start.y;
  rbsz = jc.dir_bottom_start.z;

  px = v0123;
  py = 0.0f;

  /* See which group of 4 pixels to compute */
  i = get_global_id(0);
  j = get_global_id(1);

  i_iv = (int) i;
  j_iv = (int) j;

  i_fv = convert_float4(i_iv);
  j_fv = convert_float4(j_iv);

  px = px + 4.0f * i_fv; /* Each vector computes 4 pixels in a row */
  py = py + j_fv;

  /* Set the direction of the vector to the pixel group */
  curr_dir[0] = px * ddXx + rbsx;
  curr_dir[1] = px * ddXy + rbsy;
  curr_dir[2] = px * ddXz + rbsz;
  curr_dir[0] = py * ddYx + curr_dir[0];
  curr_dir[1] = py * ddYy + curr_dir[1];
  curr_dir[2] = py * ddYz + curr_dir[2];

  fragmentShader4(rO, curr_dir, mu, jc.epsilon,
                  light, jc.maxIterations, renderShadows, &pcolors);

  /* Output the argb of the 4 pixels into the output array */
  output16 = & framebuffer[(j * jc.stride) / 4 + i];
  output16[0] = pcolors;
}
