/*************************************************************************/
/*                                                                       */
/* Licensed Materials - Property of IBM                                  */
/*                                                                       */
/*                                                                       */
/*                                                                       */
/* (C) Copyright IBM Corp. 200x, 200y                                    */
/*                                                                       */
/*************************************************************************/
/* PROLOG END TAG zYx                                                    */

#ifdef PS3

#include <sched.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/mman.h>
#include <stdint.h>
#include <sys/ioctl.h>
#include <linux/fb.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <signal.h>
#include <pthread.h>
#include <curses.h>
#include <sys/wait.h>

#include <asm/ps3fb.h>

/* PS3 Framebuffer */
struct fb_info
{
  unsigned int x;
  unsigned int y;
  unsigned int x_pad;
  unsigned int y_pad;
  unsigned int num_fb;
};

#define FB_IOCTL_FSEL        _IOW('r', 6, int)
#define FB_IOCTL_OFF          _IO('r', 5 )
#define FB_IOCTL_ON           _IO('r', 4 )
#define FB_IOCTL_SCREENINFO  _IOR('r', 3, int)

int ps3_fb_fd=0;
int ps3_fb_length;
void * ps3_fb_base_addr;
struct fb_info ps3_res;

#define PS3_FB_NAME "/dev/fb0"
#define BPP 4

void *
get_image_buffer(int ps3_frame)
{
  uint32_t unused=0;

  /* wait for vsync interrupt */
  ioctl(ps3_fb_fd, FBIO_WAITFORVSYNC, (unsigned long)&unused);

  return ps3_fb_base_addr + (ps3_res.x * ps3_res.y * BPP) * ps3_frame;
}

void
put_image_buffer(int *ps3_frame)
{
  /* blit and flip with vsync request */
  ioctl(ps3_fb_fd, FB_IOCTL_FSEL, (unsigned long)ps3_frame);
}

void
ps3_finish()
{
  munmap(ps3_fb_base_addr, ps3_fb_length);

  /* Re-enable  ps3fbd */
  ioctl(ps3_fb_fd, FB_IOCTL_OFF, 0);
}

void
ps3_init(int *img_width, int *img_height, int *stride)
{
  int screen_x_res, screen_y_res;

  /* Open the PS3's framebuffer device */
  if ((ps3_fb_fd = open(PS3_FB_NAME, O_RDWR)) < 0) {
    printf("ps3_init failed - exiting\n");
    exit(-1);
  }

  /* Get the FB screen info */
  if (ioctl(ps3_fb_fd, FB_IOCTL_SCREENINFO,
            (unsigned long)&ps3_res) < 0) {
    fprintf (stderr, "ERROR: Could not get ps3 screen info\n");
    goto ps3_init_error;
  }
        
  if (ps3_res.num_fb < 2) {
    fprintf (stderr, "ERROR: Not enough framebuffer memory, reduce resoltion\n");
    goto ps3_init_error;
  }

  ps3_fb_length  = ps3_res.x * ps3_res.y * BPP * ps3_res.num_fb -
    ps3_res.x * ps3_res.y_pad * BPP * 2;

  /* Round xres and yres down to a multiple of 16 */
  screen_x_res = ps3_res.x - 2 * ps3_res.x_pad;
  screen_y_res = ps3_res.y - 2 * ps3_res.y_pad;

  if (screen_x_res < *img_width) {
    *img_width = (screen_x_res / 16) * 16;
  }

  if (screen_y_res < *img_height) {
    *img_height = (screen_y_res / 16) * 16;
  }

  /* Buy some space for the frame counter */
  if (*img_height > (screen_y_res - 16)) {
    *img_height -= 16;
  }

  *stride = (int)ps3_res.x;
  ps3_fb_base_addr = mmap(NULL, ps3_fb_length,
                          PROT_WRITE, MAP_SHARED, ps3_fb_fd, 0);

  if (!ps3_fb_base_addr) {
    goto ps3_init_error;
  }

  return;

 ps3_init_error:
  close(ps3_fb_fd);
  ps3_finish();
  return;
}

void
ps3_switch_fb()
{
  /* stop flipping in kernel thread with vsync*/
  ioctl(ps3_fb_fd, FB_IOCTL_ON, 0);
}

#endif
