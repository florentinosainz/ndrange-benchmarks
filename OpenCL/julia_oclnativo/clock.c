/*************************************************************************/
/*                                                                       */
/* Licensed Materials - Property of IBM                                  */
/*                                                                       */
/*                                                                       */
/*                                                                       */
/* (C) Copyright IBM Corp. 2006, 2009                                    */
/*                                                                       */
/*************************************************************************/


#include <stdio.h>
#include <math.h>
#include <sys/types.h>
#undef __USE_XOPEN2K
#undef __STRICT_ANSI__
#include <sys/times.h>
#include <sys/param.h>
#include <time.h>
static struct tms tbuf;
static clock_t start_time;



void startclock (void)
{
  start_time = times(&tbuf);
  return;
}

float stopclock (void)
{
  float period;
  clock_t stop_time;
  clock_t delta_time;

  stop_time = times(&tbuf);
  delta_time = stop_time - start_time;

  period = (float)(delta_time) / (float)CLK_TCK;
  //period = (float)(delta_time) / (float)CLOCKS_PER_SEC;

  return period;
}

