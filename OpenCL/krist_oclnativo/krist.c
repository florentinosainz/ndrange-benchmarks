#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include "clocks.c"
#include "CL/opencl.h"
#include <omp.h>
#include "krist_auxiliar_header.h"



long int random(void);
inline int min(int x,int y)
{
    return x<y?x:y;
}
void sincosf(float,float*,float*);
void structfac(int na, int nr, float*a, float*h, float*E);

void printa(int na, float*a);
void printh(int nr,float*h);
void deta(int na, float*a);
void deth(int nr, float*h);
void printhe(int nr, float*h, float*E);
double sumdif(float*a,float*b,int n);
int N_DEVICES=1;

cl_device_id* device_id;             // compute device id 
cl_context context;                 // compute context
cl_command_queue* commands;          // compute command queue
cl_program program;                 // compute program
cl_mem* inputs_A;
cl_mem*  inputs_H;
cl_mem*  outputs;


void structfac_gpuss (int time, int na, int nr, int NA, float*a, int NH, float*h, int NE, float*E)
{
    int ii;
	cl_int err;
	cl_kernel kernels[N_DEVICES];

    float f2=0.0f;
    int i;
    for (i=0; i<na; i++)
        f2 += a[i*DIM2_A]*a[i*DIM2_A];
    f2 = 1.0f/sqrtf(f2);
	int n_device;
	for (n_device=0; time==0 && n_device< N_DEVICES;n_device++){
		err = clEnqueueWriteBuffer(commands[n_device], inputs_A[n_device], CL_FALSE, 0, sizeof(float) * NA, a, 0, NULL, NULL);
		if (err != CL_SUCCESS)
		{
			printf("Error: Failed to write to source array %d into device %d!\n",err,n_device);
			exit(1);
		}
	}
	

    for (ii = 0; ii < nr; ii += nr/N_DEVICES) {
        int nr_2 = nr/N_DEVICES;	
		n_device=ii/nr_2;	
		err = clEnqueueWriteBuffer(commands[n_device], inputs_H[n_device], CL_FALSE, 0, sizeof(float) *  NH/N_DEVICES, &h[DIM2_H*ii], 0, NULL, NULL);
		if (err != CL_SUCCESS)
		{
			printf("Error: Failed to write to source array %d into device %d!\n",err,n_device);
			exit(1);
		}
		
		kernels[n_device] = clCreateKernel(program, "cstructfac", &err);
		if (!kernels[n_device] || err != CL_SUCCESS)
		{
			printf("Error: Failed to create compute kernel!\n");
			exit(1);
		}
		int sharedsize = 16384-2048;
		int maxatoms = sharedsize/(sizeof(float)*DIM2_A);
		clSetKernelArg(kernels[n_device], 0, sizeof(int), &na);
		clSetKernelArg(kernels[n_device], 1, sizeof(int), &nr_2);
		clSetKernelArg(kernels[n_device], 2, sizeof(int), &maxatoms);
		clSetKernelArg(kernels[n_device], 3, sizeof(float),&f2);
		clSetKernelArg(kernels[n_device], 4, sizeof(int),&NA);
		clSetKernelArg(kernels[n_device], 5, sizeof(cl_mem), &inputs_A[n_device] );
		int NHX= NH/N_DEVICES;
		clSetKernelArg(kernels[n_device], 6, sizeof(int),&NHX);
		clSetKernelArg(kernels[n_device], 7, sizeof(cl_mem), &inputs_H[n_device] );
		int NEX=NE/N_DEVICES;
		clSetKernelArg(kernels[n_device], 8, sizeof(int),&NEX);
		clSetKernelArg(kernels[n_device], 9, sizeof(cl_mem), &outputs[n_device] );

		size_t local_size_ptr[1];
		size_t global_size_ptr[1];
		local_size_ptr[0]=128;
		global_size_ptr[0]=nr_2;
		local_size_ptr[0] = global_size_ptr[0] < local_size_ptr[0] ? global_size_ptr[0] : local_size_ptr[0];
		global_size_ptr[0] = global_size_ptr[0] < local_size_ptr[0] ? global_size_ptr[0] : global_size_ptr[0] + (global_size_ptr[0] % local_size_ptr[0] == 0 ? 0 : local_size_ptr[0] - global_size_ptr[0] % local_size_ptr[0]);
         	
		//float* shared_mem= (float*) malloc(maxatoms*(sizeof(float)*DIM2_A)); 
        // cstructfac(na, nr_2,maxatoms, f2, NA, a, NH/N_DEVICES, &h[DIM2_H*ii],
                          // NE/N_DEVICES, &E[DIM2_E*ii]);
		clEnqueueNDRangeKernel(commands[n_device], kernels[n_device], 1, NULL, global_size_ptr, local_size_ptr, 0, NULL, NULL);
    }
	//Wait for end and release
	for (i=0; i<N_DEVICES;++i){
		clFinish(commands[i]);
		clReleaseKernel(kernels[i]);
	}
}

int main(int argc, char*argv[])
{

    int na=1000;   /* number of atoms */
    int nr=10000; /* number of reflections */
    int compute_serial = 0;

    float *h;  /* h[j,0] == h, h[j,1] == k, h[j,2] == l */
    float *E;  /* E[j,0] == real part of E, E[j,1] == imag part of E */
    float *E1;  /* E[j,0] == real part of E, E[j,1] == imag part of E */
    float *a;  /* a[j,0] == atomic number, a[j,1] == x, a[j,2] == y,
                 a[j,3] == z */
    double t0,dt1,dt2;

    if (argc > 1) {
        na = atoi(argv[1]);
        nr = atoi(argv[2]);
		N_DEVICES=atoi(argv[3]);
    }
	//Dirty way to initialise :)
	cl_mem inputs_A_aux[N_DEVICES];
	cl_mem inputs_H_aux[N_DEVICES];
	cl_mem outputs_aux[N_DEVICES];
	cl_device_id device_id_aux[N_DEVICES];
	cl_command_queue commands_aux[N_DEVICES];
	inputs_A=inputs_A_aux;
	inputs_H=inputs_H_aux;
	outputs=outputs_aux;
	device_id=device_id_aux;             // compute device id 
    commands=commands_aux;          // compute command queue

    if (argc == 5) {
        if (strcmp(argv[4], "--serial") == 0) {
            compute_serial = 1;
        }
    }
	
	//Init OCL
	cl_int err;
	cl_platform_id plats[4];
	clGetPlatformIDs(4,plats,NULL);

	int gpu=1;
	err = clGetDeviceIDs(plats[0], gpu ? CL_DEVICE_TYPE_GPU : CL_DEVICE_TYPE_CPU, N_DEVICES, device_id, NULL);
    if (err != CL_SUCCESS)
    {
        printf("Error: Failed to create a device group! %d\n",err);
        return EXIT_FAILURE;
    }
	context = clCreateContext(0, N_DEVICES, device_id, NULL, NULL, &err);
    if (!context)
    {
        printf("Error: Failed to create a compute context!\n");
        return EXIT_FAILURE;
    }
	int i;
	for (i=0; i<N_DEVICES;i++){	
		commands[i] = clCreateCommandQueue(context, device_id[i], 0, &err);
		if (!commands[i])
		{
			printf("Error: Failed to create a command commands!\n");
			return EXIT_FAILURE;
		}
	}
	char* ompss_code;    
    FILE *fp;
	size_t source_size;
	fp = fopen("kernel.cl", "r");
	if (!fp) {
		printf("Failed to open file when loading kernel from file\n");
		return EXIT_FAILURE;
	}      
	fseek(fp, 0, SEEK_END); // seek to end of file;
	size_t size = ftell(fp); // get current file pointer
	fseek(fp, 0, SEEK_SET); // seek back to beginning of file
	ompss_code = (char*) malloc(sizeof(char)*size);
	source_size = fread( ompss_code, 1, size, fp);
	fclose(fp); 
	ompss_code[size]=0;
	
	program = clCreateProgramWithSource(context, 1, (const char **) &ompss_code, NULL, &err);
    if (!program)
    {
        printf("Error: Failed to create compute program!\n");
        return EXIT_FAILURE;
    }
	clBuildProgram(program, 0, NULL, "", NULL, NULL);
	size_t n_kernels;
	free(ompss_code);
	

    int NH = DIM2_H*nr;
    int NA = DIM2_A*na;
    int NE = DIM2_E*nr;

    printf("Computation of crystallographic normalized structure factors\n"
           "                on the CPU and the GPU\n\n");
    printf("Number of atoms:       %d\n", na);
    printf("Number of reflections: %d\n", nr);

    h = (float*) malloc(sizeof(*h)*DIM2_H*nr);   // 3*10000 30000
    E = (float*) malloc(sizeof(*E)*DIM2_E*nr);   // 2*10000 20000
    E1 = (float*) malloc(sizeof(*E1)*DIM2_E*nr); // 2*10000 20000
    a = (float*) malloc(sizeof(*a)*DIM2_A*na);   // 4*1000   4000

    for (i=0; i<DIM2_E*nr; i++)
        E1[i] = E[i] = 0.0f;

    deta(na,a);
    deth(nr,h);

    if (compute_serial) {
        printf("Running the CPU code\n");
        t0=wallclock();
        structfac(na,nr,a,h,E);
        dt1 = wallclock() - t0;
        //printhe(nr,h,E);
        //printf("Reference: wallclock time seconds:%f\n",dt1);
        printf("computation time (in seconds): %f\n", dt1);
    }

    int times=10000;
    int tt;
    printf("Running the GPU code %d times\n",times);
	
	
	for (i = 0; i< N_DEVICES; i++){
		inputs_A[i] = clCreateBuffer(context,  CL_MEM_READ_WRITE, sizeof(float) * NA, NULL, &err);
		if (err != CL_SUCCESS){
			printf("Error: Failed to create buffer, err %d!\n",err);
			return EXIT_FAILURE;
		}
		inputs_H[i] = clCreateBuffer(context,  CL_MEM_READ_WRITE, sizeof(float) * NH, NULL, &err);
		if (err != CL_SUCCESS){
			printf("Error: Failed to create buffer, err %d!\n",err);
			return EXIT_FAILURE;
		}
		outputs[i] = clCreateBuffer(context,  CL_MEM_READ_WRITE,  sizeof(float) * NE, NULL, &err);
		if (err != CL_SUCCESS){
			printf("Error: Failed to create buffer, err %d!\n",err);
			return EXIT_FAILURE;
		}		
	}

    t0 = wallclock();
    for (tt=0; tt<times; tt++) {	
        structfac_gpuss(tt,na,nr,NA,a,NH,h,NE,E1);
    }

	
	int n_device;
	for (n_device=0; n_device< N_DEVICES;n_device++){
		clEnqueueReadBuffer(commands[n_device], outputs[n_device], CL_TRUE, 0, sizeof(float) * NE/N_DEVICES, &E[DIM2_E*n_device*(nr/N_DEVICES)], 0, NULL, NULL ); 	
	}
	
    dt2 = (wallclock() - t0) / times;

    if (compute_serial) {
        printf("Cuda:      wallclock time seconds:%f\n",dt2);
    } else {
        printf("computation time (in seconds): %f\n", dt2);
    }
    double sumdf=sumdif(E,E1,2*nr);
    printf("Cuda:      Sumdif: %f mean: %f\n",sumdf,sumdf/nr);

    return 0;
	
	for (i=0; i<N_DEVICES;i++){	
		clReleaseMemObject(inputs_A[i]);
		clReleaseMemObject(inputs_H[i]);
		clReleaseMemObject(outputs[i]);
		clReleaseCommandQueue(commands[i]);
		clReleaseContext(context);
	}
}

void structfac(int na, int nr, float*a, float*h, float*E)
{
    int i,j;
    float A,B,twopi;
    twopi = 6.28318584f;

    float f2 = 0.0f;
    for (i=0; i<na; i++)
        f2 += a[DIM2_A*i]*a[DIM2_A*i];
    f2 = 1.0f/sqrtf(f2);

    //printf("scaling factor is %f\n",f2);

    for (i=0; i<nr; i++) {
        A=0.0f;
        B=0.0f;
        for (j=0; j<na; j++) {
            float A1,B1;
            float arg = twopi*(h[DIM2_H*i+0]*a[DIM2_A*j+1] +
                               h[DIM2_H*i+1]*a[DIM2_A*j+2] +
                               h[DIM2_H*i+2]*a[DIM2_A*j+3]);
            sincosf(arg, &B1, &A1);
            A += a[DIM2_A*j]*A1;
            B += a[DIM2_A*j]*B1;
        }
        E[DIM2_E*i]   = A*f2;
        E[DIM2_E*i+1] = B*f2;
    }
}

// The cstructfac kernel uses the atom vector a and the
// vector h to compute crystallographic normalized structure factors into
// the output vector E
// Note, that this task might be invoked over the same output storage multiple times so we need
// to specify the synchronization dependences so they run properly
// #pragma omp target device (cuda) copy_deps
// #pragma omp task in([NA] a, [NH] h) out([NE] E) //firstprivate (NA, NH, NE)
// void structfac_kernel(int na, int nr, float f2,
        // int NA, float*a, int NH, float*h, int NE, float*E)
// {
    // dim3 threads;
    // threads.x = 128;
    // threads.y = 1;
    // threads.z = 1;

    // int blocks = nr/threads.x;
    // if (blocks*threads.x < nr)
        // blocks++;

    // dim3 grid;
    // grid.x = blocks;
    // grid.y = 1;
    // grid.z = 1;

    // int sharedsize = 16384-2048;
    // int maxatoms = sharedsize/sizeof(TYPE_A);

    // cudaStream_t stream = nanos_get_kernel_execution_stream();
    // if (stream)
    // {
        // cstructfac<<<grid, threads, sharedsize, stream>>> (na,nr,maxatoms,f2,
                // (TYPE_A *) a, (TYPE_H *) h, (TYPE_E *) E);
    // }
    // else
    // {
        // cstructfac<<<grid, threads, sharedsize>>> (na,nr,maxatoms,f2,
                // (TYPE_A *) a, (TYPE_H *) h, (TYPE_E *) E);
    // }
    // //cudaError_t err = cudaGetLastError();
    // //if (err!=cudaSuccess)
    // //    fprintf (stderr,"error %d: %s\n", err, cudaGetErrorString(err));
// }


void printa(int na, float*a)
{
    int i;
    for (i=0; i<na; i++) {
        printf("atom %d: %f %f %f %f\n",i,a[i*DIM2_A] ,a[i*DIM2_A+1], a[i*DIM2_A+2], a[i*DIM2_A+3]);
    }
}

void printh(int nr,float*h)
{
    int i;
    for (i=0; i<nr; i++) {
        printf("hkl %d: %d %d %d\n",i,(int)h[i*DIM2_H],(int)h[i*DIM2_H+1],(int)h[i*DIM2_H+2]);
    }
}

void deta(int na, float*a)
{
    int i,j;
    for (i=0; i<na; i++) {
        if ( i & 1 )
            a[DIM2_A*i] = 6.0;
        else
            a[DIM2_A*i] = 7.0;
        for (j=1; j<DIM2_A; j++)
            a[DIM2_A*i+j] = (float)random()/(float)RAND_MAX;
    }
}

void deth(int nr, float*h)
{
    const int hmax=20;
    const int kmax=30;
    const int lmax=15;
    int i;
    for (i=0; i<nr; i++) {
        h[DIM2_H*i+0] = rintf(2*hmax*(float)random()/(float)RAND_MAX - hmax);
        h[DIM2_H*i+1] = rintf(2*kmax*(float)random()/(float)RAND_MAX - kmax);
        h[DIM2_H*i+2] = rintf(2*lmax*(float)random()/(float)RAND_MAX - lmax);
    }
}

void printhe(int nr, float*h, float*E)
{
    int i;
    for (i=0; i<nr; i++) {
        printf("hkl %5d: %4d %4d %4d %8g %8g\n",i,(int)h[i*DIM2_H],(int)h[i*DIM2_H+1],
               (int)h[i*DIM2_H+2],E[DIM2_E*i],E[DIM2_E*i+1]);
    }
}


double sumdif(float*a, float*b, int n)
{
    double sum = 0.0;
    int i;
    for (i=0; i<n; i++)
        sum += fabsf(a[i] - b[i]);
    return sum;
}

