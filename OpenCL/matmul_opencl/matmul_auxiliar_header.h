
// Thread block size
#define BLOCK_SIZE 16
__constant int BL_SIZE= BLOCK_SIZE;

#ifdef cl_khr_fp64
    #pragma OPENCL EXTENSION cl_khr_fp64 : enable
#elif defined(cl_amd_fp64)
    #pragma OPENCL EXTENSION cl_amd_fp64 : enable
#endif


#ifdef DP
#define REAL double
#else
#define REAL float
#endif

#ifdef __cplusplus
extern "C"
{
#endif


#pragma omp target device(opencl) ndrange(2,NB,NB,BL_SIZE,BL_SIZE) copy_deps
#pragma omp task inout([NB*NB]C) in([NB*NB]A,[NB*NB]B)
__kernel void Muld(__global REAL* A,__global REAL* B, int wA, int wB,__global REAL* C,int NB);

#ifdef __cplusplus
}
#endif
