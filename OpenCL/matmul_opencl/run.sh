#!/bin/bash

# @ partition = projects
# @ output = maymul.log
# @ error = maymul.log
# @ initialdir = .
# @ total_tasks = 1
# @ cpus_per_task = 12 
# @ gpus_per_node = 2
# @ node_usage = not_shared
# @ wall_clock_limit = 00:20:00
# @ intel_opencl = 1

NX_OPENCL_DEVICE_TYPE=GPU NX_OPENCL_MAX_DEVICES=${1:-1} NX_PES=4 NX_ARGS="--disable-cuda" ./matmul
