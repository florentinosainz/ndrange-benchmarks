#!/bin/bash
#search results (Notepad++  "DEVICES RUNs of|computation time|Time in seconds|elapsed|50 Frames took")
#Every benchmark inside CUDA or OpenCL folders MUST have a "run.sh %d" script in which the %d specifies the number of devices
set -e # stop on error
N_EXECUTIONS=10 #Number of times to repeat each benchmark
N_DEVICES=`lspci | grep -i nvidia | wc -l` #number of devices to benchmark (will benchmark from 1 to N_DEVICES, stride of X*2)  Automatically detected, if not working, set manually (we'll make the asumption that they have CUDA and OpenCL working)
BACKUP_DIR=BackupLogs
PARTIAL_EXECUTION=NO #1 = means that we won't re-do benchmarks

CURR_DIR=$PWD
CURR_DATE=$(date +"%Y%m%d%H%M%S")
#function which executes the benchmark inside a folder
execute_benchmark () {    
	if [ "${PARTIAL_EXECUTION}" == "YES" ] ; then
		if [ ${D} == `cat ${CURR_DIR}/lastbenchfinished` ] ; then
			PARTIAL_EXECUTION=NO
		fi
	else
	   printf 'Benchmarking %s on' "${PWD##*/}" 
	   for ((N_D=N_DEVICES; N_D>=1; N_D=N_D/2)) ; do	
			printf ' %s' "${N_D}" 				
	   done
	   printf ' number of devices\n'
	   make clean >  ${CURR_DIR}/compilationoutput___${CURR_DATE}.log 2>&1; make >  ${CURR_DIR}/compilationoutput___${CURR_DATE}.log 2>&1
	   for ((N_D=N_DEVICES; N_D>=1; N_D=N_D/2)) ; do
		 printf 'Starting %s DEVICES RUNs of %s\n\n' "${N_D}" "${PWD##*/}"  >> ${CURR_DIR}/${N_D}devices___${CURR_DATE}.log
		   for ((I=0; I<N_EXECUTIONS; I++)) ; do
			 printf 'NEXT RUN %s\n' "${PWD##*/}"  >> ${CURR_DIR}/${N_D}devices___${CURR_DATE}.log
			 sh run.sh ${N_D} >> ${CURR_DIR}/${N_D}devices___${CURR_DATE}.log 2>&1
		   done
	   done
	   printf '%s' "${D}" > ${CURR_DIR}/lastbenchfinished
	fi
}
if [ ! -f  ${CURR_DIR}/lastbenchfinished ]; then
	mkdir -p $BACKUP_DIR
	set +e
	mv *.log ./$BACKUP_DIR
	set -e
	
	for ((N_D=N_DEVICES; N_D>=1; N_D=N_D/2)) ; do
	 printf 'Starting NEW BENCHMARK RUN\n\n\n\n\n\n\n\n'  >> ${CURR_DIR}/${N_D}devices___${CURR_DATE}.log
	done
else 
	BENCH_CONT=`cat ${CURR_DIR}/lastbenchfinished`
	printf 'Continuing previous BENCHMARK RUN after %s\n' "${BENCH_CONT}"	
	PARTIAL_EXECUTION=YES
fi

#main loop
cd CUDA
for D in *; do
    if [ -d "${D}" ]; then
		cd ./${D}
		execute_benchmark
		cd ..
    fi
done
cd ..
cd OpenCL
for D in *; do
    if [ -d "${D}" ]; then
		cd ./${D}        
		execute_benchmark
		cd ..
    fi
done
#remove semaphore (execution finished)
rm ${CURR_DIR}/lastbench