#include "perlin.h"


 int h_perm[512] = {
    151, 160, 137, 91, 90, 15, 131, 13, 201, 95, 96, 53, 194, 233,
    7, 225, 140, 36, 103, 30, 69, 142, 8, 99, 37, 240, 21, 10, 23,
    190, 6, 148, 247, 120, 234, 75, 0, 26, 197, 62, 94, 252, 219,
    203, 117, 35, 11, 32, 57, 177, 33, 88, 237, 149, 56, 87, 174,
    20, 125, 136, 171, 168, 68, 175, 74, 165, 71, 134, 139, 48,
    27, 166, 77, 146, 158, 231, 83, 111, 229, 122, 60, 211, 133,
    230, 220, 105, 92, 41, 55, 46, 245, 40, 244, 102, 143, 54, 65,
    25, 63, 161, 1, 216, 80, 73, 209, 76, 132, 187, 208, 89, 18,
    169, 200, 196, 135, 130, 116, 188, 159, 86, 164, 100, 109, 198,
    173, 186, 3, 64, 52, 217, 226, 250, 124, 123, 5, 202, 38, 147,
    118, 126, 255, 82, 85, 212, 207, 206, 59, 227, 47, 16, 58, 17,
    182, 189, 28, 42, 223, 183, 170, 213, 119, 248, 152, 2, 44,
    154, 163, 70, 221, 153, 101, 155, 167, 43, 172, 9, 129, 22, 39,
    253, 19, 98, 108, 110, 79, 113, 224, 232, 178, 185, 112, 104,
    218, 246, 97, 228, 251, 34, 242, 193, 238, 210, 144, 12, 191,
    179, 162, 241, 81, 51, 145, 235, 249, 14, 239, 107, 49, 192, 214,
    31, 181, 199, 106, 157, 184, 84, 204, 176, 115, 121, 50, 45, 127,
    4, 150, 254, 138, 236, 205, 93, 222, 114, 67, 29, 24, 72, 243,
    141, 128, 195, 78, 66, 215, 61, 156, 180, 151, 160, 137, 91, 90,
    15, 131, 13, 201, 95, 96, 53, 194, 233, 7, 225, 140, 36, 103, 30,
    69, 142, 8, 99, 37, 240, 21, 10, 23, 190, 6, 148, 247, 120, 234,
    75, 0, 26, 197, 62, 94, 252, 219, 203, 117, 35, 11, 32, 57, 177,
    33, 88, 237, 149, 56, 87, 174, 20, 125, 136, 171, 168, 68, 175,
    74, 165, 71, 134, 139, 48, 27, 166, 77, 146, 158, 231, 83, 111,
    229, 122, 60, 211, 133, 230, 220, 105, 92, 41, 55, 46, 245, 40,
    244, 102, 143, 54, 65, 25, 63, 161, 1, 216, 80, 73, 209, 76, 132,
    187, 208, 89, 18, 169, 200, 196, 135, 130, 116, 188, 159, 86, 164,
    100, 109, 198, 173, 186, 3, 64, 52, 217, 226, 250, 124, 123, 5,
    202, 38, 147, 118, 126, 255, 82, 85, 212, 207, 206, 59, 227, 47,
    16, 58, 17, 182, 189, 28, 42, 223, 183, 170, 213, 119, 248, 152,
    2, 44, 154, 163, 70, 221, 153, 101, 155, 167, 43, 172, 9, 129, 22,
    39, 253, 19, 98, 108, 110, 79, 113, 224, 232, 178, 185, 112, 104,
    218, 246, 97, 228, 251, 34, 242, 193, 238, 210, 144, 12, 191, 179,
    162, 241, 81, 51, 145, 235, 249, 14, 239, 107, 49, 192, 214, 31, 181,
    199, 106, 157, 184, 84, 204, 176, 115, 121, 50, 45, 127, 4, 150, 254,
    138, 236, 205, 93, 222, 114, 67, 29, 24, 72, 243, 141, 128, 195, 78,
    66, 215, 61, 156, 180,
};


/**
 * fade
 *
 * Compute fade value.
 */
 float h_fade(float t)
{
    return t * t * t * (t * (t * 6.0f - 15.0f) + 10.0f);
}


/**
 * lerp
 *
 * Compute linear interpolation value.
 */
 float h_lerp(float t, float a, float b)
{
    return a + t * (b - a);
}

/**
 * grad
 *
 * Compute gradient value.
 */
 float h_grad(int hash, float x, float y, float z)
{
    int h = hash & 15;            // Convert low 4 bits of hash code
    float u = (h < 8) ? x : y;    // into 12 gradient directions.
    float v = (h < 4) ? y : (h == 12 || h == 14) ? x : z;

    u = (h & 1) == 0 ? u : -u;
    v = (h & 2) == 0 ? v : -v;
    return u + v;
}

/**
 * noise3
 *
 * Compute noise value for point.
 */
 float h_noise3(float x, float y, float z)
{
    float floor_x = floor(x);
    float floor_y = floor(y);
    float floor_z = floor(z);

    int X = (int) floor_x & 255;  // Find unit cube that
    int Y = (int) floor_y & 255;  // contains point.
    int Z = (int) floor_z & 255;

    x -= floor_x;                 // Find relative x,y,z
    y -= floor_y;                 // of point in cube.
    z -= floor_z;

    float x1 = x - 1.0f;
    float y1 = y - 1.0f;
    float z1 = z - 1.0f;

    float u = h_fade(x);            // Compute fade curves
    float v = h_fade(y);            // for each of x,y,z.
    float w = h_fade(z);

    int A = h_perm[X] + Y;
    int AA = h_perm[A] + Z;
    int AB = h_perm[A + 1] + Z;     // Hash coordinates of
    int B = h_perm[X + 1] + Y;      // the 8 cube corners.
    int BA = h_perm[B] + Z;
    int BB = h_perm[B + 1] + Z;

    float g0 = h_grad(h_perm[AA], x, y, z);
    float g1 = h_grad(h_perm[BA], x1, y, z);
    float g2 = h_grad(h_perm[AB], x, y1, z);
    float g3 = h_grad(h_perm[BB], x1, y1, z);
    float g4 = h_grad(h_perm[AA + 1], x, y, z1);
    float g5 = h_grad(h_perm[BA + 1], x1, y, z1);
    float g6 = h_grad(h_perm[AB + 1], x, y1, z1);
    float g7 = h_grad(h_perm[BB + 1], x1, y1, z1);

    // Add blended results from 8 corners of cube.
    float u01 = h_lerp(u, g0, g1);
    float u23 = h_lerp(u, g2, g3);
    float u45 = h_lerp(u, g4, g5);
    float u67 = h_lerp(u, g6, g7);

    float v0 = h_lerp(v, u01, u23);
    float v1 = h_lerp(v, u45, u67);

    return h_lerp(w, v0, v1);
}
