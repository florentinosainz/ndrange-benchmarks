/*************************************************************************/
/*                                                                       */
/* Licensed Materials - Property of IBM                                  */
/*                                                                       */
/*                                                                       */
/*                                                                       */
/* (C) Copyright IBM Corp. 2009                                          */
/*                                                                       */
/*************************************************************************/

#ifndef PERLIN_H_
#define PERLIN_H_

#include <sys/types.h>

#define   MAX_IMG_WIDTH     1024
#define   MAX_IMG_HEIGHT    1024
#define   MAX_DATA_SIZE     (MAX_IMG_WIDTH * MAX_IMG_HEIGHT * 4)

typedef struct colors {
    unsigned char r;
    unsigned char g;
    unsigned char b;
    unsigned char a;
} pixel;


#ifdef __cplusplus
extern "C"
{
#endif
int compute_host_and_verify(int iterations, pixel * output_device, int rowstride,
                            int img_height, int img_width, int verify, int data_size, float device_mpix);
                            
void compute_perlin_noise_device(int N_DEVICES, pixel * output, float time, unsigned int rowstride, int img_height, int img_width);          

#ifdef __cplusplus
}
#endif
                  

#endif /*PERLIN_HOST_H_ */
