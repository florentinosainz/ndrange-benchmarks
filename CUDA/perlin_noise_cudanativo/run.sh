#!/bin/bash
# @ job_name = projects
# @ initialdir = .
# @ output = perlin.log
# @ error = perlin.log
# @ total_tasks = 1
# @ gpus_per_node = 2
# @ cpus_per_task = 12
# @ node_usage = not_shared
# @ wall_clock_limit = 00:15:00

./perlin --ndevs ${1:-1} 

