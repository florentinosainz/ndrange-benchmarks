#!/bin/bash


# @ partition = fatgpu
# @ output = nbody.log
# @ error = nbody.log
# @ initialdir = .
# @ total_tasks = 1
# @ cpus_per_task = 8
# @ gpus_per_node = 2
# @ node_usage = not_shared
# @ wall_clock_limit = 00:20:00
# Number of GPUs to use (2 by default)

# Number of GPUs to use (2 by default)
./krist 10000 20000 ${1:-1}  #${1:-1}  is the number of GPUs
