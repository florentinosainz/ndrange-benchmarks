/*************************************************************************/
/*                                                                       */
/* Licensed Materials - Property of IBM                                  */
/*                                                                       */
/*                                                                       */
/*                                                                       */
/* (C) Copyright IBM Corp. 2006, 2009                                    */
/* All Rights Reserved                                                   */
/*                                                                       */
/* US Government Users Restricted Rights - Use, duplication or           */
/* disclosure restricted by GSA ADP Schedule Contract with IBM Corp.     */
/*                                                                       */
/*************************************************************************/
/* --------------------------------------------------------------  */
/* Copyright (c) 1984-2005, Keenan Crane                           */
/* All rights reserved.                                            */
/*                                                                 */
/* Redistribution and use in source and binary forms, with or      */
/* without modification, are permitted provided that the following */
/* conditions are met:                                             */
/*                                                                 */
/* Redistributions of source code must retain the above copyright  */
/* notice, this list of conditions and the following disclaimer.   */
/* Redistributions in binary form must reproduce the above         */
/* copyright notice, this list of conditions and the following     */
/* disclaimer in the documentation and/or other materials provided */
/* with the distribution.                                          */
/*                                                                 */
/* The name of Keenan Crane may not be used to endorse or promote  */
/* products derived from this software without specific prior      */
/* written permission.                                             */
/*                                                                 */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND          */
/* CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,     */
/* INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF        */
/* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE        */
/* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR            */
/* CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,    */
/* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT    */
/* NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;    */
/* LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)        */
/* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN       */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR    */
/* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,  */
/* EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              */
/* --------------------------------------------------------------  */
/* PROLOG END TAG zYx                                              */


#include <stdio.h>
#include <math.h>
#include <sys/types.h>
#undef __USE_XOPEN2K
#undef __STRICT_ANSI__
#include <sys/times.h>
#include <sys/param.h>
#include <time.h>
static struct tms tbuf;
static clock_t start_time;



void startclock (void)
{
  start_time = times(&tbuf);
  return;
}

float stopclock (void)
{
  float period;
  clock_t stop_time;
  clock_t delta_time;

  stop_time = times(&tbuf);
  delta_time = stop_time - start_time;

  period = (float)(delta_time) / (float)CLOCKS_PER_SEC*10000;
  //period = (float)(delta_time) / (float)CLOCKS_PER_SEC;

  return period;
}
 
 
 

#include <math.h>
#include <getopt.h>
#include <sys/stat.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <cuda_runtime.h>


#ifdef PPM
#include "ppm_util.h"
#endif


#include "julia.h"

#define IMG_WIDTH	512
#define IMG_HEIGHT	512
#define DATA_SIZE (IMG_WIDTH * IMG_HEIGHT * sizeof(uint32_t))

/* Framebuffer utilities */
#define OTHER_FRAME(frame) (1 - frame)

typedef struct __uchar8_t {
   uchar4 lo;
   uchar4 hi;
} uchar8;

typedef struct __uchar16_t {
   uchar8 lo;
   uchar8 hi;
} uchar16;

__global__ void cuda_compute_julia_kernel (const float4 muP,
                                               uchar16 * framebuffer,
                                               const struct julia_context jc2);
void *alloc_frame_buffer() {
  void *retPtr;
  int rc;

  rc = posix_memalign(&retPtr, 16, DATA_SIZE);

  if (rc == 0) {
    return retPtr;
  }
  else {
    return NULL;
  }
}

void *get_frame_buffer(int frame, int ps3) {
  return alloc_frame_buffer();
}


/*
 * This structure is passed once between the host and kernel for
 * data initialized before the run.
 */
struct julia_context {
  float dir_top_start[4];
  float dir_bottom_start[4];
  float dir_bottom_stop[4];
  float eyeP[4];
  float lightP[4];
  int   window_size[2];
  float epsilon;
  int maxIterations;
  int stride;
  int pad[3];
} jc;

/* Binary and source data */
#define BIN_FILE_PATH_LEN 1024
#define MAX_DEVICE_NAME_LEN 128
//#define BIN_FILE_PATH_LEN 65536
//#define MAX_DEVICE_NAME_LEN 8192
const char *filename = "julia_kernel.cl";
const char *binary_file_base = "julia_kernel_";

/* workgroup size variables */
#define MULTIPLE(x, y) ((x/y) * y == x)

/* morphing matrix */
float mu[4][4];

/* Camera parameters */
float N[3]; /* Normal to the image plane */
float T[3]; /* Tangent of the image plane */
float B[3]; /* Binormal of the image plane */
program_context_t rc; /* Misc screen parameters */


void calculateView(program_context_t * rc)
{
  /* 
   * First apply the view transformations to the initial eye, look at,
   * and up.  These will be used later to determine the basis.
   */

  int i, j;
  float mag;
  /* eye starts on the unit sphere */
  float eyeStart[4] = { 0.0f, 0.0f, 1.0f, 1.0f };

  /* initially look at the origin */
  float lookatStart[4] = { 0.0f, 0.0f, 0.0f, 1.0f };

  /* up is initially along the y-axis */
  float upStart[4] = { 0.0f, 1.0f, 0.0f, 0.0f };

  /* point light location */
  static float lookAt[4], up[4];


  /* translate the eye and look at points */
  eyeStart[0] += rc->translate[0];
  eyeStart[1] += rc->translate[1];
  eyeStart[2] += rc->zoom;
  lookatStart[0] += rc->translate[0];
  lookatStart[1] += rc->translate[1];
  lookatStart[2] += rc->zoom;

  /* rotate eye, lookat, and up by multiplying them with the current rotation matrix */
  for (i = 0; i < 4; i++) {
    jc.eyeP[i] = 0.0f;
    lookAt[i] = 0.0f;
    up[i] = 0.0f;

    for (j = 0; j < 4; j++) {
      jc.eyeP[i] += rc->curRotation[i * 4 + j] * eyeStart[j];
      lookAt[i] += rc->curRotation[i * 4 + j] * lookatStart[j];
      up[i] += rc->curRotation[i * 4 + j] * upStart[j];
    }
  }

  /* Now we construct the basis: */
  /* N = (look at) - (eye) */
  /* T = up */
  /* B = N x T */

  /* find and normalize N = (lookat - eye) */
  for (i = 0; i < 3; i++)
    N[i] = lookAt[i] - jc.eyeP[i];
  mag = 1.0f / sqrt(N[0] * N[0] + N[1] * N[1] + N[2] * N[2]);
  for (i = 0; i < 3; i++)
    N[i] *= mag;

  /* find and normalize T = up */
  for (i = 0; i < 3; i++)
    T[i] = up[i];
  mag = 1.0f / sqrt(T[0] * T[0] + T[1] * T[1] + T[2] * T[2]);
  for (i = 0; i < 3; i++)
    T[i] *= mag;

  /* find B = N x T (already unit length) */
  B[0] = N[1] * T[2] - N[2] * T[1];
  B[1] = N[2] * T[0] - N[0] * T[2];
  B[2] = N[0] * T[1] - N[1] * T[0];

  /* move the light a little bit up and to the right of the eye.*/
  for (i = 0; i < 3; i++) {
    jc.lightP[i] = jc.eyeP[i] - B[i] * 0.5f;
    jc.lightP[i] += T[i] * 0.5f;
  }
}


/* getCurMu()
 *
 * Get the interpolated constant for the current time (used for
 * mophing between two Julia sets). 
 */
void getCurMu(float *cur, float t)
{
  int i;
  float t0, t1, t2, t3;
  float mt = 1.0f - t;
  float tsq = t * t;
  float tcb = t * t * t;
  float inv6 = 1.0f / 6.0f;

  t0 = mt * mt * mt * inv6;
  t1 = (4.0f + 3.0f * tcb - 6.0f * tsq) * inv6;
  t2 = (1.0f + 3.0f * t + 3.0f * tsq - 3.0f * tcb) * inv6;
  t3 = tcb * inv6;
  for (i = 0; i < 4; i++)
    cur[i] = t0 * mu[0][i] + t1 * mu[1][i] + t2 * mu[2][i] + t3 * mu[3][i];
}

char *usage =
  "Usage: julia_ocl [DEVICE] [KERNELTYPE] [OPTIONS...]\n"
  "\n"
  "Examples:\n"
  "  julia_ocl --accel -i 100     # Run 100 iterations on accelerator\n"
  "  julia_ocl --cpu --source     # Compile from source, run on CPU\n"
  "\n"
  " Device Types:\n"
  "\n"
  "  -a, --accel              use CBEA Accelerator for compute\n"
  "  -c, --cpu                use CPU for compute\n"              
  "  -g, --gpu                use GPU for compute\n"
  "\n"
  " Kernel types:\n"
  "\n"
  "  -b, --binary             attempt to use a precompiled binary kernel\n"
  "  -s, --source             recompile kernel from source (default)\n"
  "\n"
  " Run parameters:\n"
  "\n"
  "  -i, --iterations N       number of iterations each pass (default: %d)\n"
  "  -l, --lwgsize N          local work group size {1,2,4,8,16,32,64,128} (default: %ld)\n"
#ifdef PPM
  "\n"
  " Output options:\n"
  "\n"
  "  -j, --ppm                write ppms for each frame (default: off)\n"
  "  -d, --dumpfinal NAME     write final frame as NAME (default: off)\n"
  "\n"
#endif
  "\n"
  ;

int N_DEVICES=1;

int main(int argc, char *argv[])
{
  int i;
  int iterations = 50;
  float currMu[4];
  float morphTimer = 0.0f;
  //char *kernelSource;

  // cl_platform_id platform_id;   /* compute platform id */
  // cl_device_id device_id;       /* compute device id */
  // cl_context context;           /* compute context */
  // cl_command_queue commands;    /* compute command queue */
  // cl_program program;           /* compute program */
  // cl_kernel kernel;             /* compute kernel */
  float alpha;                  /* height for aspect ratio */
  float beta;                   /* width for aspect ratio */

  const float fRandMax = 1.0f / (float) RAND_MAX; /* used to normalize random values */

  int ps3 = 0;
#ifdef PPM
  int ppm = 0;
  int dumpfinal = 0;
  char finalframe_filename[BIN_FILE_PATH_LEN] = {0};
#endif
  //int source = 1;

  int compute_frame = 0;
  int display_frame = 1;


  //char binary_filename[BIN_FILE_PATH_LEN];

  static struct option long_options[] = {
    {"help", 0, NULL, 'h'},
    /* Devtype */
    {"accel", 0, NULL, 'a'},
    {"cpu", 0, NULL, 'c'},
    {"gpu", 0, NULL, 'g'},
    /* Control */
    {"iterations", 1, NULL, 'i'},
    /* Image output */
#ifdef PPM
    {"ppm", 0, NULL, 'j'},
    {"dumpfinal", required_argument, NULL, 'd'},
#endif
    /* Binary */
    {"binary", 0, NULL, 'b'},
    {"source", 0, NULL, 's'},
    {"lwgsize", 1, NULL, 'l'},
    {"ndevs", 1, NULL, 'k'},
  };
  
  char optstring[128] = "hacgi:bsl:";

#ifdef PPM
  strncat(optstring,"d:j",127);
#endif
    

  while (1) {
    int opt;
    
    int option_index = 0;

    opt = getopt_long(argc, argv, optstring, long_options, &option_index);
        
    if (opt == -1) {
      break;
    }

    switch (opt) {
    case 'a':
      break;
    case 'c':
      break;
    case 'g':
      break;
    case 'i':
      iterations = atoi(optarg);
      break;
#ifdef PPM
    case 'j':
      ppm = 1;
      break;
#endif
    case 'b':
      break;
    case 's':
      break;
    case 'k':
      N_DEVICES = atoi(optarg);
      break;
    case 'l':
       break;
#ifdef PPM
    case 'd':
      dumpfinal = 1;
      strncpy(finalframe_filename,optarg,BIN_FILE_PATH_LEN-1);
      break;
#endif
    default:
    case 'h':
      exit(0);
      break;
    }
  } /* while(1) */

  void* outBuffer[2][N_DEVICES];          /* device memory used for the framebuffer */
  void * framebuffer[2][N_DEVICES];        /* pointer to the framebuffer */
  
  /* Setup the initial morphing matrix */
  mu[0][0] = 0.0f;
  mu[0][1] = 0.0f;
  mu[0][2] = 0.0f;
  mu[0][3] = 0.0f;
  mu[1][0] = 0.0f;
  mu[1][1] = 0.0f;
  mu[1][2] = 0.0f;
  mu[1][3] = 0.0f;
  mu[2][0] = 0.0f;
  mu[2][1] = 0.0f;
  mu[2][2] = 0.0f;
  mu[2][3] = 0.0f;
  mu[3][0] = 2.0f * (rand() * fRandMax) - 1.0f;
  mu[3][1] = 2.0f * (rand() * fRandMax) - 1.0f;
  mu[3][2] = 2.0f * (rand() * fRandMax) - 1.0f;
  mu[3][3] = 2.0f * (rand() * fRandMax) - 1.0f;


  /* Set initial view parametrs */
  rc.translate[0] = 0.0F;
  rc.translate[1] = 0.0F;
  rc.zoom = 2.0F;
  rc.curRotation[0] = 0.0F;
  rc.curRotation[1] = 1.0F;
  rc.curRotation[2] = 0.0F;
  rc.curRotation[3] = 0.0F;
  rc.curRotation[4] = -1.0F;
  rc.curRotation[5] = 0.0F;
  rc.curRotation[6] = 0.0F;
  rc.curRotation[7] = 0.0F;
  rc.curRotation[8] = 0.0F;
  rc.curRotation[9] = 0.0F;
  rc.curRotation[10] = 1.0F;
  rc.curRotation[11] = 0.0F;
  rc.curRotation[12] = 0.0F;
  rc.curRotation[13] = 0.0F;
  rc.curRotation[14] = 0.0F;
  rc.curRotation[15] = 1.0F;
  rc.shadows = 0;

  rc.fov = 60.0f;
  rc.aspect = ((float) IMG_WIDTH) / ((float) IMG_HEIGHT);

  jc.window_size[0] = (float) IMG_WIDTH;
  jc.window_size[1] = (float) IMG_HEIGHT;
  jc.maxIterations = 4;
  jc.epsilon = 0.003f;
  jc.stride = IMG_WIDTH;

  calculateView(&rc);

  beta = tan((rc.fov * M_PI / 180.0f) / 2.0f);   /*find height */
  alpha = beta * rc.aspect;      /*find width */


  /* Create both framebuffers */
  for (i=0; i<N_DEVICES;i++){	
	  cudaSetDevice(i);
	  framebuffer[0][i] = get_frame_buffer(0, ps3);
	  outBuffer[0][i]=0;
	  outBuffer[1][i]=0;
	  cudaMalloc ((void **) &outBuffer[0][i], jc.window_size[0] * jc.window_size[1] * sizeof(uint32_t));	
	  
	  framebuffer[1][i] = get_frame_buffer(1, ps3);
	  cudaMalloc ((void **) &outBuffer[1][i], jc.window_size[0] * jc.window_size[1] * sizeof(uint32_t));	
  }

  /* rendering region: upper left corner */
  jc.dir_top_start[0] = -alpha * T[0] - beta * B[0] + N[0];
  jc.dir_top_start[1] = -alpha * T[1] - beta * B[1] + N[1];
  jc.dir_top_start[2] = -alpha * T[2] - beta * B[2] + N[2];

  /* rendering region: lower left corner */
  jc.dir_bottom_start[0] = -alpha * T[0] + beta * B[0] + N[0];
  jc.dir_bottom_start[1] = -alpha * T[1] + beta * B[1] + N[1];
  jc.dir_bottom_start[2] = -alpha * T[2] + beta * B[2] + N[2];

  /* rendering region: lower right corner */
  jc.dir_bottom_stop[0] = alpha * T[0] + beta * B[0] + N[0];
  jc.dir_bottom_stop[1] = alpha * T[1] + beta * B[1] + N[1];
  jc.dir_bottom_stop[2] = alpha * T[2] + beta * B[2] + N[2];

  // CHECK(clSetKernelArg(kernel, 2, sizeof(struct julia_context), &jc));
  
  startclock();

  /* Create 2 dummy events so we can wait on them for the first round of the loop */
	cudaStream_t stream[2][N_DEVICES];
  for (i=0; i<N_DEVICES;i++){	
	cudaSetDevice(i);
	cudaStreamCreate(&stream[0][i]);
	cudaStreamCreate(&stream[1][i]);
  }
  int n_device=-1;
  //We assume that iterations % N_DEVICES == 0
  for (i = 0; i < iterations; i=i+N_DEVICES) {
	for (n_device=0; n_device < N_DEVICES; ++n_device){
		cudaSetDevice(n_device);
		getCurMu(currMu, morphTimer);
		morphTimer += 0.05f;

		/*
		 * Wait for the previous display_frame (now compute_frame) to
		 * finish unmapping before we begin computing
		 */
		dim3 dimBlock,dimGrid;
	    dimBlock.x = 16;
	    dimBlock.y = 1;
	    dimBlock.z = 1;
	   
	    dimGrid.x = (jc.window_size[0]/4)/dimBlock.x; //_args->jc_0.window_size.x/dimBlock.x;
	    dimGrid.y = (jc.window_size[1])/dimBlock.y;//_args->BSY_0/dimBlock.y;
	    dimGrid.z = 1;
		   
		float4 cMu = { currMu[0], currMu[1], currMu[2], currMu[3] };
		cuda_compute_julia_kernel<<<dimGrid, dimBlock ,0, stream[compute_frame][n_device]>>> (cMu, (uchar16 *) outBuffer[compute_frame][n_device], jc);
		cudaError_t error = cudaGetLastError();
	   if(error!=cudaSuccess) {
		  fprintf(stderr,"ERROR: %s: %s\n", "launching kernel", cudaGetErrorString(error) );
		  exit(-1);
	   }  

		/* Alter the morphing matrix */
		if (morphTimer >= 1.0f) {
		  morphTimer -= 1.0f;

		  mu[0][0] = mu[1][0];
		  mu[0][1] = mu[1][1];
		  mu[0][2] = mu[1][2];
		  mu[0][3] = mu[1][3];

		  mu[1][0] = mu[2][0];
		  mu[1][1] = mu[2][1];
		  mu[1][2] = mu[2][2];
		  mu[1][3] = mu[2][3];

		  mu[2][0] = mu[3][0];
		  mu[2][1] = mu[3][1];
		  mu[2][2] = mu[3][2];
		  mu[2][3] = mu[3][3];

		  mu[3][0] = 2.0f * rand() / (float) (RAND_MAX) - 1.0f;
		  mu[3][1] = 2.0f * rand() / (float) (RAND_MAX) - 1.0f;
		  mu[3][2] = 2.0f * rand() / (float) (RAND_MAX) - 1.0f;
		  mu[3][3] = 2.0f * rand() / (float) (RAND_MAX) - 1.0f;
		}
	}
	
    
	for (n_device=0; n_device < N_DEVICES; ++n_device){
		cudaSetDevice(n_device);
		cudaStreamSynchronize ( stream[display_frame][n_device] );
		cudaMemcpy(framebuffer[display_frame][n_device], outBuffer[display_frame][n_device], jc.window_size[0] * jc.window_size[1] * sizeof(uint32_t), cudaMemcpyDeviceToHost);

		/* Write out ppm to a file */
		#ifdef PPM
		if (ppm && i > 0) {
		  char ppm_name[BIN_FILE_PATH_LEN];
		  snprintf(ppm_name, 1023, "julia_%04d.ppm", i - 1);
		  put_ppm(ppm_name, (unsigned int *) framebuffer[display_frame][n_device],
				  jc.window_size[0], jc.window_size[1], jc.stride);
		}
		#endif

	}
	/* Swap the frames for the next iteration */
	display_frame = compute_frame;
	compute_frame = OTHER_FRAME(compute_frame);

  } /* for */
  n_device--;
  cudaSetDevice(n_device);
  cudaMemcpy(framebuffer[display_frame][n_device], outBuffer[display_frame][n_device], jc.window_size[0] * jc.window_size[1] * sizeof(uint32_t), cudaMemcpyDeviceToHost);
  cudaDeviceSynchronize();

  #ifdef PPM
  if (ppm || dumpfinal) {
    char ppm_name[BIN_FILE_PATH_LEN];
    if (ppm) {
      snprintf(ppm_name, 1023, "julia_%04d.ppm", i - 1);
      put_ppm(ppm_name, (unsigned int *) framebuffer[display_frame][n_device],
              jc.window_size[0], jc.window_size[1], jc.stride);
    }

    if (dumpfinal) {
      put_ppm(finalframe_filename, 
              (unsigned int *) framebuffer[display_frame][n_device],
              jc.window_size[0], jc.window_size[1], jc.stride);
    }

  } /* (ppm || dumpfinal) */
  #endif

  float seconds=stopclock();

  printf("%d Frames took %f seconds. Rate = %f Mpixels/sec\n",
         iterations, seconds,
         (double) (IMG_WIDTH * IMG_HEIGHT) * (double) (iterations) /
         (1000000.0 * (double) (seconds)));



  /* Shutdown and cleanup */
  for (i=0; i<N_DEVICES;i++){	
	  cudaFree( outBuffer[1][i]);
	  cudaFree( outBuffer[1][i]);
  }
  
  return 0;
}


