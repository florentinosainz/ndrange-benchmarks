/*************************************************************************/
/*                                                                       */
/* Licensed Materials - Property of IBM                                  */
/*                                                                       */
/*                                                                       */
/*                                                                       */
/* (C) Copyright IBM Corp. 2006, 2009                                    */
/*                                                                       */
/*************************************************************************/

#ifndef _ppm_util_h_
#define _ppm_util_h_

#ifdef PPM

#include <netpbm/ppm.h>

/* PS3 framebuffer is ARGB */
#define _R(_pix)		(((_pix) >> 16) & 0xFF)
#define _G(_pix)		(((_pix) >> 8)  & 0xFF)
#define _B(_pix)		(((_pix) >> 0)  & 0xFF)
#define _A(_pix)		(((_pix) >> 24) & 0xFF)
#define _RGB(_r, _g, _b) 	(((_b) << 0) | ((_g) << 8) | ((_r) << 16))

static inline void
put_ppm (char *name, unsigned int *img,
         unsigned int w, unsigned int h, unsigned int stride)
{
  FILE *f;
  unsigned int *tmp;
  pixel **ppm_pixels, *out;
  unsigned int i, j;

  ppm_pixels = ppm_allocarray (w, h);
  if (ppm_pixels == NULL)
    {
      fprintf (stderr, "ppm_allocarray() failed.\n");
      exit (1);
    }

  tmp = img;
  for (j = 0; j < h; j++)
    {
      int idx = j;

      out = (pixel *) & ppm_pixels[idx][0];

      tmp = img + (j * stride);

      for (i = 0; i < w; i++, tmp++)
	{
	  unsigned int r = _R (tmp[0]);
	  unsigned int g = _G (tmp[0]);
	  unsigned int b = _B (tmp[0]);
	  pixel outpix;

	  PPM_PUTR (outpix, r);
	  PPM_PUTG (outpix, g);
	  PPM_PUTB (outpix, b);

	  out[i] = outpix;
	}
    }

  f = fopen (name, "wb");
  if (f == NULL)
    {
      perror ("Failed to create PPM file\n");
      exit (1);
    }
  ppm_writeppm (f, ppm_pixels, w, h, 255, 0);
  fclose (f);
  ppm_freearray (ppm_pixels, h);
}

#endif /* PPM */

#endif /* _ppm_util_h_ */
