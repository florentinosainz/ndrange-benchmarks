#include "nbody.h"

#ifdef __cplusplus
extern "C" {
#endif

#pragma omp target device(cuda) ndrange(1,size,MAX_NUM_THREADS) copy_in(d_particles[0;number_of_particles]) copy_out([size] output)
#pragma omp task out([size] output) \
                        in(d_particles[0*size;1*size], \
                        d_particles[1*size;2*size], \
                        d_particles[2*size;3*size] , \
                        d_particles[3*size;4*size] , \
                        d_particles[4*size;5*size] , \
                        d_particles[5*size;6*size] , \
                        d_particles[6*size;7*size] , \
                        d_particles[7*size;8*size] )
__global__ void calculate_force_func(int size, float time_interval,  int number_of_particles, 
                                              Particle* d_particles, Particle *output, 
											  int first_local, int last_local);
#ifdef __cplusplus
}
#endif
