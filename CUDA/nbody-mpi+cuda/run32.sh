#!/bin/bash

# @ partition = projects
# @ output = nbody.log
# @ error = nbody.log
# @ initialdir = .
# @ total_tasks = 32
# @ cpus_per_task = 12
# @ gpus_per_node = 2
# @ node_usage = not_shared
# @ wall_clock_limit = 01:00:00


NX_PES=4  NX_GPUS=$1 NX_ARGS="--gpu-cache-policy=wt --cache-policy=wt --gpu-max-memory=1000000000" mpirun -n 32 ./nbody nbody-input-2097152.in
#NX_ARGS="--disable-opencl --gpu-cache-policy=wt --gpus=1 --cache-policy=wt --gpu-max-memory=1000000000" mpirun -n 32 ./nbody nbody_input-16384.in
#NX_ARGS="--disable-opencl --gpu-cache-policy=wt --gpus=1 --cache-policy=wt --gpu-max-memory=1000000000" mpirun -n 32 ./nbody nbody_input-16384.in
