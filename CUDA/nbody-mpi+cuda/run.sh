#!/bin/bash

# @ partition = projects
# @ output = nbody.log
# @ error = nbody.log
# @ initialdir = .
# @ total_tasks = 4
# @ cpus_per_task = 12
# @ gpus_per_node = 2
# @ node_usage = not_shared
# @ wall_clock_limit = 01:00:00

NX_PES=4 NX_GPUS=${1:-1}  NX_ARGS="--gpu-cache-policy=wt --cache-policy=wt --gpu-max-memory=1000000000" mpirun -n 4 ./nbody nbody_input-16384.in ${1:-1} 
