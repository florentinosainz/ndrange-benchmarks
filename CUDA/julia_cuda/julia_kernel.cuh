
struct julia_context; 

typedef struct __uchar8_t {
   uchar4 lo;
   uchar4 hi;
} uchar8;

typedef struct __uchar16_t {
   uchar8 lo;
   uchar8 hi;
} uchar16;


typedef struct __float8_t {
   float4 lo;
   float4 hi;
} float8;

typedef struct __float16_t {
   float8 lo;
   float8 hi;
} float16;


#ifdef __cplusplus
extern "C"
{
#endif
#pragma omp target device(cuda) ndrange(2, jc2.window_size[0]/4,jc2.window_size[1], BSIZE,1) copy_deps
#pragma omp task out(framebuffer[0;(jc2.window_size[0] * jc2.window_size[1]*sizeof(uint32_t))/sizeof(uchar16)])
__global__ void cuda_compute_julia_kernel (const float muP0,const float muP1,const float muP2,const float muP3,
                                               uchar16 * framebuffer,
                                               const struct julia_context jc2, int BSIZE);
#ifdef __cplusplus
}
#endif