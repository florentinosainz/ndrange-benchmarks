#!/bin/bash

# @ partition = fatgpu
# @ output = nbody.log
# @ error = nbody.log
# @ initialdir = .
# @ total_tasks = 1
# @ cpus_per_task = 8
# @ gpus_per_node = 2
# @ node_usage = not_shared
# @ wall_clock_limit = 00:20:00

export NX_PES=4
export NX_GPUS=${1:-1} 

NX_ARGS="--disable-opencl" ./julia --dumpfinal output
