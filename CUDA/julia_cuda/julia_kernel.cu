/* frag.c - SPU-C fractal raytracer, ported from
 *
 *      QJuliaFragment.cg
 *      Keenan Crane (kcrane@uiuc.edu)
 *
 * See:
 *   http://www.devmaster.net/forums/showthread.php?t=4448
 */

#include "julia_kernel.cuh"


__device__ inline uchar4 
convert_uchar4_sat(float4 f)
{
   uchar4 c;
   if (f.x>255.0f) c.x = 255; else c.x = (unsigned char) f.x;
   if (f.y>255.0f) c.y = 255; else c.y = (unsigned char) f.y;
   if (f.z>255.0f) c.z = 255; else c.z = (unsigned char) f.z;
   if (f.w>255.0f) c.w = 255; else c.w = (unsigned char) f.w;
   return c;
}

__device__ inline float4
convert_float4(int4 v)
{
   float4 res = { (float) v.x, (float) v.y, (float) v.z, (float) v.w };
   return res;
}

#if 1
__device__ inline int4
operator~( int4 rhs)
{
   int4 res = { ~rhs.x , ~rhs.y , ~rhs.z , ~rhs.w };
   return res;
}
__device__ inline int4
operator!( int4 rhs)
{
   int4 res = { !rhs.x , !rhs.y , !rhs.z , !rhs.w };
   return res;
}
#else
__device__ inline int4
operator~( int4 *rhs)
{
   int4 res = { ~(*rhs).x , ~(*rhs).y , ~(*rhs).z , ~(*rhs).w };
   return res;
}
#endif

__device__  inline float4
operator-( float4 rhs)
{
   float4 res = { -rhs.x , -rhs.y , -rhs.z , -rhs.w };
   return res;
}

__device__  inline float4
max( float4 lhs, float4 rhs)
{
   float4 res = { max(lhs.x, rhs.x) , max(lhs.y,rhs.y) ,
                  max(lhs.z, rhs.z) , max(lhs.w,rhs.w) };
   return res;
}

__device__  inline float4
pow( float4 lhs, float4 rhs)
{
   float4 res = { powf(lhs.x, rhs.x) , powf(lhs.y,rhs.y) ,
                  powf(lhs.z, rhs.z) , powf(lhs.w,rhs.w) };
   return res;
}
#if 0
__device__  inline float4
pow( float4 & lhs, const float4 & rhs)
{
   float4 res = { pow(lhs.x, rhs.x) , pow(lhs.y,rhs.y) ,
                  pow(lhs.z, rhs.z) , pow(lhs.w,rhs.w) };
   return res;
}
#endif

__device__ inline float4
min( float4 lhs, float4 rhs)
{
   float4 res = { min(lhs.x, rhs.x) , min(lhs.y,rhs.y) ,
                  min(lhs.z, rhs.z) , min(lhs.w,rhs.w) };
   return res;
}

__device__ inline float4
operator+( float4 lhs, float4 rhs)
{
   float4 res = { lhs.x + rhs.x , lhs.y + rhs.y ,
                  lhs.z + rhs.z , lhs.w + rhs.w };
   return res;
}

#if 0
__device__ /*inline*/ float16
operator+( float16 lhs, float16 rhs)
{
   float16 res = {
      lhs.lo.lo + rhs.lo.lo, lhs.lo.hi + rhs.lo.hi,
      lhs.hi.lo + rhs.hi.lo, lhs.hi.hi + rhs.hi.hi };
   return res;
}

#endif
__device__ inline float16
operator+( float16 lhs, float16 rhs)
{
   float16 res = 
     { lhs.lo.lo.x + rhs.lo.lo.x , lhs.lo.lo.y + rhs.lo.lo.y , 
           lhs.lo.lo.z + rhs.lo.lo.z , lhs.lo.lo.w + rhs.lo.lo.w,
       lhs.lo.hi.x + rhs.lo.hi.x , lhs.lo.hi.y + rhs.lo.hi.y ,
           lhs.lo.hi.z + rhs.lo.hi.z , lhs.lo.hi.w + rhs.lo.hi.w,
       lhs.hi.lo.x + rhs.hi.lo.x , lhs.hi.lo.y + rhs.hi.lo.y ,
           lhs.hi.lo.z + rhs.hi.lo.z , lhs.hi.lo.w + rhs.hi.lo.w,
       lhs.hi.hi.x + rhs.hi.hi.x , lhs.hi.hi.y + rhs.hi.hi.y ,
           lhs.hi.hi.z + rhs.hi.hi.z , lhs.hi.hi.w + rhs.hi.hi.w,
     };
   return res;
}

#if 0
#define operatorAdd(r,lhs,rhs) { \
   r.lo.lo.x = lhs.lo.lo.x + rhs.lo.lo.x, \
   r.lo.lo.y = lhs.lo.lo.y + rhs.lo.lo.y, \
   r.
#endif

#if 1
__device__ inline float16
operator-( float16 lhs, float16 rhs)
{
   float16 res = 
     { lhs.lo.lo.x - rhs.lo.lo.x , lhs.lo.lo.y - rhs.lo.lo.y , 
           lhs.lo.lo.z - rhs.lo.lo.z , lhs.lo.lo.w - rhs.lo.lo.w,
       lhs.lo.hi.x - rhs.lo.hi.x , lhs.lo.hi.y - rhs.lo.hi.y ,
           lhs.lo.hi.z - rhs.lo.hi.z , lhs.lo.hi.w - rhs.lo.hi.w,
       lhs.hi.lo.x - rhs.hi.lo.x , lhs.hi.lo.y - rhs.hi.lo.y ,
           lhs.hi.lo.z - rhs.hi.lo.z , lhs.hi.lo.w - rhs.hi.lo.w,
       lhs.hi.hi.x - rhs.hi.hi.x , lhs.hi.hi.y - rhs.hi.hi.y ,
           lhs.hi.hi.z - rhs.hi.hi.z , lhs.hi.hi.w - rhs.hi.hi.w,
     };
   return res;
}
#endif
__device__ inline int4
operator>( float4 lhs, float4 rhs)
{
   //int4 res = { lhs.x > rhs.x , lhs.y > rhs.y ,
   //               lhs.z > rhs.z , lhs.w > rhs.w };
   int4 res = { (lhs.x > rhs.x) ? -1 : 0, (lhs.y > rhs.y) ? -1 : 0,
                (lhs.z > rhs.z) ? -1 : 0, (lhs.w > rhs.w) ? -1 : 0 };
   return res;
}

__device__ inline float4
operator-( float4 lhs, float4 rhs)
{
   float4 res = { lhs.x - rhs.x , lhs.y - rhs.y ,
                  lhs.z - rhs.z , lhs.w - rhs.w };
   return res;
}


__device__ inline float4
operator*( float4 lhs, float4 rhs)
{
   register float4 res = { lhs.x * rhs.x , lhs.y * rhs.y , 
                  lhs.z * rhs.z , lhs.w * rhs.w };
   return res;
}

__device__ inline int4
operator&( int4 lhs, int4 rhs)
{
   int4 res = { lhs.x & rhs.x , lhs.y & rhs.y , 
                  lhs.z & rhs.z , lhs.w & rhs.w };
   return res;
}

__device__ inline int4
operator&=( int4 lhs, int4 rhs)
{
   int4 res = { lhs.x &= rhs.x , lhs.y &= rhs.y , 
                  lhs.z &= rhs.z , lhs.w &= rhs.w };
   return res;
}

__device__ inline float4
operator*=( float4 lhs, float rhs)
{
   float4 res = { lhs.x *= rhs , lhs.y *= rhs , 
                  lhs.z *= rhs , lhs.w *= rhs };
   return res;
}

__device__ inline int4
operator|( int4 lhs, int4 rhs)
{
   int4 res = { lhs.x | rhs.x , lhs.y | rhs.y , 
                  lhs.z | rhs.z , lhs.w | rhs.w };
   return res;
}

__device__ inline int4
operator&( int4 lhs, int rhs)
{
   int4 res = { lhs.x & rhs , lhs.y & rhs , lhs.z & rhs , lhs.w & rhs };
   return res;
}


__device__ inline float4
operator*( float4 lhs, float rhs)
{
   float4 res = { lhs.x * rhs , lhs.y * rhs , lhs.z * rhs , lhs.w * rhs };
   return res;
}

__device__ inline float4
operator*( float lhs, float4 rhs)
{
   float4 res = { lhs * rhs.x , lhs * rhs.y , lhs * rhs.z , lhs * rhs.w };
   return res;
}

__device__ inline float4
operator/( float lhs, float4 rhs)
{
   float4 res = { lhs / rhs.x , lhs / rhs.y , lhs / rhs.z , lhs / rhs.w };
   return res;
}

__device__ inline float4
rsqrt(float4 v)
{
   float4 res = { 1.0f/sqrtf(v.x), 1.0f/sqrtf(v.y) ,
                  1.0f/sqrtf(v.z), 1.0f/sqrtf(v.w) };
   return res;
}

__device__ inline float4
sqrt(float4 v)
{
   float4 res = { sqrtf(v.x), sqrtf(v.y) ,
                  sqrtf(v.z), sqrtf(v.w) };
   return res;
}

__device__ inline float4
fabs(float4 v)
{
   float4 res = { fabsf(v.x), fabsf(v.y) ,
                  fabsf(v.z), fabsf(v.w) };
   return res;
}

__device__ inline float4
log(float4 v)
{
   float4 res = { logf(v.x), logf(v.y) ,
                  logf(v.z), logf(v.w) };
   return res;
}


__device__ inline int any(int4 v)
{
   if ( (v.x & 0x80000000) || (v.y & 0x80000000) ||
             (v.z & 0x80000000) || (v.w & 0x80000000)) return 1;
   //if ( (v.x & 0x00000001) || (v.y & 0x00000001) ||
             //(v.z & 0x00000001) || (v.w & 0x00000001)) return 1;
   return 0;
}

__device__ inline float4 as_float4(int4 v)
{
   union {
      int4 vi;
      float4 vf;
   } vv;
   vv.vi = v;
   return vv.vf;
}

__device__ inline uchar16 as_uchar16 (uint4 v)
{
   union {
      uint4   u;
      uchar16 c;
   } vv;
   vv.u = v;
   return vv.c;
}
__device__ inline uchar16 as_uchar16 (float4 v)
{
   union {
      float4   u;
      uchar16 c;
   } vv;
   vv.u = v;
   return vv.c;
}

__device__ inline float4 bitselect (float4 a, float4 b, float4 c)
{
   union {
      int4 vi;
      float4 vf;
   } va, vb, vc, r;
   va.vf = a;
   vb.vf = b;
   vc.vf = c;

   r.vi = (va.vi & ~vc.vi) | (vb.vi & vc.vi);
   return r.vf;
}



#define BOUNDING_RADIUS_2       3.0f
#define ESCAPE_THRESHOLD       10.0f
#define DEL                    1e-4f

#if 0
  struct currMu_s {
     float currMu[4];
  };
#endif

struct julia_context {
  float4 dir_top_start;
  float4 dir_bottom_start;
  float4 dir_bottom_stop;
  float4 eyeP;
  float4 lightP;
  int2 window_size;
  float epsilon;
  int maxIterations;
  int stride;
  int pad[3];
};

#if 1
/**
 * normalize3_v
 *     Normalize 3 SOA vectors
 */
__device__ __inline__
void
normalize3_v(float4 * rnx, float4 * rny, float4 * rnz,
             float4    nx, float4    ny, float4    nz)
{
#if 1
  float4 dot, invlen;

#if 0
if (vector_location < (1024*1024-4)) {
  vector_out[vector_location++] = nx;
  vector_out[vector_location++] = ny;
  vector_out[vector_location++] = nz;
  vector_out[vector_location++] = (float4){ 0.3f, 0.3f, 0.3f, 0.3f };
}
#endif
  dot = nx * nx;
  dot = ny * ny + dot;
  dot = nz * nz + dot;
  invlen = rsqrt(dot);

  *rnx = nx * invlen;
  *rny = ny * invlen;
  *rnz = nz * invlen;

#if 0
if (vector_location < (1024*1024-4)) {
  vector_out[vector_location++] = *rnx;
  vector_out[vector_location++] = *rny;
  vector_out[vector_location++] = *rnz;
  vector_out[vector_location++] = invlen;
}
#endif

#else
  float4 invlen;
  invlen = rsqrt(nx * nx+ny * ny+nz * nz);
  *rnx = nx * invlen;
  *rny = ny * invlen;
  *rnz = nz * invlen;
#endif
}


/**
 * length4_v
 *     Return the length of 4 SOA vectors
 */
__device__ inline
float4 length4_v(float4 x, float4 y, float4 z, float4 w)
{
  float4 dot;

  dot = x * x;
  dot = y * y + dot;
  dot = z * z + dot;
  dot = w * w + dot;
  return (sqrt(dot));
}

/**
 * length4_v
 *     Return the length of 4 SOA vectors
 */
__device__
inline float4 length16_v(float16 v)
{
  return length4_v(v.lo.lo, v.lo.hi, v.hi.lo, v.hi.hi);
}

/**
 * dot_product3_v
 *     compute the dot product of 3 SOA vectors
 */

__device__
inline float4
dot_product3_v(float4 ax, float4 ay, float4 az,
               float4 bx, float4 by, float4 bz)
{
  float4 dot;

  dot = ax * bx;
  dot = ay * by + dot;
  dot = az * bz + dot;
  return dot;
}

/**
 * dot_product4_v
 *     compute the dot product of 4 SOA vectors
 */
__device__
inline float4
dot_product4_v(float4 ax, float4 ay, float4 az, float4 aw,
               float4 bx, float4 by, float4 bz, float4 bw)
{
  float4 dot;

  dot = ax * bx;
  dot = ay * by + dot;
  dot = az * bz + dot;
  dot = aw * bw + dot;
  return dot;
}

__device__
inline float4 dot_product16_v(float16 a, float16 b)
{
  return dot_product4_v(a.lo.lo, a.lo.hi, a.hi.lo, a.hi.hi,
                        b.lo.lo, b.lo.hi, b.hi.lo, b.hi.hi);
}

/**
 * cross_product3_v
 *     compute cross product of 3 SOA vectors
 */
__device__
inline void
cross_product3_v(float4 * xOut, float4 * yOut, float4 * zOut,
                 float4   x1, float4   y1, float4   z1,
                 float4   x2, float4   y2, float4   z2)
{
  *xOut = (y1 * z2) - (z1 * y2);
  *yOut = (z1 * x2) - (x1 * z2);
  *zOut = (x1 * y2) - (y1 * x2);
}

/**
 * transpose
 *     transpose a 4x4 float array
 */
__device__
inline void transpose(float4 m[4])
{
  union {
     float16 v16;
     float4  v4[4];
  } fl16;
  // Read Matrix into a float16 vector
  float16 x;
  float16 t;

  fl16.v4[0] = m[0], fl16.v4[1] = m[1], fl16.v4[2] = m[2], fl16.v4[3] = m[3];
  x = fl16.v16;

#ifdef __CL_FLOAT16__
  x.v4[0] = m[0];

  x.x = m[0], x.y = m[1], x.z = m[2], x.w = m[3];

  // Transpose
  t.even = x.lo;
  t.odd = x.hi;
  x.even = t.lo;
  x.odd = t.hi;
     // suspicious
  t.hi.hi = x.lo.hi;
  t.hi.lo = x.lo.lo;
  t.lo.hi = x.hi.hi;
  t.lo.lo = x.hi.lo;
  x.hi.hi = t.lo.hi;
  x.hi.lo = t.lo.lo;
  x.lo.hi = t.hi.hi;
  x.lo.lo = t.hi.lo;
#endif
  t.lo.lo.x = x.lo.lo.x;
  t.lo.lo.y = x.lo.hi.x;
  t.lo.lo.z = x.hi.lo.x;
  t.lo.lo.w = x.hi.hi.x;

  t.lo.hi.x = x.lo.lo.y;
  t.lo.hi.y = x.lo.hi.y;
  t.lo.hi.z = x.hi.lo.y;
  t.lo.hi.w = x.hi.hi.y;

  t.hi.lo.x = x.lo.lo.z;
  t.hi.lo.y = x.lo.hi.z;
  t.hi.lo.z = x.hi.lo.z;
  t.hi.lo.w = x.hi.hi.z;

  t.hi.hi.x = x.lo.lo.w;
  t.hi.hi.y = x.lo.hi.w;
  t.hi.hi.z = x.hi.lo.w;
  t.hi.hi.w = x.hi.hi.w;

  

  // write back
  m[0] = t.lo.lo;
  m[1] = t.lo.hi;
  m[2] = t.hi.lo;
  m[3] = t.hi.hi;
}


/**
 * quatMult4
 *    Multiply 4 SOA quaternians
 */
__device__
void
quatMult4(float4  q1x, float4  q1y, float4  q1z,
          float4  q1w, float4  q2x, float4  q2y,
          float4  q2z, float4  q2w, float4 * rx,
          float4 * ry, float4 * rz, float4 * rw)
{
  float4 cy, cz, cw;

#if 1
  *rx = dot_product3_v(q1y, q1z, q1w, q2y, q2z, q2w);
  *rx = q1x * q2x - *rx;
  cross_product3_v(&cy, &cz, &cw, q1y, q1z, q1w, q2y, q2z, q2w);
  *ry = q2x * q1y + cy;
  *rz = q2x * q1z + cz;
  *rw = q2x * q1w + cw;
  *ry = q1x * q2y + *ry;
  *rz = q1x * q2z + *rz;
  *rw = q1x * q2w + *rw;
#endif
}


/**
 * quatMult4
 *    Multiply 4 SOA quaternians
 */

__device__ inline
void
quatSq4(float4  qx, float4  qy, float4  qz, float4  qw,
        float4 * rx, float4 * ry, float4 * rz, float4 * rw)
{
  float4 qx2;
  float4 dotq;

  qx2 = qx * 2.0f;
  dotq = dot_product3_v(qy, qz, qw, qy, qz, qw);
  *rx = qx * qx - dotq;
  *ry = qx2 * qy;
  *rz = qx2 * qz;
  *rw = qx2 * qw;
}

__device__ inline
void quatSq16(float16 q, float16 * r)
{
  union {
     float16 v16;
     float4  v4[4];
  } fl16;

  quatSq4(q.lo.lo, q.lo.hi, q.hi.lo, q.hi.hi,
                  &fl16.v4[0], &fl16.v4[1], &fl16.v4[2], &fl16.v4[3]);

  *r = fl16.v16; //(float16) {rlolo, rlohi, rhilo, rhihi};
}

__device__ void iterateIntersect(float4 * q, float4 * qp, float4 * c, int maxIterations)
{
  int i = 0;
  float4 dqq;
  float4 qx = q[0];
  float4 qy = q[1];
  float4 qz = q[2];
  float4 qw = q[3];
  float4 qpx = qp[0];
  float4 qpy = qp[1];
  float4 qpz = qp[2];
  float4 qpw = qp[3];
  int4 tmp;
  int4 write_mask =  { 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF };
  float4 escape = {ESCAPE_THRESHOLD,ESCAPE_THRESHOLD,
                                      ESCAPE_THRESHOLD,ESCAPE_THRESHOLD};

#if 1
  while ((i < maxIterations) && (any(write_mask))) {
#if 0
   vector_out[vector_location].x = (float) write_mask.x;
   vector_out[vector_location].y = (float) write_mask.y;
   vector_out[vector_location].z = (float) write_mask.z;
   vector_out[vector_location++].w = (float) write_mask.w;
#endif
    quatMult4(qx, qy, qz, qw, qpx, qpy, qpz, qpw, &qpx, &qpy, &qpz, &qpw);

    qpx = 2.0f * qpx;
    qpy = 2.0f * qpy;
    qpz = 2.0f * qpz;
    qpw = 2.0f * qpw;
    quatSq4(qx, qy, qz, qw, &qx, &qy, &qz, &qw);

    qx = qx + c[0];
    qy = qy + c[1];
    qz = qz + c[2];
    qw = qw + c[3];
    dqq = dot_product4_v(qx, qy, qz, qw, qx, qy, qz, qw);

    q[0] = bitselect(q[0], qx, as_float4(write_mask));
    q[1] = bitselect(q[1], qy, as_float4(write_mask));
    q[2] = bitselect(q[2], qz, as_float4(write_mask));
    q[3] = bitselect(q[3], qw, as_float4(write_mask));

    qp[0] = bitselect(qp[0], qpx, as_float4(write_mask));
    qp[1] = bitselect(qp[1], qpy, as_float4(write_mask));
    qp[2] = bitselect(qp[2], qpz, as_float4(write_mask));
    qp[3] = bitselect(qp[3], qpw, as_float4(write_mask));

#if 0
    vector_out[vector_location++] = dqq;
#endif
    tmp = (dqq > escape);
#if 0
    vector_out[vector_location].x = (float) tmp.x;
    vector_out[vector_location].y = (float) tmp.y;
    vector_out[vector_location].z = (float) tmp.z;
    vector_out[vector_location++].w = (float) tmp.w;
#endif
    tmp = ~tmp;
#if 0
    vector_out[vector_location].x = (float) tmp.x;
    vector_out[vector_location].y = (float) tmp.y;
    vector_out[vector_location].z = (float) tmp.z;
    vector_out[vector_location++].w = (float) tmp.w;
#endif
    write_mask = write_mask & tmp; //(~((dqq > escape)));

#if 0
   vector_out[vector_location].x = (float) write_mask.x;
   vector_out[vector_location].y = (float) write_mask.y;
   vector_out[vector_location].z = (float) write_mask.z;
   vector_out[vector_location++].w = (float) write_mask.w;
#endif

//((float4) {ESCAPE_THRESHOLD,ESCAPE_THRESHOLD,
//ESCAPE_THRESHOLD,ESCAPE_THRESHOLD}))));
    i++;
  }
#endif
}

#if 0
  __device__ float16 I = (float16) { DEL, DEL, DEL, DEL,
    0.0f, 0.0f, 0.0f, 0.0f,
    0.0f, 0.0f, 0.0f, 0.0f,
    0.0f, 0.0f, 0.0f, 0.0f
  };
  __device__ float16 J = (float16) { 0.0f, 0.0f, 0.0f, 0.0f,
    DEL, DEL, DEL, DEL,
    0.0f, 0.0f, 0.0f, 0.0f,
    0.0f, 0.0f, 0.0f, 0.0f
  };
  __device__ float16 K = (float16) { 0.0f, 0.0f, 0.0f, 0.0f,
    0.0f, 0.0f, 0.0f, 0.0f,
    DEL, DEL, DEL, DEL,
    0.0f, 0.0f, 0.0f, 0.0f
  };
#endif

__device__ 
void normEstimate4(float4 * norm, float4 * p, float4 * c, int maxIterations)
{
  union {
     float16 v16;
     float4  v4[4];
  } fl16;
  float16 I, J, K;
  int i;
  /*const*/ float4 zero = { 0.0f, 0.0f,0.0f,0.0f};
  /*const*/ float4 del  = { DEL, DEL, DEL, DEL };
  float16 qP;
  fl16.v4[0] = p[0], fl16.v4[1] = p[1], fl16.v4[2] = p[2], fl16.v4[3] = zero;
  qP = fl16.v16;

  float4 Nx; // = {0.0f,0.0f,0.0f,0.0f};
  float4 Ny; // = {0.0f,0.0f,0.0f,0.0f};
  float4 Nz; // = {0.0f,0.0f,0.0f,0.0f};

  float16 gx1, gx2, gy1, gy2, gz1, gz2;

  float16 C;
  fl16.v4[0] = c[0], fl16.v4[1] = c[1], fl16.v4[2] = c[2], fl16.v4[3] = c[3];
  C = fl16.v16;

  fl16.v4[0] = del;
  fl16.v4[1] = zero;
  fl16.v4[2] = zero;
  fl16.v4[3] = zero;
  I = fl16.v16;
  fl16.v4[0] = zero;
  fl16.v4[1] = del;
  fl16.v4[2] = zero;
  fl16.v4[3] = zero;
  J = fl16.v16;
  fl16.v4[0] = zero;
  fl16.v4[1] = zero;
  fl16.v4[2] = del;
  fl16.v4[3] = zero;
  K = fl16.v16;
#if 1

  gx1 = qP - I;
  gx2 = qP + I;

  gy1 = qP - J;
  gy2 = qP + J;

  gz1 = qP - K;
  gz2 = qP + K;

  for (i = 0; i < maxIterations; i++) {
    quatSq16(gx1, &gx1);
    quatSq16(gx2, &gx2);
    quatSq16(gy1, &gy1);
    quatSq16(gy2, &gy2);
    quatSq16(gz1, &gz1);
    quatSq16(gz2, &gz2);

    gx1 = gx1 + C;
    gx2 = gx2 + C;

    gy1 = gy1 + C;
    gy2 = gy2 + C;

    gz1 = gz1 + C;
    gz2 = gz2 + C;
  }

  Nx = length16_v(gx2) - length16_v(gx1);
  Ny = length16_v(gy2) - length16_v(gy1);
  Nz = length16_v(gz2) - length16_v(gz1);


  normalize3_v(&norm[0], &norm[1], &norm[2], Nx, Ny, Nz);
#endif
}

__device__
float4
intersectQJulia(float4 * rO, float4 * rD, float4 * c,
                int maxIterations, float4 epsilon)
{
  float4 rOx = rO[0];
  float4 rOy = rO[1];
  float4 rOz = rO[2];
  float4 dist;
  float4 rdist = {0.0f,0.0f,0.0f,0.0f};
  int4 write_mask = {0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF};
  float4 maxstep = {0.3f,0.3f,0.3f,0.3f};

//  int iter = 0;

#if 0
  vector_out[vector_location].x = (float) any (write_mask);
  vector_out[vector_location].y = 10.1f;
  vector_out[vector_location].z = 10.2f;
  vector_out[vector_location++].w = 10.3f;
#endif

//  while (any(write_mask) && (iter<1000)) {
  while (any(write_mask)) {
    float4 z[4];
    float4 zp[4];
    float4 normZ;
    float4 nzlog, zplen;
    float4 vdot;
    const float4 vBR = {BOUNDING_RADIUS_2,BOUNDING_RADIUS_2,
                        BOUNDING_RADIUS_2,BOUNDING_RADIUS_2};
    z[0] = rOx;
    z[1] = rOy;
    z[2] = rOz;
    z[3].x = 0.0f, z[3].y = 0.0f, z[3].z = 0.0f, z[3].w = 0.0f;
    zp[0].x = 1.0f, zp[0].y = 1.0f, zp[0].z = 1.0f, zp[0].w = 1.0f;
    zp[1].x = 0.0f, zp[1].y = 0.0f, zp[1].z = 0.0f, zp[1].w = 0.0f;
    zp[2].x = 0.0f, zp[2].y = 0.0f, zp[2].z = 0.0f, zp[2].w = 0.0f;
    zp[3].x = 0.0f, zp[3].y = 0.0f, zp[3].z = 0.0f, zp[3].w = 0.0f;


    iterateIntersect(z, zp, c, maxIterations);
    normZ = length4_v(z[0], z[1], z[2], z[3]);
#if 0
    vector_out[vector_location++] = normZ;
#endif
    nzlog = log(normZ);
    zplen = length4_v(zp[0], zp[1], zp[2], zp[3]);
    zplen = 1.0f / (zplen);
    dist = normZ * 0.5f;
    dist = dist * nzlog;
    dist = dist * zplen;
    dist = min(dist, maxstep);


    rOx = rD[0] * dist + rOx;
    rOy = rD[1] * dist + rOy;
    rOz = rD[2] * dist + rOz;

    vdot = dot_product3_v(rOx, rOy, rOz, rOx, rOy, rOz);

    rO[0] = bitselect(rO[0], rOx, as_float4(write_mask));
    rO[1] = bitselect(rO[1], rOy, as_float4(write_mask));
    rO[2] = bitselect(rO[2], rOz, as_float4(write_mask));
    rdist = bitselect(rdist, dist, as_float4(write_mask));
#if 0
   vector_out[vector_location++] = vdot;
   vector_out[vector_location++] = dist;
#endif

    write_mask = write_mask & (~((epsilon > dist) | (vdot > vBR))); // ~ -> !

#if 0
   vector_out[vector_location].x = (float) write_mask.x;
   vector_out[vector_location].y = (float) write_mask.y;
   vector_out[vector_location].z = (float) write_mask.z;
   vector_out[vector_location++].w = (float) write_mask.w;
   vector_out[vector_location].x = (float) 9999.0f;
   vector_out[vector_location].y = (float) 9998.0f;
   vector_out[vector_location].z = (float) 9997.0f;
   vector_out[vector_location++].w = (float) any(write_mask);
#endif
    //++iter;
  }
  return rdist;
}

__device__
void
phong4(float4 * color, float4 * light, float4 * eye, float4 * pt, float4 * norm)
{
  float4 NdotL;
  float4 NdotL2;
  float4 Rx, Ry, Rz;
  float4 diffR, diffG, diffB;
  float4 spec;
  const float4 specExp = { 64.0f, 64.0f, 64.0f, 64.0f };
  float4 EdotR;
  float4 Lx = light[0] - pt[0];
  float4 Ly = light[1] - pt[1];
  float4 Lz = light[2] - pt[2];
  float4 Ex = eye[0] - pt[0];
  float4 Ey = eye[1] - pt[1];
  float4 Ez = eye[2] - pt[2];
#if 1
Lx *= 10.9f;
Ly *= 10.9f;
Lz *= 10.9f;
Ex *= 10.9f;
Ey *= 10.9f;
Ez *= 10.9f;
#endif
  //const float4 diffuseR = { 1.0f, 1.0f,1.0f,1.0f};
  //const float4 diffuseG = {0.45f,0.45f,0.45f,0.45f};
  //const float4 diffuseB = {0.25f,0.25f,0.25f,0.25f};
  const float4 diffuseR = { 255.0f, 255.0f,255.0f,255.0f};
  const float4 diffuseG = {114.75f,114.75f,114.75f,114.75f};
  const float4 diffuseB = {63.75f,63.75f,63.75f,63.75f};
  float4 zero = {0.0f, 0.0f, 0.0f, 0.0f};
  const float4 vnorm_mod = {0.3f,0.3f,0.3f,0.3f};
  //const float4 vnorm_mod = {76.5f,76.5f,76.5f,76.5f};
  //const float4 specularity = {0.45f,0.45f,0.45f,0.45f};
  const float4 specularity = {45.0f,45.0f,45.0f,45.0f};

  normalize3_v(&Lx, &Ly, &Lz, Lx, Ly, Lz);
  normalize3_v(&Ex, &Ey, &Ez, Ex, Ey, Ez);
  NdotL = dot_product3_v(norm[0], norm[1], norm[2], Lx, Ly, Lz);
  NdotL2 = 2.0f * NdotL;
  Rx = NdotL2 * norm[0] - Lx;
  Ry = NdotL2 * norm[1] - Ly;
  Rz = NdotL2 * norm[2] - Lz;
  EdotR = dot_product3_v(Ex, Ey, Ez, Rx, Ry, Rz);
  diffR = fabs(norm[0]) * vnorm_mod + diffuseR;
  diffG = fabs(norm[1]) * vnorm_mod + diffuseG;
  diffB = fabs(norm[2]) * vnorm_mod + diffuseB;
  spec = specularity * pow(max(EdotR, zero), specExp);
#if 0
  spec.x = 0.9f;
  spec.y = 0.9f;
  spec.z = 0.9f;
  spec.w = 0.9f;
#endif
  color[0] = diffR * max(NdotL, zero) + spec;
  color[1] = diffG * max(NdotL, zero) + spec;
  color[2] = diffB * max(NdotL, zero) + spec;

  //color[0] *= 20000;
  //color[1] *= 20000;
  //color[2] *= 20000;
}

__device__
void
intersectSphere(float4 * rOx, float4 * rOy, float4 * rOz,
                float4   rDx, float4   rDy, float4   rDz)
{
  float4 B, negB, C, d, t0, t1, t;
  float4 zero = {0.0f, 0.0f, 0.0f, 0.0f};
  const float4 vBR = {BOUNDING_RADIUS_2, BOUNDING_RADIUS_2,
                      BOUNDING_RADIUS_2, BOUNDING_RADIUS_2};

  B = dot_product3_v(*rOx, *rOy, *rOz, rDx, rDy, rDz);
  C = dot_product3_v(*rOx, *rOy, *rOz, *rOx, *rOy, *rOz);
  B = B * 2.0f;
  C = C - vBR;
  d = (B * B) - (C * 4.0f);
  d = max(d, zero);
  d = sqrt(d);

  negB = -B;
  t0 = d - B;
  t0 = t0 * 0.5f;
  t1 = negB - d;
  t1 = t1 * 0.5f;
  t = min(t0, t1);
  *rOx = t * rDx + *rOx;
  *rOy = t * rDy + *rOy;
  *rOz = t * rDz + *rOz;
}
#endif

#if 0
__device__
void
fragmentShader4(float4 * rOT, float4 * rDT, float4 * muT,
                float4 epsilon, float4 * lightT,
                int maxIterations, int renderShadows, uchar16 * cbuffer)
{
   unsigned char * c = (unsigned char *) cbuffer;
   c[0] = (unsigned char) 255;
   c[1] = (unsigned char) 255;
   c[2] = (unsigned char) 0;
   c[3] = (unsigned char) 0;
   c[4] = (unsigned char) 255;
   c[5] = (unsigned char) 255;
   c[6] = (unsigned char) 0;
   c[7] = (unsigned char) 0;
   c[8] = (unsigned char) 255;
   c[9] = (unsigned char) 255;
   c[10] = (unsigned char) 0;
   c[11] = (unsigned char) 0;
   c[12] = (unsigned char) 255;
   c[13] = (unsigned char) 255;
   c[14] = (unsigned char) 0;
   c[15] = (unsigned char) 0;
}

#else
__device__
void
fragmentShader4(float4 * rOT, float4 * rDT, float4 * muT,
                float4 epsilon, float4 * lightT,
                int maxIterations, int renderShadows, uchar16 * cbuffer)
{
  float4 colorT[4]; /* ARGB */
  float4 distT, distTs;
  float4 rOmT[3];
  float4 eyeT[3];
  //float4 bg_colorR = {0.5f,0.5f,0.5f,0.5f};
  //float4 bg_colorG = {0.5f,0.5f,0.5f,0.5f};
  //float4 bg_colorB = {0.5f,0.5f,0.5f,0.5f};
  float4 bg_colorR = {127.5f,127.5f,127.5f,127.5f};
  float4 bg_colorG = {127.5f,127.5f,127.5f,127.5f};
  float4 bg_colorB = {127.5f,127.5f,127.5f,127.5f};
  const float4 shade = {0.4f,0.4f,0.4f,0.4f};
  uint4 bg_color = {0xff7f7f7f,0xff7f7f7f,0xff7f7f7f,0xff7f7f7f};
  const float4 n255 =  {255.0f,255.0f,255.0f,255.0f};
  int4 tmp;

  eyeT[0] = rOT[0];
  eyeT[1] = rOT[1];
  eyeT[2] = rOT[2];

  rOmT[0] = rOT[0];
  rOmT[1] = rOT[1];
  rOmT[2] = rOT[2];

  normalize3_v(&rDT[0], &rDT[1], &rDT[2], rDT[0], rDT[1], rDT[2]);
  intersectSphere(&rOmT[0], &rOmT[1], &rOmT[2], rDT[0], rDT[1], rDT[2]);

  distT = intersectQJulia(rOmT, rDT, muT, maxIterations, epsilon);

  tmp = (epsilon > distT);
#if 0
  vector_out[vector_location].x = (float) tmp.x;
  vector_out[vector_location].y = (float) tmp.y;
  vector_out[vector_location].z = (float) tmp.z;
  vector_out[vector_location++].w = (float) tmp.w;
  vector_out[vector_location].x = .256f;
  vector_out[vector_location].y = .256f;
  vector_out[vector_location].z = .256f;
  vector_out[vector_location++].w = .256f;
#endif

#if 1
  if (any(tmp)) {
    /* At least 1 ray hit the julia set */
    float4 norm[3];
    float4 rgba[4];

    normEstimate4(norm, rOmT, muT, maxIterations);

    phong4(colorT, lightT, eyeT, rOmT, norm);
    if (renderShadows) {
      float4 LT[3];
      float4 epsilon2 = epsilon * 2.0f;

      LT[0] = lightT[0] - rOmT[0];
      LT[1] = lightT[1] - rOmT[1];
    
      LT[2] = lightT[2] - rOmT[2];

      normalize3_v(&LT[0], &LT[1], &LT[2], LT[0], LT[1], LT[2]);

      rOmT[0] = norm[0] * epsilon2 + rOmT[0];
      rOmT[1] = norm[1] * epsilon2 + rOmT[1];
      rOmT[2] = norm[2] * epsilon2 + rOmT[2];

      distTs = intersectQJulia(rOmT, LT, muT, maxIterations, epsilon);

      tmp = epsilon > distTs;
      if (any(tmp)) {
        float4 tcolorR, tcolorG, tcolorB;
        tcolorR = colorT[0] * shade;
        tcolorG = colorT[1] * shade;
        tcolorB = colorT[2] * shade;
        colorT[0] = bitselect(colorT[0], tcolorR, as_float4(tmp));
        colorT[1] = bitselect(colorT[1], tcolorG, as_float4(tmp));
        colorT[2] = bitselect(colorT[2], tcolorB, as_float4(tmp));
      }
    }

    /* Any rays that missed are background */
    tmp = (epsilon > distT);
    rgba[0] = bitselect(bg_colorR, colorT[0], as_float4(tmp));
    rgba[1] = bitselect(bg_colorG, colorT[1], as_float4(tmp));
    rgba[2] = bitselect(bg_colorB, colorT[2], as_float4(tmp));
    rgba[3] = n255;


    rgba[0] *= 255.0f;
    rgba[1] *= 255.0f;
    rgba[2] *= 255.0f;

#if 0
    /* Swizzle from rgba to argb */
    colorT[0] = rgba[3];
    colorT[1] = rgba[0];
    colorT[2] = rgba[1];
    colorT[3] = rgba[2];
#else
    colorT[0] = rgba[2];
    colorT[1] = rgba[1];
    colorT[2] = rgba[0];
    colorT[3] = rgba[3];
#endif

#if 0
    colorT[0] = rgba[0];
    colorT[1] = rgba[1];
    colorT[2] = rgba[2];
    colorT[3] = rgba[3];
#endif
    transpose(colorT);

#if 1
    cbuffer[0].lo.lo = convert_uchar4_sat(colorT[0]);
    cbuffer[0].lo.hi = convert_uchar4_sat(colorT[1]);
    cbuffer[0].hi.lo = convert_uchar4_sat(colorT[2]);
    cbuffer[0].hi.hi = convert_uchar4_sat(colorT[3]);
#else
  float4 zero = {0.0f, 0.0f, 0.0f, 0.0f};
  uchar4 red = {255, 0, 0, 0};
    cbuffer[0].lo.lo = red;
    cbuffer[0].lo.hi = red;
    cbuffer[0].hi.lo = red;
    cbuffer[0].hi.hi = red;
    if (colorT[0].x > 0) cbuffer[0].lo.lo.x = 0;

    if (colorT[0].x > 2) cbuffer[0].lo.lo.x = (unsigned char) 255;
    else                   cbuffer[0].lo.lo.x = (unsigned char) ((unsigned int) colorT[0].x);
    if (colorT[0].y > 2) cbuffer[0].lo.lo.y = (unsigned char) 255;
    else                   cbuffer[0].lo.lo.y = (unsigned char) colorT[0].y;
    if (colorT[0].z > 2) cbuffer[0].lo.lo.z = (unsigned char) 255;
    else                   cbuffer[0].lo.lo.z = (unsigned char) colorT[0].z;
    if (colorT[0].w > 2) cbuffer[0].lo.lo.w = (unsigned char) 255;
    else                   cbuffer[0].lo.lo.w = (unsigned char) colorT[0].w;
    
    if (colorT[1].x > 2) cbuffer[0].lo.hi.x = (unsigned char) 255;
    else                   cbuffer[0].lo.hi.x = (unsigned char) colorT[1].x;
    if (colorT[1].y > 2) cbuffer[0].lo.hi.y = (unsigned char) 255;
    else                   cbuffer[0].lo.hi.y = (unsigned char) colorT[1].y;
    if (colorT[1].z > 2) cbuffer[0].lo.hi.z = (unsigned char) 255;
    else                   cbuffer[0].lo.hi.z = (unsigned char) colorT[1].z;
    if (colorT[1].w > 2) cbuffer[0].lo.hi.w = (unsigned char) 255;
    else                   cbuffer[0].lo.hi.w = (unsigned char) colorT[1].w;
    
    if (colorT[2].x > 2) cbuffer[0].hi.lo.x = (unsigned char) 255;
    else                   cbuffer[0].hi.lo.x = (unsigned char) colorT[2].x;
    if (colorT[2].y > 2) cbuffer[0].hi.lo.y = (unsigned char) 255;
    else                   cbuffer[0].hi.lo.y = (unsigned char) colorT[2].y;
    if (colorT[2].z > 2) cbuffer[0].hi.lo.z = (unsigned char) 255;
    else                   cbuffer[0].hi.lo.z = (unsigned char) colorT[2].z;
    if (colorT[2].w > 2) cbuffer[0].hi.lo.w = (unsigned char) 255;
    else                   cbuffer[0].hi.lo.w = (unsigned char) colorT[2].w;
    
    if (colorT[3].x > 2) cbuffer[0].hi.hi.x = (unsigned char) 255;
    else                   cbuffer[0].hi.hi.x = (unsigned char) colorT[3].x;
    if (colorT[3].y > 2) cbuffer[0].hi.hi.y = (unsigned char) 255;
    else                   cbuffer[0].hi.hi.y = (unsigned char) colorT[3].y;
    if (colorT[3].z > 2) cbuffer[0].hi.hi.z = (unsigned char) 255;
    else                   cbuffer[0].hi.hi.z = (unsigned char) colorT[3].z;
    if (colorT[3].w > 2) cbuffer[0].hi.hi.w = (unsigned char) 255;
    else                   cbuffer[0].hi.hi.w = (unsigned char) colorT[3].w;
    
#endif
  } else {
    /* All rays missed, the bundle is all background */
    cbuffer[0] = as_uchar16(bg_color);
  }
#endif
}
#endif

__global__ void cuda_compute_julia_kernel (const float muP0,const float muP1,const float muP2,const float muP3,
                                               uchar16 * framebuffer,
                                               const struct julia_context jc2, int BSIZE)
{

 const struct julia_context * jc = &jc2;
#if 1
  unsigned int i, j;
  int4 i_iv, j_iv;
  float4 i_fv, j_fv;
  uchar16 *output16;
  float4 rO[3], light[3], mu[4], curr_dir[3];
  float4 ddX, ddY;
  float4 ddXx, ddXy, ddXz;
  float4 ddYx, ddYy, ddYz;
  float4 rbsx, rbsy, rbsz;
  float4 px, py;
  float4 inv_width, inv_height;
  float4 epv;
  const float4 v0123 = {0.0f, 1.0f, 2.0f, 3.0f};
  const int renderShadows = 0;
  const float4 zero = {0.0f, 0.0f, 0.0f, 0.0f};
  uchar16 pcolors;
  float tmp;

  /* See which group of 4 pixels to compute */
  i = blockIdx.x * blockDim.x + threadIdx.x;
  j = blockIdx.y * blockDim.y + threadIdx.y;

  //if (i>511) return;
  //if (j>511) return;
#if 1
  /* Setup eye and lighting positions */
  rO[0].x = jc->eyeP.x, rO[0].y = jc->eyeP.x, 
                       rO[0].z = jc->eyeP.x, rO[0].w = jc->eyeP.x;
  rO[1].x = jc->eyeP.y, rO[1].y = jc->eyeP.y,
                       rO[1].z = jc->eyeP.y, rO[1].w = jc->eyeP.y;
  rO[2].x = jc->eyeP.z, rO[2].y = jc->eyeP.z,
                       rO[2].z = jc->eyeP.z, rO[2].w = jc->eyeP.z;
  light[0].x = jc->lightP.x, light[0].y = jc->lightP.x,
                            light[0].z = jc->lightP.x, light[0].w = jc->lightP.x;
  light[1].x = jc->lightP.y, light[1].y = jc->lightP.y,
                            light[1].z = jc->lightP.y, light[1].w = jc->lightP.y;
  light[2].x = jc->lightP.z, light[2].y = jc->lightP.z,
                            light[2].z = jc->lightP.z, light[2].w = jc->lightP.z;

#if 0
  vector_out[vector_location++] = light[0];
  vector_out[vector_location++] = light[1];
  vector_out[vector_location++] = light[2];
  vector_out[vector_location].x = 56.0f;
  vector_out[vector_location].y = 57.0f;
  vector_out[vector_location].z = 58.0f;
  vector_out[vector_location++].w = 59.0f;
#endif

  /* Copy the mutation matrix */
  mu[0].x = muP0, mu[0].y = muP0, mu[0].z = muP0, mu[0].w = muP0;
  mu[1].x = muP1, mu[1].y = muP1, mu[1].z = muP1, mu[1].w = muP1;
  mu[2].x = muP2, mu[2].y = muP2, mu[2].z = muP2, mu[2].w = muP2;
  mu[3].x = muP3, mu[3].y = muP3, mu[3].z = muP3, mu[3].w = muP3;

  tmp = (1.0f / jc->window_size.x);
  inv_width.x = tmp, inv_width.y = tmp, inv_width.z = tmp, inv_width.w = tmp;
  tmp = (1.0f / jc->window_size.y);
  inv_height.x = tmp, inv_height.y = tmp, inv_height.z = tmp, inv_height.w = tmp;

  /* The size of the screen */
  ddX = jc->dir_bottom_stop - jc->dir_bottom_start;
  ddY = jc->dir_top_start - jc->dir_bottom_start;

  ddX = ddX * inv_width;
  ddY = ddY * inv_height;

  /* The amount to iterate up and down */
  ddXx.x = ddX.x, ddXx.y = ddX.x, ddXx.z = ddX.x, ddXx.w = ddX.x;
  ddXy.x = ddX.y, ddXy.y = ddX.y, ddXy.z = ddX.y, ddXy.w = ddX.y;
  ddXz.x = ddX.z, ddXz.y = ddX.z, ddXz.z = ddX.z, ddXz.w = ddX.z;

  ddYx.x = ddY.x, ddYx.y = ddY.x, ddYx.z = ddY.x, ddYx.w = ddY.x;
  ddYy.x = ddY.y, ddYy.y = ddY.y, ddYy.z = ddY.y, ddYy.w = ddY.y;
  ddYz.x = ddY.z, ddYz.y = ddY.z, ddYz.z = ddY.z, ddYz.w = ddY.z;

  /* Offset from the bottom of the screen */
  rbsx.x = jc->dir_bottom_start.x, rbsx.y = jc->dir_bottom_start.x,
             rbsx.z = jc->dir_bottom_start.x, rbsx.w = jc->dir_bottom_start.x;
  rbsy.x = jc->dir_bottom_start.y, rbsy.y = jc->dir_bottom_start.y,
             rbsy.z = jc->dir_bottom_start.y, rbsy.w = jc->dir_bottom_start.y;
  rbsz.x = jc->dir_bottom_start.z, rbsz.y = jc->dir_bottom_start.z, 
             rbsz.z = jc->dir_bottom_start.z, rbsz.w = jc->dir_bottom_start.z;

#if 0
  vector_out[vector_location++] = rbsx;
  vector_out[vector_location++] = rbsy;
  vector_out[vector_location++] = rbsz;
  vector_out[vector_location].x = 56.0f;
  vector_out[vector_location].y = 57.0f;
  vector_out[vector_location].z = 58.0f;
  vector_out[vector_location++].w = 59.0f;
#endif
  px = v0123;
  py = zero;


  i_iv.x = (int) i, i_iv.y = (int) i, i_iv.z = (int) i, i_iv.w = (int) i;
  j_iv.x = (int) j, j_iv.y = (int) j, j_iv.z = (int) j, j_iv.w = (int) j;

  i_fv = convert_float4(i_iv);
  j_fv = convert_float4(j_iv);
  
#if 0
  vector_out[vector_location++] = i_fv;
  vector_out[vector_location++] = j_fv;
#endif
  px = px + 4.0f * i_fv; /* Each vector computes 4 pixels in a row */
  py = py + j_fv;
#if 0
  vector_out[vector_location++] = px;
  vector_out[vector_location].x = 56.0f;
  vector_out[vector_location].y = (float) jc->maxIterations;
  vector_out[vector_location].z = 58.0f;
  vector_out[vector_location++].w = 59.0f;
#endif

  /* Set the direction of the vector to the pixel group */
  curr_dir[0] = px * ddXx + rbsx;
  curr_dir[1] = px * ddXy + rbsy;
  curr_dir[2] = px * ddXz + rbsz;
  curr_dir[0] = py * ddYx + curr_dir[0];
  curr_dir[1] = py * ddYy + curr_dir[1];
  curr_dir[2] = py * ddYz + curr_dir[2];

  epv.x = jc->epsilon,epv.y = jc->epsilon,epv.z = jc->epsilon,epv.w = jc->epsilon;
#endif
#if 1
  fragmentShader4(rO, curr_dir, mu, epv,
                  light, jc->maxIterations, renderShadows, &pcolors);
  //pcolors = as_uchar16(py);//epv);
#else
  //pcolors = as_uchar16(epv);
#if 1
             /* b   g   r  - */
  uchar4 c0 = { 0, 0, 0, 0 };
  uchar4 c1 = { 0, 0, 0, 0 };
  uchar4 c2 = { 0, 0, 0, 0 };
  uchar4 c3 = { 0, 0, 0, 0 };
  pcolors.lo.lo = c0;
  pcolors.lo.hi = c1;
  pcolors.hi.lo = c2;
  pcolors.hi.hi = c3;
#endif
#endif
  //if (jc->window_size.x == 512) pcolors.lo.lo.x = 255;
//if (jc->epsilon == 0.003f) pcolors.lo.hi.x = 255;
//  if (j>500) return;
//  if (i>500/4) return;
  /* Output the argb of the 4 pixels into the output array */
  output16 = &framebuffer[(j*jc->stride)/4+ i]; //& framebuffer[(j * rowstride) / 4 + i];
  output16[0] = pcolors;
#endif
}
