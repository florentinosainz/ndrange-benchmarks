/*************************************************************************/
/*                                                                       */
/* Licensed Materials - Property of IBM                                  */
/*                                                                       */
/*                                                                       */
/*                                                                       */
/* (C) Copyright IBM Corp. 200x, 200y                                    */
/*                                                                       */
/*************************************************************************/

extern void startclock(void);
extern float stopclock(void);
