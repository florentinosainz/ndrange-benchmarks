#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

#include "driver.h"

#ifdef DP
#define REAL double
#else
#define REAL float
#endif

int N_DEVICES=2;



REAL** inputs_A;
REAL** inputs_H;
REAL** outputs;


const int BS = BSIZE;

__global__ void Muld(REAL* A, REAL* B, REAL* C, int BS);

void matmul( int m, int l, int n, int mDIM, int lDIM, int nDIM, REAL **tileA, REAL **tileB,
             REAL **tileC )
{	
	int i, j, k;
	int n_device=0;
	cudaError_t err;

	cudaStream_t stream[N_DEVICES][mDIM+nDIM];
	// for (i=0; i<mDIM+nDIM;++i){
		// clEnqueueMarker(commands[0], &events_dep[i]);
	// }
	int inA[N_DEVICES];
	int inH[N_DEVICES];
	for (i=0; i<N_DEVICES;++i){
		inA[i]=-1;
		inH[i]=-1;
		int e;
		for (e=0; e<mDIM+nDIM;++e){
			cudaSetDevice(i);
			cudaStreamCreate(&(stream[i][e]));
		}
	}
	n_device=0;
	int p;
	//Programmed for CUDA 4.1 without HyperQ, with hyperQ all this "tricks" shouldnt be needed
	for(i = 0;i < mDIM; i++){
		int max=nDIM+nDIM%N_DEVICES;
		//Split this inner loop in blocks of size=number of devices
		for (p = 0; p < max; p=p+N_DEVICES){
				int x;
				//This block does the copies
				for (x=0; x<N_DEVICES;x++){
					j=p+x;
					if (j<nDIM) {
						n_device=x;
						cudaSetDevice(n_device);
						err= cudaMemcpyAsync((void**) outputs[n_device],tileC[i*nDIM+j],  sizeof(REAL) * BS * BS,cudaMemcpyHostToDevice,stream[n_device][i+j]);
						if (err != cudaSuccess)
						{
							printf("Error: Failed to write to source array C %d into device %d!, %s\n",err,n_device, cudaGetErrorString(err));
							exit(1);
						}
						
						
						for (k = 0; k < lDIM; k++){
							//Avoid extra copies if the data is already in
							if (inA[n_device]!=i*lDIM+k){
								err= cudaMemcpyAsync((void**) inputs_A[n_device],  tileA[i*lDIM+k],  sizeof(REAL) * BS * BS,cudaMemcpyHostToDevice,stream[n_device][i+j]);
								if (err != cudaSuccess)
								{
									printf("Error: Failed to write to source array A %d into device %d!, %s\n",err,n_device, cudaGetErrorString(err));
									exit(1);
								}
								inA[n_device]=i*lDIM+k;
							}
							//Avoid extra copies if the data is already in
							if (inH[n_device]!=k*nDIM+j){
								err= cudaMemcpyAsync((void**) inputs_H[n_device],tileB[k*nDIM+j],  sizeof(REAL) * BS * BS,cudaMemcpyHostToDevice,stream[n_device][i+j]);
								if (err != cudaSuccess)
								{
									printf("Error: Failed to write to source array H %d into device %d!, %s\n",err,n_device, cudaGetErrorString(err));
									exit(1);
								}
								inH[n_device]=k*nDIM+j;
							}
							
							
							for (k = 0; k < lDIM; k++){
								n_device=x;
								cudaSetDevice(n_device);
								dim3 dimBlock, dimGrid;
								dimBlock.x=16;
								dimBlock.y=16;
								dimGrid.x=BS;
								dimGrid.y=BS;
								dimGrid.x = dimGrid.x < dimBlock.x ? 1 : dimGrid.x / dimBlock.x + (dimGrid.x % dimBlock.x == 0 ? 0 : 1);
								dimGrid.y = dimGrid.y < dimBlock.y ? 1 : dimGrid.y / dimBlock.y + (dimGrid.y % dimBlock.y == 0 ? 0 : 1);
								
								
								Muld<<<dimGrid,dimBlock,0,stream[n_device][i+j]>>>(inputs_A[n_device], inputs_H[n_device], outputs[n_device],BS);
							}
					
						}
					}
				}
					
				//Now gather back
				for (x=0; x<N_DEVICES;x++){
					j=p+x;
					if (j<nDIM) {					
						n_device=x;
						cudaSetDevice(n_device);	
						err= cudaMemcpyAsync(tileC[i*nDIM+j],(void**) outputs[n_device],  sizeof(REAL) * BS * BS,cudaMemcpyDeviceToHost,stream[n_device][i+j]);
						if (err != cudaSuccess)
						{
							printf("Error: Failed to write to source array H %d into device %d!, %s\n",err,n_device, cudaGetErrorString(err));
							exit(1);
						}
					}
				}
			}	
		}
			
	//Wait for end and release
	for (i=0; i<N_DEVICES;++i){
		cudaSetDevice(i);			
		cudaDeviceSynchronize();
		int e;
		for (e=0; e<mDIM+nDIM;++e){
			cudaFree(&(stream[i][e]));
		}
	}
}



//#define BSIZE 1024

double   cclock( void );
int      check( int nrep, int m, int l, int n, int mDIM, int nDIM, REAL **c );
void     gendat(int, int, int, int, int, int, REAL **, REAL **, REAL **);
void     matmul( int, int, int, int, int, int, REAL **a, REAL **b, REAL **c );
void     prthead( void );
void     prtspeed( int, int, int, int, double, int, unsigned long );

int calcdim(int x)
{
        int dimval;
        if(x%BSIZE != 0)
                dimval = x/BSIZE + 1;
        else
                dimval = x/BSIZE;

        return dimval;
}
	
int main (int argc, char** argv)
{ /* main */
    if (argc > 1) {
		N_DEVICES=atoi(argv[1]);
    }
	REAL* inputs_A_aux[N_DEVICES];
	REAL* inputs_H_aux[N_DEVICES];
	REAL* outputs_aux[N_DEVICES];
	inputs_A=inputs_A_aux;
	inputs_H=inputs_H_aux;
	outputs=outputs_aux;
	int       m, l, n;
	int      mDIM, lDIM, nDIM;
	int      ok, nrep;
	unsigned long nops;
	int      i;
	REAL   **a, **b, **c;
	double   time;
	FILE     *inl;
// ------------------------------------------------------------------------	
	

	inl = fopen( "test.in", "r" );
	if (inl == 0) {
		printf("No input file 'test.in' found.\n");
		exit(1);
	}

	cudaError_t err;
	for (i = 0; i< N_DEVICES; i++){	
		cudaSetDevice(i);
		err= cudaMalloc ((void **) &inputs_A[i], sizeof(REAL) * BS * BS);	
		if (err != cudaSuccess ){
			printf("Error: Failed to create buffer, err %d!\n",err);
			return EXIT_FAILURE;
		}		
		err= cudaMalloc ((void **) &inputs_H[i], sizeof(REAL)  * BS * BS);	
		if (err != cudaSuccess ){
			printf("Error: Failed to create buffer, err %d!\n",err);
			return EXIT_FAILURE;
		}
		err= cudaMalloc ((void **) &outputs[i],  sizeof(REAL)  * BS * BS);	
		if (err != cudaSuccess ){
			printf("Error: Failed to create buffer, err %d!\n",err);
			return EXIT_FAILURE;
		}		
	}

	while( ( fscanf( inl, "%d%d%d%d\n", &m, &l, &n, &nrep ) != EOF ) ){

		mDIM = calcdim(m);
		lDIM = calcdim(l);
		nDIM = calcdim(n);

		a = (REAL **)malloc( mDIM*lDIM*sizeof( REAL *) );
		b = (REAL **)malloc( lDIM*nDIM*sizeof( REAL *) );
		c = (REAL **)malloc( mDIM*nDIM*sizeof( REAL *) );
      
		for(i=0;i<mDIM*lDIM;i++)
			a[i] = (REAL *)malloc(BSIZE*BSIZE*sizeof(REAL));

		for(i=0;i<lDIM*nDIM;i++)
			b[i] = (REAL *)malloc(BSIZE*BSIZE*sizeof(REAL));

		for(i=0;i<mDIM*nDIM;i++)
			c[i] = (REAL *)malloc(BSIZE*BSIZE*sizeof(REAL));


		gendat( mDIM, lDIM, nDIM, m, l, n, a, b, c );
		

		time = cclock();
		

		for( i = 0; i < nrep; i++ ){
			matmul( m, l, n, mDIM, lDIM, nDIM, a, b, c ); 
		}
		cudaDeviceSynchronize();
		time = cclock() - time;
		ok   = check( nrep, m, l, n, mDIM, nDIM, c);

		time = time/nrep;
		nops  = (unsigned long) 2*m*l*n;
		prtspeed( m, l, n, BSIZE, time, ok, nops );

		for(i=0;i<mDIM*lDIM;i++)
			free( a[i] );

		for(i=0;i<lDIM*nDIM;i++)
			free( b[i] );
  
		for(i=0;i<mDIM*nDIM;i++)
			free( c[i] );

		free( a ); free( b ); free( c );
	}

	for (i=0; i<N_DEVICES;i++){	
		cudaFree(inputs_A[i]);
		cudaFree(inputs_H[i]);
		cudaFree(outputs[i]);
	}
	//printf( "----------------------------------------------------------\n" );
}
