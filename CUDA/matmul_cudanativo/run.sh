#!/bin/bash

# @ partition = fatgpu
# @ output = nbody.log
# @ error = nbody.log
# @ initialdir = .
# @ total_tasks = 1
# @ cpus_per_task = 8
# @ gpus_per_node = 4
# @ node_usage = not_shared
# @ wall_clock_limit = 00:20:00

touch test.in
echo "8192 8192 8192 3" > test.in
./matmul ${1:-1}  #${1:-1}  is the number of GPUs