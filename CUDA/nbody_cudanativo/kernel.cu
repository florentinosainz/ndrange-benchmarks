#include <stdio.h>
#include <math.h>
#include <unistd.h>
#include "nbody.h"

const static int MAX_NUM_THREADS = 128;
#define safe(err)	__cudaSafeCall(err, __FILE__, __LINE__)
inline void __cudaSafeCall(cudaError_t err, const char * file, const int line) {
	if( cudaSuccess != err) {
        fprintf(stderr, "cudaSafeCall() Runtime API error in file <%s>, line %i : %s.\n",
                file, line, cudaGetErrorString( err) );
        exit(1);
    }
}


__device__ void calculate_force(
         Particle* this_particle1, Particle* this_particle2,
         float* force_x, float* force_y, float* force_z)
{ /* Particle_calculate_force */
    float difference_x, difference_y, difference_z;
    float distance_squared, distance;
    float force_magnitude;

    difference_x =
        this_particle2->position_x - this_particle1->position_x;
    difference_y =
        this_particle2->position_y - this_particle1->position_y;
    difference_z =
        this_particle2->position_z - this_particle1->position_z;

    distance_squared = difference_x * difference_x +
                       difference_y * difference_y +
                       difference_z * difference_z;

    distance = sqrtf(distance_squared);

    force_magnitude =
        gravitational_constant *
        (this_particle1->mass) * (this_particle2->mass) / distance_squared;

    *force_x = (force_magnitude / distance) * difference_x;
    *force_y = (force_magnitude / distance) * difference_y;
    *force_z = (force_magnitude / distance) * difference_z;

} /* Particle_calculate_force */

__global__ void calculate_forces_kernel_naive(float time_interval, Particle* d_particles, int number_of_particles, Particle *output, int first_local, int last_local) {
	size_t id = (blockDim.x * blockIdx.x) + threadIdx.x + first_local;
	
	if (id > last_local ) return;
	
	Particle* this_particle = output + id - first_local;
	
	float force_x = 0.0f, force_y = 0.0f, force_z = 0.0f;
	float total_force_x = 0.0f, total_force_y = 0.0f, total_force_z = 0.0f;
	
	for (int i = 0; i < number_of_particles; i++) {
		if (i != id) {
			calculate_force(d_particles + id, d_particles + i, &force_x, &force_y, &force_z);
			
			total_force_x += force_x;
			total_force_y += force_y;
			total_force_z += force_z;
		}
	}
	
        float velocity_change_x, velocity_change_y, velocity_change_z;
        float position_change_x, position_change_y, position_change_z;

	this_particle->mass = d_particles[id].mass;
        
        velocity_change_x =
          total_force_x * (time_interval / this_particle->mass);
        velocity_change_y =
          total_force_y * (time_interval / this_particle->mass);
        velocity_change_z =
          total_force_z * (time_interval / this_particle->mass);

	position_change_x =
	  d_particles[id].velocity_x + velocity_change_x * (0.5 * time_interval)
;
	position_change_y =
	  d_particles[id].velocity_y + velocity_change_y * (0.5 * time_interval)
;
	position_change_z =
	  d_particles[id].velocity_z + velocity_change_z * (0.5 * time_interval)
;

	this_particle->velocity_x = d_particles[id].velocity_x + velocity_change_x;
	this_particle->velocity_y = d_particles[id].velocity_y + velocity_change_y;
	this_particle->velocity_z = d_particles[id].velocity_z + velocity_change_z;

	this_particle->position_x = d_particles[id].position_x + position_change_x;
	this_particle->position_y = d_particles[id].position_y + position_change_y;
	this_particle->position_z = d_particles[id].position_z + position_change_z;

}


extern "C"{

double wall_time ( )
{
   struct timespec ts;
   double res;

   clock_gettime(CLOCK_MONOTONIC,&ts);

   res = (double) (ts.tv_sec)  + (double) ts.tv_nsec * 1.0e-9;

   return res;
}
void Particle_array_calculate_forces_cuda(int N_DEVICES, Particle* this_particle_array, int np, float time_interval ,int number_of_timesteps, int timesteps_between_outputs) {
	Particle* inputs[N_DEVICES];
	Particle* outputs[N_DEVICES];
	int timestep;
	int number_of_particles=np;
	int i;
    const int bs = np/N_DEVICES;
	cudaError_t err;
	for (i = 0; i< N_DEVICES; i++){	
		cudaSetDevice(i);
		err= cudaMalloc ((void **) &inputs[i], sizeof(Particle) * number_of_particles);	
		if (err != cudaSuccess ){
			printf("Error: Failed to create buffer, err %d!\n",err);
		}		
		err= cudaMalloc ((void **) &outputs[i],  sizeof(Particle) * number_of_particles/N_DEVICES);	
		if (err != cudaSuccess ){
			printf("Error: Failed to create buffer, err %d!\n",err);
		}		
	}
	
	
	cudaStream_t stream[N_DEVICES];
	for (i=0; i<N_DEVICES;++i){
		cudaSetDevice(i);
		cudaStreamCreate(&(stream[i]));
	}
	
    double start = wall_time();

    for (timestep = 1; timestep <= number_of_timesteps; timestep++) {


        if ((timestep % timesteps_between_outputs) == 0 ) fprintf(stderr, "Starting timestep #%d.\n", timestep);

		size_t num_threads;
        size_t num_blocks;
        
        float time_int;	
        time_int = time_interval;
		int n_device;		
		int e;
		for (e=0; e<N_DEVICES;++e){	
			cudaSetDevice(e);
			cudaDeviceSynchronize();
		}
        for ( i = 0; i < np; i += bs )
        {
			n_device=i/bs;
			cudaSetDevice(n_device);
			err= cudaMemcpyAsync((void**) inputs[n_device],this_particle_array, sizeof(Particle) * np,cudaMemcpyHostToDevice,stream[n_device]);
			if (err != cudaSuccess)
			{
				printf("Error: Failed to write to source array C %d into device %d!, %s\n",err,n_device, cudaGetErrorString(err));
				exit(1);
			}
		    //calculate block sizes
		    num_threads =MAX_NUM_THREADS;   
		    num_blocks = bs / MAX_NUM_THREADS + (bs % MAX_NUM_THREADS == 0 ? 0 : 1);	
		    calculate_forces_kernel_naive <<< num_blocks, num_threads, 0,stream[n_device]>>> (time_int, inputs[n_device], np, outputs[n_device], i, i+bs-1);
        }
		for ( i = 0; i < np; i += bs )
        {
			n_device=i/bs;
			cudaSetDevice(n_device);
			err= cudaMemcpyAsync((void**) &this_particle_array[bs*n_device], (void**) outputs[n_device],sizeof(Particle) * bs,cudaMemcpyDeviceToHost,stream[n_device]);
			if (err != cudaSuccess)
			{
				printf("Error: Failed to write to source array C %d into device %d!, %s\n",err,n_device, cudaGetErrorString(err));
				exit(1);
			}
        }
    } /* for timestep */
	//cudaMemcpy(this_particle_array, pointer_in[0], sizeof(float)*np, cudaMemcpyDeviceToHost);
	
	printf("salgo\n");
	cudaDeviceSynchronize();
   double end = wall_time ();
   
    printf("Time in seconds: %g s.\n", end - start);
    printf("Particles per second: %g \n", (np*number_of_timesteps)/(end-start));
	for (i=0; i<N_DEVICES;++i){
		cudaFree(outputs[i]);
		cudaFree(inputs[i]);
		cudaFree(stream[i]);
	}

}
}
