#!/bin/bash

# @ partition = projects
# @ output = nbody.log
# @ error = nbody.log
# @ initialdir = .
# @ total_tasks = 1
# @ cpus_per_task = 8
# @ gpus_per_node = 4
# @ node_usage = not_shared
# @ wall_clock_limit = 00:20:00

./nbody nbody_input-16384.in ${1:-1} 
#diff nbody_out.xyz nbody_out.xyz-ref

