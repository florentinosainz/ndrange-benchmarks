#!/bin/bash

# @ partition = projects
# @ output = krist.log
# @ error = krist.log
# @ initialdir = .
# @ total_tasks = 1
# @ cpus_per_task = 12
# @ gpus_per_node = 2
# @ node_usage = not_shared
# @ wall_clock_limit = 00:20:00
# Number of GPUs to use (2 by default)

export NX_PES=4 
export NX_GPUS=${1:-1} 

NX_ARGS="--disable-opencl" ./krist 10000 20000 ${1:-1} 
