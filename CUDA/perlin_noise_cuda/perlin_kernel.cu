#include "perlin_auxiliar_header.h"


__global__ void cuda_perlin (pixel* output, float time, int j, int rowstride, int img_width, int BS)
{
    unsigned int i = blockIdx.x * blockDim.x + threadIdx.x;
    unsigned int off = blockIdx.y * blockDim.y + threadIdx.y;
    float vdx = 0.03125f;
    float vdy = 0.0125f;
    float vs = 2.0f;
    float bias = 0.35f;
    float vx = 0.0f;
    float red, green, blue;
    float xx, yy;
    float vy, vt;

    vx = ((float) i) * vdx;
    vy = ((float) (j+off)) * vdy;
    vt = time * vs;

    xx = vx * vs;
    yy = vy * vs;

    red = noise3(xx, vt, yy);
    green = noise3(vt, yy, xx);
    blue = noise3(yy, xx, vt);

    red += bias;
    green += bias;
    blue += bias;

    // Clamp to within [0 .. 1]
    red = (red > 1.0f) ? 1.0f : red;
    green = (green > 1.0f) ? 1.0f : green;
    blue = (blue > 1.0f) ? 1.0f : blue;

    red = (red < 0.0f) ? 0.0f : red;
    green = (green < 0.0f) ? 0.0f : green;
    blue = (blue < 0.0f) ? 0.0f : blue;

    red *= 255.0f;
    green *= 255.0f;
    blue *= 255.0f;

    output[(off * rowstride) + i].r = (unsigned char) red;
    output[(off * rowstride) + i].g = (unsigned char) green;
    output[(off * rowstride) + i].b = (unsigned char) blue;
    output[(off * rowstride) + i].a = (unsigned char) 255; 
}
