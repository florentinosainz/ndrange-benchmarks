#!/bin/bash
# @ job_name = projects
# @ initialdir = .
# @ output = perlin.log
# @ error = perlin.log
# @ total_tasks = 1
# @ gpus_per_node = 2
# @ cpus_per_task = 12
# @ node_usage = not_shared
# @ wall_clock_limit = 00:15:00

export NX_PES=4 
export NX_GPUS=${1:-1} 

#NX_ARGS="--disable-binding --throttle-limit=2000 --disable-yield --spins=1000" ./perlin ${1:-1}  --serial --dim 64
./perlin --ndevs ${1:-1} 

