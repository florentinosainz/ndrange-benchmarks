
// Thread block size
#define BLOCK_SIZE 16


#ifdef DP
#define REAL double
#else
#define REAL float
#endif

#ifdef __cplusplus
extern "C"
{
#endif


#pragma omp target device(cuda) ndrange(2, BS, BS, 16, 16) copy_deps
#pragma omp task   inout( [BS][BS]C ) in( [BS][BS]A, [BS][BS]B )
__global__ void Muld(REAL* A, REAL* B, REAL* C,int BS);

#ifdef __cplusplus
}
#endif
