#include <stdio.h>
#include <sys/time.h>
#include <unistd.h>
#include "matmul_auxiliar_header.h"

#include "driver.h"

#ifdef DP
#define REAL double
#else
#define REAL float
#endif


const int BS = BSIZE;

void matmul( int m, int l, int n, int mDIM, int lDIM, int nDIM, REAL **tileA, REAL **tileB,
             REAL **tileC )
{
	const int hA = m / mDIM, wA = l / lDIM, wB = n / nDIM;

	int i, j, k;
	for(i = 0;i < mDIM; i++){
		for (j = 0; j < nDIM; j++){ 
			for (k = 0; k < lDIM; k++){
				//Kernel call
				Muld(tileA[i*lDIM+k], tileB[k*nDIM+j], tileC[i*nDIM+j], BS);
			}
		}
	}
}


