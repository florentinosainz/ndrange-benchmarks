#!/bin/bash

# @ partition = projects
# @ output = nbody.log
# @ error = nbody.log
# @ initialdir = .
# @ total_tasks = 4
# @ cpus_per_task = 12
# @ gpus_per_node = 2
# @ node_usage = not_shared
# @ wall_clock_limit = 00:20:00
# @ intel_opencl = 1

NX_PES=4 NX_GPUS=${1:-1}  NX_ARGS="--gpu-max-memory=500000000" mpirun -n 4 ./stream-strongscaling

