#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>

#define DIM2_H 4
#define DIM2_A 4
#define DIM2_E 2

#if DIM2_H == 4
#define TYPE_H float4
#endif
#if DIM2_H == 3
#define TYPE_H float3
#endif
#if DIM2_A == 4
#define TYPE_A float4
#endif
#if DIM2_A == 3
#define TYPE_A float3
#endif
#if DIM2_E == 4
#define TYPE_E float4
#endif
#if DIM2_E == 3
#define TYPE_E float3
#endif
#if DIM2_E == 2
#define TYPE_E float2
#endif

extern __shared__ TYPE_A ashared[];

__global__ void cstructfac(int maxatoms, float f2, int NA,
                           float* a_, int NH, float* h_, int NE, float* E_out){
    TYPE_A* a = (TYPE_A*) a_;
    TYPE_H* h = (TYPE_H*) h_;
    TYPE_E* E = (TYPE_E*) E_out;

    int a_start;

    int i = blockDim.x * blockIdx.x + threadIdx.x;
    if (i < NE) E[i].x = E[i].y = 0.0f;

    for (a_start = 0; a_start < NA; a_start += maxatoms) {
        int a_end = min(a_start + maxatoms, NA);
        int k = threadIdx.x;
        while (k < a_end - a_start) {
            ashared[k] = a[k + a_start];
            k += blockDim.x;
        }

        __syncthreads();

        if (i < NE) {
            int j;
            float A,B;
            const float twopi = 6.28318584f;

            TYPE_H hi  = h[i];
            A          = 0.0f;
            B          = 0.0f;

            int jmax = a_end - a_start;
            for (j=0; j < jmax; j++) {
                float A1,B1;
                float4 aj = ashared[j];
                float arg = twopi*(hi.x*aj.y +
                                   hi.y*aj.z +
                                   hi.z*aj.w);
                sincosf(arg, &B1, &A1);
                A += aj.x*A1;
                B += aj.x*B1;
            }
            E[i].x += A*f2;
            E[i].y += B*f2;
        }
        __syncthreads();
    }
}


// __global__ void cstructfac_aux(int na, int nr, int nc, float f2, int NA, TYPE_A* a, int NH, TYPE_H* h, int NE, TYPE_E* E)
// {
//     // TYPE_A* a = (TYPE_A*) a_;
//     // TYPE_H* h = (TYPE_H*) h_;
//     // TYPE_E* E = (TYPE_E*) E_;
// 
//     int a_start;
// 
//     int i = blockDim.x * blockIdx.x + threadIdx.x;
//     if (i < nr) E[i].x = E[i].y = 0.0f;
// 
//     for (a_start = 0; a_start < na; a_start += nc) {
//         int a_end = min(a_start + nc, na);
//         int k = threadIdx.x;
//         while (k < a_end - a_start) {
//             ashared[k] = a[k + a_start];
//             k += blockDim.x;
//         }
// 
//         __syncthreads();
// 
//         if (i < nr) {
//             int j;
//             float A,B;
//             const float twopi = 6.28318584f;
// 
//             TYPE_H hi  = h[i];
//             A          = 0.0f;
//             B          = 0.0f;
// 
//             int jmax = a_end - a_start;
//             for (j=0; j < jmax; j++) {
//                 float A1,B1;
//                 float4 aj = ashared[j];
//                 float arg = twopi*(hi.x*aj.y +
//                         hi.y*aj.z +
//                         hi.z*aj.w);
//                 sincosf(arg, &B1, &A1);
//                 A += aj.x*A1;
//                 B += aj.x*B1;
//             }
//             E[i].x += A*f2;
//             E[i].y += B*f2;
//         }
//         __syncthreads();
//     }
// }
//  
//  
//  
// void cstructfac(int na, int nr, int nc, float f2, int NA, float* a_, int NH, float* h_, int NE, float* E_)
// {
//   dim3 dimBlock;
//   dim3 dimGrid;
//   dimBlock.x = (((nr) < 128) ? ((nr)) : (128));
//   dimGrid.x = (((nr) < 128) ? 1 : (((nr) / 128) + (((nr) % 128 == 0) ? 0 : 1)));
//   dimBlock.y = 1;
//   dimGrid.y = 1;
//   dimBlock.z = 1;
//   dimGrid.z = 1;
//   cstructfac_aux<<<dimGrid, dimBlock, 14336>>>(na, nr, nc, f2, NA, (TYPE_A*)a_, NH, (TYPE_H*)h_, NE, (TYPE_E*) E_);
// }
