#!/bin/bash

for gpu in 1 2 4
do
	echo -n $gpu$'\t'
	NX_GPUS=$gpu ./matmul_gpu ./matmul_gpu 2> /dev/null
done

echo "NO OVERLAPPING"

for gpu in 1 2 4
do
	echo -n $gpu$'\t'
	NX_ARGS="--no-gpu-overlap" NX_GPUS=$gpu ./matmul_gpu 2> /dev/null
done
	
echo "Test finished"

