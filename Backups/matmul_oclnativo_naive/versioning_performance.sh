#!/bin/bash

# Matrix multiplication calling CUBLAS *gemm kernels
#for gpus in 1 2
#do
#  for smps in 1 2 4 8
#  do
#    for sched in default affinity versioning
#    do
#
#NX_GPUCUBLASINIT=yes NX_GPUS=$gpus NX_PES=$smps NX_SCHEDULE=$sched NX_GPUPREFETCH=yes NX_GPUOVERLAP=yes run matmul_cublas3
#NX_GPUCUBLASINIT=yes NX_GPUS=$gpus NX_PES=$smps NX_SCHEDULE=$sched NX_GPUPREFETCH=yes NX_GPUOVERLAP=yes run matmul_cublas3
#NX_GPUCUBLASINIT=yes NX_GPUS=$gpus NX_PES=$smps NX_SCHEDULE=$sched NX_GPUPREFETCH=yes NX_GPUOVERLAP=yes run matmul_cublas3
#NX_GPUCUBLASINIT=yes NX_GPUS=$gpus NX_PES=$smps NX_SCHEDULE=$sched NX_GPUPREFETCH=yes NX_GPUOVERLAP=yes run matmul_cublas3
#NX_GPUCUBLASINIT=yes NX_GPUS=$gpus NX_PES=$smps NX_SCHEDULE=$sched NX_GPUPREFETCH=yes NX_GPUOVERLAP=yes run matmul_cublas3
#
#    done
#  done
#done


# Matrix multiplication with 3 versions of *gemm: CUBLAS, CUDA and CBLAS
for gpus in 1 2
do
  for smps in 1 2 4 8
  do

NX_GPUCUBLASINIT=yes NX_GPUS=$gpus NX_PES=$smps NX_SCHEDULE=versioning NX_GPUPREFETCH=yes NX_GPUOVERLAP=yes run matmul_hyb2
NX_GPUCUBLASINIT=yes NX_GPUS=$gpus NX_PES=$smps NX_SCHEDULE=versioning NX_GPUPREFETCH=yes NX_GPUOVERLAP=yes run matmul_hyb2
NX_GPUCUBLASINIT=yes NX_GPUS=$gpus NX_PES=$smps NX_SCHEDULE=versioning NX_GPUPREFETCH=yes NX_GPUOVERLAP=yes run matmul_hyb2
NX_GPUCUBLASINIT=yes NX_GPUS=$gpus NX_PES=$smps NX_SCHEDULE=versioning NX_GPUPREFETCH=yes NX_GPUOVERLAP=yes run matmul_hyb2
NX_GPUCUBLASINIT=yes NX_GPUS=$gpus NX_PES=$smps NX_SCHEDULE=versioning NX_GPUPREFETCH=yes NX_GPUOVERLAP=yes run matmul_hyb2

  done
done

# Matrix multiplication with 3 versions of *gemm: CUBLAS, CUDA and CBLAS
#for bsize in 512 1024 #2048 4096
#do
#  for gpus in 1 2
#  do
#    for smps in 1 2 4 8
#    do
#
#NX_GPUCUBLASINIT=yes NX_GPUS=$gpus NX_PES=$smps NX_SCHEDULE=versioning NX_GPUPREFETCH=yes NX_GPUOVERLAP=yes run matmul_hyb_$bsize
#NX_GPUCUBLASINIT=yes NX_GPUS=$gpus NX_PES=$smps NX_SCHEDULE=versioning NX_GPUPREFETCH=yes NX_GPUOVERLAP=yes run matmul_hyb_$bsize
#NX_GPUCUBLASINIT=yes NX_GPUS=$gpus NX_PES=$smps NX_SCHEDULE=versioning NX_GPUPREFETCH=yes NX_GPUOVERLAP=yes run matmul_hyb_$bsize
#NX_GPUCUBLASINIT=yes NX_GPUS=$gpus NX_PES=$smps NX_SCHEDULE=versioning NX_GPUPREFETCH=yes NX_GPUOVERLAP=yes run matmul_hyb_$bsize
#NX_GPUCUBLASINIT=yes NX_GPUS=$gpus NX_PES=$smps NX_SCHEDULE=versioning NX_GPUPREFETCH=yes NX_GPUOVERLAP=yes run matmul_hyb_$bsize
#
#    done
#  done
#done
