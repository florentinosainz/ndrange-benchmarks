#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

#include "driver.h"

#ifdef DP
#define REAL double
#else
#define REAL float
#endif

int N_DEVICES=2;
int MULTIPLIER_QUEUE=4;


REAL** inputs_A;
REAL** inputs_H;
REAL** copy_ahead_inputs_A;
REAL** copy_ahead_inputs_H;
REAL** outputs;
REAL** copy_ahead_outputs;


const int BS = BSIZE;

__global__ void Muld(REAL* A, REAL* B, REAL* C, int BS);

void cudaSetDeviceAux(int i){
	//Every "virtual" device will map to the real device
	cudaSetDevice(i%N_DEVICES); 
}

void matmul( int m, int l, int n, int mDIM, int lDIM, int nDIM, REAL **tileA, REAL **tileB,
             REAL **tileC )
{	
	int i, j, k;
	int n_device=0;
	cudaError_t err;

	cudaStream_t stream[N_DEVICES*MULTIPLIER_QUEUE*2];
	cudaStream_t streamcout[N_DEVICES*MULTIPLIER_QUEUE];
	cudaStream_t streamcopys[N_DEVICES*MULTIPLIER_QUEUE];
	cudaStream_t streamcopys2[N_DEVICES*MULTIPLIER_QUEUE];
	cudaStream_t streamfinalout[N_DEVICES*MULTIPLIER_QUEUE];
	// for (i=0; i<mDIM+nDIM;++i){
		// clEnqueueMarker(commands[0], &events_dep[i]);
	// }
	int firstTime[N_DEVICES*MULTIPLIER_QUEUE];
	for (i=0; i<N_DEVICES*MULTIPLIER_QUEUE;++i){
		firstTime[i]=0;
		cudaSetDeviceAux(i); 
		cudaStreamCreate(&(stream[i]));
		cudaStreamCreate(&(streamcout[i]));
		cudaStreamCreate(&(streamcopys[i]));
		cudaStreamCreate(&(streamcopys2[i]));
		cudaStreamCreate(&(streamfinalout[i]));
	}
	n_device=0;
	int p;
	//Programmed for CUDA 4.1 without HyperQ, with hyperQ all this "tricks" shouldnt be needed
	for(i = 0;i < mDIM; i++){
		int max=nDIM+nDIM%(N_DEVICES*MULTIPLIER_QUEUE);
		//Split this inner loop in blocks of size=number of devices
		for (p = 0; p < max; p=p+N_DEVICES*MULTIPLIER_QUEUE){
				int x;
				//This block does the copies
				for (x=0; x<N_DEVICES*MULTIPLIER_QUEUE;x++){
					j=p+x;
					if (j<nDIM) {
						n_device=x;
						cudaSetDeviceAux(n_device);
						if (firstTime[n_device]==0){
							err= cudaMemcpyAsync((void**) outputs[n_device],tileC[i*nDIM+j],  sizeof(REAL) * BS * BS,cudaMemcpyHostToDevice,stream[n_device]);
							if (err != cudaSuccess)
							{
								printf("Error: Failed to write to source array C %d into device %d!, %s\n",err,n_device, cudaGetErrorString(err));
								exit(1);
							}
						} else {		
							REAL* tmp_output;
							tmp_output=outputs[n_device];
							outputs[n_device]=copy_ahead_outputs[n_device];
							copy_ahead_outputs[n_device]=tmp_output;
						}
						
						for (k = 0; k < lDIM; k++){
							if (firstTime[n_device]==0){
								firstTime[n_device]=1;
									err= cudaMemcpyAsync((void**) inputs_A[n_device*lDIM+k],  tileA[i*lDIM+k],  sizeof(REAL) * BS * BS,cudaMemcpyHostToDevice,stream[n_device]);
									if (err != cudaSuccess)
									{
										printf("Error: Failed to write to source array A %d into device %d!, %s\n",err,n_device, cudaGetErrorString(err));
										exit(1);
									}
									err= cudaMemcpyAsync((void**) inputs_H[n_device*lDIM+k],tileB[k*nDIM+j],  sizeof(REAL) * BS * BS,cudaMemcpyHostToDevice,stream[n_device]);
									if (err != cudaSuccess)
									{
										printf("Error: Failed to write to source array H %d into device %d!, %s\n",err,n_device, cudaGetErrorString(err));
										exit(1);
									}
								} else {							
									REAL* tmp_H;
									REAL* tmp_A;
									tmp_A=inputs_A[n_device];
									tmp_H=inputs_H[n_device];
									inputs_A[n_device]=copy_ahead_inputs_A[n_device];
									inputs_H[n_device]=copy_ahead_inputs_H[n_device];
									copy_ahead_inputs_A[n_device]=tmp_A;
									copy_ahead_inputs_H[n_device]=tmp_H;
								}
							}
							
							for (k = 0; k < lDIM; k++){
								cudaSetDeviceAux(n_device);
								dim3 dimBlock, dimGrid;
								dimBlock.x=16;
								dimBlock.y=16;
								dimGrid.x=BS;
								dimGrid.y=BS;
								dimGrid.x = dimGrid.x < dimBlock.x ? 1 : dimGrid.x / dimBlock.x + (dimGrid.x % dimBlock.x == 0 ? 0 : 1);
								dimGrid.y = dimGrid.y < dimBlock.y ? 1 : dimGrid.y / dimBlock.y + (dimGrid.y % dimBlock.y == 0 ? 0 : 1);
								
								cudaStreamSynchronize(streamcopys[n_device]);
								cudaStreamSynchronize(streamcopys2[n_device]);
								cudaStreamSynchronize(streamcout[n_device]);
								Muld<<<dimGrid,dimBlock,0,stream[n_device]>>>(inputs_A[n_device*lDIM+k], inputs_H[n_device*lDIM+k], outputs[n_device],BS);								
								
								if (i<mDIM-1){
									err= cudaMemcpyAsync((void**) copy_ahead_inputs_A[n_device*lDIM+k],  tileA[((i+1)*lDIM+k)],  sizeof(REAL) * BS * BS,cudaMemcpyHostToDevice,streamcopys[n_device]);
									if (err != cudaSuccess)
									{
										printf("Error: Failed cahead to write to source array A %d into device %d!, %s\n",err,n_device, cudaGetErrorString(err));
										exit(1);
									}
								}
								if (j<nDIM-1){
									err= cudaMemcpyAsync((void**) copy_ahead_inputs_H[n_device*lDIM+k],tileB[(k*nDIM+(j+1))],  sizeof(REAL) * BS * BS,cudaMemcpyHostToDevice,streamcopys2[n_device]);
									if (err != cudaSuccess)
									{
										printf("Error: Failed  cahead to write to source array H %d into device %d!, %s\n",err,n_device, cudaGetErrorString(err));
										exit(1);
									}
								}
							}	
						
							if (i<mDIM-1){
								err= cudaMemcpyAsync((void**) copy_ahead_outputs[n_device],tileC[(i+1)*nDIM+j],  sizeof(REAL) * BS * BS,cudaMemcpyHostToDevice,streamcout[n_device]);
								if (err != cudaSuccess)
								{
									printf("Error: Failed to write to source array C %d into device %d!, %s\n",err,n_device, cudaGetErrorString(err));
									exit(1);
								}
							}
						
					}
				}
				//Now gather back
				for (x=0; x<N_DEVICES*MULTIPLIER_QUEUE;x++){
					j=p+x;
					if (j<nDIM) {					
						n_device=x;
						cudaSetDeviceAux(n_device);	
						//err= cudaMemcpyAsync(tileC[i*nDIM+j],(void**) outputs[n_device],  sizeof(REAL) * BS * BS,cudaMemcpyDeviceToHost,stream[n_device]);
						if (err != cudaSuccess)
						{
							printf("Error: Failed to write to source array H %d into device %d!, %s\n",err,n_device, cudaGetErrorString(err));
							exit(1);
						}
					}
				}
			}	
		}
			
	//Wait for end and release
	for (i=0; i<N_DEVICES;++i){
		cudaSetDeviceAux(i);			
		cudaDeviceSynchronize();
	}
	for (i=0; i<N_DEVICES*MULTIPLIER_QUEUE;++i){
		cudaFree(&(streamfinalout[i]));
	}
}



//#define BSIZE 1024

double   cclock( void );
int      check( int nrep, int m, int l, int n, int mDIM, int nDIM, REAL **c );
void     gendat(int, int, int, int, int, int, REAL **, REAL **, REAL **);
void     matmul( int, int, int, int, int, int, REAL **a, REAL **b, REAL **c );
void     prthead( void );
void     prtspeed( int, int, int, int, double, int, unsigned long );

int calcdim(int x)
{
        int dimval;
        if(x%BSIZE != 0)
                dimval = x/BSIZE + 1;
        else
                dimval = x/BSIZE;

        return dimval;
}
	
int main (int argc, char** argv)
{ /* main */
    if (argc > 1) {
		N_DEVICES=atoi(argv[1]);
    }
	int       m, l, n;
	int      mDIM, lDIM, nDIM;
	int      ok, nrep;
	unsigned long nops;
	int      i,e;
	REAL   **a, **b, **c;
	double   time;
	FILE     *inl;
// ------------------------------------------------------------------------	
	

	inl = fopen( "test.in", "r" );
	if (inl == 0) {
		printf("No input file 'test.in' found.\n");
		exit(1);
	}

	while( ( fscanf( inl, "%d%d%d%d\n", &m, &l, &n, &nrep ) != EOF ) ){

		mDIM = calcdim(m);
		lDIM = calcdim(l);
		nDIM = calcdim(n);
		
		
		REAL* inputs_A_aux[N_DEVICES*MULTIPLIER_QUEUE*lDIM];
		REAL* inputs_H_aux[N_DEVICES*MULTIPLIER_QUEUE*lDIM];
		REAL* outputs_aux[N_DEVICES*MULTIPLIER_QUEUE];
		REAL* copy_ahead_inputs_A_aux[N_DEVICES*MULTIPLIER_QUEUE*lDIM];
		REAL* copy_ahead_inputs_H_aux[N_DEVICES*MULTIPLIER_QUEUE*lDIM];
		REAL* copy_ahead_outputs_aux[N_DEVICES*MULTIPLIER_QUEUE];
		inputs_A=inputs_A_aux;
		inputs_H=inputs_H_aux;
		outputs=outputs_aux;
		copy_ahead_inputs_A=copy_ahead_inputs_A_aux;
		copy_ahead_inputs_H=copy_ahead_inputs_H_aux;
		copy_ahead_outputs=copy_ahead_outputs_aux;
		cudaError_t err;
		for (i = 0; i< N_DEVICES*MULTIPLIER_QUEUE; i++){	
			cudaSetDeviceAux(i);
			for (e=0; e<lDIM;e++){
				err= cudaMalloc ((void **) &inputs_A[i*lDIM+e], sizeof(REAL) * BS * BS);	
				if (err != cudaSuccess ){
					printf("Error: Failed to create buffer, err %s!\n", cudaGetErrorString(err));
					return EXIT_FAILURE;
				}		
				err= cudaMalloc ((void **) &inputs_H[i*lDIM+e], sizeof(REAL)  * BS * BS);	
				if (err != cudaSuccess ){
					printf("Error: Failed to create buffer, err %s!\n", cudaGetErrorString(err));
					return EXIT_FAILURE;
				}
				
				err= cudaMalloc ((void **) &copy_ahead_inputs_A[i*lDIM+e], sizeof(REAL) * BS * BS);	
				if (err != cudaSuccess ){
					printf("Error: Failed to create buffer, err %d!\n",err);
					return EXIT_FAILURE;
				}		
				err= cudaMalloc ((void **) &copy_ahead_inputs_H[i*lDIM+e], sizeof(REAL)  * BS * BS);	
				if (err != cudaSuccess ){
					printf("Error: Failed to create buffer, err %d!\n",err);
					return EXIT_FAILURE;
				}
			}
			err= cudaMalloc ((void **) &outputs[i],  sizeof(REAL)  * BS * BS);	
			if (err != cudaSuccess ){
				printf("Error: Failed to create buffer, err %d!\n",err);
				return EXIT_FAILURE;
			}		
			err= cudaMalloc ((void **) &copy_ahead_outputs[i],  sizeof(REAL)  * BS * BS);	
			if (err != cudaSuccess ){
				printf("Error: Failed to create buffer, err %d!\n",err);
				return EXIT_FAILURE;
			}		
		}


		a = (REAL **)malloc( mDIM*lDIM*sizeof( REAL *) );
		b = (REAL **)malloc( lDIM*nDIM*sizeof( REAL *) );
		c = (REAL **)malloc( mDIM*nDIM*sizeof( REAL *) );
      
		for(i=0;i<mDIM*lDIM;i++)
			a[i] = (REAL *)malloc(BSIZE*BSIZE*sizeof(REAL));

		for(i=0;i<lDIM*nDIM;i++)
			b[i] = (REAL *)malloc(BSIZE*BSIZE*sizeof(REAL));

		for(i=0;i<mDIM*nDIM;i++)
			c[i] = (REAL *)malloc(BSIZE*BSIZE*sizeof(REAL));


		gendat( mDIM, lDIM, nDIM, m, l, n, a, b, c );
		

		time = cclock();
		

		for( i = 0; i < nrep; i++ ){
			matmul( m, l, n, mDIM, lDIM, nDIM, a, b, c ); 
		}
		cudaDeviceSynchronize();
		time = cclock() - time;
		ok   = check( nrep, m, l, n, mDIM, nDIM, c);

		time = time/nrep;
		nops  = (unsigned long) 2*m*l*n;
		prtspeed( m, l, n, BSIZE, time, ok, nops );

		for(i=0;i<mDIM*lDIM;i++)
			free( a[i] );

		for(i=0;i<lDIM*nDIM;i++)
			free( b[i] );
  
		for(i=0;i<mDIM*nDIM;i++)
			free( c[i] );


		for (i=0; i<N_DEVICES*MULTIPLIER_QUEUE;i++){	
			cudaFree(inputs_A[i]);
			cudaFree(inputs_H[i]);
			cudaFree(outputs[i]);
		}
		free( a ); free( b ); free( c );
	}
	//printf( "----------------------------------------------------------\n" );
}
