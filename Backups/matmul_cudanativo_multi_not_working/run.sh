#!/bin/bash

# @ partition = projects
# @ output = matmul.log
# @ error = matmul.log
# @ initialdir = .
# @ total_tasks = 1
# @ cpus_per_task = 12
# @ gpus_per_node = 2
# @ node_usage = not_shared
# @ wall_clock_limit = 00:20:00

touch test.in
./matmul ${1:-1}  #${1:-1}  is the number of GPUs