typedef unsigned long int size_t;
struct  cudaPitchedPtr
{
  void *ptr;
  size_t pitch;
  size_t xsize;
  size_t ysize;
};
static __inline  struct cudaPitchedPtr make_cudaPitchedPtr(void *d, size_t p, size_t xsz, size_t ysz)
{
  struct cudaPitchedPtr s;
  s.ptr = d;
  s.pitch = p;
  s.xsize = xsz;
  s.ysize = ysz;
  return s;
}
struct  cudaPos
{
  size_t x;
  size_t y;
  size_t z;
};
static __inline  struct cudaPos make_cudaPos(size_t x, size_t y, size_t z)
{
  struct cudaPos p;
  p.x = x;
  p.y = y;
  p.z = z;
  return p;
}
struct  cudaExtent
{
  size_t width;
  size_t height;
  size_t depth;
};
static __inline  struct cudaExtent make_cudaExtent(size_t w, size_t h, size_t d)
{
  struct cudaExtent e;
  e.width = w;
  e.height = h;
  e.depth = d;
  return e;
}
struct  char1
{
  signed char x;
};
typedef struct char1 char1;
static __inline  char1 make_char1(signed char x)
{
  char1 t;
  t.x = x;
  return t;
}
struct  uchar1
{
  unsigned char x;
};
typedef struct uchar1 uchar1;
static __inline  uchar1 make_uchar1(unsigned char x)
{
  uchar1 t;
  t.x = x;
  return t;
}
struct __attribute__((aligned(2))) char2
{
  signed char x;
  signed char y;
};
typedef struct char2 char2;
static __inline  char2 make_char2(signed char x, signed char y)
{
  char2 t;
  t.x = x;
  t.y = y;
  return t;
}
struct __attribute__((aligned(2))) uchar2
{
  unsigned char x;
  unsigned char y;
};
typedef struct uchar2 uchar2;
static __inline  uchar2 make_uchar2(unsigned char x, unsigned char y)
{
  uchar2 t;
  t.x = x;
  t.y = y;
  return t;
}
struct  char3
{
  signed char x;
  signed char y;
  signed char z;
};
typedef struct char3 char3;
static __inline  char3 make_char3(signed char x, signed char y, signed char z)
{
  char3 t;
  t.x = x;
  t.y = y;
  t.z = z;
  return t;
}
struct  uchar3
{
  unsigned char x;
  unsigned char y;
  unsigned char z;
};
typedef struct uchar3 uchar3;
static __inline  uchar3 make_uchar3(unsigned char x, unsigned char y, unsigned char z)
{
  uchar3 t;
  t.x = x;
  t.y = y;
  t.z = z;
  return t;
}
struct __attribute__((aligned(4))) char4
{
  signed char x;
  signed char y;
  signed char z;
  signed char w;
};
typedef struct char4 char4;
static __inline  char4 make_char4(signed char x, signed char y, signed char z, signed char w)
{
  char4 t;
  t.x = x;
  t.y = y;
  t.z = z;
  t.w = w;
  return t;
}
struct __attribute__((aligned(4))) uchar4
{
  unsigned char x;
  unsigned char y;
  unsigned char z;
  unsigned char w;
};
typedef struct uchar4 uchar4;
static __inline  uchar4 make_uchar4(unsigned char x, unsigned char y, unsigned char z, unsigned char w)
{
  uchar4 t;
  t.x = x;
  t.y = y;
  t.z = z;
  t.w = w;
  return t;
}
struct  short1
{
  short int x;
};
typedef struct short1 short1;
static __inline  short1 make_short1(short int x)
{
  short1 t;
  t.x = x;
  return t;
}
struct  ushort1
{
  unsigned short int x;
};
typedef struct ushort1 ushort1;
static __inline  ushort1 make_ushort1(unsigned short int x)
{
  ushort1 t;
  t.x = x;
  return t;
}
struct __attribute__((aligned(4))) short2
{
  short int x;
  short int y;
};
typedef struct short2 short2;
static __inline  short2 make_short2(short int x, short int y)
{
  short2 t;
  t.x = x;
  t.y = y;
  return t;
}
struct __attribute__((aligned(4))) ushort2
{
  unsigned short int x;
  unsigned short int y;
};
typedef struct ushort2 ushort2;
static __inline  ushort2 make_ushort2(unsigned short int x, unsigned short int y)
{
  ushort2 t;
  t.x = x;
  t.y = y;
  return t;
}
struct  short3
{
  short int x;
  short int y;
  short int z;
};
typedef struct short3 short3;
static __inline  short3 make_short3(short int x, short int y, short int z)
{
  short3 t;
  t.x = x;
  t.y = y;
  t.z = z;
  return t;
}
struct  ushort3
{
  unsigned short int x;
  unsigned short int y;
  unsigned short int z;
};
typedef struct ushort3 ushort3;
static __inline  ushort3 make_ushort3(unsigned short int x, unsigned short int y, unsigned short int z)
{
  ushort3 t;
  t.x = x;
  t.y = y;
  t.z = z;
  return t;
}
struct __attribute__((aligned(8))) short4
{
  short int x;
  short int y;
  short int z;
  short int w;
};
typedef struct short4 short4;
static __inline  short4 make_short4(short int x, short int y, short int z, short int w)
{
  short4 t;
  t.x = x;
  t.y = y;
  t.z = z;
  t.w = w;
  return t;
}
struct __attribute__((aligned(8))) ushort4
{
  unsigned short int x;
  unsigned short int y;
  unsigned short int z;
  unsigned short int w;
};
typedef struct ushort4 ushort4;
static __inline  ushort4 make_ushort4(unsigned short int x, unsigned short int y, unsigned short int z, unsigned short int w)
{
  ushort4 t;
  t.x = x;
  t.y = y;
  t.z = z;
  t.w = w;
  return t;
}
struct  int1
{
  int x;
};
typedef struct int1 int1;
static __inline  int1 make_int1(int x)
{
  int1 t;
  t.x = x;
  return t;
}
struct  uint1
{
  unsigned int x;
};
typedef struct uint1 uint1;
static __inline  uint1 make_uint1(unsigned int x)
{
  uint1 t;
  t.x = x;
  return t;
}
struct __attribute__((aligned(8))) int2
{
  int x;
  int y;
};
typedef struct int2 int2;
static __inline  int2 make_int2(int x, int y)
{
  int2 t;
  t.x = x;
  t.y = y;
  return t;
}
struct __attribute__((aligned(8))) uint2
{
  unsigned int x;
  unsigned int y;
};
typedef struct uint2 uint2;
static __inline  uint2 make_uint2(unsigned int x, unsigned int y)
{
  uint2 t;
  t.x = x;
  t.y = y;
  return t;
}
struct  int3
{
  int x;
  int y;
  int z;
};
typedef struct int3 int3;
static __inline  int3 make_int3(int x, int y, int z)
{
  int3 t;
  t.x = x;
  t.y = y;
  t.z = z;
  return t;
}
struct  uint3
{
  unsigned int x;
  unsigned int y;
  unsigned int z;
};
typedef struct uint3 uint3;
static __inline  uint3 make_uint3(unsigned int x, unsigned int y, unsigned int z)
{
  uint3 t;
  t.x = x;
  t.y = y;
  t.z = z;
  return t;
}
struct __attribute__((aligned(16))) int4
{
  int x;
  int y;
  int z;
  int w;
};
typedef struct int4 int4;
static __inline  int4 make_int4(int x, int y, int z, int w)
{
  int4 t;
  t.x = x;
  t.y = y;
  t.z = z;
  t.w = w;
  return t;
}
struct __attribute__((aligned(16))) uint4
{
  unsigned int x;
  unsigned int y;
  unsigned int z;
  unsigned int w;
};
typedef struct uint4 uint4;
static __inline  uint4 make_uint4(unsigned int x, unsigned int y, unsigned int z, unsigned int w)
{
  uint4 t;
  t.x = x;
  t.y = y;
  t.z = z;
  t.w = w;
  return t;
}
struct  long1
{
  long int x;
};
typedef struct long1 long1;
static __inline  long1 make_long1(long int x)
{
  long1 t;
  t.x = x;
  return t;
}
struct  ulong1
{
  unsigned long int x;
};
typedef struct ulong1 ulong1;
static __inline  ulong1 make_ulong1(unsigned long int x)
{
  ulong1 t;
  t.x = x;
  return t;
}
struct __attribute__((aligned(2 * sizeof(long int)))) long2
{
  long int x;
  long int y;
};
typedef struct long2 long2;
static __inline  long2 make_long2(long int x, long int y)
{
  long2 t;
  t.x = x;
  t.y = y;
  return t;
}
struct __attribute__((aligned(2 * sizeof(unsigned long int)))) ulong2
{
  unsigned long int x;
  unsigned long int y;
};
typedef struct ulong2 ulong2;
static __inline  ulong2 make_ulong2(unsigned long int x, unsigned long int y)
{
  ulong2 t;
  t.x = x;
  t.y = y;
  return t;
}
struct  long3
{
  long int x;
  long int y;
  long int z;
};
typedef struct long3 long3;
static __inline  long3 make_long3(long int x, long int y, long int z)
{
  long3 t;
  t.x = x;
  t.y = y;
  t.z = z;
  return t;
}
struct  ulong3
{
  unsigned long int x;
  unsigned long int y;
  unsigned long int z;
};
typedef struct ulong3 ulong3;
static __inline  ulong3 make_ulong3(unsigned long int x, unsigned long int y, unsigned long int z)
{
  ulong3 t;
  t.x = x;
  t.y = y;
  t.z = z;
  return t;
}
struct __attribute__((aligned(16))) long4
{
  long int x;
  long int y;
  long int z;
  long int w;
};
typedef struct long4 long4;
static __inline  long4 make_long4(long int x, long int y, long int z, long int w)
{
  long4 t;
  t.x = x;
  t.y = y;
  t.z = z;
  t.w = w;
  return t;
}
struct __attribute__((aligned(16))) ulong4
{
  unsigned long int x;
  unsigned long int y;
  unsigned long int z;
  unsigned long int w;
};
typedef struct ulong4 ulong4;
static __inline  ulong4 make_ulong4(unsigned long int x, unsigned long int y, unsigned long int z, unsigned long int w)
{
  ulong4 t;
  t.x = x;
  t.y = y;
  t.z = z;
  t.w = w;
  return t;
}
struct  float1
{
  float x;
};
typedef struct float1 float1;
static __inline  float1 make_float1(float x)
{
  float1 t;
  t.x = x;
  return t;
}
struct __attribute__((aligned(8))) float2
{
  float x;
  float y;
};
typedef struct float2 float2;
static __inline  float2 make_float2(float x, float y)
{
  float2 t;
  t.x = x;
  t.y = y;
  return t;
}
struct  float3
{
  float x;
  float y;
  float z;
};
typedef struct float3 float3;
static __inline  float3 make_float3(float x, float y, float z)
{
  float3 t;
  t.x = x;
  t.y = y;
  t.z = z;
  return t;
}
struct __attribute__((aligned(16))) float4
{
  float x;
  float y;
  float z;
  float w;
};
typedef struct float4 float4;
static __inline  float4 make_float4(float x, float y, float z, float w)
{
  float4 t;
  t.x = x;
  t.y = y;
  t.z = z;
  t.w = w;
  return t;
}
struct  longlong1
{
  long long int x;
};
typedef struct longlong1 longlong1;
static __inline  longlong1 make_longlong1(long long int x)
{
  longlong1 t;
  t.x = x;
  return t;
}
struct  ulonglong1
{
  unsigned long long int x;
};
typedef struct ulonglong1 ulonglong1;
static __inline  ulonglong1 make_ulonglong1(unsigned long long int x)
{
  ulonglong1 t;
  t.x = x;
  return t;
}
struct __attribute__((aligned(16))) longlong2
{
  long long int x;
  long long int y;
};
typedef struct longlong2 longlong2;
static __inline  longlong2 make_longlong2(long long int x, long long int y)
{
  longlong2 t;
  t.x = x;
  t.y = y;
  return t;
}
struct __attribute__((aligned(16))) ulonglong2
{
  unsigned long long int x;
  unsigned long long int y;
};
typedef struct ulonglong2 ulonglong2;
static __inline  ulonglong2 make_ulonglong2(unsigned long long int x, unsigned long long int y)
{
  ulonglong2 t;
  t.x = x;
  t.y = y;
  return t;
}
struct  longlong3
{
  long long int x;
  long long int y;
  long long int z;
};
typedef struct longlong3 longlong3;
static __inline  longlong3 make_longlong3(long long int x, long long int y, long long int z)
{
  longlong3 t;
  t.x = x;
  t.y = y;
  t.z = z;
  return t;
}
struct  ulonglong3
{
  unsigned long long int x;
  unsigned long long int y;
  unsigned long long int z;
};
typedef struct ulonglong3 ulonglong3;
static __inline  ulonglong3 make_ulonglong3(unsigned long long int x, unsigned long long int y, unsigned long long int z)
{
  ulonglong3 t;
  t.x = x;
  t.y = y;
  t.z = z;
  return t;
}
struct __attribute__((aligned(16))) longlong4
{
  long long int x;
  long long int y;
  long long int z;
  long long int w;
};
typedef struct longlong4 longlong4;
static __inline  longlong4 make_longlong4(long long int x, long long int y, long long int z, long long int w)
{
  longlong4 t;
  t.x = x;
  t.y = y;
  t.z = z;
  t.w = w;
  return t;
}
struct __attribute__((aligned(16))) ulonglong4
{
  unsigned long long int x;
  unsigned long long int y;
  unsigned long long int z;
  unsigned long long int w;
};
typedef struct ulonglong4 ulonglong4;
static __inline  ulonglong4 make_ulonglong4(unsigned long long int x, unsigned long long int y, unsigned long long int z, unsigned long long int w)
{
  ulonglong4 t;
  t.x = x;
  t.y = y;
  t.z = z;
  t.w = w;
  return t;
}
struct  double1
{
  double x;
};
typedef struct double1 double1;
static __inline  double1 make_double1(double x)
{
  double1 t;
  t.x = x;
  return t;
}
struct __attribute__((aligned(16))) double2
{
  double x;
  double y;
};
typedef struct double2 double2;
static __inline  double2 make_double2(double x, double y)
{
  double2 t;
  t.x = x;
  t.y = y;
  return t;
}
struct  double3
{
  double x;
  double y;
  double z;
};
typedef struct double3 double3;
static __inline  double3 make_double3(double x, double y, double z)
{
  double3 t;
  t.x = x;
  t.y = y;
  t.z = z;
  return t;
}
struct __attribute__((aligned(16))) double4
{
  double x;
  double y;
  double z;
  double w;
};
typedef struct double4 double4;
static __inline  double4 make_double4(double x, double y, double z, double w)
{
  double4 t;
  t.x = x;
  t.y = y;
  t.z = z;
  t.w = w;
  return t;
}
typedef float2 cuFloatComplex;
static __inline  float cuCrealf(cuFloatComplex x)
{
  return x.x;
}
static __inline  float cuCimagf(cuFloatComplex x)
{
  return x.y;
}
static __inline  cuFloatComplex make_cuFloatComplex(float r, float i)
{
  cuFloatComplex res;
  res.x = r;
  res.y = i;
  return res;
}
static __inline  cuFloatComplex cuConjf(cuFloatComplex x)
{
  return make_cuFloatComplex(cuCrealf(x),  -cuCimagf(x));
}
static __inline  cuFloatComplex cuCaddf(cuFloatComplex x, cuFloatComplex y)
{
  return make_cuFloatComplex(cuCrealf(x) + cuCrealf(y), cuCimagf(x) + cuCimagf(y));
}
static __inline  cuFloatComplex cuCsubf(cuFloatComplex x, cuFloatComplex y)
{
  return make_cuFloatComplex(cuCrealf(x) - cuCrealf(y), cuCimagf(x) - cuCimagf(y));
}
static __inline  cuFloatComplex cuCmulf(cuFloatComplex x, cuFloatComplex y)
{
  cuFloatComplex prod;
  prod = make_cuFloatComplex(cuCrealf(x) * cuCrealf(y) - cuCimagf(x) * cuCimagf(y), cuCrealf(x) * cuCimagf(y) + cuCimagf(x) * cuCrealf(y));
  return prod;
}
extern float fabsf(float __x)__attribute__((__nothrow__)) __attribute__((__const__));
static __inline  cuFloatComplex cuCdivf(cuFloatComplex x, cuFloatComplex y)
{
  cuFloatComplex quot;
  float s = fabsf(cuCrealf(y)) + fabsf(cuCimagf(y));
  float oos = 1.000000000000000000000000e+00f / s;
  float ars = cuCrealf(x) * oos;
  float ais = cuCimagf(x) * oos;
  float brs = cuCrealf(y) * oos;
  float bis = cuCimagf(y) * oos;
  s = brs * brs + bis * bis;
  oos = 1.000000000000000000000000e+00f / s;
  quot = make_cuFloatComplex((ars * brs + ais * bis) * oos, (ais * brs - ars * bis) * oos);
  return quot;
}
extern float sqrtf(float __x)__attribute__((__nothrow__));
static __inline  float cuCabsf(cuFloatComplex x)
{
  float v;
  float w;
  float t;
  float a = cuCrealf(x);
  float b = cuCimagf(x);
  a = fabsf(a);
  b = fabsf(b);
  if (a > b)
    {
      v = a;
      w = b;
    }
  else
    {
      v = b;
      w = a;
    }
  t = w / v;
  t = 1.000000000000000000000000e+00f + t * t;
  t = v * sqrtf(t);
  if ((v == 0.000000000000000000000000e+00f || v > 3.402823466385288598117042e+38f) || w > 3.402823466385288598117042e+38f)
    {
      t = v + w;
    }
  return t;
}
typedef double2 cuDoubleComplex;
static __inline  double cuCreal(cuDoubleComplex x)
{
  return x.x;
}
static __inline  double cuCimag(cuDoubleComplex x)
{
  return x.y;
}
static __inline  cuDoubleComplex make_cuDoubleComplex(double r, double i)
{
  cuDoubleComplex res;
  res.x = r;
  res.y = i;
  return res;
}
static __inline  cuDoubleComplex cuConj(cuDoubleComplex x)
{
  return make_cuDoubleComplex(cuCreal(x),  -cuCimag(x));
}
static __inline  cuDoubleComplex cuCadd(cuDoubleComplex x, cuDoubleComplex y)
{
  return make_cuDoubleComplex(cuCreal(x) + cuCreal(y), cuCimag(x) + cuCimag(y));
}
static __inline  cuDoubleComplex cuCsub(cuDoubleComplex x, cuDoubleComplex y)
{
  return make_cuDoubleComplex(cuCreal(x) - cuCreal(y), cuCimag(x) - cuCimag(y));
}
static __inline  cuDoubleComplex cuCmul(cuDoubleComplex x, cuDoubleComplex y)
{
  cuDoubleComplex prod;
  prod = make_cuDoubleComplex(cuCreal(x) * cuCreal(y) - cuCimag(x) * cuCimag(y), cuCreal(x) * cuCimag(y) + cuCimag(x) * cuCreal(y));
  return prod;
}
extern double fabs(double __x)__attribute__((__nothrow__)) __attribute__((__const__));
static __inline  cuDoubleComplex cuCdiv(cuDoubleComplex x, cuDoubleComplex y)
{
  cuDoubleComplex quot;
  double s = fabs(cuCreal(y)) + fabs(cuCimag(y));
  double oos = 1.00000000000000000000000000000000000000000000000000000e+00 / s;
  double ars = cuCreal(x) * oos;
  double ais = cuCimag(x) * oos;
  double brs = cuCreal(y) * oos;
  double bis = cuCimag(y) * oos;
  s = brs * brs + bis * bis;
  oos = 1.00000000000000000000000000000000000000000000000000000e+00 / s;
  quot = make_cuDoubleComplex((ars * brs + ais * bis) * oos, (ais * brs - ars * bis) * oos);
  return quot;
}
extern double sqrt(double __x)__attribute__((__nothrow__));
static __inline  double cuCabs(cuDoubleComplex x)
{
  double v;
  double w;
  double t;
  double a = cuCreal(x);
  double b = cuCimag(x);
  a = fabs(a);
  b = fabs(b);
  if (a > b)
    {
      v = a;
      w = b;
    }
  else
    {
      v = b;
      w = a;
    }
  t = w / v;
  t = 1.00000000000000000000000000000000000000000000000000000e+00 + t * t;
  t = v * sqrt(t);
  if ((v == 0.00000000000000000000000000000000000000000000000000000e+00 || v > 1.79769313486231570814527423731704356798070567525844997e+308) || w > 1.79769313486231570814527423731704356798070567525844997e+308)
    {
      t = v + w;
    }
  return t;
}
typedef cuFloatComplex cuComplex;
static __inline  cuComplex make_cuComplex(float x, float y)
{
  return make_cuFloatComplex(x, y);
}
static __inline  cuDoubleComplex cuComplexFloatToDouble(cuFloatComplex c)
{
  return make_cuDoubleComplex((double)cuCrealf(c), (double)cuCimagf(c));
}
static __inline  cuFloatComplex cuComplexDoubleToFloat(cuDoubleComplex c)
{
  return make_cuFloatComplex((float)cuCreal(c), (float)cuCimag(c));
}
static __inline  cuComplex cuCfmaf(cuComplex x, cuComplex y, cuComplex d)
{
  float real_res;
  float imag_res;
  real_res = cuCrealf(x) * cuCrealf(y) + cuCrealf(d);
  imag_res = cuCrealf(x) * cuCimagf(y) + cuCimagf(d);
  real_res =  -(cuCimagf(x) * cuCimagf(y)) + real_res;
  imag_res = cuCimagf(x) * cuCrealf(y) + imag_res;
  return make_cuComplex(real_res, imag_res);
}
static __inline  cuDoubleComplex cuCfma(cuDoubleComplex x, cuDoubleComplex y, cuDoubleComplex d)
{
  double real_res;
  double imag_res;
  real_res = cuCreal(x) * cuCreal(y) + cuCreal(d);
  imag_res = cuCreal(x) * cuCimag(y) + cuCimag(d);
  real_res =  -(cuCimag(x) * cuCimag(y)) + real_res;
  imag_res = cuCimag(x) * cuCreal(y) + imag_res;
  return make_cuDoubleComplex(real_res, imag_res);
}
struct mcc_struct_anon_24;
typedef struct mcc_struct_anon_24 Particle;
struct  mcc_struct_anon_24
{
  float position_x;
  float position_y;
  float position_z;
  float velocity_x;
  float velocity_y;
  float velocity_z;
  float mass;
  float pad;
};
 void calculate_force(Particle *this_particle1, Particle *this_particle2, float *force_x, float *force_y, float *force_z)
{
  float difference_x;
  float difference_y;
  float difference_z;
  float distance_squared;
  float distance;
  float force_magnitude;
  difference_x = (*this_particle2).position_x - (*this_particle1).position_x;
  difference_y = (*this_particle2).position_y - (*this_particle1).position_y;
  difference_z = (*this_particle2).position_z - (*this_particle1).position_z;
  distance_squared = difference_x * difference_x + difference_y * difference_y + difference_z * difference_z;
  distance = sqrtf(distance_squared);
  force_magnitude = 6.67260000000000030511017635472502396165594973354018293e-11 * (*this_particle1).mass * (*this_particle2).mass / distance_squared;
  *force_x = force_magnitude / distance * difference_x;
  *force_y = force_magnitude / distance * difference_y;
  *force_z = force_magnitude / distance * difference_z;
}
const int MAX_NUM_THREADS = 128;
struct  nanos_args_0_t
{
  int size;
  float time_interval;
  int number_of_particles;
  Particle *d_particles;
  Particle *output;
  int first_local;
  int last_local;
};
typedef unsigned int nanos_event_key_t;
enum mcc_enum_anon_8
{
  NANOS_OK = 0,
  NANOS_UNKNOWN_ERR = 1,
  NANOS_UNIMPLEMENTED = 2,
  NANOS_ENOMEM = 3,
  NANOS_INVALID_PARAM = 4
};
typedef enum mcc_enum_anon_8 nanos_err_t;
extern nanos_err_t nanos_instrument_get_key(const char *key, nanos_event_key_t *event_key);
extern void nanos_handle_error(nanos_err_t err);
typedef unsigned long long int nanos_event_value_t;
extern nanos_err_t nanos_instrument_register_value_with_val(nanos_event_value_t val, const char *key, const char *value, const char *description, _Bool abort_when_registered);
enum mcc_enum_anon_4
{
  NANOS_STATE_START = 0,
  NANOS_STATE_END = 1,
  NANOS_SUBSTATE_START = 2,
  NANOS_SUBSTATE_END = 3,
  NANOS_BURST_START = 4,
  NANOS_BURST_END = 5,
  NANOS_PTP_START = 6,
  NANOS_PTP_END = 7,
  NANOS_POINT = 8,
  EVENT_TYPES = 9
};
typedef enum mcc_enum_anon_4 nanos_event_type_t;
enum mcc_enum_anon_6
{
  NANOS_WD_DOMAIN = 0,
  NANOS_WD_DEPENDENCY = 1,
  NANOS_WAIT = 2,
  NANOS_WD_REMOTE = 3,
  NANOS_XFER_PUT = 4,
  NANOS_XFER_GET = 5
};
typedef enum mcc_enum_anon_6 nanos_event_domain_t;
typedef long long int nanos_event_id_t;
struct  mcc_struct_anon_20
{
  nanos_event_type_t type;
  nanos_event_key_t key;
  nanos_event_value_t value;
  nanos_event_domain_t domain;
  nanos_event_id_t id;
};
typedef struct mcc_struct_anon_20 nanos_event_t;
extern nanos_err_t nanos_instrument_events(unsigned int num_events, nanos_event_t events[]);
void gpu_ol_calculate_force_func_1_unpacked(int *const mcc_arg_0, float *const mcc_arg_1, int *const mcc_arg_2, Particle **const mcc_arg_3, Particle **const mcc_arg_4, int *const mcc_arg_5, int *const mcc_arg_6);
extern nanos_err_t nanos_instrument_close_user_fun_event();
static void gpu_ol_calculate_force_func_1(struct nanos_args_0_t *const args)
{
  nanos_err_t err;
  nanos_event_t event;
  {
    static int nanos_funct_id_init = 0;
    static nanos_event_key_t nanos_instr_uf_location_key = 0;
    if (nanos_funct_id_init == 0)
      {
        err = nanos_instrument_get_key("user-funct-location", &nanos_instr_uf_location_key);
        if (err != NANOS_OK)
          nanos_handle_error(err);
        err = nanos_instrument_register_value_with_val((nanos_event_value_t)(void (*)(void *))(void (*)(struct nanos_args_0_t *const ))gpu_ol_calculate_force_func_1, "user-funct-location", "gpu_ol_calculate_force_func_1", "void calculate_force_func(int, float, int, Particle *, Particle *, int, int)@kernel.c@17", 0);
        if (err != NANOS_OK)
          nanos_handle_error(err);
        nanos_funct_id_init = 1;
      }
    event.type = NANOS_BURST_START;
    event.key = nanos_instr_uf_location_key;
    event.value = (nanos_event_value_t)(void (*)(void *))(void (*)(struct nanos_args_0_t *const ))gpu_ol_calculate_force_func_1;
    err = nanos_instrument_events(1, &event);
    gpu_ol_calculate_force_func_1_unpacked(&((*args).size), &((*args).time_interval), &((*args).number_of_particles), &((*args).d_particles), &((*args).output), &((*args).first_local), &((*args).last_local));
    err = nanos_instrument_close_user_fun_event();
  }
}
typedef void *nanos_wd_t;
typedef unsigned int nanos_copy_id_t;
extern nanos_err_t nanos_get_addr(nanos_copy_id_t copy_id, void **addr, void *cwd);
static void nanos_xlate_fun_0(struct nanos_args_0_t *const arg, void *wd)
{
  {
    void *device_base_address;
    nanos_err_t err;
    device_base_address = 0;
    err = nanos_get_addr(0, &device_base_address, wd);
    if (err != NANOS_OK)
      nanos_handle_error(err);
    (*arg).d_particles = (Particle *)device_base_address;
  }
  {
    void *device_base_address;
    nanos_err_t err;
    device_base_address = 0;
    err = nanos_get_addr(1, &device_base_address, wd);
    if (err != NANOS_OK)
      nanos_handle_error(err);
    (*arg).output = (Particle *)device_base_address;
  }
}
struct  nanos_args_1_t
{
  int size;
  float time_interval;
  int number_of_particles;
  Particle *d_particles;
  Particle *out;
  int first_local;
  int last_local;
};
static void nanos_xlate_fun_1(struct nanos_args_1_t *const arg, void *wd)
{
  {
    void *device_base_address;
    nanos_err_t err;
    device_base_address = 0;
    err = nanos_get_addr(0, &device_base_address, wd);
    if (err != NANOS_OK)
      nanos_handle_error(err);
    (*arg).d_particles = (Particle *)device_base_address;
  }
  {
    void *device_base_address;
    nanos_err_t err;
    device_base_address = 0;
    err = nanos_get_addr(1, &device_base_address, wd);
    if (err != NANOS_OK)
      nanos_handle_error(err);
    (*arg).out = (Particle *)device_base_address;
  }
}
struct  mcc_struct_anon_19
{
  void (*outline)(void *);
};
typedef struct mcc_struct_anon_19 nanos_smp_args_t;
struct  mcc_struct_anon_16
{
  _Bool mandatory_creation:1;
  _Bool tied:1;
  _Bool clear_chunk:1;
  _Bool reserved0:1;
  _Bool reserved1:1;
  _Bool reserved2:1;
  _Bool reserved3:1;
  _Bool reserved4:1;
};
typedef struct mcc_struct_anon_16 nanos_wd_props_t;
struct  nanos_const_wd_definition_tag
{
  nanos_wd_props_t props;
  size_t data_alignment;
  size_t num_copies;
  size_t num_devices;
  size_t num_dimensions;
  const char *description;
};
typedef struct nanos_const_wd_definition_tag nanos_const_wd_definition_t;
struct  mcc_struct_anon_18
{
  void *(*factory)(void *);
  void *arg;
};
typedef struct mcc_struct_anon_18 nanos_device_t;
struct  nanos_const_wd_definition_1
{
  nanos_const_wd_definition_t base;
  nanos_device_t devices[1];
};
void *nanos_gpu_factory(void *args);
typedef void *nanos_thread_t;
struct  mcc_struct_anon_17
{
  void *tie_to;
  unsigned int priority;
};
typedef struct mcc_struct_anon_17 nanos_wd_dyn_props_t;
struct mcc_struct_anon_9;
typedef struct mcc_struct_anon_9 nanos_copy_data_internal_t;
typedef nanos_copy_data_internal_t nanos_copy_data_t;
struct mcc_struct_anon_5;
typedef struct mcc_struct_anon_5 nanos_region_dimension_internal_t;
typedef void *nanos_wg_t;
extern nanos_err_t nanos_create_wd_compact(void **wd, nanos_const_wd_definition_t *const_data, nanos_wd_dyn_props_t *dyn_props, size_t data_size, void **data, void *wg, nanos_copy_data_t **copies, nanos_region_dimension_internal_t **dimensions);
extern void *nanos_current_wd(void);
struct  mcc_struct_anon_5
{
  size_t size;
  size_t lower_bound;
  size_t accessed_length;
};
typedef nanos_region_dimension_internal_t nanos_region_dimension_t;
struct  mcc_struct_anon_6
{
  _Bool input:1;
  _Bool output:1;
  _Bool can_rename:1;
  _Bool concurrent:1;
  _Bool commutative:1;
};
typedef struct mcc_struct_anon_6 nanos_access_type_internal_t;
typedef long int ptrdiff_t;
struct  mcc_struct_anon_7
{
  void *address;
  nanos_access_type_internal_t flags;
  short int dimension_count;
  const nanos_region_dimension_internal_t *dimensions;
  ptrdiff_t offset;
};
typedef struct mcc_struct_anon_7 nanos_data_access_internal_t;
typedef nanos_data_access_internal_t nanos_data_access_t;
enum mcc_enum_anon_3
{
  NANOS_PRIVATE = 0,
  NANOS_SHARED = 1
};
typedef enum mcc_enum_anon_3 nanos_sharing_t;
struct  mcc_struct_anon_10
{
  _Bool input:1;
  _Bool output:1;
};
struct  mcc_struct_anon_9
{
  void *address;
  nanos_sharing_t sharing;
  struct mcc_struct_anon_10 flags;
  short int dimension_count;
  const nanos_region_dimension_internal_t *dimensions;
  ptrdiff_t offset;
};
typedef void (*nanos_translate_args_t)(void *, void *);
extern nanos_err_t nanos_set_translate_function(void *wd, void (*translate_args)(void *, void *));
typedef void *nanos_team_t;
extern nanos_err_t nanos_submit(void *wd, size_t num_data_accesses, nanos_data_access_t *data_accesses, void *team);
extern nanos_err_t nanos_create_wd_and_run_compact(nanos_const_wd_definition_t *const_data, nanos_wd_dyn_props_t *dyn_props, size_t data_size, void *data, size_t num_data_accesses, nanos_data_access_t *data_accesses, nanos_copy_data_t *copies, nanos_region_dimension_internal_t *dimensions, void (*translate_args)(void *, void *));
struct  mcc_struct_anon_23
{
  void (*outline)(void *);
};
typedef struct mcc_struct_anon_23 nanos_opencl_args_t;
static void ocl_ol_calculate_force_func_ocl_3(struct nanos_args_1_t *const args);
extern void *nanos_opencl_factory(void *args);
void Particle_array_calculate_forces_cuda(Particle *this_particle_array, Particle *output_array, int number_of_particles, float time_interval)
{
  int i;
  const int bs = number_of_particles / 8;
  for (i = 0; i < number_of_particles; i += bs)
    {
      if (i / bs % 2)
        {
          int mcc_arg_0 = bs;
          float mcc_arg_1 = time_interval;
          int mcc_arg_2 = number_of_particles;
          Particle *mcc_arg_3 = this_particle_array;
          Particle *mcc_arg_4 = &output_array[i];
          int mcc_arg_5 = i;
          int mcc_arg_6 = i + bs - 1;
          {
            nanos_wd_dyn_props_t nanos_wd_dyn_props;
            struct nanos_args_0_t *ol_args;
            nanos_err_t err;
            struct nanos_args_0_t imm_args;
            /* CUDA device descriptor */
            static nanos_smp_args_t gpu_ol_calculate_force_func_1_args = { (void (*)(void *))gpu_ol_calculate_force_func_1 };
            static struct nanos_const_wd_definition_1 nanos_wd_const_data = { { { 1, 1, 0, 0, 0, 0, 0, 0 }, __alignof__(struct nanos_args_0_t), 2, 1, 2, "calculate_force_func" }, { { &nanos_gpu_factory, &gpu_ol_calculate_force_func_1_args } } };
            nanos_wd_dyn_props.tie_to = 0;
            nanos_wd_dyn_props.priority = 0;
            ol_args = (struct nanos_args_0_t *)0;
            void *nanos_wd_ = (void *)0;
            nanos_copy_data_t *ol_copy_data = (nanos_copy_data_t *)0;
            nanos_region_dimension_internal_t *ol_copy_dimensions = (nanos_region_dimension_internal_t *)0;
            err = nanos_create_wd_compact(&nanos_wd_, &nanos_wd_const_data.base, &nanos_wd_dyn_props, sizeof(struct nanos_args_0_t), (void **)&ol_args, nanos_current_wd(), &ol_copy_data, &ol_copy_dimensions);
            if (err != NANOS_OK)
              nanos_handle_error(err);
            if (nanos_wd_ != (void *)0)
              {
                (*ol_args).size = mcc_arg_0;
                (*ol_args).time_interval = mcc_arg_1;
                (*ol_args).number_of_particles = mcc_arg_2;
                (*ol_args).d_particles = mcc_arg_3;
                (*ol_args).output = mcc_arg_4;
                (*ol_args).first_local = mcc_arg_5;
                (*ol_args).last_local = mcc_arg_6;
                nanos_region_dimension_t dimensions_0[1] = { { sizeof(Particle) * (((1 * mcc_arg_0 + 0 * mcc_arg_0 - 1) - (0 * mcc_arg_0) - 0) + 1), sizeof(Particle) * (0 * mcc_arg_0 - 0), sizeof(Particle) * ((1 * mcc_arg_0 + 0 * mcc_arg_0 - 1 - 0 * mcc_arg_0) + 1) } };
                nanos_region_dimension_t dimensions_1[1] = { { sizeof(Particle) * (((2 * mcc_arg_0 + 1 * mcc_arg_0 - 1) - (1 * mcc_arg_0) - 0) + 1), sizeof(Particle) * (1 * mcc_arg_0 - 0), sizeof(Particle) * ((2 * mcc_arg_0 + 1 * mcc_arg_0 - 1 - 1 * mcc_arg_0) + 1) } };
                nanos_region_dimension_t dimensions_2[1] = { { sizeof(Particle) * (((3 * mcc_arg_0 + 2 * mcc_arg_0 - 1) - (2 * mcc_arg_0) - 0) + 1), sizeof(Particle) * (2 * mcc_arg_0 - 0), sizeof(Particle) * ((3 * mcc_arg_0 + 2 * mcc_arg_0 - 1 - 2 * mcc_arg_0) + 1) } };
                nanos_region_dimension_t dimensions_3[1] = { { sizeof(Particle) * (((4 * mcc_arg_0 + 3 * mcc_arg_0 - 1) - (3 * mcc_arg_0) - 0) + 1), sizeof(Particle) * (3 * mcc_arg_0 - 0), sizeof(Particle) * ((4 * mcc_arg_0 + 3 * mcc_arg_0 - 1 - 3 * mcc_arg_0) + 1) } };
                nanos_region_dimension_t dimensions_4[1] = { { sizeof(Particle) * (((5 * mcc_arg_0 + 4 * mcc_arg_0 - 1) - (4 * mcc_arg_0) - 0) + 1), sizeof(Particle) * (4 * mcc_arg_0 - 0), sizeof(Particle) * ((5 * mcc_arg_0 + 4 * mcc_arg_0 - 1 - 4 * mcc_arg_0) + 1) } };
                nanos_region_dimension_t dimensions_5[1] = { { sizeof(Particle) * (((6 * mcc_arg_0 + 5 * mcc_arg_0 - 1) - (5 * mcc_arg_0) - 0) + 1), sizeof(Particle) * (5 * mcc_arg_0 - 0), sizeof(Particle) * ((6 * mcc_arg_0 + 5 * mcc_arg_0 - 1 - 5 * mcc_arg_0) + 1) } };
                nanos_region_dimension_t dimensions_6[1] = { { sizeof(Particle) * (((7 * mcc_arg_0 + 6 * mcc_arg_0 - 1) - (6 * mcc_arg_0) - 0) + 1), sizeof(Particle) * (6 * mcc_arg_0 - 0), sizeof(Particle) * ((7 * mcc_arg_0 + 6 * mcc_arg_0 - 1 - 6 * mcc_arg_0) + 1) } };
                nanos_region_dimension_t dimensions_7[1] = { { sizeof(Particle) * (((8 * mcc_arg_0 + 7 * mcc_arg_0 - 1) - (7 * mcc_arg_0) - 0) + 1), sizeof(Particle) * (7 * mcc_arg_0 - 0), sizeof(Particle) * ((8 * mcc_arg_0 + 7 * mcc_arg_0 - 1 - 7 * mcc_arg_0) + 1) } };
                nanos_region_dimension_t dimensions_8[1] = { { sizeof(Particle) * (((mcc_arg_0) - 1 - 0) + 1), sizeof(Particle) * 0, sizeof(Particle) * (((mcc_arg_0) - 1 - 0) + 1) } };
                nanos_data_access_t dependences[9] = { { mcc_arg_3, { 1, 0, 0, 0, 0 }, 1, dimensions_0, 32 * (((0 * mcc_arg_0) - (0))) }, { mcc_arg_3, { 1, 0, 0, 0, 0 }, 1, dimensions_1, 32 * (((1 * mcc_arg_0) - (0))) }, { mcc_arg_3, { 1, 0, 0, 0, 0 }, 1, dimensions_2, 32 * (((2 * mcc_arg_0) - (0))) }, { mcc_arg_3, { 1, 0, 0, 0, 0 }, 1, dimensions_3, 32 * (((3 * mcc_arg_0) - (0))) }, { mcc_arg_3, { 1, 0, 0, 0, 0 }, 1, dimensions_4, 32 * (((4 * mcc_arg_0) - (0))) }, { mcc_arg_3, { 1, 0, 0, 0, 0 }, 1, dimensions_5, 32 * (((5 * mcc_arg_0) - (0))) }, { mcc_arg_3, { 1, 0, 0, 0, 0 }, 1, dimensions_6, 32 * (((6 * mcc_arg_0) - (0))) }, { mcc_arg_3, { 1, 0, 0, 0, 0 }, 1, dimensions_7, 32 * (((7 * mcc_arg_0) - (0))) }, { mcc_arg_4, { 0, 1, 0, 0, 0 }, 1, dimensions_8, 0 } };
                ;
                ol_copy_dimensions[0].size = ((mcc_arg_2 + 0 - 1 - 0) + 1) * sizeof(Particle);
                ol_copy_dimensions[0].lower_bound = 0 * sizeof(Particle);
                ol_copy_dimensions[0].accessed_length = (mcc_arg_2 + 0 - 1 - 0 + 1) * sizeof(Particle);
                ol_copy_data[0].sharing = NANOS_SHARED;
                ol_copy_data[0].address = (void *)mcc_arg_3;
                ol_copy_data[0].flags.input = 1;
                ol_copy_data[0].flags.output = 0;
                ol_copy_data[0].dimension_count = 1;
                ol_copy_data[0].dimensions = &ol_copy_dimensions[0];
                ol_copy_data[0].offset = 32 * (((0) - (0)));
                ol_copy_dimensions[1].size = (((mcc_arg_0) - 1 - 0) + 1) * sizeof(Particle);
                ol_copy_dimensions[1].lower_bound = 0 * sizeof(Particle);
                ol_copy_dimensions[1].accessed_length = ((mcc_arg_0) - 1 - 0 + 1) * sizeof(Particle);
                ol_copy_data[1].sharing = NANOS_SHARED;
                ol_copy_data[1].address = (void *)mcc_arg_4;
                ol_copy_data[1].flags.input = 0;
                ol_copy_data[1].flags.output = 1;
                ol_copy_data[1].dimension_count = 1;
                ol_copy_data[1].dimensions = &ol_copy_dimensions[1];
                ol_copy_data[1].offset = 0;
                err = nanos_set_translate_function(nanos_wd_, (void (*)(void *, void *))nanos_xlate_fun_0);
                if (err != NANOS_OK)
                  nanos_handle_error(err);
                err = nanos_submit(nanos_wd_, 9, dependences, (void *)0);
                if (err != NANOS_OK)
                  nanos_handle_error(err);
              }
            else
              {
                nanos_region_dimension_internal_t imm_copy_dimensions[2];
                nanos_copy_data_t imm_copy_data[2];
                imm_args.size = mcc_arg_0;
                imm_args.time_interval = mcc_arg_1;
                imm_args.number_of_particles = mcc_arg_2;
                imm_args.d_particles = mcc_arg_3;
                imm_args.output = mcc_arg_4;
                imm_args.first_local = mcc_arg_5;
                imm_args.last_local = mcc_arg_6;
                nanos_region_dimension_t dimensions_0[1] = { { sizeof(Particle) * (((1 * mcc_arg_0 + 0 * mcc_arg_0 - 1) - (0 * mcc_arg_0) - 0) + 1), sizeof(Particle) * (0 * mcc_arg_0 - 0), sizeof(Particle) * ((1 * mcc_arg_0 + 0 * mcc_arg_0 - 1 - 0 * mcc_arg_0) + 1) } };
                nanos_region_dimension_t dimensions_1[1] = { { sizeof(Particle) * (((2 * mcc_arg_0 + 1 * mcc_arg_0 - 1) - (1 * mcc_arg_0) - 0) + 1), sizeof(Particle) * (1 * mcc_arg_0 - 0), sizeof(Particle) * ((2 * mcc_arg_0 + 1 * mcc_arg_0 - 1 - 1 * mcc_arg_0) + 1) } };
                nanos_region_dimension_t dimensions_2[1] = { { sizeof(Particle) * (((3 * mcc_arg_0 + 2 * mcc_arg_0 - 1) - (2 * mcc_arg_0) - 0) + 1), sizeof(Particle) * (2 * mcc_arg_0 - 0), sizeof(Particle) * ((3 * mcc_arg_0 + 2 * mcc_arg_0 - 1 - 2 * mcc_arg_0) + 1) } };
                nanos_region_dimension_t dimensions_3[1] = { { sizeof(Particle) * (((4 * mcc_arg_0 + 3 * mcc_arg_0 - 1) - (3 * mcc_arg_0) - 0) + 1), sizeof(Particle) * (3 * mcc_arg_0 - 0), sizeof(Particle) * ((4 * mcc_arg_0 + 3 * mcc_arg_0 - 1 - 3 * mcc_arg_0) + 1) } };
                nanos_region_dimension_t dimensions_4[1] = { { sizeof(Particle) * (((5 * mcc_arg_0 + 4 * mcc_arg_0 - 1) - (4 * mcc_arg_0) - 0) + 1), sizeof(Particle) * (4 * mcc_arg_0 - 0), sizeof(Particle) * ((5 * mcc_arg_0 + 4 * mcc_arg_0 - 1 - 4 * mcc_arg_0) + 1) } };
                nanos_region_dimension_t dimensions_5[1] = { { sizeof(Particle) * (((6 * mcc_arg_0 + 5 * mcc_arg_0 - 1) - (5 * mcc_arg_0) - 0) + 1), sizeof(Particle) * (5 * mcc_arg_0 - 0), sizeof(Particle) * ((6 * mcc_arg_0 + 5 * mcc_arg_0 - 1 - 5 * mcc_arg_0) + 1) } };
                nanos_region_dimension_t dimensions_6[1] = { { sizeof(Particle) * (((7 * mcc_arg_0 + 6 * mcc_arg_0 - 1) - (6 * mcc_arg_0) - 0) + 1), sizeof(Particle) * (6 * mcc_arg_0 - 0), sizeof(Particle) * ((7 * mcc_arg_0 + 6 * mcc_arg_0 - 1 - 6 * mcc_arg_0) + 1) } };
                nanos_region_dimension_t dimensions_7[1] = { { sizeof(Particle) * (((8 * mcc_arg_0 + 7 * mcc_arg_0 - 1) - (7 * mcc_arg_0) - 0) + 1), sizeof(Particle) * (7 * mcc_arg_0 - 0), sizeof(Particle) * ((8 * mcc_arg_0 + 7 * mcc_arg_0 - 1 - 7 * mcc_arg_0) + 1) } };
                nanos_region_dimension_t dimensions_8[1] = { { sizeof(Particle) * (((mcc_arg_0) - 1 - 0) + 1), sizeof(Particle) * 0, sizeof(Particle) * (((mcc_arg_0) - 1 - 0) + 1) } };
                nanos_data_access_t dependences[9] = { { mcc_arg_3, { 1, 0, 0, 0, 0 }, 1, dimensions_0, 32 * (((0 * mcc_arg_0) - (0))) }, { mcc_arg_3, { 1, 0, 0, 0, 0 }, 1, dimensions_1, 32 * (((1 * mcc_arg_0) - (0))) }, { mcc_arg_3, { 1, 0, 0, 0, 0 }, 1, dimensions_2, 32 * (((2 * mcc_arg_0) - (0))) }, { mcc_arg_3, { 1, 0, 0, 0, 0 }, 1, dimensions_3, 32 * (((3 * mcc_arg_0) - (0))) }, { mcc_arg_3, { 1, 0, 0, 0, 0 }, 1, dimensions_4, 32 * (((4 * mcc_arg_0) - (0))) }, { mcc_arg_3, { 1, 0, 0, 0, 0 }, 1, dimensions_5, 32 * (((5 * mcc_arg_0) - (0))) }, { mcc_arg_3, { 1, 0, 0, 0, 0 }, 1, dimensions_6, 32 * (((6 * mcc_arg_0) - (0))) }, { mcc_arg_3, { 1, 0, 0, 0, 0 }, 1, dimensions_7, 32 * (((7 * mcc_arg_0) - (0))) }, { mcc_arg_4, { 0, 1, 0, 0, 0 }, 1, dimensions_8, 0 } };
                ;
                imm_copy_dimensions[0].size = ((mcc_arg_2 + 0 - 1 - 0) + 1) * sizeof(Particle);
                imm_copy_dimensions[0].lower_bound = 0 * sizeof(Particle);
                imm_copy_dimensions[0].accessed_length = (mcc_arg_2 + 0 - 1 - 0 + 1) * sizeof(Particle);
                imm_copy_data[0].sharing = NANOS_SHARED;
                imm_copy_data[0].address = (void *)mcc_arg_3;
                imm_copy_data[0].flags.input = 1;
                imm_copy_data[0].flags.output = 0;
                imm_copy_data[0].dimension_count = 1;
                imm_copy_data[0].dimensions = &imm_copy_dimensions[0];
                imm_copy_data[0].offset = 32 * (((0) - (0)));
                imm_copy_dimensions[1].size = (((mcc_arg_0) - 1 - 0) + 1) * sizeof(Particle);
                imm_copy_dimensions[1].lower_bound = 0 * sizeof(Particle);
                imm_copy_dimensions[1].accessed_length = ((mcc_arg_0) - 1 - 0 + 1) * sizeof(Particle);
                imm_copy_data[1].sharing = NANOS_SHARED;
                imm_copy_data[1].address = (void *)mcc_arg_4;
                imm_copy_data[1].flags.input = 0;
                imm_copy_data[1].flags.output = 1;
                imm_copy_data[1].dimension_count = 1;
                imm_copy_data[1].dimensions = &imm_copy_dimensions[1];
                imm_copy_data[1].offset = 0;
                err = nanos_create_wd_and_run_compact(&nanos_wd_const_data.base, &nanos_wd_dyn_props, sizeof(struct nanos_args_0_t), &imm_args, 9, dependences, imm_copy_data, imm_copy_dimensions, (void (*)(void *, void *))nanos_xlate_fun_0);
                if (err != NANOS_OK)
                  nanos_handle_error(err);
              }
          }
        }
      else
        {
          int mcc_arg_7 = bs;
          float mcc_arg_8 = time_interval;
          int mcc_arg_9 = number_of_particles;
          Particle *mcc_arg_10 = this_particle_array;
          Particle *mcc_arg_11 = &output_array[i];
          int mcc_arg_12 = i;
          int mcc_arg_13 = i + bs - 1;
          {
            static nanos_opencl_args_t ocl_ol_calculate_force_func_ocl_3_args;
            nanos_wd_dyn_props_t nanos_wd_dyn_props;
            struct nanos_args_1_t *ol_args;
            nanos_err_t err;
            struct nanos_args_1_t imm_args;
            /* OpenCL device descriptor */
            ocl_ol_calculate_force_func_ocl_3_args.outline = (void (*)(void *))ocl_ol_calculate_force_func_ocl_3;
            static struct nanos_const_wd_definition_1 nanos_wd_const_data = { { { 1, 1, 0, 0, 0, 0, 0, 0 }, __alignof__(struct nanos_args_1_t), 2, 1, 2, "calculate_force_func_ocl" }, { { &nanos_opencl_factory, &ocl_ol_calculate_force_func_ocl_3_args } } };
            nanos_wd_dyn_props.tie_to = 0;
            nanos_wd_dyn_props.priority = 0;
            ol_args = (struct nanos_args_1_t *)0;
            void *nanos_wd_ = (void *)0;
            nanos_copy_data_t *ol_copy_data = (nanos_copy_data_t *)0;
            nanos_region_dimension_internal_t *ol_copy_dimensions = (nanos_region_dimension_internal_t *)0;
            err = nanos_create_wd_compact(&nanos_wd_, &nanos_wd_const_data.base, &nanos_wd_dyn_props, sizeof(struct nanos_args_1_t), (void **)&ol_args, nanos_current_wd(), &ol_copy_data, &ol_copy_dimensions);
            if (err != NANOS_OK)
              nanos_handle_error(err);
            if (nanos_wd_ != (void *)0)
              {
                (*ol_args).size = mcc_arg_7;
                (*ol_args).time_interval = mcc_arg_8;
                (*ol_args).number_of_particles = mcc_arg_9;
                (*ol_args).d_particles = mcc_arg_10;
                (*ol_args).out = mcc_arg_11;
                (*ol_args).first_local = mcc_arg_12;
                (*ol_args).last_local = mcc_arg_13;
                nanos_region_dimension_t dimensions_0[1] = { { sizeof(Particle) * (((1 * mcc_arg_7 + 0 * mcc_arg_7 - 1) - (0 * mcc_arg_7) - 0) + 1), sizeof(Particle) * (0 * mcc_arg_7 - 0), sizeof(Particle) * ((1 * mcc_arg_7 + 0 * mcc_arg_7 - 1 - 0 * mcc_arg_7) + 1) } };
                nanos_region_dimension_t dimensions_1[1] = { { sizeof(Particle) * (((2 * mcc_arg_7 + 1 * mcc_arg_7 - 1) - (1 * mcc_arg_7) - 0) + 1), sizeof(Particle) * (1 * mcc_arg_7 - 0), sizeof(Particle) * ((2 * mcc_arg_7 + 1 * mcc_arg_7 - 1 - 1 * mcc_arg_7) + 1) } };
                nanos_region_dimension_t dimensions_2[1] = { { sizeof(Particle) * (((3 * mcc_arg_7 + 2 * mcc_arg_7 - 1) - (2 * mcc_arg_7) - 0) + 1), sizeof(Particle) * (2 * mcc_arg_7 - 0), sizeof(Particle) * ((3 * mcc_arg_7 + 2 * mcc_arg_7 - 1 - 2 * mcc_arg_7) + 1) } };
                nanos_region_dimension_t dimensions_3[1] = { { sizeof(Particle) * (((4 * mcc_arg_7 + 3 * mcc_arg_7 - 1) - (3 * mcc_arg_7) - 0) + 1), sizeof(Particle) * (3 * mcc_arg_7 - 0), sizeof(Particle) * ((4 * mcc_arg_7 + 3 * mcc_arg_7 - 1 - 3 * mcc_arg_7) + 1) } };
                nanos_region_dimension_t dimensions_4[1] = { { sizeof(Particle) * (((5 * mcc_arg_7 + 4 * mcc_arg_7 - 1) - (4 * mcc_arg_7) - 0) + 1), sizeof(Particle) * (4 * mcc_arg_7 - 0), sizeof(Particle) * ((5 * mcc_arg_7 + 4 * mcc_arg_7 - 1 - 4 * mcc_arg_7) + 1) } };
                nanos_region_dimension_t dimensions_5[1] = { { sizeof(Particle) * (((6 * mcc_arg_7 + 5 * mcc_arg_7 - 1) - (5 * mcc_arg_7) - 0) + 1), sizeof(Particle) * (5 * mcc_arg_7 - 0), sizeof(Particle) * ((6 * mcc_arg_7 + 5 * mcc_arg_7 - 1 - 5 * mcc_arg_7) + 1) } };
                nanos_region_dimension_t dimensions_6[1] = { { sizeof(Particle) * (((7 * mcc_arg_7 + 6 * mcc_arg_7 - 1) - (6 * mcc_arg_7) - 0) + 1), sizeof(Particle) * (6 * mcc_arg_7 - 0), sizeof(Particle) * ((7 * mcc_arg_7 + 6 * mcc_arg_7 - 1 - 6 * mcc_arg_7) + 1) } };
                nanos_region_dimension_t dimensions_7[1] = { { sizeof(Particle) * (((8 * mcc_arg_7 + 7 * mcc_arg_7 - 1) - (7 * mcc_arg_7) - 0) + 1), sizeof(Particle) * (7 * mcc_arg_7 - 0), sizeof(Particle) * ((8 * mcc_arg_7 + 7 * mcc_arg_7 - 1 - 7 * mcc_arg_7) + 1) } };
                nanos_region_dimension_t dimensions_8[1] = { { sizeof(Particle) * (((mcc_arg_7) - 1 - 0) + 1), sizeof(Particle) * 0, sizeof(Particle) * (((mcc_arg_7) - 1 - 0) + 1) } };
                nanos_data_access_t dependences[9] = { { mcc_arg_10, { 1, 0, 0, 0, 0 }, 1, dimensions_0, 32 * (((0 * mcc_arg_7) - (0))) }, { mcc_arg_10, { 1, 0, 0, 0, 0 }, 1, dimensions_1, 32 * (((1 * mcc_arg_7) - (0))) }, { mcc_arg_10, { 1, 0, 0, 0, 0 }, 1, dimensions_2, 32 * (((2 * mcc_arg_7) - (0))) }, { mcc_arg_10, { 1, 0, 0, 0, 0 }, 1, dimensions_3, 32 * (((3 * mcc_arg_7) - (0))) }, { mcc_arg_10, { 1, 0, 0, 0, 0 }, 1, dimensions_4, 32 * (((4 * mcc_arg_7) - (0))) }, { mcc_arg_10, { 1, 0, 0, 0, 0 }, 1, dimensions_5, 32 * (((5 * mcc_arg_7) - (0))) }, { mcc_arg_10, { 1, 0, 0, 0, 0 }, 1, dimensions_6, 32 * (((6 * mcc_arg_7) - (0))) }, { mcc_arg_10, { 1, 0, 0, 0, 0 }, 1, dimensions_7, 32 * (((7 * mcc_arg_7) - (0))) }, { mcc_arg_11, { 0, 1, 0, 0, 0 }, 1, dimensions_8, 0 } };
                ;
                ol_copy_dimensions[0].size = ((mcc_arg_9 + 0 - 1 - 0) + 1) * sizeof(Particle);
                ol_copy_dimensions[0].lower_bound = 0 * sizeof(Particle);
                ol_copy_dimensions[0].accessed_length = (mcc_arg_9 + 0 - 1 - 0 + 1) * sizeof(Particle);
                ol_copy_data[0].sharing = NANOS_SHARED;
                ol_copy_data[0].address = (void *)mcc_arg_10;
                ol_copy_data[0].flags.input = 1;
                ol_copy_data[0].flags.output = 0;
                ol_copy_data[0].dimension_count = 1;
                ol_copy_data[0].dimensions = &ol_copy_dimensions[0];
                ol_copy_data[0].offset = 32 * (((0) - (0)));
                ol_copy_dimensions[1].size = (((mcc_arg_7) - 1 - 0) + 1) * sizeof(Particle);
                ol_copy_dimensions[1].lower_bound = 0 * sizeof(Particle);
                ol_copy_dimensions[1].accessed_length = ((mcc_arg_7) - 1 - 0 + 1) * sizeof(Particle);
                ol_copy_data[1].sharing = NANOS_SHARED;
                ol_copy_data[1].address = (void *)mcc_arg_11;
                ol_copy_data[1].flags.input = 0;
                ol_copy_data[1].flags.output = 1;
                ol_copy_data[1].dimension_count = 1;
                ol_copy_data[1].dimensions = &ol_copy_dimensions[1];
                ol_copy_data[1].offset = 0;
                err = nanos_set_translate_function(nanos_wd_, (void (*)(void *, void *))nanos_xlate_fun_1);
                if (err != NANOS_OK)
                  nanos_handle_error(err);
                err = nanos_submit(nanos_wd_, 9, dependences, (void *)0);
                if (err != NANOS_OK)
                  nanos_handle_error(err);
              }
            else
              {
                nanos_region_dimension_internal_t imm_copy_dimensions[2];
                nanos_copy_data_t imm_copy_data[2];
                imm_args.size = mcc_arg_7;
                imm_args.time_interval = mcc_arg_8;
                imm_args.number_of_particles = mcc_arg_9;
                imm_args.d_particles = mcc_arg_10;
                imm_args.out = mcc_arg_11;
                imm_args.first_local = mcc_arg_12;
                imm_args.last_local = mcc_arg_13;
                nanos_region_dimension_t dimensions_0[1] = { { sizeof(Particle) * (((1 * mcc_arg_7 + 0 * mcc_arg_7 - 1) - (0 * mcc_arg_7) - 0) + 1), sizeof(Particle) * (0 * mcc_arg_7 - 0), sizeof(Particle) * ((1 * mcc_arg_7 + 0 * mcc_arg_7 - 1 - 0 * mcc_arg_7) + 1) } };
                nanos_region_dimension_t dimensions_1[1] = { { sizeof(Particle) * (((2 * mcc_arg_7 + 1 * mcc_arg_7 - 1) - (1 * mcc_arg_7) - 0) + 1), sizeof(Particle) * (1 * mcc_arg_7 - 0), sizeof(Particle) * ((2 * mcc_arg_7 + 1 * mcc_arg_7 - 1 - 1 * mcc_arg_7) + 1) } };
                nanos_region_dimension_t dimensions_2[1] = { { sizeof(Particle) * (((3 * mcc_arg_7 + 2 * mcc_arg_7 - 1) - (2 * mcc_arg_7) - 0) + 1), sizeof(Particle) * (2 * mcc_arg_7 - 0), sizeof(Particle) * ((3 * mcc_arg_7 + 2 * mcc_arg_7 - 1 - 2 * mcc_arg_7) + 1) } };
                nanos_region_dimension_t dimensions_3[1] = { { sizeof(Particle) * (((4 * mcc_arg_7 + 3 * mcc_arg_7 - 1) - (3 * mcc_arg_7) - 0) + 1), sizeof(Particle) * (3 * mcc_arg_7 - 0), sizeof(Particle) * ((4 * mcc_arg_7 + 3 * mcc_arg_7 - 1 - 3 * mcc_arg_7) + 1) } };
                nanos_region_dimension_t dimensions_4[1] = { { sizeof(Particle) * (((5 * mcc_arg_7 + 4 * mcc_arg_7 - 1) - (4 * mcc_arg_7) - 0) + 1), sizeof(Particle) * (4 * mcc_arg_7 - 0), sizeof(Particle) * ((5 * mcc_arg_7 + 4 * mcc_arg_7 - 1 - 4 * mcc_arg_7) + 1) } };
                nanos_region_dimension_t dimensions_5[1] = { { sizeof(Particle) * (((6 * mcc_arg_7 + 5 * mcc_arg_7 - 1) - (5 * mcc_arg_7) - 0) + 1), sizeof(Particle) * (5 * mcc_arg_7 - 0), sizeof(Particle) * ((6 * mcc_arg_7 + 5 * mcc_arg_7 - 1 - 5 * mcc_arg_7) + 1) } };
                nanos_region_dimension_t dimensions_6[1] = { { sizeof(Particle) * (((7 * mcc_arg_7 + 6 * mcc_arg_7 - 1) - (6 * mcc_arg_7) - 0) + 1), sizeof(Particle) * (6 * mcc_arg_7 - 0), sizeof(Particle) * ((7 * mcc_arg_7 + 6 * mcc_arg_7 - 1 - 6 * mcc_arg_7) + 1) } };
                nanos_region_dimension_t dimensions_7[1] = { { sizeof(Particle) * (((8 * mcc_arg_7 + 7 * mcc_arg_7 - 1) - (7 * mcc_arg_7) - 0) + 1), sizeof(Particle) * (7 * mcc_arg_7 - 0), sizeof(Particle) * ((8 * mcc_arg_7 + 7 * mcc_arg_7 - 1 - 7 * mcc_arg_7) + 1) } };
                nanos_region_dimension_t dimensions_8[1] = { { sizeof(Particle) * (((mcc_arg_7) - 1 - 0) + 1), sizeof(Particle) * 0, sizeof(Particle) * (((mcc_arg_7) - 1 - 0) + 1) } };
                nanos_data_access_t dependences[9] = { { mcc_arg_10, { 1, 0, 0, 0, 0 }, 1, dimensions_0, 32 * (((0 * mcc_arg_7) - (0))) }, { mcc_arg_10, { 1, 0, 0, 0, 0 }, 1, dimensions_1, 32 * (((1 * mcc_arg_7) - (0))) }, { mcc_arg_10, { 1, 0, 0, 0, 0 }, 1, dimensions_2, 32 * (((2 * mcc_arg_7) - (0))) }, { mcc_arg_10, { 1, 0, 0, 0, 0 }, 1, dimensions_3, 32 * (((3 * mcc_arg_7) - (0))) }, { mcc_arg_10, { 1, 0, 0, 0, 0 }, 1, dimensions_4, 32 * (((4 * mcc_arg_7) - (0))) }, { mcc_arg_10, { 1, 0, 0, 0, 0 }, 1, dimensions_5, 32 * (((5 * mcc_arg_7) - (0))) }, { mcc_arg_10, { 1, 0, 0, 0, 0 }, 1, dimensions_6, 32 * (((6 * mcc_arg_7) - (0))) }, { mcc_arg_10, { 1, 0, 0, 0, 0 }, 1, dimensions_7, 32 * (((7 * mcc_arg_7) - (0))) }, { mcc_arg_11, { 0, 1, 0, 0, 0 }, 1, dimensions_8, 0 } };
                ;
                imm_copy_dimensions[0].size = ((mcc_arg_9 + 0 - 1 - 0) + 1) * sizeof(Particle);
                imm_copy_dimensions[0].lower_bound = 0 * sizeof(Particle);
                imm_copy_dimensions[0].accessed_length = (mcc_arg_9 + 0 - 1 - 0 + 1) * sizeof(Particle);
                imm_copy_data[0].sharing = NANOS_SHARED;
                imm_copy_data[0].address = (void *)mcc_arg_10;
                imm_copy_data[0].flags.input = 1;
                imm_copy_data[0].flags.output = 0;
                imm_copy_data[0].dimension_count = 1;
                imm_copy_data[0].dimensions = &imm_copy_dimensions[0];
                imm_copy_data[0].offset = 32 * (((0) - (0)));
                imm_copy_dimensions[1].size = (((mcc_arg_7) - 1 - 0) + 1) * sizeof(Particle);
                imm_copy_dimensions[1].lower_bound = 0 * sizeof(Particle);
                imm_copy_dimensions[1].accessed_length = ((mcc_arg_7) - 1 - 0 + 1) * sizeof(Particle);
                imm_copy_data[1].sharing = NANOS_SHARED;
                imm_copy_data[1].address = (void *)mcc_arg_11;
                imm_copy_data[1].flags.input = 0;
                imm_copy_data[1].flags.output = 1;
                imm_copy_data[1].dimension_count = 1;
                imm_copy_data[1].dimensions = &imm_copy_dimensions[1];
                imm_copy_data[1].offset = 0;
                err = nanos_create_wd_and_run_compact(&nanos_wd_const_data.base, &nanos_wd_dyn_props, sizeof(struct nanos_args_1_t), &imm_args, 9, dependences, imm_copy_data, imm_copy_dimensions, (void (*)(void *, void *))nanos_xlate_fun_1);
                if (err != NANOS_OK)
                  nanos_handle_error(err);
              }
          }
        }
    }
}
extern void *nanos_create_current_kernel(const char *kernel_name, const char *opencl_code, const char *compiler_opts);
extern nanos_err_t nanos_opencl_set_arg(void *opencl_kernel, int arg_num, size_t size, void *pointer);
extern nanos_err_t nanos_opencl_set_bufferarg(void *opencl_kernel, int arg_num, void *pointer);
extern nanos_err_t nanos_exec_kernel(void *opencl_kernel, int work_dim, size_t *ndr_offset, size_t *ndr_local_size, size_t *ndr_global_size);
static void ocl_ol_calculate_force_func_ocl_3_unpacked(int *const mcc_arg_7, float *const mcc_arg_8, int *const mcc_arg_9, Particle **const mcc_arg_10, Particle **const mcc_arg_11, int *const mcc_arg_12, int *const mcc_arg_13)
{
  {
    nanos_err_t err;
    _Bool local_size_zero;
    void *ompss_kernel_ocl = nanos_create_current_kernel("calculate_force_func_ocl", "opencl_kernels.cl", "");
    err = nanos_opencl_set_arg(ompss_kernel_ocl, 0, sizeof(int), (int *) mcc_arg_7);
    err = nanos_opencl_set_arg(ompss_kernel_ocl, 1, sizeof(float), (float *) mcc_arg_8);
    err = nanos_opencl_set_arg(ompss_kernel_ocl, 2, sizeof(int), (int *) mcc_arg_9);
    err = nanos_opencl_set_bufferarg(ompss_kernel_ocl, 3, (*mcc_arg_10));
    err = nanos_opencl_set_bufferarg(ompss_kernel_ocl, 4, (*mcc_arg_11));
    err = nanos_opencl_set_arg(ompss_kernel_ocl, 5, sizeof(int), (int *) mcc_arg_12);
    err = nanos_opencl_set_arg(ompss_kernel_ocl, 6, sizeof(int), (int *) mcc_arg_13);
    int num_dim = 1;
    const int mcc_vla_0 = num_dim;
    size_t offset_arr[mcc_vla_0];
    const int mcc_vla_1 = num_dim;
    size_t local_size_arr[mcc_vla_1];
    const int mcc_vla_2 = num_dim;
    size_t global_size_arr[mcc_vla_2];
    local_size_zero = 0;
    offset_arr[0] = 0;
    local_size_arr[0] = MAX_NUM_THREADS;
    if (local_size_arr[0] == 0)
      {
        local_size_zero = 1;
      }
    global_size_arr[0] = (*mcc_arg_7);
    if (!local_size_zero)
      {
        int i;
        for (i = 0; i < num_dim; i = i + 1)
          {
            if (global_size_arr[i] < local_size_arr[i])
              {
                local_size_arr[i] = global_size_arr[i];
              }
            else
              {
                if (global_size_arr[i] % local_size_arr[i] != 0)
                  {
                    global_size_arr[i] = global_size_arr[i] + (local_size_arr[i] - global_size_arr[i] % local_size_arr[i]);
                  }
              }
          }
      }
    if (local_size_zero)
      {
        err = nanos_exec_kernel(ompss_kernel_ocl, num_dim, offset_arr, 0, global_size_arr);
      }
    else
      {
        err = nanos_exec_kernel(ompss_kernel_ocl, num_dim, offset_arr, local_size_arr, global_size_arr);
      }
  }
}
static void ocl_ol_calculate_force_func_ocl_3(struct nanos_args_1_t *const args)
{
  nanos_err_t err;
  nanos_event_t event;
  {
    static int nanos_funct_id_init = 0;
    static nanos_event_key_t nanos_instr_uf_location_key = 0;
    if (nanos_funct_id_init == 0)
      {
        err = nanos_instrument_get_key("user-funct-location", &nanos_instr_uf_location_key);
        if (err != NANOS_OK)
          nanos_handle_error(err);
        err = nanos_instrument_register_value_with_val((nanos_event_value_t)(void (*)(void *))(void (*)(struct nanos_args_1_t *const ))ocl_ol_calculate_force_func_ocl_3, "user-funct-location", "ocl_ol_calculate_force_func_ocl_3", "void calculate_force_func_ocl(int, float, int, Particle *, Particle *, int, int)@kernel.c@19", 0);
        if (err != NANOS_OK)
          nanos_handle_error(err);
        nanos_funct_id_init = 1;
      }
    event.type = NANOS_BURST_START;
    event.key = nanos_instr_uf_location_key;
    event.value = (nanos_event_value_t)(void (*)(void *))(void (*)(struct nanos_args_1_t *const ))ocl_ol_calculate_force_func_ocl_3;
    err = nanos_instrument_events(1, &event);
    ocl_ol_calculate_force_func_ocl_3_unpacked(&((*args).size), &((*args).time_interval), &((*args).number_of_particles), &((*args).d_particles), &((*args).out), &((*args).first_local), &((*args).last_local));
    err = nanos_instrument_close_user_fun_event();
  }
}
typedef void nanos_init_func_t(void *);
struct  mcc_struct_anon_21
{
  void (*func)(void *);
  void *data;
};
typedef struct mcc_struct_anon_21 nanos_init_desc_t;
void nanos_omp_set_interface(void *);
__attribute__((weak)) __attribute__((section("nanos_init"))) nanos_init_desc_t __section__nanos_init = { nanos_omp_set_interface, (void *)0 };
