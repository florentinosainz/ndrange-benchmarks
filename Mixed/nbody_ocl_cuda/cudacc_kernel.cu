struct mcc_struct_anon_24;
typedef struct mcc_struct_anon_24 Particle;
extern "C"
{
  const int MAX_NUM_THREADS = 128;
}
extern "C"
{
  struct CUstream_st *nanos_get_kernel_execution_stream();
}
extern "C"
{
  void calculate_force_func(int size, float time_interval, int number_of_particles, Particle *d_particles, Particle *output, int first_local, int last_local)__attribute__((global));
}
extern "C"
{
  void gpu_ol_calculate_force_func_1_unpacked(int *const mcc_arg_0, float *const mcc_arg_1, int *const mcc_arg_2, Particle **const mcc_arg_3, Particle **const mcc_arg_4, int *const mcc_arg_5, int *const mcc_arg_6)
  {
    {
      dim3 dimBlock;
      dim3 dimGrid;
      dimBlock.x = (*mcc_arg_0) < MAX_NUM_THREADS ? (*mcc_arg_0) : MAX_NUM_THREADS;
      dimGrid.x = (*mcc_arg_0) < MAX_NUM_THREADS ? 1 : (*mcc_arg_0) / MAX_NUM_THREADS + ((*mcc_arg_0) % MAX_NUM_THREADS == 0 ? 0 : 1);
      dimBlock.y = 1;
      dimGrid.y = 1;
      dimBlock.z = 1;
      dimGrid.z = 1;
      calculate_force_func<<<dimGrid, dimBlock, 0, nanos_get_kernel_execution_stream()>>>((*mcc_arg_0), (*mcc_arg_1), (*mcc_arg_2), (*mcc_arg_3), (*mcc_arg_4), (*mcc_arg_5), (*mcc_arg_6));
    }
  }
}
