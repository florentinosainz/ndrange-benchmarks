#include"nbody.h"

#ifdef __cplusplus
extern "C"
{
#endif

#pragma omp target device(cuda) ndrange(1,size,MAX_NUM_THREADS)  copy_in(d_particles[0;number_of_particles]) copy_out([size] output)
#pragma omp task out([size] output) \
                        in(d_particles[0*size;1*size], \
                        d_particles[1*size;2*size], \
                        d_particles[2*size;3*size] , \
                        d_particles[3*size;4*size] , \
                        d_particles[4*size;5*size] , \
                        d_particles[5*size;6*size] , \
                        d_particles[6*size;7*size] , \
                        d_particles[7*size;8*size] )
__global__
void calculate_force_func(int size, float time_interval,  int number_of_particles,
                                              Particle* d_particles, Particle *output,
											  int first_local, int last_local);
#ifdef __cplusplus
}
#endif


// The function 'calculate_force' is defined here because It's shared between two different
// devices: smp and cuda

__device__
void calculate_force(Particle* this_particle1, Particle* this_particle2,
        float* force_x, float* force_y, float* force_z)
{ /* Particle_calculate_force */
    float difference_x, difference_y, difference_z;
    float distance_squared, distance;
    float force_magnitude;

    difference_x = this_particle2->position_x - this_particle1->position_x;
    difference_y = this_particle2->position_y - this_particle1->position_y;
    difference_z = this_particle2->position_z - this_particle1->position_z;

    distance_squared = difference_x * difference_x + difference_y * difference_y + difference_z * difference_z;
    distance = sqrtf(distance_squared);
    force_magnitude = gravitational_constant * (this_particle1->mass) * (this_particle2->mass) / distance_squared;

    *force_x = (force_magnitude / distance) * difference_x;
    *force_y = (force_magnitude / distance) * difference_y;
    *force_z = (force_magnitude / distance) * difference_z;
}
