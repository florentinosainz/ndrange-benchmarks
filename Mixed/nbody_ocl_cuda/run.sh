#!/bin/bash

# @ partition = fatgpu
# @ output = nbody.log
# @ error = nbody.log
# @ initialdir = .
# @ total_tasks = 1
# @ cpus_per_task = 8
# @ gpus_per_node = 4
# @ node_usage = not_shared
# @ wall_clock_limit = 00:20:00

NX_OPENCL_DEVICE_TYPE=CPU NX_OPENCL_MAX_DEVICES=2 NX_ARGS="--instrumentation=extrae --cache-policy wt --gpus=1 --gpu-max-memory 10000000 --opencl-cache-policy=wt" ./nbody nbody_input-16384.in -K
